const funcionesGenerales = {};

funcionesGenerales.eliminarDuplicados = (dato) => {
    let sinRepetidosx = dato.filter((valorActual, indiceActual, arreglo) => {
        return arreglo.findIndex(
            valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)
        ) === indiceActual
    });
    return sinRepetidosx;
}

funcionesGenerales.ordenarFechaAscendente = (array, propiedad) => {
    return array.sort((a, b) => new Date(a[propiedad]).getTime() - new Date(b[propiedad]).getTime());
}

funcionesGenerales.eliminarDuplicadosPorAtri = (dato, attr) => {
    let sinRepetidosx = dato.filter((valorActual, indiceActual, arreglo) => {
        return arreglo.findIndex(valorDelArreglo => JSON.stringify(valorDelArreglo[attr]) === JSON.stringify(valorActual[attr])) == indiceActual
    });
    return sinRepetidosx;
}

funcionesGenerales.ordenarAcendente = (array, propiedad) => {
    array.sort((a, b) => {
        if (
            a[propiedad] > b[propiedad]) {
            return 1;
        }
        if (a[propiedad] < b[propiedad]) {
            return -1;
        }
        return 0;
    });

    return array;
}

funcionesGenerales.formatearFecha = (date) => {
    const f = new Date(date);

    let dia = '' + f.getDate();
    let mes = '' + (f.getMonth() + 1);
    let min = '' + f.getMinutes();
    let h = '' + f.getHours();
    if (dia.length == 1) {
        dia = '0' + dia;
    }
    if (mes.length == 1) {
        mes = '0' + mes;
    }
    if (min.length == 1) {
        min = '0' + min;
    }
    if (h.length == 1) {
        h = '0' + h;
    }
    return dia + '-' + mes + '-' + f.getFullYear() + '  ' + h + ':' + min;
}

funcionesGenerales.formatearFechaCorta = (date) => {
    const f = new Date(date);
    let dia = '' + f.getDate();
    let mes = '' + (f.getMonth() + 1);
    let min = '' + f.getMinutes();
    let h = '' + f.getHours();
    if (dia.length == 1) { dia = '0' + dia; }
    if (mes.length == 1) { mes = '0' + mes; }
    if (min.length == 1) { min = '0' + min; }
    if (h.length == 1) { h = '0' + h; }
    return dia + '-' + mes + '-' + f.getFullYear();
}


funcionesGenerales.ordenarFecha = (array, propiedad) => {
    return array.sort((a, b) => {
        if (new Date(a[propiedad].replace(/(\d{2})-(\d{2})-(\d{4})/, '$2/$1/$3')) < new Date(b[propiedad].replace(/(\d{2})-(\d{2})-(\d{4})/, '$2/$1/$3')))
            return 1;
        if (new Date(a[propiedad].replace(/(\d{2})-(\d{2})-(\d{4})/, '$2/$1/$3')) > new Date(b[propiedad].replace(/(\d{2})-(\d{2})-(\d{4})/, '$2/$1/$3'))) 
            return -1;
        return 0;
    });
}

module.exports = funcionesGenerales;