const express = require("express");
const router = express.Router();

const dashboardController = require("../Controllers/DasboardController.js");
const md_audit = require("../Middlewares/Audit.js");
const md_authen = require("../Middlewares/Authenticator.js");

router.get("/", md_authen.validateSesion, dashboardController.getAll);
router.get('/graficos', md_authen.validateSesion, dashboardController.obtenerGraficosDashboard);
router.get('/graficos/subgraficos', md_authen.validateSesion, dashboardController.obtenerSubgraficos);
router.get('/obtenerusers_dashboard', md_authen.validateSesion, dashboardController.obtenerDashboardsUser);
router.post('/guardar', md_authen.validateSesion, dashboardController.saveDashboardUser);

module.exports = router;