const express = require("express");
const router = express.Router();
const deliverynoteController = require("../Controllers/DeliverynoteController.js");
const md_authen = require("../Middlewares/Authenticator.js");
const md_audit = require("../Middlewares/Audit.js");

//Routes
router.get("/", md_authen.validateSesion, deliverynoteController.getAll);
router.get("/id", md_authen.validateSesion, deliverynoteController.getById);
router.post("/", md_authen.validateSesion, md_authen.validateHeadersExtras, deliverynoteController.create, md_audit.ensureAuth);
router.put("/", md_authen.validateSesion, md_authen.validateHeadersExtras, deliverynoteController.getDeliverynote, deliverynoteController.update, md_audit.ensureAuth);
router.delete("/", md_authen.validateSesion, md_authen.validateHeadersExtras, deliverynoteController.getDeliverynote, deliverynoteController.delete, md_audit.ensureAuth);
router.post("/observations", md_authen.validateSesion, md_authen.validateHeadersExtras, deliverynoteController.createObservations, md_audit.ensureAuth);
router.get("/observations", md_authen.validateSesion, md_authen.validateHeadersExtras, deliverynoteController.GetObservations);
router.get("/getTimeLab", md_authen.validateSesion, deliverynoteController.getTimeLab);
router.post("/updateAns", md_authen.validateSesion, md_authen.validateHeadersExtras, deliverynoteController.updateAns, md_audit.ensureAuth);


//Modulos
/* router.get("/packing", deliverynoteController.getlist); */
module.exports = router;