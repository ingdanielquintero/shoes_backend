const express = require("express");
const router = express.Router();
const incidenceController = require("../Controllers/IncidenceController.js");
const md_authen = require("../Middlewares/Authenticator.js");
const md_audit = require("../Middlewares/Audit.js");

router.get("/", md_authen.validateSesion, incidenceController.findAll);
router.get("/get", md_authen.validateSesion, md_authen.validateHeaderId, incidenceController.find);
router.put("/", md_authen.validateSesion, md_authen.validateHeadersExtras, incidenceController.getIncidence, incidenceController.update, md_audit.ensureAuth);
router.put("/close", md_authen.validateSesion, md_authen.validateHeadersExtras, incidenceController.close);
router.put("/closeParadaTiempo", md_authen.validateSesion, incidenceController.closeParadaTiempo);
router.post("/", md_authen.validateSesion, md_authen.validateHeadersExtras, incidenceController.save, md_audit.ensureAuth);
router.get("/getByFilter", md_authen.validateSesion, md_authen.validateHeaderId, incidenceController.findByFilter);
router.get("/getIncidenceByStatus", md_authen.validateSesion, incidenceController.findIncidenceByStatus);
router.get("/getByStatusTechnical", md_authen.validateSesion, incidenceController.findIncidenceByStatusTechnical);
router.get("/count", md_authen.validateSesion, incidenceController.count);
router.get("/getCommerceByCode", md_authen.validateSesion, incidenceController.getCommerceByCode);
router.get("/getIncidenceByCodeCommerce", md_authen.validateSesion, incidenceController.getIncidenceByCodeCommerce);
router.get("/getAssingIncidencesByCode", md_authen.validateSesion, incidenceController.getAssingIncidencesByCode);
router.get("/getTerminal", md_authen.validateSesion, incidenceController.getTerminalBySerialAndLocation);
router.get("/getTerminalByCase", md_authen.ensureAuthGetAllCommerce, incidenceController.getTerminalByCaseAndLocation);
router.get("/getTecnico", md_authen.validateSesion, incidenceController.getAllTechnical);
router.get("/getInstalled", md_authen.validateSesion, incidenceController.getElementsInstalled);
router.get("/getFound", md_authen.validateSesion, incidenceController.getElementsFound);
router.get("/getRemoved", md_authen.validateSesion, incidenceController.getElementsRemoved);
router.get("/getObservatios", md_authen.validateSesion, incidenceController.getByIncidence);
router.get("/getAssingCitys", md_authen.validateSesion, incidenceController.getAssingCitysByUser);
router.get("/getAssingTechnical", md_authen.validateSesion, incidenceController.getAssingTechnicalByCitys);
router.post("/saveObservation", md_authen.validateSesion, incidenceController.saveObservation);
router.post("/updateDataIncidence", md_authen.validateSesion, incidenceController.updateDataIncidence);
router.post("/updateDataCommerce", md_authen.validateSesion, incidenceController.updateDataCommerce);
router.post("/reopen", md_authen.validateSesion, incidenceController.reopen);
router.post("/assingIncidence", md_authen.validateSesion, incidenceController.assingIncidence);
router.get("/getQualification", md_authen.validateSesion, incidenceController.getQualification);
router.post("/stage", md_authen.validateSesion, incidenceController.getStage);
router.get("/byTechnical", md_authen.validateSesion, incidenceController.findAllByTechnical);
router.get("/byTechComm", md_authen.validateSesion, incidenceController.findAllByTechnicalAndCommerce);
router.get("/byDateUser", md_authen.validateSesion, incidenceController.getIncidencesByDateUser);
router.get("/byRangeDateTechnical", md_authen.validateSesion, incidenceController.getIncidenceByRangeDateTechnical);

router.post("/calificacion", md_authen.validateSesion, incidenceController.guardarCalificacion);

router.post("/closeInstalation", md_authen.validateSesion, incidenceController.CloseIncidenceInstalation);
router.post("/closeRetiro", md_authen.validateSesion, incidenceController.CloseIncidenceRetiro);
router.post("/closeSoporte", md_authen.validateSesion, incidenceController.CloseIncidenceSoporte);

router.post("/productividadDia", md_authen.validateSesion, incidenceController.productividadDia);

router.post("/productividadSemana", md_authen.validateSesion, incidenceController.productividadSemana);


router.post("/productividadMes", md_authen.validateSesion, incidenceController.productividadMes);


router.post("/productividadCampoCiudad", md_authen.validateSesion, incidenceController.findProductivityTechnicalByCity);


router.post("/several", md_authen.validateSesion, incidenceController.saveSeveral);

router.get('/getStatus', md_authen.validateSesion, incidenceController.getStatusIncidence);

router.post('/getIncidenceCommerceReport', md_authen.validateSesion, incidenceController.getIncedenciasPorComercioReporte);

router.get('/getReportIncidenceClose', md_authen.validateSesion, incidenceController.getIncidenceCommerceReport);

router.get('/getReportIncidenceByTechnical', md_authen.validateSesion, incidenceController.getReportIncidenceByTechnical);

router.post('/getIncidencesCitys', md_authen.validateSesion, incidenceController.getIncidencesCitys);

router.post('/assignTat', md_authen.validateSesion, incidenceController.assignTat);

/**
 * App de comercios
 */
router.post('/saveIncidenceAppCommerceReporteFallas', md_authen.ensureAuthGetAllCommerce, incidenceController.saveIncidenceAppCommerceReporteFallas);
router.post('/saveIncidenceAppCommerceRetiroPos', md_authen.ensureAuthGetAllCommerce, incidenceController.saveIncidenceAppCommerceRetiroPos);
router.post('/saveIncidenceAppCommercePuntoVenta', md_authen.ensureAuthGetAllCommerce, incidenceController.saveIncidenceAppCommercePuntoVenta);
router.post('/saveIncidenceAppCommercePorEvento', md_authen.ensureAuthGetAllCommerce, incidenceController.saveIncidenceAppCommercePorEvento);

module.exports = router;