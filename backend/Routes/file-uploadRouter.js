var express = require('express');
var _router = express.Router();
const md_authen = require("../Middlewares/Authenticator.js");
const md_audit = require("../Middlewares/Audit.js");
const fileUploadController = require("../Controllers/FileUploadController.js");
var path = require('path');
const fs = require('fs')
var OS = process.platform;
var pathC = "";


var pathOS = function () {
    if (OS == "win32") {
        pathC = 'C:/polaris/imagesProfiles';
        pathO = 'C:/polaris/imagesObservations';
        pathI = 'C:/polaris/imagesIncidences';
        pathM = 'C:/polaris/imagesModelsTerminals';
    } else if (OS == "linux") {
        pathC = '/opt/polaris/imagesProfiles';
        pathO = '/opt/polaris/imagesObservations';
        pathI = '/opt/polaris/imagesIncidences';
        pathM = '/opt/polaris/imagesModelsTerminals';
    }
};

pathOS();

_router.post('/upload/:identificacion', fileUploadController.save);

_router.post('/saveObservationsIncidencesImages', fileUploadController.saveObservationsIncidencesImages);

_router.post('/uploadImgProfiles/', md_authen.ensureAuthGetAllAndroid, fileUploadController.saveImgProfiles);

_router.post('/uploadImgObservations/', md_authen.ensureAuthGetAllAndroid, fileUploadController.saveObservations);

_router.post('/uploadModel/:data', md_authen.ensureAuthGetAllAndroid, fileUploadController.saveImgModel);

_router.post('/saveObservationsIncidences/', md_authen.ensureAuthGetAllAndroid, fileUploadController.saveObservationsIncidences);

_router.post('/uploadObservation/:serial', md_authen.ensureAuthGetAllAndroid, fileUploadController.saveObservationQA);

_router.get('/view/:name', (req, res) => {
    var ruta = pathC + "/" + req.params.name;
    fs.access(ruta, err => {
        if (err) {
            res.sendFile(path.join(pathC + "/default.png"));
        } else {
            res.sendFile(path.join(ruta));
        }
    });
});
_router.get('/viewObservation/:name', (req, res) => {
    var ruta = pathO + "/" + req.params.name;
    fs.access(ruta, err => {
        if (err) {
            res.sendFile(path.join(pathO + "/default.png"));
        } else {
            res.sendFile(path.join(ruta));
        }
    });
});


_router.get('/viewObservationIncidence/:name', (req, res) => {
    var ruta = pathI + "/" + req.params.name;
    fs.access(ruta, err => {
        if (err) {
            console.log(err);
            
            res.sendFile(path.join(pathI + "/default.png"));
        } else {
            res.sendFile(path.join(ruta));
        }
    });
});

_router.get('/viewModel/:name', (req, res) => {
    var ruta = pathM + "/" + req.params.name;
    fs.access(ruta, err => {
        if (err) {
            res.sendFile(path.join(pathM + "/default.svg"));
        } else {
            res.sendFile(path.join(ruta));
        }
    });
});

_router.post('/download', md_authen.ensureAuthGetAllAndroid, fileUploadController.download);

module.exports = _router;