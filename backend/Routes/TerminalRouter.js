const express = require("express");
const router = express.Router();
const terminalsController = require("../Controllers/TerminalController.js");
const terminalComplementController = require("../Controllers/TerminalComplementController.js");
const md_authen = require("../Middlewares/Authenticator.js");
const md_audit = require("../Middlewares/Audit.js");

//Routes 
router.get("/", md_authen.validateSesion, terminalsController.findAll);
router.get("/get", md_authen.validateSesion, md_authen.validateHeaderId, terminalsController.find);
router.put("/", md_authen.validateSesion, md_authen.validateHeadersExtras, terminalsController.getTerminal, terminalsController.update, md_audit.ensureAuth);
router.put("/status", md_authen.validateSesion, md_authen.validateHeadersExtras, terminalsController.getTerminal, terminalsController.updateStatus, md_audit.ensureAuth);
router.post("/", md_authen.validateSesion, md_authen.validateHeadersExtras, terminalsController.save, md_audit.ensureAuth);
router.delete("/", md_authen.validateSesion, md_authen.validateHeadersExtras, terminalsController.getTerminal, terminalsController.delete, md_audit.ensureAuth);
//router.post("/loadMassive",md_authen.ensureAuthPost,terminalsController.save,md_audit.ensureAuth);
router.get("/getQuotitation", md_authen.validateSesion, terminalsController.findAllQuotitation);
router.get("/getStatus", md_authen.validateSesion, terminalComplementController.findAllStates);
router.get("/getModels", md_authen.validateSesion, terminalComplementController.findAllModels);
router.get("/getTecnologies", md_authen.validateSesion, terminalComplementController.findAllTecnologies);
// App de comercios
router.post("/getTecnologies", md_authen.ensureAuthGetAllCommerce, terminalComplementController.findAllTecnologies);

router.post("/getTerminal", md_authen.validateSesion, terminalsController.findTerminalsBySerial);
router.post("/several", md_authen.validateSesion, terminalsController.saveSeveral);
router.post("/changes", md_authen.validateSesion, terminalsController.changeLocation);
router.post("/getTerminalByLocation", md_authen.validateSesion, terminalsController.getTerminalByLocation);
router.get("/getAllByStatus", md_authen.validateSesion, terminalsController.findAllByEstatus);
router.post("/changeWhitSpare", md_authen.validateSesion, terminalsController.changeLocationWhitSpare);
router.get("/getTerminalByOrigin", md_authen.validateSesion, terminalsController.getTerminalByOrigin);
router.post("/ChangeByCommerce", md_authen.validateSesion, terminalsController.ChangeByCommerce);
router.post("/ChangeByWareHouses", md_authen.validateSesion, terminalsController.ChangeByWareHouses);

//servicios para android 
router.post("/associatedsWithDiagnosis", md_authen.validateSesion, terminalsController.findTerminalsAssociatedDiagnosis);
router.post("/observations", md_authen.validateSesion, terminalsController.findTerminalsObservation);
router.post("/validatorTerminal", md_authen.validateSesion, terminalsController.findAllValidatorTerminal);
router.post("/tipesValidatorTerminal", md_authen.validateSesion, terminalsController.findAllTipesValidatorTerminal);
router.post("/spares", md_authen.validateSesion, terminalsController.findTerminalsSpares);
router.post('/saveDiagnosis', md_authen.validateSesion, terminalsController.saveDiagnosis);
router.put("/status2", md_authen.validateSesion, terminalsController.updateStatus2);
router.post("/stock", md_authen.validateSesion, terminalsController.getTerminalAndSparesAsigned);
router.post('/saveObservations', md_authen.validateSesion, terminalsController.saveObservations);
router.post('/saveValidation', md_authen.validateSesion, terminalsController.saveValidationWeb);
router.post("/associatedsWithRepair", md_authen.validateSesion, terminalsController.findTerminalsAssociateRepairs);
router.put("/ans", md_authen.validateSesion, md_authen.validateHeadersExtras, terminalsController.updateDateAns, md_audit.ensureAuth);
router.post('/saveDiagnosisQa', md_authen.validateSesion, terminalsController.saveDiagnosisQA);
router.post('/saveNewDiagnosis', md_authen.validateSesion, terminalsController.saveNewDiagnosis);
router.post('/saveDiagnosisSpare', md_authen.validateSesion, terminalsController.saveDiagnosisSpare);
router.post("/findserial", md_authen.validateSesion, terminalsController.terminalsFind);
router.post("/finddate", md_authen.validateSesion, terminalsController.terminalsFindDate);
router.post("/findTermComm", md_authen.validateSesion, terminalsController.findTerminalWithCommerce);

//servicios para control de terminales
router.get("/getTerminalObservations", md_authen.validateSesion, md_authen.validateHeaderId, terminalsController.getTerminalObservations);
router.get("/getTerminalSparesWarranty", md_authen.validateSesion, md_authen.validateHeaderId, terminalsController.getTerminalSparesWarranty);
router.get("/getTerminalValidationStatus", md_authen.validateSesion, md_authen.validateHeaderId, terminalsController.getTerminalValidationStatus);

router.get("/getTerminalTypesStatus", md_authen.validateSesion, md_authen.validateHeaderId, terminalsController.getTerminalTypesStatus);
router.get("/getAllTerminalTypesStatus", md_authen.validateSesion, terminalsController.getAllTerminalTypesStatus);

router.post("/incrementasociated", md_authen.validateSesion, terminalsController.asociarTerminalUser);
router.get("/getTerminalesAsignadas", md_authen.validateSesion, md_authen.validateHeaderId, terminalsController.getTerminalesAsignadas);
router.post("/productivity", md_authen.validateSesion, terminalsController.terminalProductive);
router.post('/saveSpares', md_authen.validateSesion, terminalsController.saveSparesWeb);
router.post('/closeDiagnosis', md_authen.validateSesion, terminalsController.closeDiagnosis);
router.post("/incrementasociatedTSR", md_authen.validateSesion, terminalsController.asociarTerminalUserTSR);
router.post("/sum", md_authen.validateSesion, terminalsController.summary);

router.get("/UbicationByCode", md_authen.validateSesion, terminalsController.getUbicationByCode);
router.put("/updateHistoryTerminal", md_authen.validateSesion, md_authen.validateHeaderId, terminalsController.updateTerminalHistory);
router.get("/getHistoryTerminal", md_authen.validateSesion, md_authen.validateHeaderId, terminalsController.getTerminalHistory);
router.get("/getAllTerminalesAsignadas", md_authen.validateSesion, terminalsController.getAllTerminalesAsignadas);
router.get("/getAllTechnology", md_authen.validateSesion, terminalsController.findAllTecnologies);


router.post("/incrementarGestionadas", md_authen.validateSesion, terminalsController.gestionarTerminalUser);

// Servicio para obtener la historia clinica de una terminal por serial
router.post('/getClinicHistoryTerminal', md_authen.validateSesion, terminalsController.getClinicHistoryTerminal);
router.post('/saveClinicHistoryTerminal', md_authen.validateSesion, terminalsController.saveClinicHistoryTerminal);

const conection = require("../database.js");
router.get("/x", (req, res) => {
    var data = [];
    // const options = { prepare: true, fetchSize: 1000 };
    // conection.eachRow('select * from terminal', [], options, async function (n, row) {
    //     // Invoked per each row in all the pages
    //     // console.log(n);
    //     // if(n >= 17000 && n <= 19000){
    //     //     row.term_technology = await convertirTecnologiaLetras(row.term_technology);
    //     // }
    //     data.push(row);
    // },
    //     function (err, result) {
    //         // Called once the page has been retrieved.
    //         if (result.nextPage) {
    //             // Retrieve the following pages:
    //             // the same row handler from above will be used
    //             // console.log(result.rows);
    //             result.nextPage();
    //         } else {
    //             res.json(data);
    //         }
    //     }
    // );


    conection.stream('select * from terminal', [], { prepare: true, autoPage: true })
        .on('readable', async function () {
            var row;
            while (row = this.read()) {
                row.term_technology = await convertirTecnologiaLetras(row.term_technology);
                data.push(row);
                if(data.length == 1000) {
                    console.log('emite');
                    
                    emiterGetAllTerminals({code: req.headers.usercodeid, data: data});
                    data = [];
                }
            }
        }).on('end', async function () {
            res.json([]);
        });
});

const io = require('socket.io-client');   //client.js

function emiterGetAllTerminals(data) {
  var socket = io.connect('http://' + process.env.IP_HOST + `:${process.env.PORT}`, { reconnect: true });
  socket.emit('terminals', data);
//   socket.disconnect();
}

async function convertirTecnologiaLetras(tecnologia) {
    var tecnologias = tecnologia.split(",");
    var dato = '';
    try {
        return new Promise((resolve, reject) => {
            for (let i = 0; i < tecnologias.length; i++) {
                if (tecnologias[i] == 1) {
                    dato += "DIAL,";
                }
                if (tecnologias[i] == 2) {
                    dato += "LAN,";
                }
                if (tecnologias[i] == 3) {
                    dato += "GPRS,";
                }
                if (tecnologias[i] == 4) {
                    dato += "WIFI,";
                }
                if (tecnologias[i] == 5) {
                    dato += "BLUETOOTH,";
                }
            }
            resolve(dato.substr(0, dato.length - 1));
        });
    } catch (error) {
        logger.error(error.stack);
        resolve([]);
    }

}


//exports
module.exports = router;