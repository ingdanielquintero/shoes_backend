const express = require("express");
const router = express.Router();
const ObservationIncidenceController = require("../Controllers/ObservationIncidenceController.js");
const md_authen = require("../Middlewares/Authenticator.js");

//Routes
router.post("/", md_authen.validateSesion, md_authen.validateHeaderId, ObservationIncidenceController.create);
router.get("/", md_authen.validateSesion, md_authen.validateHeaderId, ObservationIncidenceController.getByIncidence);

/**
 * App Comercios
 */

router.post("/AppCommercios", md_authen.ensureAuthGetAllCommerce, ObservationIncidenceController.create);

module.exports = router;