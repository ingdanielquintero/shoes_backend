const express = require("express");
const router = express.Router();
const rolesController = require("../Controllers/RoleController.js");
const md_audit = require("../Middlewares/Audit.js");
const md_authen = require("../Middlewares/Authenticator.js");

//Routes
router.get("/", rolesController.getAll);
router.get("/ids", rolesController.getByIds);
router.post("/", rolesController.create);
router.post("/view", rolesController.createPermision);
router.put("/", rolesController.update);
router.delete("/", rolesController.delete);

//Modulos
router.get("/modules", rolesController.getModules);
router.get("/modules/sub", rolesController.getSubModules);
router.get("/modules/views", rolesController.getViews);
router.get("/ids2", rolesController.getByIds2);

router.post("/update", rolesController.updaterol);

router.get("/icons", rolesController.getIcons);

module.exports = router;