const express = require("express");
const router = express.Router();
const LocalicationController = require("../Controllers/LocalicationController.js");
const md_authen = require("../Middlewares/Authenticator.js");

//Routes
router.post("/", md_authen.validateSesion, LocalicationController.save);
router.get("/", md_authen.validateSesion, md_authen.validateHeaderId, LocalicationController.getByCode);
router.get("/getAll", md_authen.validateSesion, LocalicationController.getAll);
router.get("/history", md_authen.validateSesion, LocalicationController.getHistoryByCode);


module.exports = router;