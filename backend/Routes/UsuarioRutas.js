const express = require("express");
const router = express.Router();
const usuarioShoe = require("../Controllers/UsuarioShoe");
const md_authen = require("../Middlewares/Authenticator.js");
const md_audit = require("../Middlewares/Audit.js");
const mailController = require('../Controllers/MailController.js');

//Routes
router.post("/", md_authen.validateToken, md_authen.validateUsuLogin, usuarioShoe.save);
router.post("/Login", usuarioShoe.logIn);


module.exports = router;