const express = require("express");
const router = express.Router();
const simcardController = require("../Controllers/SimcardController.js");
const md_authen = require("../Middlewares/Authenticator.js");

//Routes 
router.post("/several", md_authen.validateSesion, simcardController.saveSeveral);
router.post("/registerSimCard", md_authen.validateSesion, simcardController.saveSimCard);
router.post("/location", md_authen.validateSesion, simcardController.location);
router.get("/", md_authen.validateSesion, simcardController.findAll);
router.delete("/", md_authen.validateSesion, simcardController.delete);
router.post("/get", md_authen.validateSesion, simcardController.findSerial);
router.get("/status", md_authen.validateSesion, simcardController.findAllStatus);
router.get("/brand", md_authen.validateSesion, simcardController.findAllBrand);
router.put("/status", md_authen.validateSesion, simcardController.updateStatusSimcard);
router.post("/getSimCard", md_authen.validateSesion, simcardController.findSimCardsBySerial);
router.post("/getSimCardByLocation", md_authen.validateSesion, simcardController.getSimCardByLocation);
router.get("/bySerial", md_authen.validateSesion, simcardController.findBySerial);
router.post("/getSimCardUbication", md_authen.validateSesion, simcardController.findSimCardsBySerialUbication);
router.get("/byTecAndroid", md_authen.validateSesion, simcardController.getSimCardByLocationAndroid);


module.exports = router;  