const express = require("express");
const router = express.Router();
const typification_incidence = require("../Controllers/typification_incidenceController");
const md_authen = require("../Middlewares/Authenticator.js");
const md_audit = require("../Middlewares/Audit.js");

router.get("/typi", md_authen.validateSesion, typification_incidence.findAll);
router.get("/typiType", md_authen.validateSesion, typification_incidence.findByType);

/**
 * App Comercios
 */

router.get("/obtenerMotivosReporteDeFallas", md_authen.ensureAuthGetAllCommerce, typification_incidence.obtenerMotivosReporteDeFallas);

module.exports = router;