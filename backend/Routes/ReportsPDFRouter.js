const express = require("express");
const router = express.Router();

const reportsPDFController = require('../Controllers/ReportsPDFController.js');
const md_authen = require("../Middlewares/Authenticator.js");

router.get('/', reportsPDFController.reports);

module.exports = router;