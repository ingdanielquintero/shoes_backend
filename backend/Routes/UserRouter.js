const express = require("express");
const router = express.Router();
const userController = require("../Controllers/UserController.js");
const md_authen = require("../Middlewares/Authenticator.js");
const md_audit = require("../Middlewares/Audit.js");
const mailController = require('../Controllers/MailController.js');

//Routes
router.post("/", md_authen.validateSesion, md_authen.validateHeadersExtras, userController.getMaxUser, userController.validateEmail, userController.save, userController.updateMaxUser, md_audit.ensureAuth);
router.put("/", md_authen.validateSesion, md_authen.validateHeadersExtras, userController.getUser, userController.validateEmail, userController.update, md_audit.ensureAuth);
router.get("/", md_authen.validateSesion, userController.findAll);
router.get("/get", md_authen.validateSesion, md_authen.validateHeaderId, userController.find);
router.get("/ByCode", md_authen.validateSesion, userController.findByCode);
router.post("/ByCode", md_authen.ensureAuthGetAllCommerce, userController.findByCodeUserCommerce);
router.delete("/", md_authen.validateSesion, md_authen.validateHeadersExtras, userController.getUser, userController.getUser, userController.delete, md_audit.ensureAuth);
router.post("/login", userController.logIn);
router.put("/updatePassword", md_authen.validateSesion, userController.updatePassword);
router.put("/updateSession", userController.updateSession);
router.post("/p", userController.save);
router.get("/code", userController.getUserByCode);
router.get("/getTechnical", md_authen.validateSesion, userController.getAllTechTechnical);
router.get("/getTechnicalLaboratorio", md_authen.validateSesion, userController.getAllTechTechnicalLaboratorio);
router.post('/verification', md_authen.validateSesion, userController.verificationPassword);
router.get("/userByCode", userController.getUserByCodeToRestartPassword);

router.post('/closeSesionsExternal', userController.closeSesionsExternal, mailController.sendMailCloseSessionExternal);
router.post('/close', userController.closeSesion);

router.put("/securityQuestions", userController.updateSecurityQuestions);
router.get("/securityQuestions", userController.getSecurityQuestions);
router.get("/validateToken", md_authen.validateSesion, userController.verificationToken);
router.post("/recuperarToken", userController.RecuperarToken);

router.post("/RegisterUserCommerce", md_authen.ensureAuthRegister, userController.validarUsuarioComercio, userController.validateCommerce, userController.saveUserCommerce);
router.post("/validateUserCommerce", md_authen.ensureAuthRegister, userController.validarUsuarioComercio);
router.post("/loginUserCommerce", md_authen.ensureAuthRegister, userController.logInUserCommerce);
router.post("/recuperarTokenUserCommerce", md_authen.ensureAuthRegister, userController.RecuperarToken);
router.get("/validateTokenCommerce", md_authen.ensureAuthGetAllCommerce, userController.verificationToken);
router.put("/updatePasswordAppCommerce", userController.updatePasswordAppCommerce);

module.exports = router;

