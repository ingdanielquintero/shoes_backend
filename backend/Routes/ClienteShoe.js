const express = require("express");
const router = express.Router();
const clienteShoe = require("../Controllers/ClienteShoe");
const md_authen = require("../Middlewares/Authenticator.js");;

//Routes
router.post("/", md_authen.validateToken, clienteShoe.save);

router.put("/", md_authen.validateToken, clienteShoe.update);
router.put("/updateEstado", md_authen.validateToken, clienteShoe.updateEstado);


router.get("/", md_authen.validateToken, clienteShoe.getAllByUsuario);
router.get("/getById", md_authen.validateToken, clienteShoe.getById);

module.exports = router;
