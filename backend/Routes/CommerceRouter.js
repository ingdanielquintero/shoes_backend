const express = require("express");
const router = express.Router();
const commController = require("../Controllers/CommerceController");
const md_authen = require("../Middlewares/Authenticator.js");
const md_audit = require("../Middlewares/Audit.js");


//Routes 
router.post("/severalc", md_authen.validateSesion, commController.saveSeveral);
router.post("/", md_authen.validateSesion, md_authen.validateHeadersExtras, commController.create, md_audit.ensureAuth);
router.get("/", md_authen.validateSesion, commController.getAll);
router.get("/id", md_authen.validateSesion, md_authen.validateHeaderId, commController.getById);
router.get("/prioritys", md_authen.validateSesion, commController.getPrioritys);
router.put("/", md_authen.validateSesion, md_authen.validateHeadersExtras, commController.getCommerce, commController.update, md_audit.ensureAuth);
router.delete("/", md_authen.validateSesion, md_authen.validateHeadersExtras, commController.getCommerce, commController.delete, commController.deleteUserCommerce, md_audit.ensureAuth);
router.get("/code",md_authen.validateSesion, md_authen.validateHeaderId, commController.getByCode);

router.get("/idAppCommerce", md_authen.ensureAuthRegister, commController.getById);

router.get("/getAllByCity", md_authen.validateSesion, commController.getAllByCity);

module.exports = router;