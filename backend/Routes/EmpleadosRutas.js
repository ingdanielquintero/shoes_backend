const express = require("express");
const router = express.Router();
const empleadoShoe = require("../Controllers/EmpleadoShoe");
const md_authen = require("../Middlewares/Authenticator.js");;

//Routes
router.post("/", md_authen.validateToken, empleadoShoe.save);

router.put("/", md_authen.validateToken, empleadoShoe.update);
router.put("/updateEstado", md_authen.validateToken, empleadoShoe.updateEstado);

router.get("/", md_authen.validateToken, empleadoShoe.getAll);
router.get("/getById", md_authen.validateToken, empleadoShoe.getById);
router.get("/getByArea", md_authen.validateToken, empleadoShoe.getByArea);

module.exports = router;