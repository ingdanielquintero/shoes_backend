const express = require("express");
const router = express.Router();
const accesoriesController = require("../Controllers/AccesoryController.js");
const terminalComplementController = require("../Controllers/TerminalComplementController.js");
const middlewares = require("../Middlewares/Authenticator.js");
const md_audit = require("../Middlewares/Audit.js");

//Routes 
router.get("/", middlewares.validateSesion, accesoriesController.findAll);
router.get("/get", middlewares.validateSesion, middlewares.validateHeaderId, accesoriesController.find);
router.put("/", middlewares.validateSesion, middlewares.validateHeadersExtras, terminalComplementController.findModels, accesoriesController.getAccessory, accesoriesController.update, md_audit.ensureAuth);
router.post("/", middlewares.validateSesion, middlewares.validateHeadersExtras, terminalComplementController.findModels, accesoriesController.save, md_audit.ensureAuth);
router.delete("/", middlewares.validateSesion, middlewares.validateHeadersExtras, accesoriesController.getAccessory, accesoriesController.delete, md_audit.ensureAuth);
router.post("/severalAcc", middlewares.validateSesion, accesoriesController.saveSeveral);
router.get("/status", middlewares.validateSesion, accesoriesController.findAllAccesoryStatus);
router.get("/getAccessory", middlewares.validateSesion, accesoriesController.getAllAccessoryByStatus);
router.get("/code", middlewares.validateSesion, accesoriesController.getByCode);
router.get("/id", middlewares.validateSesion, accesoriesController.getById);
router.post("/AccTechnical", middlewares.validateSesion, accesoriesController.getAllAccessoryByTechnical);
router.post('/getByUbication', middlewares.validateSesion, accesoriesController.getByUbication); // obtiene los accesorios por ubicacion
router.get("/Acc", middlewares.validateSesion, accesoriesController.findAllAndorid);
router.get("/codeLocationStatus", middlewares.validateSesion, accesoriesController.getByCodeLocationStatus);


module.exports = router;