const express = require("express");
const router = express.Router();
const AtentionTimeController = require("../Controllers/AtentionTimeController.js");
const md_authen = require("../Middlewares/Authenticator.js");

//Routes
router.put("/", md_authen.validateSesion, AtentionTimeController.updateAtentionTime);
router.get("/", md_authen.validateSesion, AtentionTimeController.getAll);


module.exports = router;