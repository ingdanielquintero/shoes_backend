const express = require("express");
const router = express.Router();
const notificationsController = require("../Controllers/NotificationController.js");
const md_authen = require("../Middlewares/Authenticator.js");

//Routes
router.post("/", md_authen.validateSesion, notificationsController.create);
router.get("/id", md_authen.validateSesion, notificationsController.getByDest);
router.delete("/", md_authen.validateSesion, notificationsController.delete);
router.put("/", md_authen.validateSesion, notificationsController.update);
router.get("/noti", md_authen.validateSesion, notificationsController.getByDestToMob);

router.get("/idUserCommerce", md_authen.ensureAuthGetAllCommerce, notificationsController.getByDestUserCommerce);
router.put("/idUserCommerce", md_authen.ensureAuthGetAllCommerce, notificationsController.updateUserCommerce);
router.delete("/idUserCommerce", md_authen.ensureAuthGetAllCommerce, notificationsController.deleteUserCommerce);
/* router.get("/", notificationsController.getAll);

 */

module.exports = router;