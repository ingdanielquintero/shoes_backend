const express = require("express");
const router = express.Router();
const tiqueteShoe = require("../Controllers/TiqueteShoe");
const md_authen = require("../Middlewares/Authenticator.js");;

//Routes
router.post("/", md_authen.validateToken, tiqueteShoe.save);

router.put("/", md_authen.validateToken, tiqueteShoe.update);
router.put("/updateStatus", md_authen.validateToken, tiqueteShoe.updateStatus);
router.put("/pay", md_authen.validateToken, tiqueteShoe.validatePay, tiqueteShoe.pay);

router.get("/", md_authen.validateToken, tiqueteShoe.getAll);
router.get("/getById", md_authen.validateToken, tiqueteShoe.getById);
router.get("/getByCodigo", md_authen.validateToken, tiqueteShoe.getByCodigo);

module.exports = router;