const express = require("express");
const router = express.Router();
const dashboardController = require("../Controllers/DashboardController.js");
const md_authen = require("../Middlewares/Authenticator.js");


router.post("/", md_authen.validateSesion, dashboardController.example);
router.post("/graficoIncidencias", md_authen.validateSesion, dashboardController.graficoIncidencias);

module.exports = router;