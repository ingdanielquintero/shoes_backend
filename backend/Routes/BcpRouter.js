const express = require("express");
const router = express.Router();
const bcpController = require("../Controllers/BcpController.js");
const md_authen = require("../Middlewares/Authenticator.js");

//Routes
router.post("/login", md_authen.validateSesion, bcpController.loginAgente);
router.post("/datos", md_authen.validateSesion, bcpController.confirmacion);

//exports
module.exports = router;