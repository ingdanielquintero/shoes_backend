const express = require("express");
const router = express.Router();
const extraController = require("../Controllers/ExtraController.js");
const md_authen = require("../Middlewares/Authenticator.js");
const md_audit = require("../Middlewares/Audit.js");

//Country and City Routes
router.get("/cities", md_authen.validateSesion, extraController.findAllCities);
router.get("/countries", md_authen.validateSesion, extraController.findAllCountries);
router.get("/Tmodels", md_authen.validateSesion, extraController.findAllTerminalModels);
router.get("/Tbrands", md_authen.validateSesion, extraController.findAllTerminalBrands);
router.get("/typesTypifications", md_authen.validateSesion, extraController.findAllTypesTypifications);
router.get("/productivityTechnical", md_authen.validateSesion, extraController.findAllProductivityTechnical);
router.post("/cities", md_authen.validateSesion, extraController.saveCity);
router.post("/countries", md_authen.validateSesion, extraController.saveCountry);
router.post("/Tmodels", md_authen.validateSesion, extraController.saveTerminalModel);
router.post("/Tbrands", md_authen.validateSesion, extraController.saveTerminalBrand);
router.post("/typifications", md_authen.validateSesion, extraController.saveTypifications);
router.post("/productivityTechnical", md_authen.validateSesion, extraController.saveProductivityTechnical);
router.put("/cities", md_authen.validateSesion, extraController.updateCity);
router.put("/countries", md_authen.validateSesion, extraController.updateCountry);
router.put("/Tmodels", md_authen.validateSesion, extraController.updateTerminalModel);
router.put("/Tbrands", md_authen.validateSesion, extraController.updateTerminalBrand);
router.put("/typifications", md_authen.validateSesion, extraController.updateTypifications);
router.put("/productivityTechnical", md_authen.validateSesion, extraController.updateProductivityTechnical);
router.get("/company", md_authen.validateSesion, extraController.company);
router.put("/company", md_authen.validateSesion, extraController.updateConfigCompany);

module.exports = router;