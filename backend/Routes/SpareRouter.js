const express = require("express");
const router = express.Router();
const spareController = require("../Controllers/SpareController.js");
const md_authen = require("../Middlewares/Authenticator.js");
const md_audit = require("../Middlewares/Audit.js");

router.get("/", md_authen.validateSesion, spareController.findAll);
router.get("/status", md_authen.validateSesion, spareController.findAllSpareStatus);
router.put("/", md_authen.validateSesion, md_authen.validateHeadersExtras, spareController.getSpare, spareController.update, md_audit.ensureAuth);
router.get("/code", md_authen.validateSesion, spareController.getByCode);
router.delete("/", md_authen.validateSesion, md_authen.validateHeadersExtras, spareController.getSpare, spareController.delete, md_audit.ensureAuth);
router.post("/Several", md_authen.validateSesion, spareController.saveSeveral);
router.post("/getSpareByLocation", md_authen.validateSesion, spareController.getSpareByLocation);
router.get("/id", md_authen.validateSesion, spareController.getById);
router.get("/codeLocationStatus", md_authen.validateSesion, spareController.getByCodeLocationStatus);

router.post("/actualizarSpare", md_authen.validateSesion, spareController.updateSpare);

module.exports = router;