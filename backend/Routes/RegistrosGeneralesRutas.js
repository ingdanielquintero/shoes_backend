const express = require("express");
const router = express.Router();
const registrosGeneralesShoe = require("../Controllers/RegistrosGeneralesShoe");
const md_authen = require("../Middlewares/Authenticator.js");;

//Routes
router.post("/", md_authen.validateToken, registrosGeneralesShoe.save);

router.put("/", md_authen.validateToken, registrosGeneralesShoe.update);

router.get("/", md_authen.validateToken, registrosGeneralesShoe.getAll);
router.get("/getByTipo", md_authen.validateToken, registrosGeneralesShoe.getAllByTipo);
router.get("/getById", md_authen.validateToken, registrosGeneralesShoe.getById);
router.get("/getPreciosReferencias", md_authen.validateToken, registrosGeneralesShoe.getPreciosReferencias);
router.get("/getPreciosReferenciasById", md_authen.validateToken, registrosGeneralesShoe.getPreciosReferenciasById);


module.exports = router;



