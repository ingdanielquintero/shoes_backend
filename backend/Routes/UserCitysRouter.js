const express = require("express");
const router = express.Router();
const md_authen = require("../Middlewares/Authenticator.js");
const userCityController = require('../Controllers/UserCityController');

router.get('/obtener', md_authen.validateSesion, userCityController.obtenerCitysUser);
router.post('/save', md_authen.validateSesion, userCityController.saveUserCity);

router.post('/obtenerUsuariosPais', md_authen.validateSesion, userCityController.obtenerUsuariosPais);

module.exports = router;