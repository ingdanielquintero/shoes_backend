const express = require("express");
const router = express.Router();
const auditController = require("../Controllers/AuditController.js");
const md_authen = require("../Middlewares/Authenticator.js");

//Routes
router.get("/",md_authen.validateSesion, auditController.findAll);

//exports
module.exports = router;
