const express = require("express");
const router = express.Router();
const wareController = require("../Controllers/WarehouseController");
const md_authen = require("../Middlewares/Authenticator.js");
const md_audit = require("../Middlewares/Audit.js");

//Routes 
router.get("/", md_authen.validateSesion, wareController.findAll);
router.get("/get", md_authen.validateSesion, md_authen.validateHeaderId, wareController.find);
router.put("/", md_authen.validateSesion, md_authen.validateHeadersExtras, wareController.getWarehouse, wareController.update, md_audit.ensureAuth);
router.post("/", md_authen.validateSesion, md_authen.validateHeadersExtras, wareController.getMaxWarehouse, wareController.save, wareController.updateMaxWarehouse, md_audit.ensureAuth);
router.delete("/", md_authen.validateSesion, md_authen.validateHeadersExtras, wareController.getWarehouse, wareController.delete, md_audit.ensureAuth);

router.post("/severalw", md_authen.validateSesion, wareController.getMaxWarehouse, wareController.saveSeveral, wareController.updateMaxWarehouse);

router.put("/warehouseuser", md_authen.validateSesion, wareController.updateWarehouseUser);
router.get("/warehouseuser", md_authen.validateSesion, wareController.findWarehouseUser);
router.get("/all_warehouse_user", md_authen.validateSesion, wareController.findAllWarehouseAllUser);
router.get("/warehouseuserAll", md_authen.validateSesion, wareController.findAllWarehouseUser);
router.get("/allWarehouseByUser", md_authen.validateSesion, wareController.findAllWarehouseByUser);


module.exports = router;