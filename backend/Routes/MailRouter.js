const express = require("express");
const router = express.Router();
const mailController = require("../Controllers/MailController.js");
const md_authen = require("../Middlewares/Authenticator.js");

router.post('/sendMail', mailController.sendMailRestartPassword);
router.get("/sendMail", mailController.sendMail);

router.post("/sendMailPasswordUserCommerce", md_authen.ensureAuthRegister, mailController.sendMailPasswordUserCommerce);

router.get('/p', mailController.sendMailCloseSessionExternal);
module.exports = router;