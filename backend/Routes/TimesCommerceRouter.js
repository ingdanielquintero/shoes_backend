const express = require("express");
const router = express.Router();
const timesCommerceController = require("../Controllers/TimesCommerceController");
const md_authen = require("../Middlewares/Authenticator.js");
const md_audit = require("../Middlewares/Audit.js");

router.put("/updateTimesCommerce", md_authen.validateSesion, timesCommerceController.updateTimesCommerce);
router.get("/getAll", md_authen.validateSesion, timesCommerceController.findAll);
router.get("/getCommerceByCode", md_authen.validateSesion, timesCommerceController.getCommerceByCode);
router.post("/insertTimesCommerce", md_authen.validateSesion, timesCommerceController.insertTimesCommerce);
router.get("/getAllCommerce", md_authen.validateSesion, timesCommerceController.getAllCommerce);
router.get("/getTimesLaboratory", md_authen.validateSesion, timesCommerceController.findAllLaboratory);
router.put("/updateTimesLaboratory", md_authen.validateSesion, timesCommerceController.updateTimesLaboratory);

//router.delete("/",md_authen.ensureAuthGeneric,timesCommerceController.getSpare,timesCommerceController.delete,md_audit.ensureAuth);

module.exports = router;