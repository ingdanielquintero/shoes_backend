const express = require("express");
const app = express();
const cors = require("cors");
const dir = process.platform == 'win32' ? 'C:/polaris' : '/opt/polaris';
const morgan = require("morgan");
const logger = require('./bin/logger');

app.use(morgan("dev"));

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    logger.info(`Service ${req.protocol}://${req.get('host')}${req.originalUrl} method: ${req.method} ip: ${req.ip}`);
    next();
});

// var whitelist = ['http://181.52.84.254:8081', 'http://54.196.144.54', 'http://54.196.144.54:8080', 'http://localhost:4200' ]
app.use(cors({
    limit: '500mb',
    extended: true,
    origin: [`http://${process.env.HOST_FRONTEND}`, `http://${process.env.HOST_FRONTEND_MEDIANET}`, "http://localhost:4200"],
    exposedHeaders: ['to-token-refresh', 'ids', 'usercodeid', 'authenticator', 'usercode', 'Pragma', 'Cache-Control', 'cache-control', 'pragma'],
}));

app.use(express.json({ limit: '500mb' }));
app.use(express.urlencoded({ extended: true }))

app.use('/static', express.static('Reports/resources'));
app.use('/photos', express.static(dir));
app.use('/downloads', express.static('Resources'));
app.use('/AppImages', express.static('AppImages'));

//routes
app.use("/BCP", require("./Routes/BcpRouter.js"));

app.use("/PolarisCore/Roles", require("./Routes/RoleRouter.js"));
app.use("/PolarisCore/Warehouses", require("./Routes/WarehouseRouter.js"));
app.use("/PolarisCore/Audits", require("./Routes/AuditRouter.js"));
app.use("/PolarisCore/Accesories", require("./Routes/AccesoryRouter.js"));
app.use("/PolarisCore/Terminals", require("./Routes/TerminalRouter.js"));
app.use("/PolarisCore/Commerce", require("./Routes/CommerceRouter.js"));
app.use("/PolarisCore/Extras", require("./Routes/ExtraRouter"));
app.use("/PolarisCore/DeliveryNote", require("./Routes/DeliverynoteRouter.js"));
app.use("/PolarisCore/Spares", require("./Routes/SpareRouter.js"));
app.use("/PolarisCore/Simcard", require("./Routes/SimcardRouter.js"));
app.use("/PolarisCore/Notifications", require("./Routes/NotificationRouter.js"));
app.use("/PolarisCore/upload", require("./Routes/file-uploadRouter"));
app.use("/PolarisCore/Mail", require("./Routes/MailRouter"));
app.use("/PolarisCore/TimesCommerce", require("./Routes/TimesCommerceRouter"));
app.use("/PolarisCore/ObservationIncidence", require("./Routes/ObservationIncidenceRouter"));
app.use("/PolarisCore/Incidencies", require("./Routes/IncidenceRouter"));
app.use("/PolarisCore/AtentionTime", require("./Routes/AtentionTimeRouter"));
app.use("/PolarisCore/Localication", require("./Routes/LocalicationRouter"));
app.use("/PolarisCore/Dashboard", require("./Routes/DashboardRouter"));
app.use("/PolarisCore/TypificationIncidence", require("./Routes/typification_incidenceRouter"));
app.use("/PolarisCore/Dasboard", require("./Routes/DashboardRoute"));
app.use("/PolarisCore/UserCity", require("./Routes/UserCitysRouter"));
app.use("/PolarisCore/Reports", require("./Routes/ReportsPDFRouter"));



app.use("/Shoes/Usuario", require("./Routes/UsuarioRutas"));
app.use("/Shoes/Tiquete", require("./Routes/TiquetesRutas"));
app.use("/Shoes/Empleado", require("./Routes/EmpleadosRutas"));
app.use("/Shoes/Roles", require("./Routes/RoleRouter.js"));
app.use("/Shoes/Users", require("./Routes/UserRouter.js"));

app.use("/Shoes/RegistrosGenerales", require("./Routes/RegistrosGeneralesRutas"));

app.use("/Shoes/Cliente", require("./Routes/ClienteShoe"));



module.exports = app;