const CronJob = require('./cron.js').CronJob;
const conection = require('../database');
const logger = require('../bin/logger');

/* const job = new CronJob('* * * * *', function () { */

const job = new CronJob('20 1 * * *', function () {

  const d = new Date();
  try {
    let tecnicos = getUserTecnicos();
    if (tecnicos === false) {
      return;
    }
  } catch (ex) {
    logger.error(ex.stack);
  }
});

async function getUserTecnicos() {
  var queryConsulta = "select user_id_user from user where user_position = 'TÉCNICO' and user_state= 'ACTIVO' allow filtering";
  const moment = require("moment");
  const fecha = moment().date() + '/' + (moment().month() + 1) + '/' + moment().year()

  var respuesta = false;
  try {
    conection.execute(queryConsulta, [], function (err, result) {
      if (err) {
        logger.error(err.stack);
        return false;
      } else {
        if (result.rowLength > 0) {
          for (let entry of result.rows) {
            if (entry.user_id_user) {
              let ok = getFindAll(entry.user_id_user, fecha);
              respuesta = ok;
            }
          }
        }
      }
    });
    return new Promise((resolve, reject) => {
      resolve(respuesta);
    });
  } catch (ex) {
    logger.error(ex.stack);
    return false;
  }
}

async function getFindAll(user, date) {
  let asociadas = await TermAsociadas(user);
  let Autorizadas = await TermAutorizadas(user);
  Autorizadas = Autorizadas.filter(c => { return !c.term_user_reparation || c.term_user_reparation === '' });
  var totalTerminales = asociadas.length + Autorizadas.length;
  var queryConsulta = "insert into user_terminals (uste_user,uste_associated_terminals,uste_completed_terminals,uste_date) values (?,?,?,?) IF NOT EXISTS;";
  parameters = [user, totalTerminales + '', '0', date];

  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          if (Object.values(result.rows[0])[0]) {
            resolve(true);
          } else {
            resolve(false);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);
  }
};


async function Validate(user, date) {
  const moment = require("moment");
  const fechaAnterior = moment().subtract(1, 'd').format('DD/MM/YYYY');
  var res = fechaAnterior.split('/');
  var dia = parseInt(res[0]);
  var mes = parseInt(res[1]);
  const fechaAnt = dia + "/" + mes + "/" + res[2];

  var queryConsulta = "select * from user_terminals where uste_user =? and uste_date =? allow filtering";
  parameters = [user, fechaAnt];
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          if (result.rows) {
            resolve(result.rows);
          } else {
            resolve(false);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);
  }
};


async function TermAutorizadas(user) {
  var queryConsulta = "select * from terminal where term_status=? AND term_localication=? ALLOW FILTERING;";
  const parameters = ['REPARACIÓN', user];
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          if (result.rows) {
            resolve(result.rows);
          } else {
            resolve(false);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);
  }
};

async function TermAsociadas(user) {
  var queryConsulta = "select * from terminal where term_status=? AND term_localication=? ALLOW FILTERING;";
  const parameters = ['PREDIAGNÓSTICO', user];
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          if (result.rows) {
            resolve(result.rows);
          } else {
            resolve(false);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);
  }
};



async function getUserSesion() {
  var queryConsulta = "select * from user";
  const moment = require("moment");
  const fecha = (moment().date()) + "/" + (moment().month() + 1) + "/" + moment().year();
  var respuesta = false;
  try {
    conection.execute(queryConsulta, [], function (err, result) {
      if (err) {
        logger.error(err.stack);
        return false;
      } else {
        if (result.rowLength > 0) {
          for (let entry of result.rows) {
            if (entry.user_identification) {
              let ok = getFindAllUsers(entry.user_identification);
              respuesta = ok;
              // console.log("se cerro session a : "+ entry.user_identification);
            }
          }
        }
      }
    });
    return new Promise((resolve, reject) => {
      resolve(respuesta);
    });
  } catch (ex) {
    logger.error(ex.stack);
    return false;
  }
}

async function getFindAllUsers(user) {
  var queryConsulta = "update user set user_session='0' where user_identification=? if exists";
  parameters = [user];
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          if (Object.values(result.rows[0])[0]) {
            resolve(true);
          } else {
            resolve(false);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);
  }
};

const jobSesion = new CronJob('0 2 * * *', function () {
  const d = new Date();
  try {
    let tecnicos = getUserSesion();
    if (tecnicos === false) {
      return;
    }
  } catch (ex) {
    logger.error(ex.stack);
  }
});

const jobSesion2 = new CronJob('0 11 * * *', function () {
  const d = new Date();
  try {
    let tecnicos = getUserSesion();
    if (tecnicos === false) {
      return;
    }
  } catch (ex) {
    logger.error(ex.stack);
  }
});

const jobSesion3 = new CronJob('0 15 * * *', function () {
  const d = new Date();
  try {
    let tecnicos = getUserSesion();
    if (tecnicos === false) {
      return;
    }
  } catch (ex) {
    logger.error(ex.stack);
  }
});

const notificacionIncidencias = new CronJob('0,30 * * * *', async function () {
  var queryPermisosIncidencias = "SELECT * from roles WHERE role_modules LIKE ? ALLOW FILTERING;"
  var parametersPermisosIncidencias = ['%GESTIÓN DE INCIDENCIAS%']; //modulo gestion de incidencias
  var queryUserRol = "SELECT user_role from user WHERE user_id_user = ? ALLOW FILTERING;"
  var queryCity = "SELECT comm_city FROM commerce where comm_uniqe_code = ? ALLOW FILTERING"

  let permisosIncidencias = await queryDinamic(queryPermisosIncidencias, parametersPermisosIncidencias);

  let incidencias = await geIncidences();

  for (var w = 0; w < incidencias.length; w++) {
    if (incidencias[w]['inci_notification_flag'] == null && incidencias[w]['inci_id'] != undefined) {
      var id = incidencias[w]['inci_id'];
      let ciudad = await queryDinamic(queryCity, [incidencias[w]['inci_code_commerce']]);

      const FechaCreacion = new Date(incidencias[w]['inci_date_create']);
      const FechaLimite = new Date(incidencias[w]['inci_date_limit_attention']);

      const FechaActual = new Date();

      var cantMin = ((FechaLimite.getTime() / 60000) - (FechaCreacion.getTime() / 60000));
      var tiempo75 = (cantMin) * (0.75);

      var cantMinActual = ((FechaActual.getTime() / 60000) - (FechaCreacion.getTime() / 60000));

      if (cantMinActual >= tiempo75) {
        if (incidencias[w]['inci_technical'] != "") {
          notificacion(incidencias[w]['inci_technical'], incidencias[w]['inci_id']);
        }

        if (ciudad.length > 0) {
          let users = await getUserCity(ciudad[0]['comm_city']);
          users.forEach(async usuario => {
            let rolUser = await queryDinamic(queryUserRol, [usuario['usercode']]);
            if (rolUser.length > 0) {
              for (let index = 0; index < permisosIncidencias.length; index++) {
                const rol = permisosIncidencias[index];
                if (rolUser[0]['user_role'].includes(rol['role_id'])) {
                  notificacion(usuario['usercode'], id);
                  break;
                }
              }
            }
          });
        }
      }
    }
  }
});

async function geIncidences() {
  var queryConsulta = "select * from incidence where inci_status_of_visit = 'EN PROCESO DE ATENCIÓN' allow filtering";
  parameters = [];
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve([]);
        } else {
          if (result.rows) {
            resolve(result.rows);
          } else {
            resolve([]);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve([]);
  }
};

async function getUserCity(ciudad) {
  var queryConsulta = "SELECT * FROM user_city WHERE citys LIKE ?  allow filtering;";
  parameters = ['%' + ciudad + '%'];
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve([]);
        } else {
          if (result.rows) {
            resolve(result.rows);
          } else {
            resolve([]);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve([]);
  }
};

async function actualizarIncidencia(id) {
  var queryConsulta = "update incidence set inci_notification_flag = ? where inci_id = ?";
  const fecha = new Date();
  parameters = ['1', id];
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          if (result) {
            resolve(true);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(err.stack);
    resolve(false);
  }
};

async function crearNotificacion(destino, msg, origen) {
  var queryConsulta = "INSERT INTO notifications (noti_id,noti_date_create,noti_dest,noti_msg,noti_origin,noti_state,noti_type) VALUES (now(), ?,?,?,?,?,?)";
  const moment = require("moment");
  const fecha = moment().format("LLL");
  parameters = ['' + fecha, destino, msg, origen, 'No leída', 'Exitosa'];
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          if (result) {
            resolve(true);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);
  }
};

async function queryDinamic(queryConsulta, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve([]);
        } else {
          if (result.rows) {
            resolve(result.rows);
          } else {
            resolve([]);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve([]);
  }
};

async function notificacion(tecnico, idIncidencia) {
  try {
    const msg = 'La incidencia: ' + idIncidencia + ' esta por vencerse';
    let crearNot = await crearNotificacion(tecnico, msg, 'Coordinador incidencias');
    let actInci = await actualizarIncidencia(idIncidencia)
  } catch (ex) {
    logger.error(ex.stack);
  }
};

job.start();
jobSesion.start();
jobSesion2.start();
jobSesion3.start();
notificacionIncidencias.start();