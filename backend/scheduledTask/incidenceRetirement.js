const CronJob = require('./cron.js').CronJob;
const conection = require('../database');
const email = require('../Controllers/MailController');
const moment = require("moment");
const logger = require('../bin/logger');
const zone = "America/Bogota";
const formatBd = "YYYY-MM-DD HH:mm:ss";

const job = new CronJob('00 00 * * *', function () {
	const d = new Date();
	try {
		var query = 'select * from incidence';

		getFindAll(query, []).then(async (incidences) => {
			const date = moment.utc(d).format("DD-MM-YYYY");

			incidences = incidences.filter(c => {
				return c.inci_date_finish_post_event != null &&
					c.inci_date_finish_post_event != '' &&
					c.inci_date_finish_post_event != undefined && c.inci_date_finish_post_event != '' && c.inci_date_finish_post_event != null &&
					`${date}` == `${moment.utc(c.inci_date_finish_post_event).format("DD-MM-YYYY")}`
			});

			for (let i = 0; i < incidences.length; i++) {
				const e = incidences[i];

				var observacion = await getFindAll(`select * from observation_incidence where obin_incidence = '${e.inci_id}' allow filtering`);
				e.observation = observacion[0];

				var inci_motive = 'RETIRO DE POS';
				var type = 'RETIRO';
				var status = 'EN PROCESO DE ATENCIÓN';

				var incidencia = {
					inci_code_commerce: e.inci_code_commerce,
					inci_contac_person: e.inci_contac_person,
					inci_contact_attended: null,
					inci_date_assignment: null,
					inci_date_close: null,
					inci_date_create: `${d}`,
					inci_date_finish_post_event: null,
					inci_date_reception: null,
					inci_motive: inci_motive,
					inci_name_commerce: e.inci_name_commerce,
					inci_notification_flag: null,
					inci_priority: e.inci_priority,
					inci_schedule_of_attention: e.inci_schedule_of_attention,
					inci_status_of_visit: status,
					inci_technical: '',
					inci_tecnology: e.inci_tecnology,
					inci_term_brand: e.inci_term_brand,
					inci_term_model: e.inci_term_model,
					inci_term_number: e.inci_term_number,
					inci_term_serial: e.inci_term_serial,
					inci_time_atention: null,
					inci_type: type,
					inci_typing: null,
					inci_id: e.inci_id,
					observation: observacion[0]
				}

				saveIncidenceAppCommerce(incidencia, 'RETIRO', 'EN PROCESO DE ATENCIÓN', d).then((id) => {
					getFindAll('update incidence set inci_date_finish_post_event = ? where inci_id = ?', [null, e.inci_id]);
					getFindAll('SELECT comm_name, comm_email FROM commerce WHERE comm_uniqe_code = ?', [e.inci_code_commerce]).then((comercio) => {
						email.sendMailNewIncidence({
							inci_ids: id,
							comercio: { nombre: e.inci_name_commerce, correo: comercio[0].comm_email }
						});
					})
				})
			}
		});
	} catch (ex) {
		logger.error(ex.stack);
	}
});

function saveIncidenceAppCommerce(request, inci_type, inci_status_of_visit, d) {
	return new Promise(async (resolve, reject) => {
		try {
			var id = await getByAutoId();
			let ok = await updateIdIncidences(id);
			if (!ok) return false;

			var inci_date_limit_attention = d;
			var inci_date_create = new Date();

			var tiempo = await getFindAll('select * from atention_time', []);
			tiempo = tiempo.filter(c => { return c.atti_priority === request.inci_priority });

			var horas = tiempo[0].atti_support_and_removal;
			inci_date_limit_attention.setHours(inci_date_create.getHours() + parseInt(horas));

			if(request.inci_date_finish_post_event != null){
				request.inci_date_finish_post_event = 	moment(new Date(request.inci_date_finish_post_event)).tz(zone).format(formatBd)
			}

			const parameters = [
				`${id}`, request.inci_name_commerce, request.inci_priority, inci_status_of_visit, request.inci_code_commerce,
				request.inci_contac_person, moment(inci_date_create).tz(zone).format(formatBd), moment(inci_date_limit_attention).tz(zone).format(formatBd), request.inci_schedule_of_attention,
				request.inci_technical, request.inci_tecnology, request.inci_term_brand, request.inci_term_model, request.inci_term_number,
				request.inci_term_serial, inci_type, request.inci_motive, request.inci_date_finish_post_event
			];

			var query = "insert into  polaris_core.inci_incidence  (inci_id,inci_name_commerce,inci_priority,inci_status_of_visit, " +
				"inci_code_commerce,inci_contac_person,inci_date_create,inci_date_limit_attention,inci_schedule_of_attention, " +
				"inci_technical, inci_tecnology, inci_term_brand, inci_term_model, inci_term_number, inci_term_serial, inci_type, " +
				"inci_motive, inci_date_finish_post_event) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) if not exists;";

			var query2 = "insert into  polaris_core.incidence  (inci_id,inci_name_commerce,inci_priority,inci_status_of_visit," +
				"inci_code_commerce,inci_contac_person,inci_date_create,inci_date_limit_attention,inci_schedule_of_attention, " +
				"inci_technical, inci_tecnology, inci_term_brand, inci_term_model, inci_term_number, inci_term_serial, inci_type, " +
				"inci_motive, inci_date_finish_post_event) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) if not exists;";

			getFindAll(query, parameters).then(() => {
				getFindAll(query2, parameters).then((res) => {
					InsertarObservacionIncidencia(id, request.observation.obin_observation, `${request.inci_code_commerce} - ${request.inci_name_commerce}`)
						.then((res) => {
							resolve(id);
						});
				});
			});

		} catch (error) {
			logger.error(error.stack);
			resolve(false)
		}
	});
}

async function InsertarObservacionIncidencia(incidence, observacion, user) {
	try {
		return new Promise((resolve, reject) => {
			var query = "INSERT INTO observation_incidence (id,obin_date,obin_firm,obin_incidence,obin_observation,obin_photo,obin_user) VALUES (now(), ?,?,?,?,?,?)"
			const moment = require("moment");
			const fecha = moment().format("LLL");
			var parameter = [fecha, '0', '' + incidence, observacion, '', user];

			conection.execute(query, parameter, { prepare: true }, function (err, result) {
				if (err) {
					logger.error(err.stack);
					resolve(false);
				} else {
					resolve(true);
				}
			});
		});
	} catch (error) {
		logger.error(error.stack);
		resolve(true);
	}
}

async function getFindAll(query, parameters) {
	try {
		return new Promise((resolve, reject) => {
			conection.execute(query, parameters, (err, result) => {
				if (err) {
					logger.error(err.stack);
					resolve([]);
				} else {
					if (result.rows)
						resolve(result.rows);
					else resolve(true);
				}
			});
		});
	} catch (ex) {
		logger.error(ex.stack);
		resolve([]);
	}
}

async function getByAutoId() {
	var rta = 0;
	const query = "select auid_number from polaris_core.AutoId where auid_name= 'incidences'";
	try {
		return new Promise((resolve, reject) => {
			conection.execute(query, [], (err, result) => {
				if (err) {
					logger.error(err.stack);
					resolve(false);
				} else {
					if (result.rows.length < 1) {
						resolve(false);
					} else {
						rta = result.rows[0];
						resolve(rta['auid_number'] + 1);
					}
				}
			});
		});
	} catch (error) {
		logger.error(error.stack);
		resolve(false);
	}
}

async function updateIdIncidences(idActual) {
	const query = "update polaris_core.autoid set auid_number = " + idActual + " where auid_name = 'incidences' ";
	try {
		return new Promise((resolve, reject) => {
			conection.execute(query, [], (err, result) => {
				if (err) {
					logger.error(err.stack);
					resolve(false);
				} else {
					resolve(true);
				}
			});

		});
	} catch (error) {
		logger.error(error.stack);
		resolve(false);
	}
}

job.start();