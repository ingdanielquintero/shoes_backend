const clienteShoe = {};
const jwt = require("../Services/Jwt.js");
//Importamos el modulo paraa encriptar las contraseñas
const conection = require('../database');
const logger = require('../bin/logger');


//methods

clienteShoe.save = async (req, res, next) => {
    const request = req.body;
    const date = new Date();

    const query = "insert into cliente (cli_id, cli_nombre, cli_celular, cli_direccion, cli_usuario, cli_fecha_creacion, cli_estado) values (now(),?,?,?,?,?,?) if not exists";
    const parameters = [request.cli_nombre, request.cli_celular, request.cli_direccion, req.user.id, date.format("%Y-%m-%d %H:%M:%S", false), "ACTIVO"];
    try {
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: err });
            } else if (result) {
                return res.status(200).send({ status: true, message: "Cliente creado" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ status: false, error: "catch error" });
    }
}


clienteShoe.update = async (req, res, next) => {
    const request = req.body;
    var query = "update cliente set cli_nombre =?, cli_celular =?, cli_direccion=? where cli_id =? if exists";
    try {
        const parameters = [request.cli_nombre, request.cli_celular, request.cli_direccion, request.cli_id];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ status: false, message: "query error", err });
                return;
            } else {
                if (Object.values(result.rows[0])[0]) {
                    return res.status(200).send({ status: true, message: "Cliente actualizado" });
                } else {
                    return res.status(200).send({ status: false, message: Object.values(result.rows[0])[0] });
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ status: false, error: "catch error" });
    }
}

clienteShoe.updateEstado = async (req, res, next) => {
    const request = req.body;
    var query = "update cliente set cli_estado =? where cli_id =? if exists";
    try {
        const parameters = [request.cli_estado, request.cli_id];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ status: false, message: "query error", err });
                return;
            } else {
                if (Object.values(result.rows[0])[0]) {
                    return res.status(200).send({ status: true, message: "Cliente actualizado" });
                } else {
                    return res.status(200).send({ status: false, message: Object.values(result.rows[0])[0] });
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ status: false, error: "catch error" });
    }
}

clienteShoe.getAllByUsuario = async (req, res) => {
    const request = req.headers;
    const query = "select * from cliente where cli_usuario =? allow filtering";
    try {
        conection.execute(query, [req.user.id], (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: "error in BD" });
            } else {
                return res.status(200).send({ status: true, data: result.rows });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ status: false, message: "Not Acceptable" });
    }
};

clienteShoe.getById = async (req, res) => {
    const request = req.headers;
    const query = "select * from cliente where cli_id =?";
    try {
        conection.execute(query, [request.id], (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: "error in BD" });
            } else {
                return res.status(200).send({ status: true, data: result.rows });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ status: false, message: "Not Acceptable" });
    }
};


//exports
module.exports = clienteShoe;