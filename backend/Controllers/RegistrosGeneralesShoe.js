const registrosGeneralesShoe = {};
const jwt = require("../Services/Jwt.js");
//Importamos el modulo paraa encriptar las contraseñas
const conection = require('../database');
const logger = require('../bin/logger');


//methods

registrosGeneralesShoe.save = async (req, res, next) => {
    const request = req.body;
    const date = new Date();

    let registro = await validarRegistro(request.rege_nombre, request.rege_tipo, req.user.id);
    if (registro.status) {
        return res.status(200).send({ status: false, message: "Este dato de " + request.rege_tipo + " ya está registrado" });
    }

    const cassandra = require('cassandra-driver');
    const Uuid = cassandra.types.Uuid;
    const rege_id = Uuid.random();

    if (request.rege_tipo == 'REFERENCIA') {
        registrarPrecios(rege_id, request.prere_cortada, request.prere_guarnicion, request.prere_montada, request.prere_limpiada, req.user.id);
    }

    const query = "insert into registros_generales (rege_id, rege_nombre, rege_tipo, rege_usuario, rege_fecha_registro) values (?,?,?,?,?) if not exists";
    const parameters = [rege_id, request.rege_nombre, request.rege_tipo, req.user.id, date.format("%Y-%m-%d %H:%M:%S", false)];
    try {
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: err });
            } else if (result) {
                return res.status(200).send({ status: true, message: "Registro creado" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ status: false, message: "catch error" });
    }
}

async function validarRegistro(rege_nombre, rege_tipo, rege_usuario) {
    var rta = 0;
    const query = "select rege_id from registros_generales where rege_nombre=? and rege_tipo =? and rege_usuario =? allow filtering";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [rege_nombre, rege_tipo, rege_usuario], (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve({ status: false, "message": "error in BD" });
                }
                else {
                    if (result.rows.length < 1) {
                        resolve({ status: false, "message": "No data in BD" });
                    } else {
                        rta = result.rows[0];
                        resolve({ status: true, "message": rta['rege_id'] });
                    }
                }
            });

        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ message: "Not Acceptable" });
    }
}


async function registrarPrecios(prere_referencia, prere_precio_cortada, prere_precio_guarnicion, prere_precio_montada, prere_precio_limpiada, prere_usuario) {

    const query = "insert into precios_referencia (prere_referencia, prere_precio_cortada, prere_precio_guarnicion, prere_precio_montada, prere_precio_limpiada, prere_usuario) values (?,?,?,?,?,?) if not exists";
    const parameters = [prere_referencia, prere_precio_cortada, prere_precio_guarnicion, prere_precio_montada, prere_precio_limpiada, prere_usuario];
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, parameters, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve({ status: false, "message": "error in BD" });
                }
                else {
                    if (result.rows.length < 1) {
                        resolve({ status: false, "message": "No data in BD" });
                    } else {
                        resolve({ status: true, "message": "Presio registrado" });
                    }
                }
            });

        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ message: "Not Acceptable" });
    }
}


registrosGeneralesShoe.update = async (req, res, next) => {
    const request = req.body;

    let registro = await validarRegistro(request.rege_nombre, request.rege_tipo);
    if (registro.status) {
        return res.status(200).send({ status: true, message: "Registro actualizado" });
    }

    if (request.rege_tipo == 'REFERENCIA') {
        actualizarPrecios(request.rege_id, request.prere_cortada, request.prere_guarnicion, request.prere_montada, request.prere_limpiada);
    }

    var query = "update registros_generales set rege_nombre =?, rege_tipo =? where rege_id =? if exists";
    try {
        const parameters = [request.rege_nombre, request.rege_tipo, request.rege_id];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ status: false, message: "query error", err });
                return;
            } else {
                if (Object.values(result.rows[0])[0]) {
                    return res.status(200).send({ status: true, message: "Registro actualizado" });
                } else {
                    return res.status(200).send({ status: false, message: Object.values(result.rows[0])[0] });
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ status: false, error: "catch error" });
    }
}


async function actualizarPrecios(prere_referencia, prere_precio_cortada, prere_precio_guarnicion, prere_precio_montada, prere_precio_limpiada) {

    const query = "update precios_referencia set prere_precio_cortada =?, prere_precio_guarnicion =?, prere_precio_montada =?, prere_precio_limpiada =? where prere_referencia =? if exists";
    const parameters = [prere_precio_cortada, prere_precio_guarnicion, prere_precio_montada, prere_precio_limpiada, prere_referencia];
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, parameters, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve({ status: false, "message": "error in BD" });
                }
                else {
                    if (result.rows.length < 1) {
                        resolve({ status: false, "message": "No data in BD" });
                    } else {
                        resolve({ status: true, "message": "Presio registrado" });
                    }
                }
            });

        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ message: "Not Acceptable" });
    }
}


registrosGeneralesShoe.getAll = async (req, res) => {
    const query = "select * from registros_generales where rege_usuario =? allow filtering";
    try {
        conection.execute(query, [req.user.id], (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: "error in BD" });
            } else {
                return res.status(200).send({ status: true, data: result.rows });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ status: false, message: "Not Acceptable" });
    }
};

registrosGeneralesShoe.getAllByTipo = async (req, res) => {
    const request = req.headers;
    const query = "select * from registros_generales where rege_tipo =? and rege_usuario =? allow filtering";
    try {
        conection.execute(query, [request.rege_tipo, req.user.id], (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: "error in BD" });
            } else {
                return res.status(200).send({ status: true, data: result.rows });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ status: false, message: "Not Acceptable" });
    }
};

registrosGeneralesShoe.getById = async (req, res) => {
    const request = req.headers;
    const query = "select * from registros_generales where rege_id =?";
    try {
        conection.execute(query, [request.id], (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: "error in BD" });
            } else {
                return res.status(200).send({ status: true, data: result.rows });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ status: false, message: "Not Acceptable" });
    }
};

registrosGeneralesShoe.getPreciosReferencias = async (req, res) => {
    const query = "select * from precios_referencia where prere_usuario =? allow filtering";
    try {
        conection.execute(query, [req.user.id], (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: "error in BD" });
            } else {
                return res.status(200).send({ status: true, data: result.rows });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ status: false, message: "Not Acceptable" });
    }
};


registrosGeneralesShoe.getPreciosReferenciasById = async (req, res) => {
    const request = req.headers;
    const query = "select * from precios_referencia where prere_referencia =?";
    try {
        conection.execute(query, [request.prere_referencia], (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: "error in BD" });
            } else {
                return res.status(200).send({ status: true, data: result.rows });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ status: false, message: "Not Acceptable" });
    }
};


//exports
module.exports = registrosGeneralesShoe;
