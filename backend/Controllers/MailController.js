const mailController = {};
const conection = require("../database.js");
const bcrypt = require('bcrypt');
const fs = require('fs-extra');
const path = require('path');
const hbs = require('handlebars');
const logger = require('../bin/logger');

var host = process.platform == 'win32' ? `http://${process.env.IP_HOST}:4200/#/` : `http://${process.env.IP_HOST}/#`

const compile = async function (templateName, data) {
    const filepath = path.join(process.cwd(), 'Reports', `${templateName}.hbs`);
    const html = await fs.readFile(filepath, 'utf-8');
    return await hbs.compile(html)(data);
}

const compileMailers = async function (templateName, data) {
    const filepath = path.join(process.cwd(), 'Mails', `${templateName}.hbs`);
    const html = await fs.readFile(filepath, 'utf-8');
    return await hbs.compile(html)(data);
}


mailController.sendMailRestartPassword = async (req, res) => {
    var correoDestino = req.body.correo;
    var codigoUsuario = req.body.identificacion;
    var nodemailer = require('nodemailer');
    var temporalPassword = Math.random().toString(20).substring(2, 10) + Math.random().toString(20).substring(2, 10);
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'soporte.polariscore@gmail.com',
            pass: 'PolarisCore2019'
        }
    });

    var bodyPassword = await compileMailers('PasswordReset', {});

    var mailOptions = {
        from: 'soporte@polarisCore.com',
        to: correoDestino,
        subject: 'Restablecer contraseña',
        html: bodyPassword.replace("@", temporalPassword).replace("*", "plataforma")
    };

    try {
        bcrypt.hash(temporalPassword, 10, function (err, hash) {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: "Error when creating password", status: 'fail', aplicated: 'false' });
            } else {
                const query = "update user set  user_login = ?, user_password = ? where user_identification = ?";
                const parameters = ['0', hash, codigoUsuario];
                conection.execute(query, parameters, function (err, result) {
                    if (err) {
                        logger.error(err.stack);
                        res.status(200).send({ message: "Query error", status: 'fail', aplicated: 'false', error: err });
                        return;
                    } else {
                        transporter.sendMail(mailOptions, function (error, info) {
                            if (error) {
                                logger.error(error.stack);
                                res.status(200).send({ message: "Mail error", status: 'fail', aplicated: 'false' });
                            } else {
                                res.status(200).send({ message: "success", status: 'ok', aplicated: 'true' });
                            }
                        });
                    }
                });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ message: "Catch error", status: 'fail', aplicated: 'false' });
    }
};

mailController.sendMailCloseSessionExternal = async (req, res) => {
    var nodemailer = require('nodemailer');
    var transporter = nodemailer.createTransport({
        service: 'Gmail',
        auth: {
            user: 'soporte.polariscore@gmail.com',
            pass: 'PolarisCore2019'
        }
    });

    var html = await compileMailers('CloseSesion', {
        link: `${host}?k=${new Buffer(`token=${req.body.token}&code=${req.body.code}&identification=${req.body.documento}`).toString('base64')}`
    });

    var mailOptions = {
        from: 'soporte@polarisCore.com',
        to: req.body.email,
        subject: 'Solicitud cierre de sesión',
        html: html
    };

    try {
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                logger.error(error.stack);
                res.status(200).send({ message: "Mail error", status: 'fail', aplicated: 'false' });
            }

            return res.status(200).send({ message: "success", status: 'ok' });
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ message: "Catch error", status: 'fail', aplicated: 'false' });
    }
}

mailController.sendMail = (req, res) => {
    try {

        var correoDestino = "diegolopez@wposs.com";
        var nodemailer = require('nodemailer');
        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'soporte.polariscore@gmail.com',
                pass: 'PolarisCore2019'
            }
        });
        var mailOptions = {
            from: 'soporte@polarisCore.com',
            to: correoDestino,
            subject: 'Restablecer contraseña',
            html: ''
        };
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                logger.error(error.stack);
                res.status(200).send({ message: "Mail error", status: 'fail', aplicated: 'false' });
            } else {
                res.status(200).send({ message: "success", status: 'ok', aplicated: 'true' });
            }
        });
    } catch (error) {
        logger.error(error.stack);
    }

};

mailController.sendMailAssignTechnical = async (data) => {
    try {

        var correoDestino = data['infoComercio']['comm_email'];
        var nombreComercio = data['infoComercio']['comm_name'];
        var tecnico = data['infoTecnico']

        var nodemailer = require('nodemailer');
        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'soporte.polariscore@gmail.com',
                pass: 'PolarisCore2019'
            }
        });

        let html = await compileMailers('AssignTechical', {
            nombreComercio: nombreComercio,
            numeroIncidencia: data['numeroIncidencia'],
            fotoTecnico: tecnico['user_photo'],
            tecnicoNombre: tecnico['user_name'],
            tecnicoUsercode: tecnico['user_id_user'],
            userIdentificacion: tecnico['user_identification'],
        });

        var mailOptions = {
            from: 'soporte@polarisCore.com',
            to: correoDestino,
            subject: 'Asignación de técnico',
            html: html
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                logger.error(error.stack);
                return { message: "Mail error", status: 'fail', aplicated: 'false' };
            } else {
                return { message: "success", status: 'ok', aplicated: 'true' };
            }
        });
    } catch (error) {
        logger.error(error.stack);
    }
};

mailController.sendMailNewIncidence = async (data) => {
    try {

        var nodemailer = require('nodemailer');
        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'soporte.polariscore@gmail.com',
                pass: 'PolarisCore2019'
            }
        });

        var html = await compile('templates/emails/newIncidence', {
            nombreComercio: data.comercio.nombre,
            ids: data.inci_ids
        });

        var mailOptions = {
            from: 'soporte@polarisCore.com',
            to: data.comercio.correo,
            subject: 'Solicitud de creación de incidencia',
            html: html
        };

        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                logger.error(error.stack);
                return { message: "Mail error", status: 'fail', aplicated: 'false' };
            } else {
                return { message: "success", status: 'ok', aplicated: 'true' };
            }
        });
    } catch (error) {
        logger.error(error.stack);
    }
};

mailController.sendMailPasswordUserCommerce = (req, res) => {
    var correoDestino = req.body.correo;
    var codigoUsuario = req.body.usco_id;

    try {
        var nodemailer = require('nodemailer');
        var temporalPassword = Math.random().toString(20).substring(2, 10) + Math.random().toString(20).substring(2, 10);
        var transporter = nodemailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'soporte.polariscore@gmail.com',
                pass: 'PolarisCore2019'
            }
        });
        var mailOptions = {
            from: 'soporte@polarisCore.com',
            to: correoDestino,
            subject: 'Restablecer contraseña',
            html: bodyPassword.replace("@", temporalPassword).replace("*", "aplicación")
        };

        bcrypt.hash(temporalPassword, 10, function (err, hash) {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: "Error when creating password", status: 'fail', aplicated: 'false' });
            } else {
                const query = "update user_commerce set  usco_password = ? where usco_id = ?";
                const parameters = [hash, codigoUsuario];
                conection.execute(query, parameters, function (err, result) {
                    if (err) {
                        logger.error(err.stack);
                        res.status(200).send({ message: "Query error", status: 'fail', aplicated: 'false', error: err });
                        return;
                    } else {
                        transporter.sendMail(mailOptions, function (error, info) {
                            if (error) {
                                logger.error(error.stack);
                                res.status(200).send({ message: "Mail error", status: 'fail', aplicated: 'false' });
                            } else {
                                res.status(200).send({ message: "success", status: 'ok', aplicated: 'true' });
                            }
                        });
                    }
                });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ message: "Catch error", status: 'fail', aplicated: 'false' });
    }
};
//exports
module.exports = mailController;