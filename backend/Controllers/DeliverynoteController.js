"use strict"
const deliverynoteController = {};
const terminals = require('../Controllers/TerminalController');
const logger = require('../bin/logger');
const conection = require('../database');

//métodos

//Servicio que crea un albarán
deliverynoteController.create = async (req, res, next) => {
    const date = new Date();
    const request = req.body;
    try {
        if (!req.body || !req.headers.authenticator) {
            res.status(400).send({ message: "incomplete parameters to make the request" });
            return;
        }
        var deno_date_creation = date.format("%Y-%m-%d %H:%M:%S", false);
        let deno_id = await getByIdNext();
        const deno_origin = request.deno_origin;
        const deno_dest = request.deno_dest;
        const deno_observations = request.deno_observations;
        const deno_user = req.user.code;
        const deno_packinglist = request.packinglist;
        const deno_senditems = request.senditems;
        const deno_state = "ABIERTO";
        const parameters = [deno_id, deno_origin, deno_dest, deno_date_creation, deno_observations, deno_packinglist, deno_senditems, deno_user, deno_state, " "];
        var query = "insert into polaris_core.deliverynote (deno_id, deno_origin, deno_dest, deno_date_creation, deno_observations,deno_packinglist, deno_senditems, deno_user, deno_state, deno_date_send ) values (?,?,?,?,?,?,?,?,?,?) IF NOT EXISTS";
        let execute = await insertDeliverynote(query, parameters);
        let update = await updateIdDeliverunote(deno_id);
        req.action = "crear/albarán";
        req.before = "{}";
        req.after = JSON.stringify(parameters);
        next();
        res.status(200).send({ message: "Success", Deliverynote_id: deno_id, Date_create: deno_date_creation });
    }
    catch (error) {
        clogger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};

//  Método que inserta un Albarán
function insertDeliverynote(query, parameters) {
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, parameters, { prepare: true }, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    reject({ "Data": "error in BD on insert" });
                }
                else {
                    if (result.rows.length < 1) {
                        reject({ "Data": "No data in BD" });
                    } else {
                        resolve(result.rows);
                    }
                }
            });
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ message: "Not Acceptable" });
    }
}



//Servicio que actualiza un albarán
deliverynoteController.update = async (req, res, next) => {

    try {
        if (!req.body || !req.headers.id) {
            res.status(400).send({ message: "incomplete parameters to make the request" });
            return;
        }
        const request = req.body;

        if (request.deno_state == "PENDIENTE" || request.deno_state == "CERRADO" || request.deno_state == "ANULADO") {
            const date = new Date();
            var deno_date_send = date.format("%Y-%m-%d %H:%M:%S", false);
            var parameters = [request.deno_origin, request.deno_dest, request.deno_observations, request.deno_packinglist, request.deno_senditems, request.deno_simcard_send, request.deno_state, deno_date_send, req.headers.id];
            var query = "update polaris_core.deliverynote set deno_origin = ?, deno_dest = ?,  deno_observations = ?,deno_packinglist = ?, deno_senditems = ?, deno_simcard_send = ?, deno_state = ?, deno_date_received= ? where  deno_id = ? IF EXISTS";
            if (!request.deno_state != "ANULADO") {
                req.action = "recibir/albarán";
            }
            else {
                req.action = "anular/albarán";
            }
        }

        if (request.deno_state == "ENVIADO") {
            const date = new Date();
            var deno_date_send = date.format("%Y-%m-%d %H:%M:%S", false);

            var parameters = [request.deno_origin, request.deno_dest, request.deno_observations, request.deno_packinglist, request.deno_senditems, request.deno_simcard_send, request.deno_state, deno_date_send, request.deno_user, req.headers.id];
            var query = "update polaris_core.deliverynote set deno_origin = ?, deno_dest = ?,  deno_observations = ?,deno_packinglist = ?, deno_senditems = ?, deno_simcard_send = ?, deno_state = ?, deno_date_send= ?, deno_user= ? where  deno_id = ? IF EXISTS";
            req.action = "enviar/albarán";
        }
        if (request.deno_state == "ABIERTO") {

            var parameters = [request.deno_origin, request.deno_dest, request.deno_observations, request.deno_packinglist, request.deno_senditems, request.deno_simcard_send, request.deno_state, request.deno_user, req.headers.id];
            var query = "update polaris_core.deliverynote set deno_origin = ?, deno_dest = ?,  deno_observations = ?,deno_packinglist = ?, deno_senditems = ?, deno_simcard_send = ?, deno_state = ?, deno_user= ? where  deno_id = ? IF EXISTS";
            req.action = "actualizar/albarán";
        }

        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error", err });
                return;
            } else if (result) {
                req.after = JSON.stringify(req.body);
                res.json({ message: "success" });
                next();
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};


//Servicio que elimina un albarán
deliverynoteController.delete = async (req, res, next) => {
    const status = req.estado;
    try {
        const query = "update  polaris_core.deliverynote set deno_state = 'Anulado'  where deno_id = " + req.headers.id + "IF EXISTS";
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " error in BD" });
                return;
            } else {
                req.action = "eliminar/albarán ";
                req.after = "{}";
                next();
                res.json({ message: "success" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};



//Servicio que devuelve todos los albarán
deliverynoteController.getAll = async (req, res) => {
    const query = "select * from polaris_core.deliverynote ";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD" });
                return;
            } else {
                res.json(result.rows);
            }
        });

    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};

deliverynoteController.getDeliverynote = async (req, res, next) => {
    const query = "select * from polaris_core.deliverynote where deno_id = ?";
    var estado = false;
    try {
        conection.execute(query, [req.headers.id], { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "query error" });
                return;
            }
            if (result.rows.length > 0) {
                req.before = JSON.stringify(result.rows);
                estado = true;
            }
            req.estado = estado;
            next();
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};


//Servicio que devuelve un albarán dado un Id
deliverynoteController.getById = async (req, res) => {
    try {
        if (!req.headers.id) {
            res.status(400).send({ message: "incomplete parameters to make the request" });
            return;
        }
        const deno_id = req.headers.id;
        const query = "select * from polaris_core.deliverynote where deno_id = " + deno_id;

        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.json({ "Data": "error in BD" });
            }
            else {
                if (result.rows.length < 1) {
                    res.json({ "Data": "No data in BD" });
                } else {
                    var rta = result.rows[0];
                    res.json(rta);
                }
            }
        });

    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};

async function getByIdNext() {
    var rta = 0;
    const query = "select Auid_number from polaris_core.AutoId where Auid_name= 'deliverynote'";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [], (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    reject({ "Data": "error in BD" });
                }
                else {
                    if (result.rows.length < 1) {
                        reject({ "Data": "No data in BD" });
                    } else {
                        rta = result.rows[0];
                        resolve(rta['auid_number'] + 1);
                    }
                }
            });

        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ message: "Not Acceptable" });
    }
}

//Método que actualiza el Id autoincrementable del  albarán 
async function updateIdDeliverunote(idActual) {
    const query = "update polaris_core.autoid set auid_number = " + idActual + " where auid_name = 'deliverynote' ";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [], (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    reject({ "Data": "error in BD" });
                }
                else {
                    resolve(result);
                }
            });

        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
}
// método que realiza el envío del albarán
async function enviarAlbarán(senditems) {
    try {
        var terminales = senditems['terminales'];
        var accerepu = senditems['accerepu'];
        let accesorios = await obtenerAccesorios(accerepu);
        let repuestos = await obtenerRepuestos(accerepu);
        var okTerminales = enviarTerminales(terminales);
        var okAccesorios = enviarAccesorios(accesorios);
        var okRepuestos = enviarRepuestos(repuestos);
        if (!okTerminales || !okAccesorios || !okRepuestos) {
            if (!okTerminales) {
                devolverTerminales(terminales);
            }
            if (!okAccesorios) {
                devolverAccesorios(accesorios);
            }
            if (!okRepuestos) {
                devolverRepuestos(repuestos);
            }
            return { "message": "El albarán no pudo ser enviado, intente nuevamente" };
        }
        else {
            return { "message": "El albarán ha sido enviado con éxito" };
        }
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
}

// método que realiza la recepción del albarán
function recibirAlbarán(senditems) {
    var terminales = senditems['terminales'];
    var accesorios = senditems['accesorios'];
    var repuestos = senditems['repuestos'];
}

//Método para obtener los accesorios de una lista de de repuestos y accesorios
async function obtenerAccesorios(accerepu) {
    try {
        return new Promise((resolve, reject) => {
            var accesorios = {};
            var item;
            for (var x = 0; x < accerepu.length; x++) {
                item = accerepu[x];
                if (item.referencia.substr(1, 1) == "a") {
                    repuestos.push(item);
                }
            }
            resolve(accesorios);
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};

//Método para obtener los repuestos de una lista de de repuestos y accesorios
async function obtenerRepuestos(accerepu) {
    try {
        return new Promise((resolve, reject) => {
            var repuestos = {};
            var item;
            for (var x = 0; x < accerepu.length; x++) {
                item = accerepu[x];
                if (item.referencia.substr(1, 1) == "r") {
                    repuestos.push(item);
                }
            }
            resolve(repuestos);
        });

    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};

// Método que cambia de estado las terminales enviadas en un albarán
function enviarTerminales(terminales) {
    var state = false;
    for (var x; x < terminales.length; x++) {
        terminal_serial = terminales[x].serial;
        terminal_status_temporal = terminales[x].status;
        terminal_status = "En tránsito";
        state = terminals.updateStatus(terminal_status, terminal_status_temporal, terminal_serial);
        if (!state) {
            return state;
        }

    }
    return state;
}

// Método que cambia de estado los accesorios enviados en un albarán
async function enviarAccesorios(accesorios) {
    var state = false;
    for (var x; x < accesorios.length; x++) {
        accesory_serial = accesorios[x].serial;
        accesory_status_temporal = accesorios[x].status;
        accesory_status = "En tránsito";
        state = accesorys.updateStatus(accesorys_status, accesorys_status_temporal, accesorys_serial);
        if (!state) {
            return state;
        }

    }
    return state;
}

// Método que cambia de estado las terminales enviadas en un albarán
function enviarRepuestos(repuestos) {
    var state = false;
    for (var x; x < repuestos.length; x++) {
        spare_serial = repuestos[x].serial;
        spare_status_temporal = repuestos[x].status;
        spare_status = "En tránsito";
        state = terminals.updateStatus(spare_status, spare_status_temporal, spare_serial);
        if (!state) {
            return state;
        }

    }
    return state;
}



// método que formatea una variable Date
Date.prototype.format = function (fstr, utc) {
    var that = this;
    utc = utc ? 'getUTC' : 'get';
    return fstr.replace(/%[YmdHMS]/g, function (m) {
        switch (m) {
            case '%Y': return that[utc + 'FullYear']();
            case '%m': m = 1 + that[utc + 'Month'](); break;
            case '%d': m = that[utc + 'Date'](); break;
            case '%H': m = that[utc + 'Hours'](); break;
            case '%M': m = that[utc + 'Minutes'](); break;
            case '%S': m = that[utc + 'Seconds'](); break;
            default: return m.slice(1);
        }
        return ('0' + m).slice(-2);
    });
};


deliverynoteController.createObservations = async (req, res) => {
    try {
        if (!req.body) {
            res.status(400).send({ message: "incomplete parameters to make the request" });
            return;
        }
        const request = req.body;
        const parameters = [request.dde_deliveynote, request.dde_elements, request.dde_observation, request.dde_cantidad];
        var query = "insert into deliverynote_delete_elements (dde_id, dde_deliverynote, dde_elements, dde_observation,  dde_cantidad) values (now(),?,?,?,?)";
        conection.execute(query, parameters, { prepare: true }, (err, result) => {

            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD", err });
                return;
            } else {
                res.status(200).send({ message: "success", applied: "true" });
                req.action = "registrar/observacionesDeAlbarán";
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};


deliverynoteController.GetObservations = async (req, res) => {
    try {
        if (!req.body) {
            res.status(200).send({ message: "incomplete parameters to make the request" });
            return;
        }
        const request = req.headers;
        const parameters = [request.id];
        var query = "select * from deliverynote_delete_elements where dde_deliverynote = ?";
        conection.execute(query, parameters, { prepare: true }, (err, result) => {

            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: "error in BD", err });
                return;
            } else {
                res.status(200).send(result.rows);
                req.action = "obtener/observacionesDeAlbarán";
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ message: "Not Acceptable" });
    }
};

deliverynoteController.getTimeLab = async (req, res) => {
    try {
        var query = "select * from  times_by_laboratory";
        conection.execute(query, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: "Query error", err });
                return;
            } else {
                res.status(200).send(result.rows[0]);
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ message: "Not Acceptable" });
    }
};

deliverynoteController.updateAns = async (req, res) => {
    try {
        const parameter = [req.body.ans, req.body.hoy, req.body.serial]
        var query = "update terminal set term_date_ans = ?, term_date_reception = ? where term_serial = ?";
        conection.execute(query, parameter, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: "Query error", err });
                return;
            } else {
                res.status(200).send(result);
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ message: "Not Acceptable" });
    }
};


//exports
module.exports = deliverynoteController;
