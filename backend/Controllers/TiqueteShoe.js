const tiqueteShoe = {};
const jwt = require("../Services/Jwt.js");
//Importamos el modulo paraa encriptar las contraseñas
const conection = require('../database');
const logger = require('../bin/logger');


//methods

tiqueteShoe.save = async (req, res, next) => {
    const request = req.body;
    const date = new Date();

    let tiq_codigo = await getByIdNext(req.user.id);
    let codigo_formateado = await formatearCodio('' + tiq_codigo)

    const cassandra = require('cassandra-driver');
    const Uuid = cassandra.types.Uuid;
    const tiq_id = Uuid.random();

    const query = "insert into tiquete (tiq_id, tiq_codigo, tiq_color, tiq_estado, tiq_fecha_creacion, tiq_material, tiq_observaciones, tiq_pares, " +
        "tiq_planta, tiq_referencia, tiq_tallas, tiq_cliente, tiq_usuario) values (?,?,?,?,?,?,?,?,?,?,?,?,?) if not exists";
    const parameters = [tiq_id, codigo_formateado, request.tiq_color, request.tiq_estado, date.format("%Y-%m-%d %H:%M:%S", false), request.tiq_material, request.tiq_observaciones,
        request.tiq_pares, request.tiq_planta, request.tiq_referencia, request.tiq_tallas, request.tiq_cliente, req.user.id];
    try {
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: err });
            } else if (result) {
                updateIdTiquete(tiq_codigo, req.user.id);
                createTiquesEmpleado('CORTADA', tiq_id, codigo_formateado);
                createTiquesEmpleado('GUARNICIÓN', tiq_id, codigo_formateado);
                createTiquesEmpleado('MONTADA', tiq_id, codigo_formateado);
                createTiquesEmpleado('LIMPIADA', tiq_id, codigo_formateado);
                return res.status(200).send({ status: true, message: "Tiquete: " + codigo_formateado + " creado" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ status: false, error: "catch error" });
    }
}

async function getByIdNext(usu_id) {
    var rta = 0;
    const query = "select auid_number from autoId where auid_name=?";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, ['tiquete' + usu_id], { prepare : true }, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    reject({ "Data": "error in BD" });
                }
                else {
                    if (result.rows.length < 1) {
                        reject({ "Data": "No data in BD" });
                    } else {
                        rta = result.rows[0];
                        resolve(rta['auid_number'] + 1);
                    }
                }
            });

        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ message: "Not Acceptable" });
    }
}

//Método que actualiza el Id autoincrementable 
async function updateIdTiquete(idActual, usu_id) {
    const query = "update autoid set auid_number =? where auid_name =?";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [idActual, 'tiquete' + usu_id], { prepare : true }, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    console.log("ereror: ", err.stack)
                    resolve({ "Data": "error in BD" });
                }
                else {
                    resolve(result);
                }
            });

        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
}


async function createTiquesEmpleado(referencia, tiq_id, tiq_codigo) {
    const query = "insert into tiquete_empleado (tiqem_id, tiqem_empleado, tiqem_estado, tiqem_fecha_pago, tiqem_referencia, tiqem_tiquete, tiqem_codigo ) values (now(),?,?,?,?,?,?) if not exists";
    const parameters = ['', 'CONFECCIÓN', '', referencia, '' + tiq_id, '' + tiq_codigo]
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, parameters, { prepare: true }, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    reject({ "Data": "error in BD" });
                }
                else {
                    resolve(result);
                }
            });

        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
}

async function formatearCodio(codigo) {
    var cant = codigo.length;
    for (let index = 0; index < 5 - cant; index++) {
        codigo = '0' + codigo;
    }
    return codigo;
}


tiqueteShoe.updateStatus = async (req, res, next) => {
    const request = req.body;
    var query = "update tiquete set tiq_estado =? where tiq_id =? if exists";
    try {
        const parameters = [request.tiq_estado, request.tiq_id];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ status: false, message: "query error", err });
                return;
            } else {
                if (Object.values(result.rows[0])[0]) {
                    return res.status(200).send({ status: true, message: "Tiquete actualizado" });
                } else {
                    return res.status(200).send({ status: false, message: Object.values(result.rows[0])[0] });
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ status: false, error: "catch error" });
    }
}

tiqueteShoe.update = async (req, res, next) => {
    const request = req.body;
    var query = "update tiquete set tiq_color =?, tiq_estado =?, tiq_material =?, tiq_observaciones =?, tiq_pares =?, " +
        "tiq_planta =?, tiq_referencia =?, tiq_tallas =?, tiq_cliente =? where tiq_id =? if exists";
    try {
        const parameters = [request.tiq_color, request.tiq_estado, request.tiq_material, request.tiq_observaciones, request.tiq_pares,
        request.tiq_planta, request.tiq_referencia, request.tiq_tallas, request.tiq_cliente, request.tiq_id];

        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ status: false, message: "query error", err });
                return;
            } else {
                if (Object.values(result.rows[0])[0]) {
                    return res.status(200).send({ status: true, message: "Tiquete actualizado" });
                } else {
                    return res.status(200).send({ status: false, message: Object.values(result.rows[0])[0] });
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ status: false, error: "catch error" });
    }
}

tiqueteShoe.getAll = async (req, res) => {
    const query = "select * from tiquete where tiq_usuario =? allow filtering";
    try {
        conection.execute(query, [req.user.id], (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: "error in BD" });
            } else {
                return res.status(200).send({ status: true, data: result.rows });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ status: false, message: "Not Acceptable" });
    }
};

tiqueteShoe.getById = async (req, res) => {
    const request = req.headers;
    const query = "select * from tiquete where tiq_id =?";
    try {
        conection.execute(query, [request.id], (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: "error in BD" });
            } else {
                return res.status(200).send({ status: true, data: result.rows });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ status: false, message: "Not Acceptable" });
    }
};



tiqueteShoe.getByCodigo = async (req, res) => {
    const request = req.headers;
    const query = "select * from tiquete where tiq_codigo =? and tiq_usuario =? allow filtering";
    try {
        conection.execute(query, [request.tiq_codigo, req.user.id], (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: "error in BD" });
            } else {
                return res.status(200).send({ status: true, data: result.rows });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ status: false, message: "Not Acceptable" });
    }
};



tiqueteShoe.pay = async (req, res, next) => {
    const request = req.body;
    const date = new Date();

    try {
        const tiqem_id = await findTiqueteEmpleado(request.tiqem_codigo, request.tiqem_referencia)
        if (tiqem_id.status) {
            var query = "update tiquete_empleado set tiqem_estado =?, tiqem_fecha_pago =?, tiqem_empleado =? where tiqem_id =? if exists";
            const parameters = ['PAGO', date.format("%Y-%m-%d %H:%M:%S", false), request.tiqem_empleado, tiqem_id.message];

            conection.execute(query, parameters, { prepare: true }, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    res.status(200).send({ status: false, message: "query error", err });
                    return;
                } else {
                    if (Object.values(result.rows[0])[0]) {
                        return res.status(200).send({ status: true, message: "Tiquete pagado" });
                    } else {
                        return res.status(200).send({ status: false, message: Object.values(result.rows[0])[0] });
                    }
                }
            });
        } else {
            res.status(200).send({ status: false, message: "Tiquete no encontrado" });
        }
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ status: false, error: "catch error" });
    }
}


async function findTiqueteEmpleado(tiqem_codigo, tiqem_referencia) {
    const query = "select * from tiquete_empleado where tiqem_codigo =? and tiqem_referencia =? allow filtering;";
    const parameters = [tiqem_codigo, tiqem_referencia]
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, parameters, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve({ status: false, message: "error in BD" });
                }
                else {
                    if (result.rows.length < 1) {
                        resolve({ status: false, message: "No data in BD" });
                    } else {
                        rta = result.rows[0];
                        resolve({ status: true, message: rta['tiqem_id'] });
                    }
                }
            });

        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ message: "Not Acceptable" });
    }
}

tiqueteShoe.validatePay = (req, res, next) => {
    const request = req.body;
    const query = "select * from tiquete_empleado where tiqem_codigo =? and tiqem_referencia =? and tiqem_estado =? allow filtering;";
    const parameters = [request.tiqem_codigo, request.tiqem_referencia, 'PAGO']
    try {
        conection.execute(query, parameters, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: 'query error' });
            } else {
                if (result.rows.length > 0) {
                    return res.status(200).send({ status: false, message: "Este tiquete ya está pago", data: result.rows[0] });
                } else {
                    next();
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error" });
    }
};


//exports
module.exports = tiqueteShoe;
