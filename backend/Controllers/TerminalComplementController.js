const terminalComplementController = {};
const conection = require("../database.js");
const c = require('colors');
const logger = require('../bin/logger');

terminalComplementController.findModels = async (req, res, next) => {
    const query = "select * from terminal_model where temo_name = ?";
    var model = "";
    try {
        if (req.body.spar_terminal_model) {
            model = req.body.spar_terminal_model;
        } else {
            if (req.body.acce_terminal_model) {
                model = req.body.acce_terminal_model;
            } else if (req.body.term_model) {
                model = req.body.term_model;
            } else {
                res.status(400).send({ message: "error, wrong json syntax" });
            }
        }

        const parameters = [model];
        conection.execute(query, parameters, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "query error" });
                return;
            } else {
                if (result.rows.length > 0) {
                    next();
                } else {
                    res.status(400).send({ message: "Terminal model not found" });
                    return;
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

terminalComplementController.findAllModels = async (req, res) => {
    const query = "select * from terminal_model";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

terminalComplementController.findStates = async (req, res, next) => {
    const query = "select * from terminal_state where test_id = ?";
    var state = "";
    try {
        if (req.body.term_state) {
            state = req.body.term_state;
        } else {
            res.status(400).send({ message: "error, wrong json syntax" });
        }
        const parameters = [state];
        conection.execute(query, parameters, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "query error " });
                return;
            } else {
                if (result.rows.length > 0) {
                    next();
                } else {
                    res.status(400).send({ message: "Terminal state not found" });
                    return;
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

terminalComplementController.findAllStates = async (req, res) => {
    const query = "select * from terminal_state";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

terminalComplementController.findAllSpareStatus = async (req, res) => {
    const query = "select * from spare_status";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

terminalComplementController.findAllAccesoriesStatus = async (req, res) => {
    const query = "select * from accesory_status";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

terminalComplementController.findAllTecnologies = async (req, res) => {
    const query = "select * from technology";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error" });
    }
};

//exports
module.exports = terminalComplementController;