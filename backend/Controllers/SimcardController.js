const simcardController = {};
const conection = require("../database.js");
const c = require('colors');
const moment = require('moment-timezone');
const zone = `${process.env.TIME_ZONE}`;
const format = `${process.env.DATE_FORMAT}`;

simcardController.saveSeveral = async (req, res, next) => {
  try {
    var notInserted = [];
    var countInserted = 0;
    const date = moment().tz(zone).format(format);
    const logueado = req.user.code;
    var request = req.body;
    var opt = req.body[request.length - 1]['opcion'];
    let resultStatus = await getStatus();
    let resultBrand = await getBrand();
    let resultWarehoure = await executeQuery("select * from warehouse where ware_status = 'ACTIVO' ALLOW FILTERING", []);
    var booleanBrand = false;
    var booleanLocation = false;
    var booleanStatus = false;
    var totalNoInsert = [];
    var banderaInserted = false;
    let resultadoUbicacion;
    let resultadoInsert;
    let i;
    var query = "INSERT INTO polaris_core.sim_card(sica_serial,sica_brand,sica_status,sica_location,sica_status_temporal,sica_date_register,sica_register_by)values(?,?,?,?,?,?,?)IF NOT EXISTS";
    if (request.length != 0 && resultStatus.rows.length != 0 && resultBrand.rows.length != 0) {
      for (i = 0, totalSize = request.length - 1; i < totalSize; i++) {
        switch (opt) {
          case "1":
            resultadoUbicacion = resultWarehoure.some(c => c.ware_id_warehouse == request[i].sica_location);
            if (!resultadoUbicacion) {
              notInserted.push(" Bodega inválida");
              booleanLocation = true;
            }
            break;
          case "2":
            queryLocation = "select * from commerce where comm_uniqe_code = ?";
            resultadoUbicacion = await getValidarUbicacion(request[i].sica_location, queryLocation);
            if (resultadoUbicacion.length == 0) {
              notInserted.push(" Comercio inválido");
              booleanLocation = true;
            }
            break;
          case "3":
            queryLocation = "select user_position from user where user_id_user = ?";
            resultadoUbicacion = await getValidarUbicacionUsuario(request[i].sica_location, queryLocation);
            if (resultadoUbicacion == 0) {
              notInserted.push(" Técnico inválido");
              booleanLocation = true;
            }
            break;
        }

        booleanBrand = resultBrand.rows.some((element) => `${element.scbr_description}` == `${request[i].sica_brand}`)
        if (!booleanBrand) {
          notInserted.push(" marca inválida");
        }

        booleanStatus = resultStatus.rows.some((element) => `${element.scst_description}` == `${request[i].sica_status}`)
        if (!booleanStatus) {
          notInserted.push(" Estado inválido");
        }

        if (!booleanLocation && booleanBrand && booleanStatus) {
          const parameters = [request[i].sica_serial, `${request[i].sica_brand}`, `${request[i].sica_status}`, `${request[i].sica_location}`, " ", date, logueado];
          resultadoInsert = await getRelaizarInsert(query, parameters);
          banderaInserted = Object.values(resultadoInsert[0])[0];
          if (!banderaInserted) {
            notInserted.push(" El Serial de la sim card ya existe");
            totalNoInsert.push("Fila ==>  " + (i + 2) + notInserted); 0
          }
          booleanSerial = false;
          booleanLocation = false;
          booleanBrand = false;
          booleanStatus = false;
          notInserted = [];
        } else {
          totalNoInsert.push("Fila ==>  " + (i + 2) + notInserted);
          booleanSerial = false;
          booleanLocation = false;
          booleanBrand = false;
          booleanStatus = false;
          notInserted = [];
        }

        if (banderaInserted) {
          countInserted++;
          banderaInserted = false;
        }

        if ((i % 100) == 0) {
          console.log("cargando sim cards ... ", i);
        }
      }
      return res.status(200).send({ message: "Resultados de el cargue ", response: totalNoInsert, countInserted: countInserted });
    } else {
      res.status(400).send({ error: "Ha ocurrido un problema, alguno de los valores en la consulta esta vacio." });
    }

  } catch (error) {
    logger.error(error.stack);
  }

};


simcardController.saveSimCard = async (req, res, next) => {
  const moment = require("moment");
  const date = moment().format("LLL");
  const logueado = req.user.code;
  var request = req.body;
  var query = "INSERT INTO polaris_core.sim_card(sica_serial,sica_brand,sica_status,sica_location,sica_status_temporal,sica_date_register,sica_register_by)values(?,?,?,?,?,?,?) IF NOT EXISTS";

  if (request.length != 0) {

    const parameters = [request.sica_serial, "" + request.sica_brand, "" + request.sica_status, "" + request.sica_location, " ", date, logueado];

    try {
      conection.execute(query, parameters, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          res.status(200).send({ message: "Error", applied: false });
        } else {
          res.status(200).send({ message: "Success", applied: Object.values(result.rows[0])[0] });
          return;
        }
      });
    } catch (ex) {
      logger.error(ex.stack);
      return;
    }

  } else {
    res.status(400).send({ error: "Ha ocurrido un problema, alguno de los valores en la consulta esta vacio." });
  }

};

//tecnologia de Estado
async function getStatus() {
  var resultStatus;
  var queryConsulta = "SELECT scst_description FROM sim_card_status";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          return;
        } else {
          resultStatus = result;
          resolve(resultStatus);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(402).send({ error: "catch error 3" });
  }
}

// marca de marcas
async function getBrand() {
  var resultBrand;
  var queryConsulta = "SELECT scbr_description FROM sim_card_brand";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          return;
        } else {
          resultBrand = result;
          resolve(resultBrand);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(402).send({ error: "catch error 4" });
  }
}

simcardController.location = async (req, res, next) => {
  var resultLocation;
  var opt = req.body[0];
  var codigo = req.body[1];
  switch (opt) {
    case "1":
      var queryConsulta = "select ware_name from warehouse where ware_id_warehouse = ?";
      try {
        return new Promise((resolve, reject) => {
          conection.execute(queryConsulta, codigo, { prepare: true }, (err, result) => {
            if (err) {
              logger.error(err.stack);
              return;
            } else {
              resultLocation = result;
              resolve(resultLocation);
            }
          });
        });
      } catch (ex) {
        logger.error(ex.stack);
        res.status(402).send({ error: "catch error 2" });
      }
      break;
    case "2":
      var queryConsulta = "select comm_name from commerce where comm_uniqe_code = ?";
      try {
        return new Promise((resolve, reject) => {
          conection.execute(queryConsulta, codigo, { prepare: true }, (err, result) => {
            if (err) {
              logger.error(err.stack);
              return;
            } else {
              resultLocation = result;
              resolve(resultLocation);
            }
          });
        });
      } catch (ex) {
        logger.error(ex.stack);
        res.status(402).send({ error: "catch error 2" });
      }
      break;
    case "3":
      var queryConsulta = "select user_name from user where user_id_user = ? ";
      var queryConsulta2 = "select user_name from user where user_id_user = ? ";
      try {
        return new Promise((resolve, reject) => {
          conection.execute(queryConsulta, codigo, { prepare: true }, (err, result) => {
            if (err) {
              logger.error(err.stack);
              return;
            } else {
              resultLocation.push(result.rows);
              conection.execute(queryConsulta2, codigo, { prepare: true }, (err, result) => {
                if (err) {
                  logger.error(err.stack);
                  return;
                } else {
                  resultLocation.push(result.rows);
                  resolve(resultLocation);
                }
              });
              resolve(resultLocation);
            }
          });
        });
      } catch (ex) {
        logger.error(ex.stack);
        res.status(402).send({ error: "catch error 2" });
      }
      break;
  }
  if (resultLocation.rows.length != 0) {
    res.status(200).send({ data: resultLocation });
    return;
  }
};

simcardController.findAll = async (req, res) => {
  const query = "select * from sim_card";
  try {
    conection.execute(query, [], (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: " query error" });
        return;
      } else {
        res.json(result.rows);
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

simcardController.findSerial = async (req, res) => {
  const query = "select * from sim_card where sica_serial=?";
  try {
    conection.execute(query, [req.body.sica_serial], (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: " query error" });
        return;
      } else {
        res.json(result.rows);
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

simcardController.findAllStatus = async (req, res) => {
  const query = "select * from sim_card_status";
  try {
    conection.execute(query, [], (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: " query error" });
        return;
      } else {
        res.json(result.rows);
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

simcardController.updateStatusSimcard = async (req, res) => {
  try {
    request = req.body;
    var query = "update sim_card set sica_status=?, sica_status_temporal=?, sica_location=? where sica_serial=? IF EXISTS";
    const parameters = [request.sica_status, request.sica_status_temporal, request.sica_location, request.sica_serial];
    conection.execute(query, parameters, { prepare: true }, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: " query error", err });
        return;
      } else if (result) {
        if (Object.values(result.rows[0])[0] === true) {
          res.json({ "respuesta": true, res: result.rows[0] });
        } else {
          res.json({ "respuesta": false, res: result.rows[0] });
        }
      }
    });
  }
  catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

simcardController.findSimCardsBySerial = async (req, res) => {
  const query = "SELECT * FROM sim_card WHERE sica_serial = ? AND sica_status ='NUEVO' ALLOW FILTERING;";
  const query2 = "SELECT * FROM sim_card WHERE sica_serial = ? AND sica_status ='OPERATIVO' ALLOW FILTERING;";
  const request = req.body;
  var respuesta = [];
  try {
    if (!request) {
      res.status(402).send({ message: "incomplete petition" });
    } else {
      const parameters = [request['serial']];
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          res.status(401).send({ message: " query error" });
          return;
        } else {
          respuesta.push(result.rows);
          conection.execute(query2, parameters, (err, result) => {
            if (err) {
              res.status(401).send({ message: " query error" });
              return;
            } else {
              respuesta.push(result.rows);
              res.json(respuesta);
            }
          });
        }
      });
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.status(400).send({ error: "catch error" });
  }
};

simcardController.findAllBrand = async (req, res) => {
  const query = "select * from sim_card_brand";
  try {
    conection.execute(query, [], (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: " query error" });
        return;
      } else {
        res.json(result.rows);
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};


simcardController.delete = async (req, res, next) => {
  const status = req.estado;
  try {
    if (status === false) {
      res.status(404).send({ message: "error, simcard not exits" });
      return;
    };
    var query = "delete from sim_card  where sica_serial =?";
    const parameters = [req.headers.id];
    conection.execute(query, parameters, { prepare: true }, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: "query error" });
        return;
      } else {
        req.action = "eliminar/simcard";
        req.after = "{}";
        res.json({ message: "success" });
        next();
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

async function getValidarUbicacionUsuario(dato, query) {
  var parameters = [dato];
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          console.log("error al Consultar ", err);
        } else {
          if (result.rows != 0) {
            if (result.rows[0]['user_position'] == 'TÉCNICO' || result.rows[0]['user_position'] == 'TÉCNICO EN CAMPO') {
              resolve(1);
            } else {
              resolve(0);
            }
          } else {
            resolve(result.rows.length);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}
async function getValidarUbicacion(dato, query) {
  var parameters = [dato];
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          console.log("error al Consultar ", err);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}
async function getRelaizarInsert(query, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          console.log("error al Consultar ", err);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}

simcardController.getSimCardByLocation = async (req, res) => {
  const query = "SELECT * FROM sim_card WHERE sica_location = ? ALLOW FILTERING;";
  const request = req.body[0];
  try {
    if (!request) {
      res.status(402).send({ message: "incomplete petition" });
    } else {
      const parameters = [request];
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          res.status(401).send({ message: " query error" });
          return;
        } else {
          res.json(result.rows);
        }
      });
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.status(400).send({ error: "catch error" });
  }
};

simcardController.findBySerial = async (req, res) => {
  const query = "select * from sim_card where sica_serial = ?";
  try {
    conection.execute(query, [req.headers.serial], (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.json({ message: "success", status: "fail" });
        return;
      } else {
        res.json({ message: "success", status: "ok", incidencias: result.rows });
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "success", status: "fail" });
  }
};

simcardController.findSimCardsBySerialUbication = async (req, res) => {
  var estados = req.body.estados;

  if (estados.length <= 0) res.status(402).send({ message: "incomplete petition" });
  else {
    try {
      for (let i = 0; i < estados.length; i++) {
        const estado = estados[i];
        var query = "SELECT * FROM sim_card WHERE sica_serial = ? AND sica_status = '" + estado + "' AND sica_location = ? ALLOW FILTERING;";
        const parameters = [req.body.serial, req.body.ubication];
        var rows = await executeQuery(query, parameters);
        if (rows.length > 0)
          return res.json(rows[0]);
      }
      return res.json([]);
    } catch (ex) {
      logger.error(ex.stack);
      res.status(400).send({ error: "catch error" });
    }
  }
};

function executeQuery(query, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          console.log("error en el query ", err);
          resolve([]);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);

  }

}

simcardController.getSimCardByLocationAndroid = async (req, res) => {
  const query = "SELECT * FROM sim_card WHERE sica_location = ? ALLOW FILTERING;";
  const request = req.headers.code;
  try {
    if (!request) {
      res.status(200).send({ message: "Incomplete data", status: "fail" });
    } else {
      const parameters = [request];
      conection.execute(query, parameters, async (err, result) => {
        if (err) {
          logger.error(err.stack);
          res.status(200).send({ message: "Query error", status: "fail" });
          return;
        } else {
          if (result.rows.length > 0) {
            let respuesta = await procesarRespuesta(result.rows);
            res.status(200).send({ message: "success", status: "ok", summary: respuesta, answer: result.rows });
          } else {
            res.status(200).send({ message: "success", status: "ok", summary: [''], answer: result.rows });
          }
        }
      });
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ message: "Catch error", status: "fail", });
  }
};
async function procesarRespuesta(result) {
  var modelos = [];
  var resultados = [];
  var temporal = [];
  try {
    return new Promise((resolve, reject) => {
      result.forEach((element, pos) => {
        modelos.push({ marca: element['sica_brand'] });
      });
      modelos = modelos.filter((valorActual, indiceActual, arreglo) => {
        return arreglo.findIndex(
          valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)
        ) === indiceActual
      });
      modelos.forEach((element, pos) => {
        temporal = result.filter(c => { return c.sica_brand == element['marca'] });
        resultados.push({ marca: element['marca'], cantidad: temporal.length });
      });
      resolve(resultados);
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve([]);
  }
};
//exports
module.exports = simcardController;