const auditController = {};
const conection = require("../database.js");
const c = require('colors');
const logger = require('../bin/logger');

auditController.findAll = async (req, res) => {
    const query = "select * from audit";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);                
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

module.exports = auditController;