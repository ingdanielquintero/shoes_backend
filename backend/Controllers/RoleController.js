"use strict"

const rolesController = {};
const logger = require('../bin/logger');


//crear conexión
const conection = require('../database');

//métodos

//Servicio que crea un rol
rolesController.create = async (req, res, next) => {

    try {
        if (!req.headers.name || !req.body) {
            res.status(200).send({ message: "incomplete parameters to make the request" });
            return;
        }

        let deno_id = await getByIdNext();
        const role_id = "" + deno_id;
        const role_modules = req.body.rol;
        const role_name = req.headers.name;
        const parameters = { role_id, role_modules, role_name };
        var query = "insert into roles (role_id, role_modules, role_name ) values (?,?,?)";
        conection.execute(query, parameters, { prepare: true }, (err, result) => {

            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD" });
                return;
            } else {
                let update = updateIdRoles(deno_id);
                res.status(200).send({ message: "success", applied: "true" });

                req.action = "crear/rol";
                req.before = "{}";
                req.headers.view = "Crear roles"
                req.after = JSON.stringify(role_modules);
                req.headers.client = ""
                next();
            }
        });

    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};


rolesController.updaterol = async (req, res) => {


    try {
        if (!req.headers.name || !req.body) {
            res.status(400).send({ message: "incomplete parameters to make the request" });
            return;
        }

        const role_id = "" + req.headers.id;
        const role_modules = req.body.rol;
        const role_name = req.headers.name;
        const parameters = { role_modules, role_name, role_id };
        var query = "update roles  set role_modules =?, role_name=? where role_id =?";
        conection.execute(query, parameters, { prepare: true }, (err, result) => {

            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD", err });
                return;
            } else {
                res.status(200).send({ message: "success", applied: "true" });
                req.action = "registrar/rol";
                req.before = "{}";
                req.after = JSON.stringify(role_modules);
            }
        });

    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};


//Método que devuelve un albarán dado su usuario y fecha de creación
async function getByIdNext(date, user) {
    var rta = 0;
    const query = "select Auid_number from AutoId where Auid_name= 'roles'";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [], (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    reject({ "Data": "error in BD" });
                }
                else {
                    if (result.rows.length < 1) {
                        reject({ "Data": "No data in BD" });
                    } else {
                        rta = result.rows[0];
                        resolve(rta['auid_number'] + 1);
                    }
                }
            });

        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
}

//Método que actualiza el Id autoincrementable del  albarán 
async function updateIdRoles(idActual) {
    const query = "update autoid set auid_number = " + idActual + " where auid_name = 'roles' ";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [], (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    reject({ "Data": "error in BD" });
                }
                else {
                    resolve(result);
                }
            });

        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
}


//Servicio que actualiza un rol
rolesController.update = async (req, res) => {

    try {
        if (!req.headers.id || !req.body) {
            res.status(400).send({ message: "incomplete parameters to make the request" });
            return;
        }
        const role_id = req.headers.id;
        const role_modules = req.body;
        var newQuery = "update roles set role_modules =" + JSON.stringify(role_modules) + "  where role_id= " + role_id + "IF EXISTS";
        var regex = new RegExp("\"", "g");
        var query = newQuery.replace(regex, "'");
        conection.execute(query, [], (err, result) => {

            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD" });
                return;
            } else {
                res.status(200).send({ message: "success" });
                req.action = "actualizar/rol";
                req.before = "{}";
                req.after = JSON.stringify(role_modules);

            }
        });

    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};

//Servicio que elimina un rol
rolesController.delete = async (req, res) => {
    const status = req.estado;
    try {

        var query = "delete from roles  where role_id =?";
        const parameters = [req.headers.id];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " error in BD" });
                return;
            } else {
                req.action = "eliminar/rol";
                req.after = "{}";
                res.json({ message: "success" });

            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

//Servicio que agrega permiso de una vista a un rol
rolesController.createPermision = async (req, res) => {

    try {
        if (!req.headers.id || !req.headers.module || !req.headers.submodule || !req.headers.vista) {
            res.status(400).send({ message: "incomplete parameters to make the request" });
            return;

        }
        const id_rol = req.headers.id;
        const module = req.headers.module;
        const submodule = req.headers.submodule;
        const permisions = req.headers.vista;
        var roles = [];
        var view = [];
        const query = "select role_modules from roles where role_id=" + id_rol;

        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD" });
                return;
            } else {
                roles = result.rows[0];

                if (!roles['role_modules']['module'][module] || !roles['role_modules']['module'][module]['submodules'][submodule]) {
                    res.status(404).send({ message: "nonexistent parameters in BD: Module or Submodule" });
                    return;
                } else {
                    view = roles['role_modules']['module'][module]['submodules'][submodule]['views'];
                    let view_permissions = view;
                    view_permissions.push(permisions);
                    view = [];
                    view.push(...new Set(view_permissions));
                    roles['role_modules']['module'][module]['submodules'][submodule]['views'] = view;

                    var rta = "UPDATE roles SET role_modules = " + JSON.stringify(roles['role_modules']) + " WHERE role_id = " + id_rol + " IF EXISTS";
                    var regex = new RegExp("\"", "g");
                    var newQuery = rta.replace(regex, "'");
                    let respons = updateRol(newQuery);
                    res.json(view);
                }
            }
        });

    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};

//método que actualiza las vistas de un rol
async function updateRol(query) {
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [], (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    reject({ "Data": "error in BD" });
                }
                else {
                    resolve(result);
                }
            });
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
}

//Servicio que devuelve todos los roles
rolesController.getAll = async (req, res) => {
    console.log(req.headers.authenticator);
    const query = "select * from roles";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD" });
                return;
            } else {
                res.json(result.rows);
            }
        });

    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};


//Servicio que devuelve un rol dado un Id
rolesController.getByIds = async (req, res) => {
    var bodymodules = {};
    var aux = {};
    const idmodules = [];
    var p = req.headers.ids;
    try {
        if (p) {
            if (p.length > 0) {
                for (var x = 0; x < p.length; x++) {
                    if (x < 6 && p[x] < 7) {
                        idmodules.push(p[x]);
                        if (p[x] == "1") {
                            break;
                        }
                    } else {
                        res.status(406).send({ message: "out range" });
                    }
                }
                for (var z = 0; z < idmodules.length; z++) {
                    let resultmodules = await getModulesxrol(idmodules[z]);
                    aux = Object.assign(bodymodules, resultmodules);
                    bodymodules = Object.assign({}, aux);
                }
                res.json(bodymodules);
            }

        }
        else {
            res.json({ Data: "No parameters" });
        }
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};

//método que retorna los modulos de un rol por su Id
async function getModulesxrol(idmodule) {
    var rta = {};
    try {
        const query = "select role_modules from roles where role_id =" + idmodule;
        return new Promise((resolve, reject) => {
            conection.execute(query, [], (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    reject({ "Data": "error in BD" });
                }
                else {
                    if (result.rows.length < 1) {
                        reject({ "Data": "No data in BD" });
                    } else {
                        rta = result.rows[0]['role_modules'];
                        resolve(rta);
                    }
                }
            });
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
}


//Servicio que devuelve todos los modulos existentes
rolesController.getModules = async (req, res) => {
    var rta = {};
    try {
        rta = await getAllModules();
        res.json(rta);
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};

// método que busca todos los módulos en BD
async function getAllModules() {
    try {
        const query = "select  module_id, module_name from module";
        return new Promise((resolve, reject) => {
            conection.execute(query, [], (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    reject({ "Data": "error in BD" });
                }
                else {
                    if (result.rows.length < 1) {
                        reject({ "Data": "No data in BD" });
                    } else {
                        resolve(result.rows);
                    }
                }
            });
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
}


//Servicio que devuelve todos los submodulos de un módulo dado su Id
rolesController.getSubModules = async (req, res) => {
    const m = req.headers.module;
    var submodules = {};
    const query = "select submodule_id, submodule_name from submodule where submodule_idmodule=" + m + " ALLOW FILTERING";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD" });
                return;
            } else {
                submodules = result.rows;
                res.json(submodules);
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};

//Servicio que devuelve todas las vistas de un submódulo dado su Id
rolesController.getViews = async (req, res) => {
    const s = req.headers.submodule;
    var views = {};
    const query = "select view_id, view_name from views where view_idsubmodule= " + s + " ALLOW FILTERING";
    
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD" });
                return;
            } else {
                views = result.rows;
                res.json(views);
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};

//método que retorna los modulos de un rol por su Id
async function getById(id_rol) {
    var rta = {};
    try {
        const query = "select role_modules from roles where role_id =" + id_rol;
        return new Promise((resolve, reject) => {
            conection.execute(query, [], (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    reject({ "Data": "error in BD" });
                }
                else {
                    if (result.rows.length < 1) {
                        reject({ "Data": "No data in BD" });
                    } else {
                        rta = result.rows[0]['role_modules'];
                        resolve(rta);
                    }
                }
            });
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
}

//Servicio que devuelve un rol dado un Id
rolesController.getByIds2 = async (req, res) => {
    var bodymodules = [];
    var p = req.headers.ids;
    try {
        if (p) {
            if (p.length > 0) {
                var rol = p.split(",");

                for (var i = 0; i < rol.length; i++) {
                    let resultmodules = await getById(rol[i]);
                    bodymodules.push(resultmodules);
                }
                res.json({ respuesta: bodymodules });
            }
        }
        else {
            res.json({ Data: "No parameters" });
        }
    } catch (ex) {
        logger.error(ex.stack);
    }
};


rolesController.getIcons = async (req, res) => {

    const query = "select * from icons";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD" });
                return;
            } else {
                var icons = result.rows;
                res.json(icons);
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};


//exports
module.exports = rolesController;