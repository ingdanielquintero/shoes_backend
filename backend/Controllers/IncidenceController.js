const incidenceController = {};
const conection = require("../database.js");
const c = require('colors');
const email = require('./MailController');
const logger = require('../bin/logger');
const moment = require("moment");
const momentT = require('moment-timezone');
const funcionesGenerales = require('../bin/funcionesGenerales');
const zone = `${process.env.TIME_ZONE}`;
const format = `${process.env.DATE_FORMAT}`;
const formatBd = `${process.env.DATE_FORMAT_BD}`;
const formatValidate = `${process.env.DATE_FORMAT_VALIDATE}`;

incidenceController.findAll = async (req, res) => {
    const query = "select * from incidence";
    try {
        conection.execute(query, [], { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail", incidencias: [] });
            } else {
                res.json({ message: "success", status: "ok", incidencias: result.rows });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

incidenceController.getIncedenciasPorComercioReporte = async (req, res) => {
    const query = "select inci_date_create, inci_code_commerce, inci_id, inci_type, inci_status_of_visit, inci_technical from incidence where inci_code_commerce = ? AND  inci_date_create >= ?  AND  inci_date_create <= ? allow filtering";
    const range_dates = req.body.range_dates.range_dates;
    let fechaInicial = moment(new Date(range_dates[0])).format(formatBd);
    let fechaFinal = moment(new Date(range_dates[1]).setHours(23, 59, 59, 0)).format(formatBd);

    try {
        conection.execute(query, [req.body.code_commerce, fechaInicial, fechaFinal], { prepare: true }, async function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail" });
            } else {
                var incidencias = result.rows;
                const incidence_type = req.body.incidence_type.incidence_type;
                const incidence_status = req.body.incidences_status.incidences_status;
                const range_dates = req.body.range_dates.range_dates;

                var data = [];

                for (var i = 0; i < incidencias.length; i++) {
                    const incidencia = incidencias[i];
                    if (incidence_type.indexOf(incidencia.inci_type) >= 0) {
                        if (incidence_status.indexOf(incidencia.inci_status_of_visit) >= 0) {
                            incidencia.inci_date_create = formatearFecha(incidencia.inci_date_create);
                            data.push(incidencia);
                        }
                    }
                }
                res.json(data);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
}

incidenceController.getIncidenceCommerceReport = async (req, res) => {
    const query = "select inci_date_close, inci_date_create, inci_code_commerce, inci_id, inci_type, inci_status_of_visit, inci_technical from incidence WHERE inci_date_create >= ?  AND  inci_date_create <= ? ALLOW FILTERING";
    var body = JSON.parse(req.headers.info);
    let fechaInicial = moment(new Date(body.range_dates[0])).format(formatBd);
    let fechaFinal = moment(new Date(body.range_dates[1]).setHours(23, 59, 59, 0)).format(formatBd);
    try {
        conection.execute(query, [fechaInicial, fechaFinal], { prepare: true }, async function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail" });
            } else {
                var incidencias = result.rows;
                var data = [];
                incidencias.forEach(incidencia => {
                    if (body.incidence_type.includes(incidencia.inci_type) && body.incidences_status.includes(incidencia.inci_status_of_visit) && body.user_technicals.includes(incidencia.inci_technical)) {
                        incidencia.inci_date_close = formatearFecha(incidencia.inci_date_close);
                        data.push(incidencia);
                    }
                });

                res.json(data);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
}

incidenceController.getReportIncidenceByTechnical = async (req, res) => {
    let body = JSON.parse(req.headers.info);
    body.fechaFinal = new Date(body.fechaFinal).setHours(23, 59, 59, 0)
    const query = "select * from incidence WHERE inci_date_close >= ?  AND  inci_date_close <= ? and inci_technical = ? AND inci_type= ? ALLOW FILTERING";
    let fechaInicial = moment(body.fechaInicial).format(formatBd);
    let fechaFinal = moment(body.fechaFinal).format(formatBd);
    let tecnico = body.tecnico;
    let tipos = body.tipo;
    let data = [];
    try {
        for (let index = 0; index < tipos.length; index++) {
            const element = tipos[index];
            await getFindAll(query, [fechaInicial, fechaFinal, tecnico, element]).then((response) => {
                if (response.length > 0) {
                    data.push(returnAverages(response, element));
                }
            });
        }
        res.json(data);
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
}

function returnAverages(data, tipo) {
    let hora1 = 0;
    let minuto1 = 0;
    let hora2 = 0;
    let minuto2 = 0;
    let cantidadDias = 0;
    let promedioAtendidas = 0;
    let fecha;
    data = funcionesGenerales.ordenarFechaAscendente(data, 'inci_date_close');
    for (let index = 0; index < data.length; index++) {
        const element = data[index];
        if (index == 0 || funcionesGenerales.formatearFechaCorta(element['inci_date_close']) != funcionesGenerales.formatearFechaCorta(data[index - 1]['inci_date_close'])) {
            cantidadDias++;
            fecha = new Date(element['inci_date_close'])
            hora1 += fecha.getHours();
            minuto1 += fecha.getMinutes();
        }
        if (index == data.length - 1 || funcionesGenerales.formatearFechaCorta(element['inci_date_close']) != funcionesGenerales.formatearFechaCorta(data[index + 1]['inci_date_close'])) {
            fecha = new Date(element['inci_date_close'])
            hora2 += fecha.getHours();
            minuto2 += fecha.getMinutes();
        }
    }

    hora1 = Math.round(hora1 / cantidadDias);
    hora1 = hora1 > 9 ? hora1 : '0' + hora1;
    minuto1 = Math.round(minuto1 / cantidadDias);
    minuto1 = minuto1 > 9 ? minuto1 : '0' + minuto1;
    hora2 = Math.round(hora2 / cantidadDias);
    hora2 = hora2 > 9 ? hora2 : '0' + hora2;
    minuto2 = Math.round(minuto2 / cantidadDias);
    minuto2 = minuto2 > 9 ? minuto2 : '0' + minuto2;
    promedioAtendidas = Math.round(data.length / cantidadDias);

    return { tipo: tipo, primera: hora1 + ':' + minuto1, ultima: hora2 + ':' + minuto2, dias: cantidadDias, total: data.length, promedio: promedioAtendidas }
}

function formatearFecha(date) {
    var f = new Date(date)

    var dia = "" + f.getDate();
    var mes = "" + (f.getMonth() + 1);
    var min = "" + f.getMinutes();
    var h = "" + f.getHours();
    if (dia.length == 1) {
        dia = "0" + dia;
    }
    if (mes.length == 1) {
        mes = "0" + mes;
    }
    if (min.length == 1) {
        min = "0" + min;
    }
    if (h.length == 1) {
        h = "0" + h;
    }
    return dia + "-" + mes + "-" + f.getFullYear() + "  " + h + ":" + min;
}

incidenceController.find = async (req, res) => {
    var resultado = [];
    const query = "select * from incidence where inci_id=?";
    const queryC = "select comm_name, comm_country, comm_city, comm_address from commerce where comm_uniqe_code = ?";
    try {
        const parameters = [req.headers.id];
        conection.execute(query, parameters, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error1", status: "fail" });
            } else {
                result.rows.sort((a, b) => {
                    if (new Date(a.inci_date_limit_attention) < new Date(b.inci_date_limit_attention)) {
                        return 1;
                    }
                    if (new Date(a.inci_date_limit_attention) > new Date(b.inci_date_limit_attention)) {
                        return -1;
                    }
                    return 0;
                });

                if (result.rows.length == 0) {
                    res.json({ message: "success", status: "ok", incidencias: resultado });
                } else {
                    resultado.push(result.rows[0]);
                    var comercio = resultado[0]['inci_code_commerce'];
                    conection.execute(queryC, [comercio], { prepare: true }, function (err2, result2) {
                        if (err) {
                            logger.error(err.stack);
                            res.json({ message: "query error", status: "fail" });
                        } else {
                            if (result2.rows.length > 0) {
                                resultado.push(result2.rows[0]);
                                res.json({ message: "success", status: "ok", incidencias: resultado });
                            } else {
                                res.json({ message: "success", status: "ok", incidencias: resultado });
                            }
                        }
                    });
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};


incidenceController.getIncidence = async (req, res, next) => {
    const query = "select * from incidence where inci_id = ?";
    var estado = false;
    try {
        conection.execute(query, [req.headers.id], { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "query error" });
                return;
            }

            if (result.rows.length > 0) {
                req.before = JSON.stringify(result.rows);
                estado = true;
            }
            req.estado = estado;
            next();
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

incidenceController.getStatusIncidence = async (req, res, next) => {
    var status = {};
    const query = "select * from incidence_status where views = '" + req.headers.views + "' ALLOW FILTERING";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                console.log("error: " + err);
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD" });
                return;
            } else {
                status = result.rows;
                res.json(status);
            }
        });
    } catch (error) {

        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};


incidenceController.update = async (req, res, next) => {

    try {
        const queryNotification = "INSERT INTO notifications_user_commerce (nouc_id,nouc_date_create,nouc_dest,nouc_msg,nouc_origin,nouc_state,nouc_type, nouc_data) VALUES (now(),?,?,?,?,?,?,?)";
        const request = req.body;
        const queryCommerce = "SELECT comm_name, comm_email FROM commerce WHERE comm_uniqe_code = ?";
        const queryTecnico = "SELECT user_identification, user_id_user, user_photo,user_name from user WHERE user_id_user = ?";
        req.action = "actualizar/incidencia";
        const status = req.estado;
        var auditBefore = [];
        var auditAfter = [];

        if (status === false) {
            res.status(200).send({ message: "error, terminal not exits" });
            return;
        }
        const row = JSON.parse(req.before);
        for (let j = 0; j < Object.keys(request).length; j++) {
            if (Object.keys(row[0])[j] === Object.keys(request)[j]) {
                if (Object.values(row[0])[j] !== Object.values(request)[j]) {
                    auditBefore.push(JSON.parse('{"' + Object.keys(row[0])[j] + '":"' + Object.values(row[0])[j] + '"}'));
                    auditAfter.push(JSON.parse('{"' + Object.keys(request)[j] + '":"' + Object.values(request)[j] + '"}'));
                }
            } else {
                res.status(200).send({ message: "error, wrong json syntax" });
                return;
            }
        }
        req.before = JSON.stringify(auditBefore);
        req.after = JSON.stringify(auditAfter);

        if (!req.headers.id || !req.body) {
            res.status(200).send({ message: "incomplete parameters to make the request", status: "fail" });
            return;
        }

        const inci_id = "" + req.headers.id;


        let incidenciaAnterior = await validarTecnico(inci_id);


        var date = null;
        if (incidenciaAnterior != false && incidenciaAnterior['inci_technical'] != undefined) {
            if (incidenciaAnterior['inci_technical'] != request.inci_technical) {
                date = moment().tz(zone).format(formatBd);
                if (request.inci_status_of_visit == 'EN PROCESO DE ATENCIÓN') {
                    let infoComercio = await queryDinamic(queryCommerce, [request.inci_code_commerce]);
                    let infoTecnico = await queryDinamic(queryTecnico, [request.inci_technical]);

                    if (infoComercio && infoTecnico) {
                        var data = {
                            infoComercio: infoComercio,
                            infoTecnico: infoTecnico,
                            numeroIncidencia: inci_id
                        }
                        email.sendMailAssignTechnical(data);
                        const dateTechnical = momentT().tz(zone).format(format);
                        const msg = "Para la incidencia Nº " + inci_id + " ha sido asignado el técnico:";
                        const origin = "ASIGNACIÓN DE TÉCNICO";
                        const state = "No leída";
                        const type = "TÉCNICO"
                        queryDinamic(queryNotification, [dateTechnical, request.inci_code_commerce, msg, origin, state, type, request.inci_technical]);
                    }
                }
            } else if (incidenciaAnterior['inci_date_assignment'] != "" && incidenciaAnterior['inci_date_assignment'] != null) {
                date = moment(new Date(incidenciaAnterior['inci_date_assignment'])).tz(zone).format(formatBd);
            }
        }

        if (request.inci_date_limit_attention != null) {
            request.inci_date_limit_attention = moment(new Date(request.inci_date_limit_attention)).tz(zone).format(formatBd)
        }

        const parameters = [request.inci_name_commerce, request.inci_priority, request.inci_status_of_visit, request.inci_code_commerce,
        request.inci_contac_person, moment(new Date(request.inci_date_create)).tz(zone).format(formatBd), request.inci_date_limit_attention, request.inci_schedule_of_attention,
        request.inci_technical, request.inci_tecnology, request.inci_term_brand, request.inci_term_model, request.inci_term_number, request.inci_term_serial,
        request.inci_type, date, request.inci_motive, inci_id
        ];

        var query = "update polaris_core.incidence  set inci_name_commerce =?, inci_priority =?,inci_status_of_visit =?," +
            "inci_code_commerce =?, inci_contac_person =?, inci_date_create =?, inci_date_limit_attention =?, inci_schedule_of_attention =?, " +
            "inci_technical =?, inci_tecnology =?, inci_term_brand =?, inci_term_model =?, inci_term_number =?, inci_term_serial =?, inci_type =?, inci_date_assignment =?, inci_motive = ? where inci_id =?";

        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                console.log("error: " + err);
                res.status(200).send({ message: "query error", status: "fail" });
                return;
            } else {
                res.status(200).send({ message: "success", status: "ok" });
                next();
            }
        });

    } catch (error) {

        logger.error(error.stack);
        res.status(200).send({ message: "catch error", status: "fail" });
    }
};



async function validarTecnico(id) {
    var rta = 0;
    const query = "select * from polaris_core.incidence where inci_id= ? allow filtering";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [id], (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve(false);
                } else {
                    if (result.rows.length < 1) {
                        resolve(false);
                    } else {
                        rta = result.rows[0];
                        resolve(rta);
                    }
                }
            });

        });
    } catch (error) {
        logger.error(error.stack);
        resolve(false);
    }
}

async function queryDinamic(query, parameters) {
    var rta = 0;
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, parameters, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve([]);
                } else if (result.rows) {
                    if (result.rows.length < 1) {
                        resolve([]);
                    } else {
                        rta = result.rows[0];
                        resolve(rta);
                    }
                } else {
                    resolve(true);
                }
            });

        });
    } catch (error) {
        logger.error(error.stack);
        resolve([]);
    }
}

async function queryGeneric(query, parameters) {
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, parameters, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve([]);
                } else if (result.rows) {
                    if (result.rows.length < 1) {
                        resolve([]);
                    } else {
                        resolve(result.rows);
                    }
                } else {
                    resolve([]);
                }
            });

        });
    } catch (error) {
        logger.error(error.stack);
        resolve([]);
    }
}

incidenceController.close = async (req, res) => {

    try {
        if (!req.headers.id || !req.body) {
            res.status(200).send({ message: "incomplete parameters to make the request", status: "fail" });
            return;
        }

        const inci_id = "" + req.headers.id;
        const request = req.body;
        const date = moment().tz(zone).format(formatBd);
        const parameters = [request.inci_typing, date, inci_id];

        var query = "update polaris_core.incidence set inci_typing =?, inci_date_close =? where inci_id =?";

        conection.execute(query, parameters, { prepare: true }, (err, result) => {

            if (err) {
                logger.error(err.stack);
                console.log("error: " + err);
                res.status(200).send({ message: "query error", status: "fail" });
                return;
            } else {
                res.status(200).send({ message: "success", status: "ok" });
            }
        });

    } catch (error) {

        logger.error(error.stack);
        res.status(200).send({ message: "catch error", status: "fail" });
    }
};

incidenceController.closeParadaTiempo = async (req, res) => {

    try {
        if (!req.headers.id || !req.body) {
            res.status(200).send({ message: "incomplete parameters to make the request", status: "fail" });
            return;
        }

        const inci_id = "" + req.headers.id;
        const request = req.body;
        const date = moment().tz(zone).format(formatBd);
        const dateReception = moment(new Date(request.inci_date_reception)).tz(zone).format(formatBd);
        const parameters = [request.inci_typing, date, dateReception, request.user, "PARADA DE TIEMPO", request.inci_contact_attended, inci_id];

        var query = "update polaris_core.incidence set inci_typing = ?, inci_date_close =?, inci_date_reception =?, inci_technical  =?, inci_status_of_visit = ? , inci_contact_attended = ? where inci_id =?";

        conection.execute(query, parameters, { prepare: true }, (err, result) => {

            if (err) {
                logger.error(err.stack);
                console.log("error: " + err);
                res.status(200).send({ message: "query error", status: "fail" });
                return;
            } else {
                res.status(200).send({ message: "success", status: "ok" });
            }
        });

    } catch (error) {

        logger.error(error.stack);
        res.status(200).send({ message: "catch error", status: "fail" });
    }
};

async function saveIncidence(request, id, inci_date_limit_attention, inci_date_create, inci_type, inci_status_of_visit, res, next, respuesta = true, enviarEmail = false) {

    try {
        const queryNotification = "INSERT INTO notifications_user_commerce (nouc_id,nouc_date_create,nouc_dest,nouc_msg,nouc_origin,nouc_state,nouc_type, nouc_data) VALUES (now(),?,?,?,?,?,?,?)";

        request.inci_date_finish_post_event == undefined ? request.inci_date_finish_post_event = null : request.inci_date_finish_post_event = moment(new Date(request.inci_date_finish_post_event)).tz(zone).format(formatBd);

        inci_date_create = moment(new Date(inci_date_create)).tz(zone).format(formatBd);
        if (inci_date_limit_attention != null && inci_date_limit_attention != "") {
            inci_date_limit_attention = moment(new Date(inci_date_limit_attention)).tz(zone).format(formatBd);
        } else {
            inci_date_limit_attention = null;
        }

        const parameters = [
            `${id}`, request.inci_name_commerce, request.inci_priority, inci_status_of_visit, request.inci_code_commerce,
            request.inci_contac_person, inci_date_create, inci_date_limit_attention, request.inci_schedule_of_attention,
            request.inci_technical, request.inci_tecnology, request.inci_term_brand, request.inci_term_model, request.inci_term_number,
            request.inci_term_serial, inci_type, request.inci_motive, request.inci_date_finish_post_event
        ];


        var query = "insert into  polaris_core.inci_incidence  (inci_id,inci_name_commerce,inci_priority,inci_status_of_visit, " +
            "inci_code_commerce,inci_contac_person,inci_date_create,inci_date_limit_attention,inci_schedule_of_attention, " +
            "inci_technical, inci_tecnology, inci_term_brand, inci_term_model, inci_term_number, inci_term_serial, inci_type, " +
            "inci_motive, inci_date_finish_post_event) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) if not exists;";

        var query2 = "insert into  polaris_core.incidence  (inci_id,inci_name_commerce,inci_priority,inci_status_of_visit," +
            "inci_code_commerce,inci_contac_person,inci_date_create,inci_date_limit_attention,inci_schedule_of_attention, " +
            "inci_technical, inci_tecnology, inci_term_brand, inci_term_model, inci_term_number, inci_term_serial, inci_type, " +
            "inci_motive, inci_date_finish_post_event) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) if not exists;";

        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {

                logger.error(err.stack);
                // res.status(200).send({ message: "query error", status: "fail" }); 
            }
        });

        conection.execute(query2, parameters, { prepare: true }, (err, result) => {
            if (err) {

                logger.error(err.stack);
                if (!respuesta) return false;
                else return res.status(200).send({ message: "query error", status: "fail" });

            } else {
                if (Object.values(result.rows[0])[0]) {
                    if (!respuesta) {
                        return true;
                    } else {
                        if (enviarEmail) {
                            const queryCommerce = "SELECT comm_name, comm_email FROM commerce WHERE comm_uniqe_code = ?";

                            queryDinamic(queryCommerce, [request.inci_code_commerce]).then((comercio) => {
                                email.sendMailNewIncidence({ inci_ids: [{ id: id }], comercio: { nombre: request.inci_name_commerce, correo: comercio.comm_email } });
                                const date = momentT().tz(zone).format(format);
                                const msg = "Su solicitud ha sido recibida con éxito, el número de incidencia asignado para su atención es:";
                                const origin = request.inci_motive;
                                const state = "No leída";
                                const type = "INCIDENCIA";

                                queryDinamic(queryNotification, [date, request.inci_code_commerce, msg, origin, state, type, JSON.stringify([{ id: id }])]);
                            });
                        }


                        res.status(200).send({ message: "success", applied: true, numero: id + '', status: 'ok' });
                        request.action = "registrar/incidencia";
                        request.before = "{}";
                        request.after = JSON.stringify(request);
                        next();
                    }
                } else {
                    if (!respuesta) {
                        return false;
                    } else return res.status(200).send({ message: "incidence already exists", applied: Object.values(result.rows[0])[0] });
                }
            }
        });

    } catch (error) {

        logger.error(error.stack);
        res.status(200).send({ message: "catch error", status: "fail" });
    }
}

/* 
  Servicio para la aplicación de comercios
*/

async function saveIncidenceAppCommerce(req, res, next, inci_type, inci_status_of_visit, respuesta = true, date_limit_attention = true, email = false) {
    try {
        if (!req.body) {
            if (!respuesta) return false;
            else return res.status(200).send({ message: "incomplete parameters to make the request", status: "fail" });
        }
        const request = req.body;

        var id = await getByAutoId();
        let ok = await updateIdIncidences(id);
        if (!ok) {
            if (!respuesta) return false;
            else return res.status(200).send({ message: "server bd error", status: "fail" });
        }

        var inci_date_limit_attention = '';
        var inci_date_create = new Date();

        if (typeof (date_limit_attention) == 'boolean') {

            var tiempo = await getFindAll('select * from atention_time', []);
            tiempo = tiempo.filter(e => { return e.atti_priority === request.inci_priority });

            var horas = tiempo[0].atti_support_and_removal;

            inci_date_limit_attention = new Date();
            inci_date_limit_attention.setHours(inci_date_create.getHours() + parseInt(horas));

        } else inci_date_limit_attention = date_limit_attention;

        saveIncidence(request, id, `${inci_date_limit_attention}`, `${inci_date_create}`, inci_type, inci_status_of_visit, res, next, respuesta, email);

        InsertarObservacionIncidencia(id, request.inci_observation, `${request.inci_code_commerce} - ${request.inci_name_commerce}`);

        if (!respuesta) return id;

    } catch (error) {

        logger.error(error.stack);
        res.status(200).send({ message: "catch error", status: "fail" });
    }
}

incidenceController.saveIncidenceAppCommerceReporteFallas = (req, res, next) => {
    saveIncidenceAppCommerce(req, res, next, 'SOPORTE', 'EN PROCESO DE ATENCIÓN', true, true, true);
}

incidenceController.saveIncidenceAppCommerceRetiroPos = (req, res, next) => {
    req.body.inci_motive = 'RETIRO DE POS';
    saveIncidenceAppCommerce(req, res, next, 'RETIRO', 'EN PROCESO DE ATENCIÓN', true, true, true);
}

incidenceController.saveIncidenceAppCommercePuntoVenta = async (req, res, next) => {
    req.body.inci_motive = 'INSTALACIÓN DE POS ADICIONAL';
    req.body.inci_tecnology = '';
    req.body.inci_term_brand = '';
    req.body.inci_term_model = '';
    req.body.inci_term_number = '';
    req.body.inci_term_serial = '';

    var tecnologias = JSON.parse(req.body.tecnologys);
    var ids = [];

    for (var i = 0; i < tecnologias.length; i++) {
        const tecnologia = tecnologias[i];
        var type = tecnologia.type_tecnology
        var observacion = `Tecnologia: ${type}. ${req.body.observation}`;

        req.body.inci_observation = observacion;

        for (let j = 0; j < parseInt(tecnologia.count); j++) {
            ids.push({ id: await saveIncidenceAppCommerce(req, res, next, 'INSTALACIÓN', 'VALIDACIÓN REQUERIDA', false, true, false) })
        }
    }

    const queryCommerce = "SELECT comm_name, comm_email FROM commerce WHERE comm_uniqe_code = ?";
    const queryNotification = "INSERT INTO notifications_user_commerce (nouc_id,nouc_date_create,nouc_dest,nouc_msg,nouc_origin,nouc_state,nouc_type, nouc_data) VALUES (now(),?,?,?,?,?,?,?)";

    queryDinamic(queryCommerce, [req.body.inci_code_commerce]).then((comercio) => {
        email.sendMailNewIncidence({ inci_ids: ids, comercio: { nombre: req.body.inci_name_commerce, correo: comercio.comm_email } });
        const date = momentT().tz(zone).format(format);
        const msg = "Su solicitud ha sido recibida con éxito, el número de incidencia asignado para su atención es:";
        const origin = req.body.inci_motive;
        const state = "No leída";
        const type = "INCIDENCIA"

        queryDinamic(queryNotification, [date, req.body.inci_code_commerce, msg, origin, state, type, JSON.stringify(ids)]);
    });

    return res.send({ message: 'success', status: 'success', data: ids });
}

incidenceController.saveIncidenceAppCommercePorEvento = async (req, res, next) => {
    const queryNotification = "INSERT INTO notifications_user_commerce (nouc_id,nouc_date_create,nouc_dest,nouc_msg,nouc_origin,nouc_state,nouc_type, nouc_data) VALUES (now(),?,?,?,?,?,?,?)";
    req.body.inci_motive = 'INSTALACIÓN DE POS POR EVENTO';
    req.body.inci_tecnology = '';
    req.body.inci_term_brand = '';
    req.body.inci_term_model = '';
    req.body.inci_term_number = '';
    req.body.inci_term_serial = '';
    req.body.inci_date_finish_post_event = `${new Date(req.body.event_date_finish)}`;

    var tecnologias = JSON.parse(req.body.tecnologys);
    var ids = [];

    for (var i = 0; i < tecnologias.length; i++) {
        const tecnologia = tecnologias[i];
        var type = tecnologia.type_tecnology;

        var observacion = `Tecnologia: ${type}. 
            Nombre del envento: ${req.body.event_name}. 
            Fecha inicio de evento: ${await funcionesGenerales.formatearFecha(req.body.event_date_start)}. 
            Dirección del evento: ${req.body.event_addres}. 
            Dirección de retiro: ${req.body.inci_addres_retirement}. 
            ${req.body.observation}`;

        req.body.inci_observation = observacion;

        for (let j = 0; j < parseInt(tecnologia.count); j++) {
            ids.push({
                id: await saveIncidenceAppCommerce(req, res, next, 'INSTALACIÓN', 'VALIDACIÓN REQUERIDA', false, `${new Date(req.body.event_date_start)}`, false)
            });
        }
    }

    const queryCommerce = "SELECT comm_name, comm_email FROM commerce WHERE comm_uniqe_code = ?";

    queryDinamic(queryCommerce, [req.body.inci_code_commerce]).then((comercio) => {
        email.sendMailNewIncidence({ inci_ids: ids, comercio: { nombre: req.body.inci_name_commerce, correo: comercio.comm_email } });
        const date = momentT().tz(zone).format(format);
        const msg = "Su solicitud ha sido recibida con éxito, el número de incidencia asignado para su atención es:";
        const origin = req.body.inci_motive;
        const state = "No leída";
        const type = "INCIDENCIA";

        queryDinamic(queryNotification, [date, req.body.inci_code_commerce, msg, origin, state, type, JSON.stringify(ids)]);
    });

    return res.send({ message: 'success', status: 'success', data: ids });
}

/** */

incidenceController.save = async (req, res, next) => {
    try {
        if (!req.body) {
            res.status(200).send({ message: "incomplete parameters to make the request", status: "fail" });
            return;
        }
        const request = req.body;
        var id = await getByAutoId();
        let ok = await updateIdIncidences(id);
        if (!ok) {
            res.status(200).send({ message: "server bd error", status: "fail" });
        }

        saveIncidence(request, id, request.inci_date_limit_attention, request.inci_date_create, request.inci_type, request.inci_status_of_visit, res, next);

    } catch (error) {
        logger.error(error.stack);

        res.status(200).send({ message: "catch error", status: "fail" });
    }
};

//Servicio que devuelve el incrementable de incidencias
async function getByAutoId() {
    var rta = 0;
    const query = "select auid_number from polaris_core.AutoId where auid_name= 'incidences'";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [], (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve(false);
                } else {
                    if (result.rows.length < 1) {
                        resolve(false);
                    } else {
                        rta = result.rows[0];
                        resolve(rta['auid_number'] + 1);
                    }
                }
            });

        });
    } catch (error) {
        logger.error(error.stack);
        resolve(false);
    }
}

//Método que actualiza el Id autoincrementable del  albarán 
async function updateIdIncidences(idActual) {
    const query = "update polaris_core.autoid set auid_number = " + idActual + " where auid_name = 'incidences' ";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [], (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve(false);
                } else {
                    resolve(true);
                }
            });

        });
    } catch (error) {
        logger.error(error.stack);
        resolve(false);
    }
}


incidenceController.count = async (req, res) => {
    try {
        let incidencia = await getByAutoId();
        if (!incidencia) {
            res.status(200).send({ message: "query error", status: "fail" });
        }
        res.status(200).send({ message: "success", incidencia: incidencia });
    } catch (error) {
        logger.error(error.stack);
        res.status(200).send({ message: "catch error", status: "fail" });
    }
};

//exports
incidenceController.findByFilter = async (req, res) => {
    const data = JSON.parse(req.headers.data);
    let fechaCierre = moment(data.fechaCierre).format(formatBd);
    let fechaCierreFilter = moment(new Date(fechaCierre).setHours(23, 59, 59, 0)).format(formatBd);
    const query = "select * from incidence WHERE inci_type = ? AND inci_status_of_visit = ? AND inci_date_close > ? AND inci_date_close < ? ALLOW FILTERING;";
    try {
        const parameters = [data.tipo, data.estado, fechaCierre, fechaCierreFilter];
        conection.execute(query, parameters, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail" });
            }
            res.json({ message: "success", status: "ok", incidencias: result.rows });
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

incidenceController.getCommerceByCode = async (req, res) => {
    const query = "select * from commerce where comm_uniqe_code = ? allow filtering";
    var parameter = [req.headers.code];
    try {
        conection.execute(query, parameter, (err, result) => {
            if (err) {
                logger.error(err.stack);

                res.json({ message: "query error", status: "fail", error: err });
                return;
            } else {
                res.json({ message: "success", status: "ok", data: result.rows });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

incidenceController.getIncidenceByCodeCommerce = async (req, res) => {
    const info = JSON.parse(req.headers.data);
    let fechaInicial = moment(new Date(info.rangoFechas[0])).format(formatBd);
    let fechaFinal = moment(new Date(info.rangoFechas[1]).setHours(23, 59, 59, 0)).format(formatBd);
    const query = "SELECT * from incidence WHERE inci_code_commerce = ? AND " + info.filterBy + " > ? AND " + info.filterBy + " < ?  allow filtering;";
    var parameter = [info.codigoComercio, fechaInicial, fechaFinal];
    try {
        conection.execute(query, parameter, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);

                res.json({ message: "query error", status: "fail", error: err });
                return;
            } else {
                res.json({ message: "success", status: "ok", data: result.rows });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

incidenceController.getIncidenceByRangeDateTechnical = async (req, res) => {
    const info = JSON.parse(req.headers.data);
    let fechaInicial = moment(new Date(info.rangoFechas[0])).format(formatBd);
    let fechaFinal = moment(new Date(info.rangoFechas[1]).setHours(23, 59, 59, 0)).format(formatBd);
    const query = "SELECT * from incidence WHERE " + info.filterBy + " > ? AND " + info.filterBy + " < ?  AND inci_status_of_visit LIKE '%ATENCIÓN%'  allow filtering;";
    var parameter = [fechaInicial, fechaFinal];
    try {
        conection.execute(query, parameter, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);

                res.json({ message: "query error", status: "fail", error: err });
                return;
            } else {
                res.json({ message: "success", status: "ok", data: result.rows });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

incidenceController.getAssingIncidencesByCode = async (req, res) => {
    const query = "SELECT * from incidence WHERE inci_technical = ? allow filtering;";
    var parameter = [req.headers.code];
    try {
        conection.execute(query, parameter, (err, result) => {
            if (err) {
                logger.error(err.stack);

                res.json({ message: "query error", status: "fail", error: err });
                return;
            } else {
                res.json({ message: "success", status: "ok", data: result.rows });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

incidenceController.getTerminalBySerialAndLocation = async (req, res) => {
    const query = "select * from terminal where term_serial = ? and term_localication = ? allow filtering";
    var parameter = [req.headers.serial, req.headers.code];
    try {
        conection.execute(query, parameter, (err, result) => {
            if (err) {
                logger.error(err.stack);

                res.json({ message: "query error", status: "fail", error: err });
                return;
            } else {
                res.json({ message: "success", status: "ok", data: result.rows });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

incidenceController.getTerminalByCaseAndLocation = async (req, res) => {
    var queryExists = "select * from terminal where term_num_terminal = ?  allow filtering"
    var query = "";
    var parameter;
    if (req.headers.serial && req.headers.serial != "") {
        queryExists = "select * from terminal where term_serial = ? allow filtering"
        query = "select * from terminal where term_serial = ? and term_localication = ? allow filtering";
        parameter = [req.headers.serial, req.headers.code];
    } else if (req.headers.number && req.headers.number != "") {
        query = "select * from terminal where term_num_terminal = ? and term_localication = ? allow filtering";
        parameter = [req.headers.number, req.headers.code];
    } else {
        return res.json({ message: "parametros incompletos", status: "fail" });
    }

    let resultado = await queryDinamic(queryExists, [parameter[0]]);
    if (resultado.length == 0) {
        return res.json({ message: "Terminal no éxiste", status: "fail" });
    }
    try {
        conection.execute(query, parameter, (err, result) => {
            if (err) {
                logger.error(err.stack);

                res.json({ message: "query error", status: "fail", error: err });
                return;
            } else {
                res.json({ message: "success", status: "ok", data: result.rows });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

incidenceController.getAllTechnical = async (req, res) => {
    const query = "select * from user where user_position = 'TÉCNICO EN CAMPO' and user_state = 'ACTIVO' allow filtering";
    try {
        conection.execute(query, (err, result) => {
            if (err) {
                logger.error(err.stack);

                res.json({ message: "query error", status: "fail", error: err });
                return;
            } else {
                res.json({ message: "success", status: "ok", data: result.rows });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};
//exports
incidenceController.getElementsInstalled = async (req, res) => {
    const query = "select * from installed_incidence WHERE inin_incidence = ? ALLOW FILTERING;";
    try {
        const parameters = [req.headers.idincidence];
        conection.execute(query, parameters, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail" });
            } else {
                res.json({ message: "success", status: "ok", incidencias: result.rows });
                return;
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

//exports
incidenceController.getElementsFound = async (req, res) => {
    const query = "select * from found_incidence WHERE foin_incidence = ? ALLOW FILTERING;";
    try {
        const parameters = [req.headers.idincidence];
        conection.execute(query, parameters, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail" });
            } else {
                res.json({ message: "success", status: "ok", incidencias: result.rows });
                return;
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

//exports
incidenceController.getElementsRemoved = async (req, res) => {
    const query = "select * from removed_incidence WHERE rein_incidence = ? ALLOW FILTERING;";
    try {
        const parameters = [req.headers.id];
        conection.execute(query, parameters, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail" });
            } else {
                res.json({ message: "success", status: "ok", incidencias: result.rows });
                return;
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

incidenceController.getByIncidence = async (req, res) => {
    if (!req.headers.id) {
        res.send({ message: "incomplete parameters to make the request" });
        return;
    }
    const query = "select * from observation_incidence where obin_incidence = ? and obin_firm = '0' allow filtering";
    try {
        conection.execute(query, [req.headers.id], { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "Query error", status: "fail" });
            } else {
                res.json({ message: "success", status: "ok", data: result.rows });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "Catch error", status: "fail" });
    }
};

incidenceController.getAssingCitysByUser = async (req, res) => {
    if (!req.headers.code) {
        res.send({ message: "incomplete parameters to make the request" });
        return;
    }
    const query = "SELECT * FROM user_city WHERE  usercode = ? allow filtering";
    try {
        var data = [];
        conection.execute(query, [req.headers.code], { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "Query error", status: "fail" });
            } else {
                if (result.rows[0] != undefined) {
                    var ciudades = JSON.parse(result.rows[0].citys);
                    for (let j = 0; j < ciudades.length; j++) {
                        const citys = ciudades[j].ciudades;
                        for (let k = 0; k < citys.length; k++) {
                            const city = citys[k];
                            data.push(city);
                        }
                    }
                    res.json({ message: "success", status: "ok", data: data });
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "Catch error", status: "fail" });
    }
};

incidenceController.getAssingTechnicalByCitys = async (req, res) => {
    var ciudades = req.headers.citys;
    if (!ciudades) {
        res.send({ message: "incomplete parameters to make the request" });
        return;
    }
    const query = "select * from user where user_position = ? AND user_state = ? allow filtering; ";
    try {
        var data = [];
        conection.execute(query, ['TÉCNICO EN CAMPO', 'ACTIVO'], { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "Query error", status: "fail" });
            } else {
                if (result.rows[0] != undefined) {
                    var tecnicos = result.rows;
                    tecnicos.forEach(element => {
                        var ciudad = element.user_location.split("/");
                        if (ciudad.length > 1) {
                            ciudad = ciudad[1];
                            if (ciudades.includes(ciudad)) {
                                data.push(element);
                            }
                        }

                    });
                    res.json({ message: "success", status: "ok", data: data });
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "Catch error", status: "fail" });
    }
};

incidenceController.saveObservation = async (req, res) => {
    const date = moment().tz(zone).format("LLL");
    const logueado = req.user.code;
    const query = "INSERT INTO observation_incidence (id, obin_date, obin_incidence, obin_observation, obin_user, obin_firm) VALUES (now(),?,?,?,?,?)";
    var parameter = [date, req.body.incidencia, req.body.observacion, logueado, '0'];
    try {
        conection.execute(query, parameter, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "Query error", status: "fail" });
            } else {
                res.json({ message: "success", status: "ok" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "Catch error", status: "fail" });
    }
};

incidenceController.updateDataIncidence = async (req, res) => {
    const queryNotification = "INSERT INTO notifications_user_commerce (nouc_id,nouc_date_create,nouc_dest,nouc_msg,nouc_origin,nouc_state,nouc_type, nouc_data) VALUES (now(),?,?,?,?,?,?,?)";
    const queryCommerce = "SELECT comm_name, comm_email FROM commerce WHERE comm_uniqe_code = ?";
    const queryTecnico = "SELECT user_identification, user_id_user, user_photo,user_name from user WHERE user_id_user = ?";
    var estado = req.body.estadoVisita;
    var contacto = req.body.contacto;
    var horario = req.body.horario;
    var tecnico = req.body.tecnico;
    var tecnologia = req.body.tegnologia;
    var marca = req.body.marca;
    var modelo = req.body.modeloTerm;
    var numero = req.body.numeroTerm;
    var serial = req.body.serial;
    var incidencia = req.body.numero;
    var tipificacion = req.body.tipificacion;
    var motivo = req.body.motivo
    var fechaCierre = null;
    if (req.body.fechaClo != null && req.body.fechaClo != "") {
        fechaCierre = moment(new Date(req.body.fechaClo)).tz(zone).format(formatBd);
    }

    let incidenciaAnterior = await validarTecnico(incidencia);

    var date = null;

    if (incidenciaAnterior != false && incidenciaAnterior['inci_technical'] != undefined) {
        if (incidenciaAnterior['inci_technical'] != tecnico) {
            date = moment().tz(zone).format(formatBd);
            if (estado == 'EN PROCESO DE ATENCIÓN') {
                let infoComercio = await queryDinamic(queryCommerce, [incidenciaAnterior['inci_code_commerce']]);
                let infoTecnico = await queryDinamic(queryTecnico, [tecnico]);

                if (infoComercio && infoTecnico) {
                    var data = {
                        infoComercio: infoComercio,
                        infoTecnico: infoTecnico,
                        numeroIncidencia: incidencia
                    }
                    email.sendMailAssignTechnical(data);
                    const dateTechnical = momentT().tz(zone).format(format);
                    const msg = "Para la incidencia Nº " + incidencia + " ha sido asignado el técnico:";
                    const origin = "ASIGNACIÓN DE TÉCNICO";
                    const state = "No leída";
                    const type = "TÉCNICO"
                    queryDinamic(queryNotification, [dateTechnical, incidenciaAnterior['inci_code_commerce'], msg, origin, state, type, tecnico]);
                }
            }
        } else if (incidenciaAnterior['inci_date_assignment'] != "" && incidenciaAnterior['inci_date_assignment'] != null) {
            date = moment(new Date(incidenciaAnterior['inci_date_assignment'])).tz(zone).format(formatBd);
        }
    }

    var query = "update polaris_core.incidence  set inci_status_of_visit =?,  inci_contac_person =?,  inci_schedule_of_attention =?," +
        " inci_technical =?, inci_tecnology =?, inci_term_brand =?, inci_term_model =?, inci_term_number =?, inci_term_serial =?," +
        " inci_date_assignment=?, inci_typing=?, inci_motive = ? , inci_date_close = ? where inci_id =?";
    var parameter = [estado, contacto, horario, tecnico, tecnologia, marca, modelo, numero, serial, date, tipificacion, motivo, fechaCierre, incidencia];
    try {
        conection.execute(query, parameter, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "Query error", status: "fail" });
            } else {
                res.json({ message: "success", status: "ok" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "Catch error", status: "fail" });
    }
};

incidenceController.updateDataCommerce = async (req, res) => {
    var nombre = req.body.nombre;
    var direccion = req.body.direccion;
    var ruta = req.body.ruta;
    var correo = req.body.correo;
    var celular = req.body.celular;
    var telefono = req.body.telefono;
    var horario = req.body.horario;
    var contacto = req.body.contacto;
    var comercio = req.body.id;
    const query = "update commerce set comm_name = ?, comm_address = ?, comm_route_code = ?, comm_email = ?, comm_cellphone = ?, comm_phone = ?," +
        " comm_schedule_attention = ?, comm_contact_person = ? where comm_id = ?";
    var parameter = [nombre, direccion, ruta, correo, celular, telefono, horario, contacto, comercio];
    try {
        conection.execute(query, parameter, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "Query error", status: "fail" });
            } else {
                res.json({ message: "success", status: "ok" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "Catch error", status: "fail" });
    }
};

//exports
incidenceController.reopen = async (req, res) => {
    const query = "select * from inci_incidence where inci_id= ? ALLOW FILTERING;";
    var query2 = "update  polaris_core.incidence   set inci_name_commerce=? ,inci_priority=?, inci_status_of_visit=?,inci_code_commerce=?,inci_contac_person=?,inci_date_create=?,inci_date_limit_attention=?,inci_schedule_of_attention=?, inci_technical=?, inci_tecnology=?, inci_term_brand=?, inci_term_model=?, inci_term_number=?, inci_term_serial=?, inci_type=? where inci_id=? if exists";
    const anulada = "select * from incidence where inci_id=? and inci_status_of_visit='ANULADA' allow filtering;";
    const ecitosa = "select * from incidence where inci_id=? and inci_status_of_visit='ATENCIÓN EXITOSA' allow filtering;";
    const atentcionFallida = "select * from incidence where inci_id=? and inci_status_of_visit='ATENCIÓN FALLIDA' allow filtering;";
    try {
        if (!req.body.id) {
            res.status(200).send({ message: "parameters", status: "fail" });
        }
        const parameters = [req.body.id];
        let incidencia = await getFindAll(query, parameters);
        let exito = await getFindAll(ecitosa, parameters);
        let cancel = await getFindAll(anulada, parameters);
        let falla = await getFindAll(atentcionFallida, parameters);
        if (falla === false || exito === false || cancel === false || incidencia === false) {
            res.json({ message: "query error", status: "fail" });
            return;
        }
        if (incidencia.length < 1) {
            res.json({ message: "success", status: "nok" });
            return;
        }
        if (falla.length < 1 && exito.length < 1 && cancel.length < 1) {
            res.json({ message: "success", status: "abierta" });
            return;
        }
        const queryObserbationsIncidence = "select * from observation_incidence where obin_incidence= ? ALLOW FILTERING;";
        let observaciones = await getFindAll(queryObserbationsIncidence, parameters);
        if (observaciones) {
            var observacionesOrganizadas = observaciones.sort((a, b) => new Date(a["obin_date"]).getTime() - new Date(b["obin_date"]).getTime());
            for (var w = 1; w < observacionesOrganizadas.length; w++) {
                const queryDelete = "delete from observation_incidence where id = ?";
                await getFindAll(queryDelete, [observacionesOrganizadas[w]['id']]);
            }
        }
        incidencia = incidencia[0];
        const parametersx = [incidencia.inci_name_commerce, incidencia.inci_priority, 'EN PROCESO DE ATENCIÓN', incidencia.inci_code_commerce,
        incidencia.inci_contac_person, incidencia.inci_date_create, incidencia.inci_date_limit_attention, incidencia.inci_schedule_of_attention,
        incidencia.inci_technical, incidencia.inci_tecnology, incidencia.inci_term_brand, incidencia.inci_term_model, incidencia.inci_term_number, incidencia.inci_term_serial,
        incidencia.inci_type, req.body.id
        ];
        let ok = await getFindAll(query2, parametersx);
        if (ok === false) {
            res.json({ message: "success", status: "nokj" });
            return;
        } else {
            res.json({ message: "success", status: "ok" });
        }
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

async function getFindAll(query, parameters, select = true) {
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, parameters, { prepare: true }, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    console.log("error en el query ", err);
                    resolve(false);
                } else {
                    if (select) resolve(result.rows);
                    else resolve(result);
                }
            });
        });
    } catch (ex) {
        logger.error(ex.stack);
        resolve(false);

    }
}

//exports
incidenceController.findIncidenceByStatus = async (req, res) => {
    const query = "select * from incidence WHERE inci_type = ? AND  inci_technical = '' AND inci_status_of_visit='EN PROCESO DE ATENCIÓN' ALLOW FILTERING;";
    try {
        const parameters = [req.headers.tipo];
        conection.execute(query, parameters, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail" });
            }
            res.json({ message: "success", status: "ok", incidencias: result.rows });
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

//exports
incidenceController.findIncidenceByStatusTechnical = async (req, res) => {
    const query = "select * from incidence WHERE inci_technical = ? AND inci_status_of_visit='EN PROCESO DE ATENCIÓN' ALLOW FILTERING;";
    try {
        const parameters = [req.headers.id];
        conection.execute(query, parameters, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail" });
            }
            res.json({ message: "success", status: "ok", incidencias: result.rows });
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

incidenceController.assingIncidence = async (req, res) => {

    try {

        if (!req.headers.id || !req.body) {
            res.status(200).send({ message: "incomplete parameters to make the request", status: "fail" });
            return;
        }

        const request = req.body;
        let notInserted = await getRelaizarInsert(request);

        if (notInserted.length > 0) {
            res.status(200).send({ message: "Incidencias actualizadas algunas con errores", response: notInserted });
            return;
        } else {
            res.status(200).send({ message: "Incidencias actualizadas exitosamente", response: notInserted });
            return;
        }

    } catch (error) {
        logger.error(error.stack);
        res.status(200).send({ message: "catch error", status: "fail" });
    }
};

//exports
incidenceController.getQualification = async (req, res) => {
    const query = "SELECT * FROM incidence_qualification WHERE inqu_id = ? ALLOW FILTERING;";
    try {
        const parameters = [req.headers.id];
        conection.execute(query, parameters, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail" });
            } else {
                res.json({ message: "success", status: "ok", calificaciones: result.rows });
                return;
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

async function getRelaizarInsert(request) {
    try {
        return new Promise(async (resolve, reject) => {
            const queryNotification = "INSERT INTO notifications_user_commerce (nouc_id,nouc_date_create,nouc_dest,nouc_msg,nouc_origin,nouc_state,nouc_type, nouc_data) VALUES (now(),?,?,?,?,?,?,?)";
            const queryCommerce = "SELECT comm_name, comm_email FROM commerce WHERE comm_uniqe_code = ?";
            const queryTecnico = "SELECT user_identification, user_id_user, user_photo,user_name from user WHERE user_id_user = ?";
            var notInserted = [];
            let infoTecnico = await queryDinamic(queryTecnico, [request['idtechnical']]);

            //Recorrer las incidencias que se van asignar
            request['data'].forEach(async (element, pos) => {
                let incidenciaAnterior = await validarTecnico(element['noIncidencia']);

                var date = null;
                var tecnico = incidenciaAnterior['inci_technical'];
                if (incidenciaAnterior != false && tecnico != undefined) {
                    if (tecnico != request['idtechnical']) {
                        date = moment().tz(zone).format(formatBd);
                        if (incidenciaAnterior['inci_status_of_visit'] == 'EN PROCESO DE ATENCIÓN') {
                            let infoComercio = await queryDinamic(queryCommerce, [incidenciaAnterior['inci_code_commerce']]);

                            if (infoComercio && infoTecnico) {
                                var data = {
                                    infoComercio: infoComercio,
                                    infoTecnico: infoTecnico,
                                    numeroIncidencia: element['noIncidencia']
                                }
                                email.sendMailAssignTechnical(data);
                                const dateTechnical = momentT().tz(zone).format(format);
                                const msg = "Para la incidencia Nº " + element['noIncidencia'] + " ha sido asignado el técnico:";
                                const origin = "ASIGNACIÓN DE TÉCNICO";
                                const state = "No leída";
                                const type = "TÉCNICO"
                                queryDinamic(queryNotification, [dateTechnical, incidenciaAnterior['inci_code_commerce'], msg, origin, state, type, infoTecnico['user_id_user']]);
                            }
                        }
                    } else if (incidenciaAnterior['inci_date_assignment'] != "" && incidenciaAnterior['inci_date_assignment'] != null) {
                        date = moment(new Date(incidenciaAnterior['inci_date_assignment'])).tz(zone).format(formatBd);
                    }
                }

                const parameters = [request['idtechnical'], date, element['noIncidencia']];

                var query = "update polaris_core.incidence  set inci_technical =?, inci_date_assignment =? where inci_id =?";

                conection.execute(query, parameters, { prepare: true }, (err, result) => {
                    if (err) {
                        logger.error(err.stack);
                        notInserted.push(" Erro la incidencia " + element['noIncidencia'] + " no se pudo registrar.");
                    }
                });
            });
            resolve(notInserted);
        });
    } catch (ex) {
        logger.error(ex.stack);
        return;
    }
}

incidenceController.getStage = async (req, res) => {
    const query = "select * from observation_incidence where obin_incidence = ? allow filtering";
    var parameter = [req.body.incidence];
    try {
        conection.execute(query, parameter, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "Query error", status: "fail" });
            } else {
                result.rows.sort((a, b) => {
                    if (new Date(a.obin_date) < new Date(b.obin_date)) {
                        return 1;
                    }
                    if (new Date(a.obin_date) > new Date(b.obin_date)) {
                        return -1;
                    }
                    return 0;
                });
                res.json({ message: "success", status: "ok", dato: result.rows });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "Catch error", status: "fail" });
    }
};

incidenceController.findAllByTechnical = async (req, res) => {
    const query = "select * from incidence where inci_technical = ? allow filtering";
    try {
        const parameters = [req.headers.technical];
        conection.execute(query, parameters, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail" });
            } else {
                result.rows.sort((a, b) => {
                    if (new Date(a.inci_date_limit_attention) < new Date(b.inci_date_limit_attention)) {
                        return 1;
                    }
                    if (new Date(a.inci_date_limit_attention) > new Date(b.inci_date_limit_attention)) {
                        return -1;
                    }
                    return 0;
                });
                res.json({ message: "success", status: "ok", incidencias: result.rows });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

/**
 * Terminal instalada - Accesorio instalado
 *      Estado => OPERATIVA
 *      Ubicación => comercio
 * Terminal retirada - Accesorio retirado
 *      Estado => PREDIAGNÓSTICO
 *      Ubicación => tecnico
 * Simcard retirada
 *      Estado => DAÑADA
 *      Ubicación => tecnico
 * Terminal encontrada - Accesorio encontrado - Simcard encontrada - la información queda igual
 * 
 * Cuando se envia terminalEncontrada no se envía terminalRetirada ni terminalInstalada
 * Cuando se envia accesorioEncontrada no se envía accesorioRetirada ni accesorioInstalada
 * Cuando se envia simcardEncontrada no se envía simcardRetirada ni simcardInstalada
 */
incidenceController.CloseIncidenceSoporte = async (req, res) => {

    try {
        let query = "update incidence set inci_time_atention=?, inci_date_reception=?, inci_date_close =?," +
            "inci_status_of_visit =?, inci_tecnology = ?,inci_term_brand = ? , inci_term_model = ? ,  inci_term_serial = ?, inci_term_number = ?   where inci_id=?";

        const request = req.body;
        var parameters = [];
        var validate = ["", null, undefined, " ", {},
            []
        ];
        const date1 = new Date();
        var date = date1.format("%Y-%m-%d %H:%M:%S", false);
        const logueado = request.user_code;
        request.inci_date_reception = moment(new Date(request.inci_date_reception)).tz(zone).format(formatBd);
        request.inci_date_close = moment(new Date(request.inci_date_close)).tz(zone).format(formatBd);

        /**
         ***********************************Accesorios*******************
         */
        var accInstalados = [];

        if (request.accesoriosInstalados != undefined && request.accesoriosInstalados != "") {
            await getChangeAccessory(request.accesoriosInstalados, request.comercio, date, logueado, "OPERATIVO");
            const acc = request.accesoriosInstalados;
            accInstalados = [];
            for (let a = 0; a < acc.length; a++) {
                var item = {
                    codigo: acc[a].acce_code,
                    nombre: acc[a].acce_name,
                    cantidad: acc[a].cantidad
                }
                accInstalados.push(item);
            }
        }
        // return res.json({ message: "query error", status: "fail", err: '' });

        var accesoriosRemovidos = [];

        if (request.accesoriosRetirados != undefined && request.accesoriosRetirados != "") {
            await getChangeAccessory(request.accesoriosRetirados, logueado, date, logueado, "PREDIAGNÓSTICO");
            const acc = request.accesoriosRetirados;
            accesoriosRemovidos = [];
            for (let a = 0; a < acc.length; a++) {
                var item2 = {
                    codigo: acc[a].acce_code,
                    nombre: acc[a].acce_name,
                    cantidad: acc[a].cantidad
                }
                accesoriosRemovidos.push(item2);
            }
        }

        /** ***********************************Accesorios******************* */


        /**
         ***********************************Simcard*******************
         */

        var simInstalada = {}

        if (request.simcardInstalada != undefined && request.simcardInstalada.sica_serial != "") {
            await updateSimcard(request.comercio, "OPERATIVO", request.simcardInstalada.sica_serial);
            simInstalada = {
                serial: request.simcardInstalada.sica_serial,
                nombre: request.simcardInstalada.sica_brand
            }
        }

        var simcardRemovida = {}

        if (request.simcardRetirada != undefined && request.simcardInstalada.sica_serial != "") {
            await updateSimcard(logueado, "DAÑADA", request.simcardRetirada.sica_serial);
            simcardRemovida = {
                serial: request.simcardRetirada.sica_serial,
                nombre: request.simcardRetirada.sica_brand
            }
        }

        /** ***********************************Simcard******************* */


        /**
         ***********************************Terminal*******************
         */
        if (request.terminalInstalada != undefined && request.terminalInstalada.term_serial != "") {
            await updateTerminal(request.comercio, "OPERATIVO", request.terminalInstalada.term_serial, request.terminalInstalada.term_num_terminal);
            parameters = [
                request.inci_atention_time,
                request.inci_date_reception,
                request.inci_date_close,
                "ATENCIÓN EXITOSA",
                request.terminalInstalada.term_technology,
                request.terminalInstalada.term_brand,
                request.terminalInstalada.term_model,
                request.terminalInstalada.term_serial,
                request.terminalInstalada.term_num_terminal,
                request.inci_id
            ];

        }

        var terminalRemovida = {};

        if (request.terminalRetirada != undefined && request.terminalRetirada.term_serial != "") {
            await updateTerminal(logueado, "PREDIAGNÓSTICO", request.terminalRetirada.term_serial, request.terminalRetirada.term_num_terminal);
            terminalRemovida = {
                marca: request.terminalRetirada.term_brand,
                modelo: request.terminalRetirada.term_model,
                tecnologia: request.terminalRetirada.term_technology,
                serial: request.terminalRetirada.term_serial,
                numero: request.terminalRetirada.term_num_terminal
            }
        }

        /** ***********************************Terminal******************** */

        const queryInstalled = "insert into installed_incidence (inin_incidence, inin_accessories, inin_sim_card) values (?,?,?)"
        const parametersInstall = [request.inci_id, JSON.stringify(accInstalados), JSON.stringify([simInstalada])];
        registrarelementosInstalados(queryInstalled, parametersInstall);

        var terminalEncontrada = {};

        if (request.terminalEncontrada != undefined && request.terminalEncontrada.term_serial != "") {

            terminalEncontrada = {
                marca: request.terminalEncontrada.term_brand,
                modelo: request.terminalEncontrada.term_model,
                tecnologia: request.terminalEncontrada.term_technology,
                serial: request.terminalEncontrada.term_serial,
                numero: request.terminalEncontrada.term_num_terminal
            }

            parameters = [
                request.inci_atention_time,
                request.inci_date_reception,
                request.inci_date_close,
                "ATENCIÓN EXITOSA",
                terminalEncontrada.tecnologia,
                terminalEncontrada.marca,
                terminalEncontrada.modelo,
                terminalEncontrada.serial,
                request.terminalEncontrada.term_num_terminal,
                request.inci_id
            ];

        }

        var accesoriosEncontrados = {};

        if (request.accesoriosEncontrados != undefined && request.accesoriosEncontrados != "") {
            const acc = request.accesoriosEncontrados;
            accesoriosEncontrados = [];
            for (let a = 0; a < acc.length; a++) {
                var item2 = {
                    codigo: acc[a].acce_code,
                    nombre: acc[a].acce_name,
                    cantidad: acc[a].cantidad
                }
                accesoriosEncontrados.push(item2);
            }
        }

        var simcardEncontrada = {};

        if (request.simcardEncontrada != undefined && request.simcardEncontrada.sica_serial != "") {
            simcardEncontrada = {
                serial: request.simcardEncontrada.sica_serial,
                nombre: request.simcardEncontrada.sica_brand
            }
        }

        var queryRemove = "";

        if (!validate.includes(accesoriosRemovidos) || !validate.includes(simcardRemovida) || !validate.includes(terminalRemovida)) {
            queryRemove = "insert into removed_incidence (rein_incidence, rein_accessories, rein_sim_card, rein_terminal) values (?,?,?,?)"
            const parametersRemove = [request.inci_id, JSON.stringify(accesoriosRemovidos), JSON.stringify([simcardRemovida]), JSON.stringify([terminalRemovida])];
            registrarelementosInstalados(queryRemove, parametersRemove);
        }

        var queryFound = "";

        if (!validate.includes(accesoriosEncontrados) || !validate.includes(simcardEncontrada) || !validate.includes(terminalEncontrada)) {
            queryFound = "insert into found_incidence (foin_incidence, foin_accessories, foin_sim_card, foin_terminal) values (?,?,?,?)"
            const parametersRemove = [request.inci_id, JSON.stringify(accesoriosEncontrados), JSON.stringify([simcardEncontrada]), JSON.stringify([terminalEncontrada])];
            registrarelementosInstalados(queryFound, parametersRemove);
        }

        conection.execute(query, parameters, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail", err: err });
            } else {
                res.json({ message: "success", status: "ok", incidencias: result.rows });
            }
        });

    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

incidenceController.CloseIncidenceInstalation = async (req, res) => {
    try {
        const request = req.body;
        const date1 = new Date();
        var date = date1.format("%Y-%m-%d %H:%M:%S", false);
        const logueado = req.user.code;
        var estado = "OPERATIVO";
        request.inci_date_reception = moment(new Date(request.inci_date_reception)).tz(zone).format(formatBd);
        request.inci_date_close = moment(new Date(request.inci_date_close)).tz(zone).format(formatBd);

        if (request.accesorios != undefined && request.accesorios != "")
            await getChangeAccessory(request.accesorios, request.comercio, date, logueado, estado);

        if (request.terminal != undefined && request.terminal.term_serial != "")
            await updateTerminal(request.comercio, request.terminal.term_status, request.terminal.term_serial, request.terminal.term_num_terminal);

        if (request.simcard != undefined && request.simcard.sica_serial != "")
            await updateSimcard(request.comercio, request.simcard.sica_status, request.simcard.sica_serial);

        var accesoriosInstalados = [];

        if (request.accesorios != undefined && request.accesorios != "") {
            const acc = request.accesorios;

            for (let a = 0; a < acc.length; a++) {
                var item = {
                    codigo: acc[a].acce_code,
                    nombre: acc[a].acce_name,
                    cantidad: acc[a].cantidad
                }

                accesoriosInstalados.push(item);
            }
        }

        var simcardInstalada = {};
        if (request.simcard != undefined && request.simcard.sica_serial != "") {
            var simcardInstalada = {
                serial: request.simcard.sica_serial,
                nombre: request.simcard.sica_brand
            }
        }

        const queryInstalled = "insert into installed_incidence (inin_incidence, inin_accessories, inin_sim_card) values (?,?,?)";
        const parametersTerminal = [request.inci_id, JSON.stringify(accesoriosInstalados), JSON.stringify([simcardInstalada])];

        await registrarelementosInstalados(queryInstalled, parametersTerminal);

        var query = "update incidence set inci_time_atention=?, inci_date_reception=?, inci_date_close =?," +
            "inci_status_of_visit =?, inci_tecnology = ?,inci_term_brand = ? , inci_term_model = ? ,  inci_term_serial = ? , inci_term_number = ?   where inci_id=?";

        const parameters = [
            request.inci_atention_time,
            request.inci_date_reception,
            request.inci_date_close,
            "ATENCIÓN EXITOSA",
            request.terminal.term_technology,
            request.terminal.term_brand,
            request.terminal.term_model,
            request.terminal.term_serial,
            request.terminal.term_num_terminal,
            request.inci_id
        ];

        conection.execute(query, parameters, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail", err: err });
            } else {
                res.json({ message: "success", status: "ok", incidencias: result.rows });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};


incidenceController.CloseIncidenceRetiro = async (req, res) => {
    const request = req.body;

    const date1 = new Date();
    var date = date1.format("%Y-%m-%d %H:%M:%S", false);
    const logueado = request.user_code;
    request.inci_date_reception = moment(new Date(request.inci_date_reception)).tz(zone).format(formatBd);
    request.inci_date_close = moment(new Date(request.inci_date_close)).tz(zone).format(formatBd);

    var accesoriosInstalados = [];

    if (request.accesorios != undefined && request.accesorios != "") {
        await getChangeAccessory(request.accesorios, logueado, date, logueado, 'PREDIAGNÓSTICO');

        const acc = request.accesorios;
        accesoriosInstalados = [];
        for (let a = 0; a < acc.length; a++) {
            var item = {
                codigo: acc[a].acce_code,
                nombre: acc[a].acce_name,
                cantidad: acc[a].cantidad
            }
            accesoriosInstalados.push(item);
        }
    }

    var terminalRemovida = {}
    if (request.terminal != undefined && request.terminal.term_serial != "") {
        await updateTerminal(logueado, 'PREDIAGNÓSTICO', request.terminal.term_serial, request.terminal.term_num_terminal);
        terminalRemovida = {
            marca: request.terminal.term_brand,
            modelo: request.terminal.term_model,
            tecnologia: request.terminal.term_technology,
            serial: request.terminal.term_serial,
            numero: request.terminal.term_num_terminal
        }
    }

    var simcardRemovida = {};

    if (request.simcard != undefined && request.simcard.sica_serial != "") {
        await updateSimcard(logueado, 'DAÑADA', request.simcard.sica_serial);
        simcardRemovida = {
            serial: request.simcard.sica_serial,
            nombre: request.simcard.sica_brand
        };
    }


    const queryInstalled = "insert into removed_incidence (rein_incidence, rein_accessories, rein_sim_card, rein_terminal) values (?,?,?,?)"
    const parametersTerminal = [request.inci_id, JSON.stringify(accesoriosInstalados), JSON.stringify([simcardRemovida]), JSON.stringify([terminalRemovida])];
    registrarelementosInstalados(queryInstalled, parametersTerminal);

    var query = "update incidence set inci_time_atention=?, inci_date_reception=?, inci_date_close =?," +
        "inci_status_of_visit =?, inci_tecnology = ?,inci_term_brand = ? , inci_term_model = ? ,  inci_term_serial = ? , inci_term_number = ?  where inci_id=?";

    const parameters = [
        request.inci_atention_time,
        request.inci_date_reception,
        request.inci_date_close,
        "ATENCIÓN EXITOSA",
        request.terminal.term_technology,
        request.terminal.term_brand,
        request.terminal.term_model,
        request.terminal.term_serial,
        request.terminal.term_num_terminal,
        request.inci_id
    ];

    conection.execute(query, parameters, { prepare: true }, function (err, result) {
        if (err) {
            logger.error(err.stack);
            res.json({ message: "query error", status: "fail", err: err });
        } else {
            res.json({ message: "success", status: "ok", incidencias: result.rows });
        }
    });
};


async function updateTerminal(term_localication, term_status, term_serial, term_num_terminal) {
    var resultState;
    var queryAcSt = "update terminal set term_localication=?, term_status=?, term_num_terminal = ? where term_serial=?";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(queryAcSt, [term_localication, term_status, term_num_terminal, term_serial], { prepare: true }, (err, result) => {
                if (err) {

                    resolve("");
                } else {
                    resultState = result;
                    resolve(resultState);
                }
            });
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: ex, status: 'fail' });
    }
}


async function updateSimcard(sica_location, sica_status, sica_serial) {
    var resultState;
    var queryAcSt = "update sim_card set sica_location=?, sica_status=? where sica_serial=?";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(queryAcSt, [sica_location, sica_status, sica_serial], { prepare: true }, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    return;
                } else {
                    resultState = result;
                    resolve(resultState);
                }
            });
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error 1" });
    }
}


async function getChangeAccessory(accesorio, destino, date, logueado, estado) {
    var resultadoAsigacionAccesorio = [];

    try {
        let resultadoUbicacionAnterior = await getRealizarUbicacionAnterior(accesorio);

        for (i = 0; i < accesorio.length; i++) {

            var querySelect = "select * from accessory where acce_code = ? and acce_status = ? and acce_warehouse = ? allow filtering";
            const parametersSelect = [accesorio[i].acce_code, estado, destino];
            let dataResultados = await getIfExists(querySelect, parametersSelect);

            if (dataResultados.length != 0) {
                var queryUpdate = "update accessory set acce_quantity = ?, acce_warehouse = ?, acce_status = ? where acce_id = ? if exists";
                var quantity = `${parseInt(accesorio[i].cantidad) + parseInt(dataResultados[0]['acce_quantity'])}`;
                var idUpdate = `${dataResultados[0]['acce_id']}`;
                const parametersUpdate = [quantity, destino, estado, idUpdate];

                let resultadoUpdate = await getRealizarActualizacion(queryUpdate, parametersUpdate);

                if (Object.values(resultadoUpdate[0])[0] == true)
                    resultadoAsigacionAccesorio.push(resultadoUpdate);

            } else {
                var queryInsert = "insert into accessory (acce_id, acce_code, acce_terminal_model, acce_name, acce_status, acce_quantity, " +
                    "acce_date_register, acce_warehouse, acce_register_by) values (now(),?,?,?,?,?,?,?,?) if not exists";
                const parameterInsert = [
                    `${accesorio[i].acce_code}`,
                    `${accesorio[i].acce_terminal_model}`,
                    `${accesorio[i].acce_name}`,
                    `${estado}`,
                    `${accesorio[i].cantidad}`,
                    date,
                    `${destino}`,
                    logueado
                ];

                let resultadoInsert = await getRelaizarInsert2(queryInsert, parameterInsert);
                if (resultadoInsert == true) {
                    resultadoAsigacionAccesorio.push(resultadoInsert);
                }
            }
        }
        return new Promise((resolve, reject) => {
            resolve(resultadoAsigacionAccesorio, resultadoUbicacionAnterior);
        });
    } catch (ex) {
        logger.error(ex.stack);
    }

}

async function getRelaizarInsert2(query, parameters) {
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, parameters, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve(false);
                    console.log("error al Consultar ", err);
                } else {
                    resolve(true);
                }
            });
        });
    } catch (ex) {
        logger.error(ex.stack);
        return;
    }
}

async function registrarelementosInstalados(query, parameters) {
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, parameters, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve(false);
                    console.log("error ", err);
                } else {
                    resolve(true);
                }
            });
        });
    } catch (ex) {
        logger.error(ex.stack);
        return;
    }
}

async function getRealizarUbicacionAnterior(accesorio) {
    var queryUpdate = "update accessory set acce_quantity = ? WHERE acce_id = ?";
    var resultadoRealizarUbicacionAnterior = [];
    return new Promise((resolve, reject) => {
        for (var i = 0; i < accesorio.length; i++) {
            var cantidad = "" + ((parseInt(accesorio[i].acce_quantity)) - (parseInt(accesorio[i].cantidad)));
            const parameterUpdate = [cantidad, accesorio[i].acce_id];
            try {
                conection.execute(queryUpdate, parameterUpdate, { prepare: true }, (err, result) => {
                    if (err) {
                        logger.error(err.stack);
                        return;
                    } else {
                        resultadoRealizarUbicacionAnterior.push("Ubicacion anterior Actualizada");
                    }
                });
            } catch (ex) {
                logger.error(ex.stack);
                resolve(false);
            }
        }
        resolve(resultadoRealizarUbicacionAnterior);
    });
}


async function getIfExists(querySelect, parametersSelect) {
    try {
        return new Promise((resolve, reject) => {
            conection.execute(querySelect, parametersSelect, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    console.log("error al Consultar ", err);
                    reject();
                } else {
                    resolve(result.rows);
                }
            });
        });
    } catch (ex) {
        logger.error(ex.stack);
        return;
    }
}

async function getRealizarActualizacion(query, parameters) {
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, parameters, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    console.log("error al Consultar ", err);
                } else {
                    resolve("Actualización Exitosa");
                }
            });
        });
    } catch (ex) {
        logger.error(ex.stack);
        return;
    }
}

incidenceController.guardarCalificacion = async (req, res) => {
    const request = req.body;
    try {
        var query = "update incidence_qualification set inqu_atention_time=?, inqu_contact_person=?, inqu_position=?, inqu_date_register=?, " +
            "inqu_end_date=?, inqu_start_date=?, inqu_register_by=?, inqu_attention_technical=?, inqu_attention_time=?, inqu_presentation_technical=?, inqu_localication=? where inqu_id=?";
        const parameters = [request.inqu_atention_time, request.inqu_contact_person, request.inqu_position, request.inqu_date_register, request.inqu_end_date, request.inqu_start_date, request.inqu_register_by,
        request.inqu_attention_technical, request.inqu_attention_time, request.inqu_presentation_technical, JSON.stringify(request.inqu_localication), request.inqu_id
        ];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail", err: err });
            } else if (result) {
                res.json({ message: "success", status: "ok" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        return false;
    }
}

incidenceController.findAllByTechnicalAndCommerce = async (req, res) => {
    var dataFull = [];
    const query = "select * from incidence where inci_technical = ? allow filtering";
    const queryC = "select * from commerce where comm_uniqe_code = ? allow filtering"
    try {
        const parameters = [req.headers.technical];
        conection.execute(query, parameters, { prepare: true }, async (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail" });
            } else {
                result.rows.sort((a, b) => {
                    if (new Date(a.inci_date_limit_attention) < new Date(b.inci_date_limit_attention)) {
                        return 1;
                    }
                    if (new Date(a.inci_date_limit_attention) > new Date(b.inci_date_limit_attention)) {
                        return -1;
                    }
                    return 0;
                });
                if (result.rows.length > 0) {
                    for (var i = 0; i < result.rows.length; i++) {
                        const parameterC = [result.rows[i]['inci_code_commerce']];
                        let comercio = await traerComercio(queryC, parameterC);
                        dataFull.push({ incidencia: result.rows[i], comercio: comercio });
                        if (i == result.rows.length - 1) {
                            res.json({ message: "success", status: "ok", resultado: dataFull });
                        }
                    }
                } else {
                    res.json({ message: "success", status: "ok", resultado: dataFull });
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
}

async function traerComercio(query, parameters) {
    try {
        return new Promise((resolve, reject) => {
            if (parameters[0] != null && parameters[0] != "" && parameters[0] != undefined) {
                conection.execute(query, parameters, (err, result) => {
                    if (err) {
                        logger.error(err.stack);
                        resolve("[]");
                    } else {
                        if (result.rows.length > 0) {
                            resolve(result.rows[0]);
                        } else {
                            resolve("[]");
                        }
                    }
                });
            } else {
                resolve("[]");
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        return;
    }
}


incidenceController.productividadDia = async (req, res) => {
    const query = "select * from incidence where inci_technical = ? allow filtering";

    let incidenciasAsig = await incidenciasAsignadas(req.body.inci_technical, req.body.date);
    try {

        conection.execute(query, [req.body.inci_technical], { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail" });
            } else {
                const incidenciasExitosas = [];
                const incidenciasFallidas = [];
                for (let w = 0; w < result.rows.length; w++) {

                    if (result.rows[w]['inci_date_close'] != null) {

                        var data = new Date(result.rows[w]['inci_date_close']);
                        var fecha = data.getDate() + "-" + (data.getMonth() + 1) + "-" + data.getFullYear();

                        if (fecha == req.body.date && result.rows[w]['inci_status_of_visit'] == "ATENCIÓN EXITOSA") {
                            incidenciasExitosas.push(result.rows[w]);
                        }

                        if (fecha == req.body.date && result.rows[w]['inci_status_of_visit'] == "ATENCIÓN FALLIDA") {
                            incidenciasFallidas.push(result.rows[w]);
                        }
                    }
                }
                res.json({ message: "success", status: "ok", incidenciasExitosas: incidenciasExitosas.length, incidenciasFallidas: incidenciasFallidas.length, incidenciasAsignadas: incidenciasAsig.length });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};


async function incidenciasAsignadas(tecnico, date) {
    var rta = 0;
    date = moment(new Date(date)).tz(zone).format(formatValidate);
    const query = "select * from incidence where inci_technical= ? allow filtering";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [tecnico], (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve(false);
                } else {
                    if (result.rows.length < 1) {
                        resolve(false);
                    } else {
                        rta = result.rows;

                        var incidenciasAsignada = [];

                        for (let t = 0; t < rta.length; t++) {

                            var fecha = moment(new Date(rta[t]['inci_date_assignment'])).tz(zone).format(formatValidate);

                            if (fecha == date) {
                                incidenciasAsignada.push(rta[t]);
                            }

                        }

                        resolve(incidenciasAsignada);
                    }
                }
            });

        });
    } catch (error) {
        logger.error(error.stack);
        resolve(false);
    }
}


async function productividadDiaSemana(tecnico, date) {

    const query = "select * from incidence where inci_technical = ? allow filtering";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [tecnico], (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve(false);
                } else {
                    if (result.rows.length < 1) {
                        resolve(false);
                    } else {

                        const incidenciasExitosas = [];
                        const incidenciasFallidas = [];
                        for (let w = 0; w < result.rows.length; w++) {

                            if (result.rows[w]['inci_date_close'] != null) {

                                var data = new Date(result.rows[w]['inci_date_close']);
                                var fecha = data.getFullYear() + "-" + (data.getMonth() + 1) + "-" + data.getDate();

                                if (fecha == date && result.rows[w]['inci_status_of_visit'] == "ATENCIÓN EXITOSA") {
                                    incidenciasExitosas.push(result.rows[w]);
                                }

                                if (fecha == date && result.rows[w]['inci_status_of_visit'] == "ATENCIÓN FALLIDA") {
                                    incidenciasFallidas.push(result.rows[w]);
                                }
                            }
                        }

                        let respuesta = {
                            dia: date,
                            incidenciasExitosas: incidenciasExitosas.length,
                            incidenciasFallidas: incidenciasFallidas.length
                        }

                        resolve(respuesta);
                    }
                }
            });

        });
    } catch (error) {
        logger.error(error.stack);
        resolve(false);
    }
}


incidenceController.productividadSemana = async (req, res) => {

    try {

        var dateIni = req.body.dateIni.split("-");
        var dateFin = req.body.dateFin.split("-");

        dateIni = new Date(dateIni[2] + "-" + dateIni[1] + "-" + dateIni[0]);
        dateFin = new Date(dateFin[2] + "-" + dateFin[1] + "-" + dateFin[0]);
        var resultado = [];


        var fechaIni = dateIni.getFullYear() + "-" + (dateIni.getMonth() + 1) + "-" + dateIni.getDate();

        let item = await productividadDiaSemana(req.body.inci_technical, fechaIni);
        resultado.push(item);

        for (let w = 1; w < 7; w++) {

            if (dateIni > dateFin) {
                break;
            } else {
                dateIni.setDate(dateIni.getDate() + 1);
                var fechaIni = dateIni.getFullYear() + "-" + (dateIni.getMonth() + 1) + "-" + dateIni.getDate();

                let item2 = await productividadDiaSemana(req.body.inci_technical, fechaIni);
                const respuesta = item2;
                resultado.push(respuesta);
            }
        }

        res.json({ message: "success", status: "ok", data: resultado });

    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};


incidenceController.productividadMes = async (req, res) => {
    try {
        var dateInicial = req.body.dateIni.split("-");
        var dateFin = req.body.dateFin.split("-");

        var dateIni = new Date(dateInicial[2] + "-" + dateInicial[1] + "-" + dateInicial[0]);
        dateFin = new Date(dateFin[2] + "-" + dateFin[1] + "-" + dateFin[0]);
        var dateIni2 = new Date(dateInicial[2] + "-" + dateInicial[1] + "-" + dateInicial[0]);
        var resultado = [];

        dateIni2.setDate(dateIni2.getDate() + 6)

        let item = await productividadSemanaMes(req.body.inci_technical, dateIni, dateIni2, dateFin);
        resultado.push(item);

        for (let w = 1; w < 7; w++) {

            if (dateIni2 > dateFin) {
                break;
            } else {
                dateIni = new Date('' + dateIni2);
                dateIni.setDate(dateIni.getDate() + 1);
                dateIni2.setDate(dateIni2.getDate() + 7);
                let item2 = await productividadSemanaMes(req.body.inci_technical, dateIni, dateIni2, dateFin);
                const respuesta = item2;
                resultado.push(respuesta);
            }
        }

        res.json({ message: "success", status: "ok", data: resultado });

    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

async function productividadSemanaMes(tecnico, dateIni, dateFin, dateFinMes) {

    const query = "select * from incidence where inci_technical = ? allow filtering";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [tecnico], { prepare: true }, function (err, result) {
                if (err) {
                    logger.error(err.stack);
                    resolve(false);
                } else {
                    if (result.rows.length < 1) {
                        resolve(false);
                    } else {

                        const incidenciasExitosas = [];
                        const incidenciasFallidas = [];


                        if (dateFin > dateFinMes) {
                            dateFin = new Date("" + dateFinMes);
                        }

                        var data2 = dateIni;
                        var data3 = dateFin;

                        data3.setDate(data3.getDate() + 1);

                        for (let w = 0; w < result.rows.length; w++) {

                            if (result.rows[w]['inci_date_close'] != null) {

                                var data = new Date(result.rows[w]['inci_date_close']);


                                if (data >= data2 && data3 > data) {

                                    if (result.rows[w]['inci_status_of_visit'] == "ATENCIÓN EXITOSA") {
                                        incidenciasExitosas.push(result.rows[w]);
                                    }

                                    if (result.rows[w]['inci_status_of_visit'] == "ATENCIÓN FALLIDA") {
                                        incidenciasFallidas.push(result.rows[w]);
                                    }

                                }
                            }
                        }

                        dateFin.setDate(dateFin.getDate() - 1);

                        var fIni = dateIni.getFullYear() + "-" + (dateIni.getMonth() + 1) + "-" + dateIni.getDate();
                        var fFin = dateFin.getFullYear() + "-" + (dateFin.getMonth() + 1) + "-" + dateFin.getDate();

                        let respuesta = {
                            semana: fIni + " - " + fFin,
                            incidenciasExitosas: incidenciasExitosas.length,
                            incidenciasFallidas: incidenciasFallidas.length
                        }
                        resolve(respuesta);
                    }
                }
            });

        });
    } catch (error) {
        logger.error(error.stack);
        resolve(false);
    }
}

incidenceController.saveSeveral = async (req, res) => {
    var incidencias = req.body;
    var registrosErroneos = [];
    var erroresRegistro = "";
    var nombreComercio = '';
    var prioridad = '';
    var modeloTerm = '';
    var numeroTerm = '';
    var marcaTerm = '';
    var tecnologiaTerm = '';
    const fecha = new Date();
    var registrosExitosos = 0;
    var idsInsertados = [];
    let tiemposAtencion = await queryGeneric("select * from atention_time", []);

    try {
        if (incidencias != null && incidencias != undefined) {
            for (let w = 0; w < incidencias.length; w++) {

                var nombreComercio = '';
                var prioridad = '';
                var modeloTerm = '';
                var numeroTerm = '';
                var marcaTerm = '';
                var tecnologiaTerm = '';
                erroresRegistro = '';

                if (incidencias[w]['Código_único'] != "") {
                    let comercio = await BuscarComercio(incidencias[w]['Código_único']);

                    if (comercio.length > 0) {
                        nombreComercio = comercio[0]['comm_name']
                        prioridad = comercio[0]['comm_priority']
                    } else {
                        erroresRegistro += ", Comercio no registrado";
                    }
                } else {
                    erroresRegistro += ", Comercio vacío";
                }

                if (incidencias[w]['Serial'] != "" && incidencias[w]['Serial'] != "UNDEFINED") {
                    let terminal = await BuscarSerial(incidencias[w]['Serial']);

                    if (terminal.length > 0) {
                        if (terminal[0]['term_localication'] == incidencias[w]['Código_único']) {
                            modeloTerm = terminal[0]['term_model'];
                            numeroTerm = terminal[0]['term_num_terminal'];
                            marcaTerm = terminal[0]['term_brand'];
                            tecnologiaTerm = await convertirTecnologiaLetras(terminal[0]['term_technology']);
                            term = true;
                        } else {
                            erroresRegistro += ", Terminal no existe en el comercio";
                            term = false;
                        }

                    } else {
                        erroresRegistro += ", Terminal no registrada";
                        term = false;
                    }
                } else {
                    incidencias[w]['Serial'] = '';
                    term = true;
                }

                incidencias[w]['Ubicación'] = incidencias[w]['Ubicación'].replace(" ", '');

                if (incidencias[w]['Ubicación'] != "" && incidencias[w]['Ubicación'] != "UNDEFINED") {
                    let tecnico = await BuscarTecnico(incidencias[w]['Ubicación']);
                    if (tecnico.length > 0) {
                        if (tecnico[0]['user_position'] == "TÉCNICO EN CAMPO") {
                            tec = true;
                        } else {
                            erroresRegistro += ", no es un técnico en campo";
                            tec = false;
                        }
                    } else {
                        erroresRegistro += ", Técnico no registrado";
                        tec = false;
                    }
                } else {
                    incidencias[w]['Ubicación'] = "";
                    tec = true;
                }

                if (prioridad != "") {
                    tiempoAtencion = tiemposAtencion.find(c => c.atti_priority == prioridad);

                    var tiemLimiteAtencion = new Date();
                    if (incidencias[w]['Tipo_incidencia'] == "RETIRO" || "SOPORTE") {
                        tiemLimiteAtencion.setHours(tiemLimiteAtencion.getHours() + parseInt(tiempoAtencion['atti_support_and_removal']));
                    }

                    if (incidencias[w]['Tipo_incidencia'] == "INSTALACIÓN") {
                        tiemLimiteAtencion.setHours(tiemLimiteAtencion.getHours() + parseInt(tiempoAtencion['atti_installation']));
                    }
                }

                if (erroresRegistro == "" && term == true && incidencias[w]['Tipo_incidencia'] != "" && tec == true) {

                    var id = await getByAutoId();
                    var item = {
                        id: "Registro de la fila: " + (w + 2) + ", incidencia: " + id
                    }
                    idsInsertados.push(item);


                    const parameters = [`${id}`, nombreComercio, prioridad, incidencias[w]['Estado_de_visita'], incidencias[w]['Código_único'],
                    incidencias[w]['Persona_de_contacto'], moment().tz(zone).format(formatBd), moment(tiemLimiteAtencion).tz(zone).format(formatBd), incidencias[w]['Horario_de_atención'],
                    incidencias[w]['Ubicación'], tecnologiaTerm, marcaTerm, modeloTerm, numeroTerm, incidencias[w]['Serial'],
                    incidencias[w]['Tipo_incidencia'], incidencias[w]['Motivo']
                    ];


                    var query = "insert into  polaris_core.inci_incidence  (inci_id,inci_name_commerce,inci_priority,inci_status_of_visit,inci_code_commerce,inci_contac_person,inci_date_create,inci_date_limit_attention,inci_schedule_of_attention, inci_technical, inci_tecnology, inci_term_brand, inci_term_model, inci_term_number, inci_term_serial, inci_type, inci_motive) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) if not exists;";
                    var query2 = "insert into  polaris_core.incidence  (inci_id,inci_name_commerce,inci_priority,inci_status_of_visit,inci_code_commerce,inci_contac_person,inci_date_create,inci_date_limit_attention,inci_schedule_of_attention, inci_technical, inci_tecnology, inci_term_brand, inci_term_model, inci_term_number, inci_term_serial, inci_type, inci_motive) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) if not exists;";

                    InsertarIncidencia(parameters, query)
                    let registro = await InsertarIncidencia(parameters, query2);

                    if (registro == true) {
                        InsertarObservacionIncidencia(id, incidencias[w]['Observación'], req.user.code);
                        await updateIdIncidences(id);
                        registrosExitosos++;
                    } else {
                        erroresRegistro += ", No se pudo registrar";
                    }
                }

                if (erroresRegistro != "") {
                    registrosErroneos.push("Error en la fila: " + (w + 2) + erroresRegistro);
                }

            }

            res.status(200).send({ registrados: registrosExitosos, noRegistrados: registrosErroneos, registradosId: idsInsertados });

        }
    } catch (error) {
        logger.error(error.stack);
    }
}

incidenceController.findProductivityTechnicalByCity = async (req, res) => {
    const query = "select * from technical_productivity WHERE tepro_city = ? ALLOW FILTERING";
    let geoDist = await getFindAll('select * from company_flag where cofl_id = ?', ['1'])
    let geoDist2 = geoDist[0].cofl_geodist.split('-')[1]

    try {
        conection.execute(query, [(req.body.city).toUpperCase()], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: " query error", status: "error" });
                return;
            } else if (result.rows.length > 0) {
                return res.json({ data: result.rows[0], status: "ok" });
            } else {
                return res.status(200).send({ message: `${geoDist2.toLowerCase()} inválida`, status: "error" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error", status: "error" });
    }
};

incidenceController.getIncidencesCitys = async (req, res) => {
    var citys = req.body.citys;
    var status = req.body.status;

    if (citys == undefined) return res.json({ message: "información inválida", status: "error" });

    let commerces = await getFindAll("select comm_uniqe_code, comm_city from commerce", []);

    if (commerces.length < 1) return res.json([]);

    let incidencias = await getFindAll("select * from incidence", []);
    let codesCommerces = [];
    let data = [];

    for (let i = 0; i < citys.length; i++) {
        const city = citys[i];

        commerces.forEach(c => {
            if (c.comm_city == city) codesCommerces.push(c.comm_uniqe_code);
        });

        let incidenciasCity = incidencias.filter(c => {
            if (status) return codesCommerces.includes(c.inci_code_commerce) && c.inci_status_of_visit == status;
            else return codesCommerces.includes(c.inci_code_commerce);
        });

        data.push({
            city: city,
            incidences: incidenciasCity
        });

    }

    return res.json(data);
}

incidenceController.assignTat = async (req, res) => {
    try {
        var incidencias = req.body.incidencias;
        var tecnico = req.body.tecnico;
        for (let i = 0; i < incidencias.length; i++) {
            getFindAll(`update incidence set inci_technical = ? where inci_id = ?`, [tecnico, incidencias[i].inci_id], false)
                .then(async () => {
                    const queryCommerce = "SELECT comm_name, comm_email FROM commerce WHERE comm_uniqe_code = ?";
                    const queryTecnico = "SELECT user_identification, user_id_user, user_photo,user_name from user WHERE user_id_user = ?";

                    let infoComercio = await queryDinamic(queryCommerce, [incidencias[i].commerce]);
                    let infoTecnico = await queryDinamic(queryTecnico, [tecnico]);

                    if (infoComercio && infoTecnico) {
                        email.sendMailAssignTechnical({
                            infoComercio: infoComercio,
                            infoTecnico: infoTecnico,
                            numeroIncidencia: incidencias[i].inci_id
                        });
                    }
                })
        }
        res.json({ message: 'success', status: 'success' });
    } catch (error) {
        logger.error(error.stack);
    }

}

incidenceController.getIncidencesByDateUser = async (req, res) => {
    let data = JSON.parse(req.headers.info);
    const fecha = moment(new Date(data['date'])).tz(zone).format("DD-MM-YYYY");
    const queryConsulta = "SELECT * FROM incidence WHERE inci_technical  = ? AND inci_status_of_visit LIKE '%ATENCIÓN%' ALLOW FILTERING;";
    try {
        conection.execute(queryConsulta, [data['code']], { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "query error", status: "fail", incidencias: [] });
            } else {
                let incidencias = result.rows;
                incidencias = incidencias.filter(c => moment(new Date(c['inci_date_close'])).tz(zone).format("DD-MM-YYYY") == fecha);
                res.json({ message: "success", status: "ok", incidencias: incidencias });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

async function getCommerceCity(city) {
    try {
        return new Promise(async (resolve, reject) => {
            var data = [];

            var query = "select comm_uniqe_code from polaris_core.commerce where comm_city ='" + city + "'  allow filtering";
            var result = await getFindAll(query, []);
            for (let j = 0; j < result.length; j++) {
                data.push(result[j].comm_uniqe_code);
            }

            resolve(data);
        });
    } catch (ex) {
        logger.error(ex.stack);
        resolve(false);

    }
}

async function BuscarComercio(comercio) {

    const query = "select * from commerce where comm_uniqe_code =? allow filtering";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [comercio], { prepare: true }, function (err, result) {
                if (err) {
                    logger.error(err.stack);
                    resolve([]);
                } else {
                    if (result.rows.length < 1) {
                        resolve([]);
                    } else {
                        resolve(result.rows);
                    }
                }
            });

        });
    } catch (error) {
        logger.error(error.stack);
        resolve([]);
    }
}

async function BuscarSerial(serial) {

    const query = "select * from terminal where term_serial = ?";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [serial], { prepare: true }, function (err, result) {
                if (err) {
                    logger.error(err.stack);
                    resolve([]);
                } else {
                    if (result.rows.length < 1) {
                        resolve([]);
                    } else {
                        resolve(result.rows);
                    }
                }
            });

        });
    } catch (error) {
        logger.error(error.stack);
        resolve([]);
    }
}

async function BuscarTecnico(codigo) {

    const query = "select * from user where user_id_user = ?";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [codigo], { prepare: true }, function (err, result) {
                if (err) {
                    logger.error(err.stack);
                    resolve([]);
                } else {
                    if (result.rows.length < 1) {
                        resolve([]);
                    } else {
                        resolve(result.rows);
                    }
                }
            });

        });
    } catch (error) {
        logger.error(error.stack);
        resolve([]);
    }
}

async function BuscarMotivo(codigo) {

    const query = "SELECT *  from typification_incidence WHERE tyin_name=? AND tyin_type = 'MOTIVO' ALLOW FILTERING";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, [codigo], { prepare: true }, function (err, result) {
                if (err) {
                    logger.error(err.stack);
                    resolve([]);
                } else {
                    if (result.rows.length < 1) {
                        resolve([]);
                    } else {
                        resolve(result.rows);
                    }
                }
            });

        });
    } catch (error) {
        logger.error(error.stack);
        resolve([]);
    }
}

async function InsertarIncidencia(incidencia, query) {


    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, incidencia, { prepare: true }, function (err, result) {
                if (err) {
                    logger.error(err.stack);
                    resolve(false);
                } else {
                    resolve(true);
                }
            });

        });
    } catch (error) {
        logger.error(error.stack);
        resolve(true);
    }
}

async function convertirTecnologiaLetras(tecnologia) {
    let technologies = await getFindAll("select * from technology", [], true);
    var tecnologias = tecnologia.split(",");
    var dato = '';
    try {
        return new Promise((resolve, reject) => {
            for (let i = 0; i < technologies.length; i++) {
                let element = technologies[i];
                if (tecnologias.includes(element.tech_code)) {
                    dato += element.tech_description + ",";
                }
            }
            resolve(dato.substr(0, dato.length - 1));
        });
    } catch (error) {
        logger.error(error.stack);
        resolve([]);
    }

}


async function InsertarObservacionIncidencia(incidence, observacion, user) {
    try {
        return new Promise((resolve, reject) => {
            var query = "INSERT INTO observation_incidence (id,obin_date,obin_firm,obin_incidence,obin_observation,obin_photo,obin_user) VALUES (now(), ?,?,?,?,?,?)"
            const fecha = moment().format("LLL");
            var parameter = [fecha, '0', '' + incidence, observacion, '', user]
            conection.execute(query, parameter, { prepare: true }, function (err, result) {
                if (err) {
                    logger.error(err.stack);
                    resolve(false);
                } else {
                    resolve(true);
                }
            });

        });
    } catch (error) {
        logger.error(error.stack);
        resolve(true);
    }
}

//exports
module.exports = incidenceController;