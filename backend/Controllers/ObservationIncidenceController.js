"use strict"
const ObservationIncidenceController = {};
const conection = require("../database.js");
const c = require('colors');
const logger = require('../bin/logger');


ObservationIncidenceController.create = async (req, res) => {  
    try {
        if (!req.body) {
            res.send({ message: "incomplete parameters to make the request" });
            return;
        } 
        const request = req.body                
        const moment = require("moment");
        const date = moment().format("LLL");
        const user = req.user.code;

        const parameters =  [request.obin_incidence, request.obin_observation, user, date, '0'];
        var query = "insert into polaris_core.observation_incidence (id, obin_incidence, obin_observation, obin_user, obin_date, obin_firm) values (now(),?,?,?,?,?)";
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: "error in BD" });
                return;
            } else {                 
                res.status(200).send({ message: "success", applied: "true" });                
            }
        });

    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ message: "Not Acceptable" });
    }
};


ObservationIncidenceController.getByIncidence = async (req, res) => {
    
    if ( !req.headers.id) {
        res.send({ message: "incomplete parameters to make the request" });
        return;
    } 
    const query = "select * from polaris_core.observation_incidence where obin_incidence = ? and obin_firm = ? allow filtering";
    try {
        conection.execute(query, [req.headers.id, '0'], { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.send({ message: "query error" });
                return;
            }
            if (result.rows.length > 0) {                
                res.json(result.rows);                
            }  
              else {
                res.json({});
              }          
            
        });
    } catch (ex) {
        logger.error(ex.stack); 
        res.send({ error: "catch error" });
    }
};


//exports
module.exports = ObservationIncidenceController