const wareHouseController = {};
const conection = require("../database.js");
const c = require('colors');
const logger = require('../bin/logger');
const moment = require("moment");
const zone = `${process.env.TIME_ZONE}`;


//methods
wareHouseController.save = async (req, res, next) => {
	const request = req.body;
	const date = moment().tz(zone).format("LLL");
	const logueado = req.user.id;
	var query = "insert into warehouse (ware_id, ware_id_warehouse, ware_name, ware_country, ware_city, ware_responsable, ware_status, ware_email, ware_position, ware_phone,ware_type, ware_date_register, ware_register_by) values (now(),?,?,?,?,?,?,?,?,?,?,?,?) if not exists";
	try {
		var type = request.ware_type.replace(/ /g, "").substr(0, 1);
		var id = type + request.ware_city.replace(/ /g, "").substr(0, 3) + "" + req.cantidad;
		const parameters = [id.toUpperCase(), request.ware_name, request.ware_country, request.ware_city, request.ware_responsable, request.ware_status, request.ware_email, request.ware_position, request.ware_phone, request.ware_type, date, logueado];

		conection.execute(query, parameters, { prepare: true }, (err, result) => {
			if (err) {
				logger.error(err.stack);
				res.status(404).send({ message: "error" });
				return;
			} else {
				if (Object.values(result.rows[0])[0]) {
					res.status(200).send({ message: "success", applied: Object.values(result.rows[0])[0] });
					req.action = "registrar/bodega";
					req.before = "{}";
					req.after = JSON.stringify(request);
					next();
				} else {
					res.status(200).send({ message: "warehouse already exists", applied: Object.values(result.rows[0])[0] });
					return;
				}
			}
		});
	} catch (ex) {
		logger.error(ex.stack);
		res.status(404).send({ error: "catch error" });
	}
};

wareHouseController.find = async (req, res) => {
	const query = "select * from warehouse where ware_id_warehouse= ? and ware_status= ? ALLOW FILTERING";
	try {
		const parameters = [req.headers.id, 'ACTIVO'];
		conection.execute(query, parameters, { prepare: true }, (err, result) => {
			if (err) {
				logger.error(err.stack);
				res.status(404).send({ message: "query error " });
				return;
			} else {
				res.json(result.rows);
			}
		});
	} catch (ex) {
		logger.error(ex.stack);
		res.status(404).send({ error: "catch error" });
	}
};

wareHouseController.findAll = async (req, res) => {
	const query = "select * from warehouse";
	try {
		conection.execute(query, [], (err, result) => {
			if (err) {
				logger.error(err.stack);
				res.status(404).send({ message: " query error" });
				return;
			} else {
				res.json(result.rows);
			}
		});
	} catch (ex) {
		logger.error(ex.stack);
		res.status(404).send({ error: "catch error" });
	}

};

wareHouseController.update = async (req, res, next) => {
	const request = req.body;
	req.action = "actualizar/bodega";
	const status = req.estado;
	var auditBefore = [];
	var auditAfter = [];
	try {
		if (status === false) {
			res.status(404).send({ message: "error, warehouse not exits" });
			return;
		};
		const row = JSON.parse(req.before);
		for (let j = 1; j < Object.keys(request).length; j++) {
			if (Object.keys(row[0])[j] === Object.keys(request)[j]) {
				if (Object.values(row[0])[j] !== Object.values(request)[j]) {
					auditBefore.push(JSON.parse('{"' + Object.keys(row[0])[j] + '":"' + Object.values(row[0])[j] + '"}'));
					auditAfter.push(JSON.parse('{"' + Object.keys(request)[j] + '":"' + Object.values(request)[j] + '"}'));
				}
			} else {
				res.status(400).send({ message: "error, wrong json syntax" });
				return;
			}
		}
		req.before = JSON.stringify(auditBefore);
		req.after = JSON.stringify(auditAfter);
		var query = "update warehouse set ware_name = ?, ware_country = ?, ware_city = ?, ware_responsable= ?, ware_status=?, ware_email=?, ware_position=?, ware_phone=?, ware_type=? where ware_id =?";
		const parameters = [request.ware_name, request.ware_country, request.ware_city, request.ware_responsable, request.ware_status, request.ware_email, request.ware_position, request.ware_phone, request.ware_type, req.headers.id];
		conection.execute(query, parameters, (err, result) => {
			if (err) {
				logger.error(err.stack);
				res.status(404).send({ message: " query error" });
				return;
			} else if (result) {
				res.json({ message: "success" });
				next();
			}
		});
	} catch (ex) {
		logger.error(ex.stack);
		res.status(404).send({ error: "catch error" });
	}
};

wareHouseController.delete = async (req, res, next) => {
	const status = req.estado;
	try {
		if (status === false) {
			res.status(404).send({ message: "error, warehouse not exits" });
			return;
		};
		var query = "delete from warehouse  where ware_id =?";
		const parameters = [req.headers.id];
		conection.execute(query, parameters, { prepare: true }, (err, result) => {
			if (err) {
				logger.error(err.stack);
				res.status(404).send({ message: " query error" });
				return;
			} else {
				req.action = "eliminar/bodega";
				req.after = "{}";
				res.json({ message: "success" });
				next();
			}
		});
	} catch (ex) {
		logger.error(ex.stack);
		res.status(404).send({ error: "catch error" });
	}
};

wareHouseController.getWarehouse = async (req, res, next) => {
	const query = "select * from warehouse where ware_id = ?";
	var estado = false;
	try {
		await conection.execute(query, [req.headers.id], { prepare: true }, function (err, result) {
			if (err) {
				logger.error(err.stack);
				res.status(404).send({ message: " query error" });
				return;
			}
			if (result.rows.length > 0) {
				req.before = JSON.stringify(result.rows);
				estado = true;
			}
			req.estado = estado;
			next();
		});
	} catch (ex) {
		logger.error(ex.stack);
		res.status(404).send({ error: "catch error" });
	}
};

wareHouseController.getMaxWarehouse = async (req, res, next) => {
	const query = "select * from all_exits where alle_id = 0";
	var estado = false;
	var cantidad = 0;
	try {
		await conection.execute(query, { prepare: true }, function (err, result) {
			if (err) {
				logger.error(err.stack);
				res.status(404).send({ message: " query error" });
				return;
			}
			if (result.rows.length > 0) {
				req.cantidad = result.rows[0].alle_warehouses;
				next();
				return;
			}
		});
	} catch (ex) {
		logger.error(ex.stack);
		res.status(404).send({ error: "catch error" });
		return;
	}
};

wareHouseController.updateMaxWarehouse = async (req, res, next) => {
	const query = "update all_exits  set alle_warehouses = ? where alle_id = 0";
	var estado = false;
	var cantidadN = req.cantidad + 1;
	try {
		await conection.execute(query, [cantidadN], { prepare: true }, function (err, result) {
			if (err) {
				logger.error(err.stack);
				res.status(404).send({ message: " query error" });
				return;
			}
			next();
		});
	} catch (ex) {
		logger.error(ex.stack);
		res.status(404).send({ error: "catch error" });
		return;
	}
};

wareHouseController.updateWarehouseUser = async (req, res) => {
	const query = "update warehouse_user  set warehouses = ? where user =?";

	try {
		const parameters = [req.body.warehouses, req.body.user];
		await conection.execute(query, parameters, function (err, result) {
			if (err) {
				logger.error(err.stack);
				res.status(200).send({ message: " query error" });
				return;
			}
			if (result) {
				res.status(200).send({ message: "success" });
				return;
			}
		});
	} catch (ex) {
		logger.error(ex.stack);
		res.status(404).send({ error: "catch error" });
		return;
	}
};


wareHouseController.findWarehouseUser = async (req, res) => {
	const query = "select * from warehouse_user where user =?";
	try {
		const parameters = [req.headers.id];
		conection.execute(query, parameters, { prepare: true }, (err, result) => {
			if (err) {
				logger.error(err.stack);
				res.status(404).send({ message: "query error " });
				return;
			} else {
				res.json(result.rows);
			}
		});
	} catch (ex) {
		logger.error(ex.stack);
		res.status(404).send({ error: "catch error" });
	}
};


wareHouseController.findAllWarehouseUser = async (req, res) => {
	const query = "select * from warehouse_user";
	try {
		const parameters = [];
		conection.execute(query, parameters, { prepare: true }, (err, result) => {
			if (err) {
				logger.error(err.stack);
				res.status(404).send({ message: "query error " });
				return;
			} else {
				res.json(result.rows);
			}
		});
	} catch (ex) {
		logger.error(ex.stack);
		res.status(404).send({ error: "catch error" });
	}
};




wareHouseController.saveSeveral = async (req, res, next) => {
	const request = req.body;
	var count = req.cantidad;
	var inserted = [];
	var resInserted = [];
	const date = moment().tz(zone).format("LLL");
	const logueado = req.user.id;
	var query = "insert into warehouse (ware_id, ware_id_warehouse, ware_name, ware_country, ware_city, ware_responsable, ware_status, ware_email, ware_position, ware_phone, ware_date_register, ware_register_by) values (now(),?,?,?,?,?,?,?,?,?,?,?) if not exists";
	try {
		for (i = 0; i < request.length; i++) {
			var id = "B" + request.ware_city.replace(/ /g, "").substr(0, 3) + "" + count;
			try {
				const parameters = [id, request[i].ware_name, request[i].ware_country, request[i].ware_city, request[i].ware_responsable, request[i].ware_status, request[i].ware_email, request[i].ware_position, request[i].ware_phone, date, logueado];
				inserted.push(parameters);
				conection.execute(query, parameters, { prepare: true }, (err, result) => {
					if (err) {
						logger.error(err.stack);
						return;
					} else {
						resInserted.push(inserted);
					}
				});
			} catch (ex) {
				logger.error(ex.stack);
				return;
			}
			count++;
		}
		if ((i + 1) == request.length) {
			req.cantidad = count - 1;
			res.status(200).send({ message: "Estos  datos se insertaron con exito", inserted: inserted });
			next();
			return;
		}
	} catch (ex) {
		logger.error(ex.stack);
		res.status(404).send({ error: "catch error" });
	}
};

wareHouseController.findAllWarehouseAllUser = async (req, res) => {
	const query = "select * from warehouse_user ";
	try {
		conection.execute(query, { prepare: true }, (err, result) => {
			if (err) {
				logger.error(err.stack);
				res.status(404).send({ message: "query error " });
				return;
			} else {
				res.json(result.rows);
			}
		});
	} catch (ex) {
		logger.error(ex.stack);
		res.status(404).send({ error: "catch error" });
	}
};


wareHouseController.findAllWarehouseByUser = async (req, res) => {
	var parameter = []
	if (req.query.usercode == undefined) parameter = [req.user.code];
	else parameter = [req.query.usercode];

	const query = "select * from warehouse_user where user = ?";
	try {
		conection.execute(query, parameter, { prepare: true }, (err, result) => {
			if (err) {
				logger.error(err.stack);
				res.status(404).send({ message: "query error " });
				return;
			} else {
				res.json(result.rows);
			}
		});
	} catch (ex) {
		logger.error(ex.stack);
		res.status(404).send({ error: "catch error" });
	}
};
//exports
module.exports = wareHouseController;