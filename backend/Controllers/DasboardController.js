"use strict"
const logger = require('../bin/logger');

const dashboardController = {};


//crear conexión
const conection = require('../database');

//métodos

//Servicio que crea un rol

//Servicio que devuelve todos las dashboards
dashboardController.getAll = async (req, res) => {
    const query = "select * from dashboard";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD" });
                return;
            } else {
                res.json(result.rows);
            }
        });

    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};

//Servicio que devuelve todos los submodulos de un módulo dado su Id
dashboardController.obtenerGraficosDashboard =  async (req, res) => {
    const d = req.headers.dashboard;
    var submodules = {};
    const query = "select * from grafico_dashboard where dashboard_id=" + d + " ALLOW FILTERING";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD" });
                return;
            } else {
                submodules = result.rows;
                res.json(submodules);
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};

//Servicio que devuelve todos los submodulos de un módulo dado su Id
dashboardController.obtenerSubgraficos =  async (req, res) => {
    const d = req.headers.grafico;
    var subgraficos = {};
    const query = "select * from subgrafico where grafico_dashboard_id=" + d + " ALLOW FILTERING";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD" });
                return;
            } else {
                subgraficos = result.rows;
                res.json(subgraficos);
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};

//Servicio que devuelve todos los submodulos de un módulo dado su Id
dashboardController.obtenerDashboardsUser =  async (req, res) => {
    const code = req.headers.usercode;
    var dashboard = {};
    const query = "select * from user_dashboard where usercode='" + code + "' ALLOW FILTERING";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD" });
                return;
            } else {
                dashboard = result.rows;
                res.json(dashboard);
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(406).send({ message: "Not Acceptable" });
    }
};

dashboardController.saveDashboardUser = async (req, res) => {
    try {
        if (!req.body) {
            res.status(200).send({ message: "incomplete parameters to make the request", status: "fail" });
            return;
        }

        var usercode = req.headers.usercode;
        var dashboard = req.body.dashboard;
        
        dashboard = JSON.parse(dashboard);
        dashboard = JSON.stringify(dashboard);
        
        const parameters = [ dashboard, usercode ];
        var query = "update polaris_core.user_dashboard set dashboard =? where usercode =?";
        
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error in BD" });
                return;
            } else {
                res.status(200).send({ message: "success" });
                req.before = "{}";
                req.after = JSON.stringify(dashboard);  
            }
        });
        
    } catch (error) {
        logger.error(error.stack);
        res.status(200).send({ message: "catch error", status: "fail" });
    }
};


module.exports = dashboardController;