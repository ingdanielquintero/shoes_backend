const BcpController = {};
const conection = require("../database.js");
const c = require('colors');
const logger = require('../bin/logger');


BcpController.loginAgente = async (req, res) => {
    const query = "select agente_segmento from agente where agente_serial=? and agente_clave=? ALLOW FILTERING";
    const request = req.body;
    var segmento = '';
    try {
        if (!request || !request.agente_serial || !request.agente_clave) {
            res.json({ message: "incomplete petition", status: "fail" });
        } else {
            const parameters = [request.agente_serial, request.agente_clave];
            let ok = await login(query, parameters);
            if (!ok) {
                res.json({ message: "invalid user", status: "fail" });
                return;
            }
            if (ok == 'query') {
                res.json({ message: "query error", status: "fail" });
                return;
            }
            let resultok = await verSegmento(ok);
            res.json({ message: "success", status: "ok", data: { segmento: resultok } });
        }
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail" });
    }
};

async function verSegmento(segmento) {
    try {
        return new Promise((resolve, reject) => {
            var query = "select * from segmento where segmento_nombre=?";
            parameters = [segmento]
            conection.execute(query, parameters, function (err, result) {
                if (err) {
                    logger.error(err.stack);
                    return;
                } else {
                    if (result.rowLength > 0) {
                        resolve(result.rows);
                    } else {
                        resolve([]);
                    }
                }
            });
        });
    } catch (ex) {
        logger.error(ex.stack);
        console.log(ex);
    }
}

async function login(query, parameters) {
    try {
        return new Promise((resolve, reject) => {
            conection.execute(query, parameters, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve('query');
                    return;
                } else {
                    if (result.rowLength > 0) {
                        segmento = result.rows[0]['agente_segmento'];
                        resolve(segmento);
                    } else {
                        resolve(false);
                    }

                }
            });
        });
    } catch (ex) {
        logger.error(ex.stack);
    }
}


BcpController.confirmacion = async (req, res) => {
    const query = "select cuenta_nombre from cuenta where cuenta_numero=? and cuenta_tipo=? ALLOW FILTERING";
    const request = req.body;
    var nombre = '';
    try {
        if (!request || !request.cuenta_tipo || !request.cuenta_numero) {
            res.json({ message: "incomplete petition", status: "fail", data: { confirmacion: false, cuenta_nombre: '' } });
        } else {
            const parameters = [request.cuenta_numero, request.cuenta_tipo];
            conection.execute(query, parameters, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    res.json({ message: "query error", status: "fail", data: { confirmacion: false, cuenta_nombre: '' } });
                    return;
                } else {
                    if (result.rowLength > 0) {
                        nombre = result.rows[0]['cuenta_nombre'];
                        res.json({ message: "success", status: "ok", data: { confirmacion: true, cuenta_nombre: nombre } });
                    } else {
                        res.json({ message: "incorrect data", status: "fail", data: { confirmacion: false, cuenta_nombre: '' } });
                        return;
                    }
                }
            });
        }
    } catch (ex) {
        logger.error(ex.stack);
        res.json({ message: "catch error", status: "fail", data: { confirmacion: false, cuenta_nombre: '' } });
    }
};

//exports
module.exports = BcpController;