"use strict"
const LocalicationController = {};
const conection = require("../database.js");
const c = require('colors');
const logger = require('../bin/logger')
const moment = require('moment-timezone');
const zone = `${process.env.TIME_ZONE}`;

LocalicationController.getByCode = async (req, res) => {

    const query = "select * from polaris_core.localication where loca_id=?";
    try {
        conection.execute(query, [req.headers.id], { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: err, satus: "fail" });
                return;
            }
            if (result.rows.length > 0) {
                res.status(200).send({ message: "success", satus: "ok", data: result.rows });
            }
            else {
                res.status(200).send({ message: "success", satus: "ok", data: {} });
            }

        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error", satus: "fail" });
        return;
    }
};
LocalicationController.getAll = async (req, res) => {

    const query = "select * from polaris_core.localication";
    try {
        conection.execute(query, [], { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: err, satus: "fail" });
                return;
            }
            if (result.rows.length > 0) {
                res.status(200).send({ message: "success", satus: "ok", data: result.rows });
            }
            else {
                res.status(200).send({ message: "success", satus: "ok", data: {} });
            }

        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error", satus: "fail" });
        return;
    }
};

LocalicationController.save = async (req, res) => {
    const query = "update localication  set loca_latitude = ?, loca_length = ? where loca_id =?";
    try {
        const parameters = [req.body.loca_latitude, req.body.loca_length, req.body.loca_id];
        await conection.execute(query, parameters, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: " query error", satus: "fail" });
                return;
            }
            if (result) {
                saveLocationHistory(req.body);
                res.status(200).send({ message: "success", satus: "ok" });
                return;
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error", satus: "fail" });
        return;
    }
};


function saveLocationHistory(req) {
    try {
        const date = moment().tz(zone).format("DD-MM-YYYY");
        const time = moment().tz(zone).format("HH:mm:ss");
        const query = "INSERT INTO localication_history (lohi_id, lohi_date, lohi_latitude, lohi_length, lohi_time, lohi_user) values (now(),?,?,?,?,?);";
        const parameters = [date, req.loca_latitude, req.loca_length, time, req.loca_id];
        conection.execute(query, parameters, function (err, result) {
            if (err) {
                logger.error(err.stack);
                return;
            }
            if (result) {
                logger.info("Se añadio un nueva geolocalización");
            }
        });
    } catch (error) {

    }
}

LocalicationController.getHistoryByCode = async (req, res) => {
    let request = req.headers;
    const date = moment(new Date(request.fecha)).tz(zone).format("DD-MM-YYYY");
    const query = "SELECT * FROM localication_history WHERE lohi_user = ? AND lohi_date = ? allow filtering;";
    let parameters = [request.code, date]
    try {
        conection.execute(query, parameters, { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: err, satus: "fail" });
                return;
            }
            if (result.rows.length > 0) {
                res.status(200).send({ message: "success", satus: "ok", data: result.rows });
            }
            else {
                res.status(200).send({ message: "success", satus: "ok", data: {} });
            }

        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error", satus: "fail" });
        return;
    }
};


//codigo para retornar ubicación cercana <--->

var cities = [
    ["city1", 10, 50, "blah"],
    ["city2", 40, 60, "blah"],
    ["city3", 25, 10, "blah"],
    ["city4", 5, 80, "blah"]
];

function NearestCity(latitude, longitude) {
    var minDif = 99999;
    var closest;

    for (index = 0; index < cities.length; ++index) {
        var dif = geoDistance(latitude, longitude, cities[index][1], cities[index][2]);
        if (dif < minDif) {
            closest = index;
            minDif = dif;
        }
    }

    // echo the nearest city
    alert(cities[closest]);
}

function geoDistance(lat1, lng1, lat2, lng2){
    const a = 6378.137; // equitorial radius in km
    const b = 6356.752; // polar radius in km

    var sq = x => (x*x);
    var sqr = x => Math.sqrt(x);
    var cos = x => Math.cos(x);
    var sin = x => Math.sin(x);
    var radius = lat => sqr((sq(a*a*cos(lat))+sq(b*b*sin(lat)))/(sq(a*cos(lat))+sq(b*sin(lat))));

    lat1 = lat1 * Math.PI / 180;
    lng1 = lng1 * Math.PI / 180;
    lat2 = lat2 * Math.PI / 180;
    lng2 = lng2 * Math.PI / 180;

    var R1 = radius(lat1);
    var x1 = R1*cos(lat1)*cos(lng1);
    var y1 = R1*cos(lat1)*sin(lng1);
    var z1 = R1*sin(lat1);

    var R2 = radius(lat2);
    var x2 = R2*cos(lat2)*cos(lng2);
    var y2 = R2*cos(lat2)*sin(lng2);
    var z2 = R2*sin(lat2);

    return sqr(sq(x1-x2)+sq(y1-y2)+sq(z1-z2));
}

//exports
module.exports = LocalicationController;