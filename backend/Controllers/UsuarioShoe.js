const usuarioShoe = {};
const jwt = require("../Services/Jwt.js");
//Importamos el modulo paraa encriptar las contraseñas
const bcrypt = require('bcrypt');
const conection = require('../database');
const logger = require('../bin/logger');


//methods

usuarioShoe.save = (req, res, next) => {
    var request = req.body;
    const date = new Date();
    const query = "insert into usuario (usu_id, usu_nombre, usu_telefono, usu_fecha_creacion, usu_clave, usu_login, usu_rol) values (now(),?,?,?,?,?,?) if not exists";
    try {
        bcrypt.hash(request.usu_clave, 10, function (err, hash) {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({status: false, message: 'encryption error' });
            } else {
                const parameters = [request.usu_nombre, request.usu_telefono, date.format("%Y-%m-%d %H:%M:%S", false), hash, request.usu_login, request.usu_rol];
                conection.execute(query, parameters, function (err2, result) {
                    if (err2) {
                        logger.error(err.stack);
                        res.status(200).send({status: false, message: err2 });
                        return;
                    } else {
                        if (Object.values(result.rows[0])[0]) {
                            res.status(200).send({status: true, message: "success", applied: Object.values(result.rows[0])[0] });
                        } else {
                            res.status(200).send({status: false, message: "error", applied: Object.values(result.rows[0])[0] });
                            return;
                        }
                    }
                });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({status: false, error: "catch error" });
    }
}


usuarioShoe.logIn = async (req, res) => {
    var request = req.body;
    const query = "select * from usuario where usu_login = ? ALLOW FILTERING";
    try {
        const parameters = [request.usu_login.toLowerCase()];
        conection.execute(query, parameters, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({status: false, message: 'error', error: err });
            } else {
                if (result.rowLength > 0) {
                    bcrypt.compare(request.usu_clave, result.rows[0].usu_clave, (err, data) => {
                        if (data) {
                            let token = jwt.createToken(result.rows[0]);
                            res.status(200).send({
                                token: token,
                                message: "success",
                                nombre: result.rows[0].usu_nombre,
                                rol: result.rows[0].usu_rol,
                                status: true
                            });
                        } else {
                            res.status(200).send({status: false, message: "Contraseña inválida"});
                        }
                    });
                } else {
                    res.status(200).send({status: false, message: "Usuario inválido"});
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({status: false, error: "catch error" });
    }
};



//exports
module.exports = usuarioShoe;
