const fs = require('fs-extra');
const hbs = require('handlebars');
const path = require('path');
const conection = require("../database.js");
const nodemailer = require('nodemailer');
const funcionesGenerales = require('../bin/funcionesGenerales');
const logger = require('../bin/logger');

var dir = process.platform == 'win32' ? 'C:/polaris/tempReports' : '/opt/polaris/tempReports';

const reportsPDFController = {};

const compile = async function (templateName, data) {
  const filepath = path.join(process.cwd(), 'Reports', `${templateName}.hbs`);
  const html = await fs.readFile(filepath, 'utf-8');
  return await hbs.compile(html)(data);
}

function formatearFechas(date) {
  try {

    var f = new Date(date)
    var dia = "" + f.getDate();
    var mes = "" + (f.getMonth() + 1);
    var min = "" + f.getMinutes();
    var h = "" + f.getHours();

    if (dia.length == 1) dia = "0" + dia;
    if (mes.length == 1) mes = "0" + mes;
    if (min.length == 1) min = "0" + min;
    if (h.length == 1) h = "0" + h;

    return dia + "-" + mes + "-" + f.getFullYear() + "  " + h + ":" + min;
  } catch (error) {
    logger.error(error.stack);
  }
}

function formatearIncidencia(incidencia) {
  return {
    id: incidencia.inci_id,
    fecha_creacion: formatearFechas(incidencia.inci_date_create),
    fecha_limite: formatearFechas(incidencia.inci_date_limit_attention),
    motivo: incidencia.inci_motive,
    inicio_atencion: formatearFechas(incidencia.inci_date_reception),
    fin_atencion: formatearFechas(incidencia.inci_date_close),
    tiempo_atencion: incidencia.inci_time_atention,
    inci_contact_attended: incidencia.inci_contact_attended
  }
}

async function execute(query, parameters) {
  return new Promise((resolve, reject) => {
    try {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else resolve(result.rows);
      });
    } catch (error) {
      logger.error(error.stack);
    }
  });
}

async function renderPDF(req) {

  try {
    var id_incidencia = req.query.id_incidencia;
    var incidencia = await execute(`select * from incidence where inci_id = '${id_incidencia}'`, []);

    if (incidencia.length != 1) return 'incidencia no encontrada';
    if (incidencia[0].inci_status_of_visit != 'ATENCIÓN FALLIDA' && incidencia[0].inci_status_of_visit != 'ATENCIÓN EXITOSA')
      return 'el estado de la incidencia no corresponde a un estado permitido para cierre';

    var comercio = await execute(`select * from commerce where comm_uniqe_code = '${incidencia[0].inci_code_commerce}'`, []);
    var tecnico = await execute(`select * from user where user_id_user = '${incidencia[0].inci_technical}'`, []);
    var observaciones = await execute(`select * from observation_incidence where obin_incidence = '${id_incidencia}' allow filtering`, []);

    observaciones = await funcionesGenerales.ordenarFechaAscendente(observaciones, 'obin_date');

    var observaciones_creacion = observaciones.filter(c => (c.obin_photo == null || c.obin_photo == '') && c.obin_firm == '0');
    var obins = await funcionesGenerales.ordenarFechaAscendente(observaciones_creacion, 'obin_date');
    var observaciones_cierre = obins[obins.length - 1];

    observaciones_creacion = observaciones_creacion[0];
    observaciones_creacion.obin_observation = observaciones_creacion.obin_observation.toUpperCase()
    observaciones_cierre.obin_observation = observaciones_cierre.obin_observation.toUpperCase()

    const photos = await observaciones.filter(c => c.obin_photo != null && c.obin_photo != '' && c.obin_firm == '0');
    const firma = await observaciones.filter(c => c.obin_photo != null && c.obin_photo != '' && c.obin_firm == '1');

    let geoDist = await getFindAll('select * from company_flag where cofl_id = ?', ['1'])
    let geoDist1 = geoDist[0].cofl_geodist.split('-')[0]
    let geoDist2 = geoDist[0].cofl_geodist.split('-')[1]

    var data = {
      incidencia: formatearIncidencia(incidencia[0]),
      comercio: comercio[0],
      tecnico: tecnico[0],
      observaciones_creacion: observaciones_creacion,
      observaciones_cierre: observaciones_cierre,
      verSimcards: incidencia[0].inci_tecnology != 'DIAL,LAN',
      fallida: true,
      cargo: false, 
      geoDist1: geoDist1, 
      geoDist2: geoDist2, 
    };

    if (incidencia[0].inci_status_of_visit == 'ATENCIÓN FALLIDA') {
      data.foto1 = photos[0];
      data.foto2 = photos[1];
      data.isfirma = false;
      data.persona_contacto = data.incidencia.inci_contact_attended.toUpperCase();
      data.tipo = incidencia[0].inci_type + ' - ATENCIÓN FALLIDA';
      data.incidencia.inci_typing = incidencia[0].inci_typing
    } else {
      if (incidencia[0].inci_status_of_visit == 'ATENCIÓN EXITOSA' && incidencia[0].inci_type == 'INSTALACIÓN') {
        var incidencia_instalados = await execute(`select * from installed_incidence where inin_incidence = '${id_incidencia}' allow filtering`, []);

        var terminal_template = await compile('templates/terminales', {
          tipo: 'INSTALADA',
          terminal: {
            inci_tecnology: incidencia[0].inci_tecnology,
            inci_term_brand: incidencia[0].inci_term_brand,
            inci_term_model: incidencia[0].inci_term_model,
            inci_term_number: incidencia[0].inci_term_number,
            inci_term_serial: incidencia[0].inci_term_serial
          }
        });

        // sumo los accesorios repetidos por codigos
        var access = JSON.parse(incidencia_instalados[0].inin_accessories);

        var accesorios_insta = access.reduce(function (pv, cv) {
          if (pv[cv.codigo]) pv[cv.codigo] += cv.cantidad;
          else pv[cv.codigo] = cv.cantidad;
          return pv;
        }, {});

        var keys = Object.keys(accesorios_insta);
        var arrayfinal = [];

        keys.forEach(element => {
          var found = access.find((c) => {
            return c.codigo == element;
          });

          found.cantidad = accesorios_insta[element];
          arrayfinal.push(found);
        });

        // cargo en el template 
        var accesorios_template = await compile('templates/accesorios', {
          tipo: 'INSTALADOS',
          accesorios: arrayfinal
        });

        var simcard_template = await compile('templates/simcard', {
          tipo: 'INSTALADA',
          simcard: await JSON.parse(incidencia_instalados[0].inin_sim_card)
        });

        data.terminales_template = terminal_template;
        data.accesorios_template = accesorios_template;
        data.simcards_template = simcard_template;
        data.tipo = 'INSTALACIÓN EXITOSA';

      } else if (incidencia[0].inci_status_of_visit == 'ATENCIÓN EXITOSA' && incidencia[0].inci_type == 'RETIRO') {
        var incidencia_removidos = await execute(`select * from removed_incidence where rein_incidence = '${id_incidencia}' allow filtering`, []);
        var terminal = JSON.parse(incidencia_removidos[0].rein_terminal);

        var terminal_template = await compile('templates/terminales', {
          tipo: 'RETIRADA',
          terminal: {
            inci_tecnology: terminal[0].tecnologia,
            inci_term_brand: terminal[0].marca,
            inci_term_model: terminal[0].modelo,
            inci_term_number: terminal[0].numero.toUpperCase() || '',
            inci_term_serial: terminal[0].serial
          }
        });
        let access = JSON.parse(incidencia_removidos[0].rein_accessories)

        let accesorios_reti = access.reduce(function (pv, cv) {
          if (pv[cv.codigo]) pv[cv.codigo] += cv.cantidad;
          else pv[cv.codigo] = cv.cantidad;
          return pv;
        }, {});

        let keys = Object.keys(accesorios_reti);
        let arrayfinal = [];

        keys.forEach(element => {
          let found = access.find((c) => {
            return c.codigo == element;
          });

          found.cantidad = accesorios_reti[element];
          arrayfinal.push(found);
        });

        var accesorios_template = await compile('templates/accesorios', {
          tipo: 'RETIRADOS',
          accesorios: arrayfinal
        });

        var simcard_template = await compile('templates/simcard', {
          tipo: 'RETIRADA',
          simcard: await JSON.parse(incidencia_removidos[0].rein_sim_card)
        });

        data.terminales_template = terminal_template;
        data.accesorios_template = accesorios_template;
        data.simcards_template = simcard_template;
        data.tipo = 'RETIRO EXITOSO'

      } else if (incidencia[0].inci_status_of_visit == 'ATENCIÓN EXITOSA' && incidencia[0].inci_type == 'SOPORTE') {
        data.tipo = 'SOPORTE EXITOSO';
        var incidencia_instalados = await execute(`select * from installed_incidence where inin_incidence = '${id_incidencia}' allow filtering`, []);
        var incidencia_removidos = await execute(`select * from removed_incidence where rein_incidence = '${id_incidencia}' allow filtering`, []);
        var incidencia_encontrados = await execute(`select * from found_incidence where foin_incidence = '${id_incidencia}' allow filtering`, []);
        // TERMINAL 
        var terminal_encontrada = incidencia_encontrados[0].foin_terminal;

        if (terminal_encontrada == '[]' || terminal_encontrada == '[{}]' || terminal_encontrada == '' || terminal_encontrada == ' ' || terminal_encontrada == null || terminal_encontrada == '{}') {
          // CAMBIO DE TERMINAL
          var terminal_removida = JSON.parse(incidencia_removidos[0].rein_terminal);

          let retirada = await execute(`select * from terminal where term_serial = '${terminal_removida[0].serial}' allow filtering`, []);

          var terminal_retirada_template = await compile('templates/terminales', {
            tipo: 'RETIRADA',
            terminal: {
              inci_tecnology: terminal_removida[0].tecnologia,
              inci_term_brand: terminal_removida[0].marca,
              inci_term_model: terminal_removida[0].modelo,
              inci_term_number: retirada[0].term_num_terminal,
              inci_term_serial: terminal_removida[0].serial
            }
          });

          let instalada = await execute(`select * from terminal where term_serial = '${incidencia[0].inci_term_serial}' allow filtering`, []);

          var terminal_instalada_template = await compile('templates/terminales', {
            tipo: 'INSTALADA',
            terminal: {
              inci_tecnology: incidencia[0].inci_tecnology,
              inci_term_brand: incidencia[0].inci_term_brand,
              inci_term_model: incidencia[0].inci_term_model,
              inci_term_number: instalada[0].term_num_terminal,
              inci_term_serial: incidencia[0].inci_term_serial
            }
          });

          data.terminales_template = `${terminal_retirada_template} ${terminal_instalada_template}`;

        } else {
          // SIN CAMBIO DE TERMINAL
          terminal_encontrada = JSON.parse(terminal_encontrada);
          let encontrada = await execute(`select * from terminal where term_serial = '${terminal_encontrada[0].serial}' allow filtering`, []);

          var terminal_encontrada_template = await compile('templates/terminales', {
            tipo: 'ENCONTRADA',
            terminal: {
              inci_tecnology: terminal_encontrada[0].tecnologia,
              inci_term_brand: terminal_encontrada[0].marca,
              inci_term_model: terminal_encontrada[0].modelo,
              inci_term_number: encontrada[0].term_num_terminal,
              inci_term_serial: terminal_encontrada[0].serial
            }
          });

          data.terminales_template = `${terminal_encontrada_template}`;
        }

        // ACCESORIOS 
        var accesorios_encontrados = incidencia_encontrados[0].foin_accessories;

        if (accesorios_encontrados == '[]' || accesorios_encontrados == '[{}]' || accesorios_encontrados == '' || accesorios_encontrados == ' ' || accesorios_encontrados == null || accesorios_encontrados == '{}') {
          // CAMBIO DE ACCESORIOS
          var accesorios_removidos = JSON.parse(incidencia_removidos[0].rein_accessories);

          var accesorios_removidos_template = await compile('templates/accesorios', {
            tipo: 'RETIRADOS',
            accesorios: accesorios_removidos
          });

          var accesorios_instalados = JSON.parse(incidencia_instalados[0].inin_accessories);

          var accesorios_instalados_template = await compile('templates/accesorios', {
            tipo: 'INSTALADOS',
            accesorios: accesorios_instalados
          });

          data.accesorios_template = `${accesorios_removidos_template} ${accesorios_instalados_template}`;

        } else {
          // SIN CAMBIO DE ACCESORIOS
          accesorios_encontrados = JSON.parse(accesorios_encontrados);
          var accesorios_encontrados_template = await compile('templates/accesorios', {
            tipo: 'INSTALADOS',
            accesorios: accesorios_encontrados
          });

          data.accesorios_template = `${accesorios_encontrados_template}`;
        }


        // SIMCARDS
        var simcard_encontrada = incidencia_encontrados[0].foin_sim_card;

        if (simcard_encontrada == '[]' || simcard_encontrada == '[{}]' || simcard_encontrada == '' || simcard_encontrada == ' ' || simcard_encontrada == null || simcard_encontrada == '{}') {
          // CAMBIO DE SIMCARD
          var simcard_removidas = JSON.parse(incidencia_removidos[0].rein_sim_card);
          var simcard_removida_template = await compile('templates/simcard', {
            tipo: 'RETIRADA',
            simcard: simcard_removidas
          });

          var simcard_instaladas = JSON.parse(incidencia_instalados[0].inin_sim_card);
          var simcard_instalada_template = await compile('templates/simcard', {
            tipo: 'INSTALADA',
            simcard: simcard_instaladas
          });

          data.simcards_template = `${simcard_removida_template} ${simcard_instalada_template}`;

        } else {
          // CAMBIO DE SIMCARD
          simcard_encontrada = JSON.parse(simcard_encontrada);
          var simcard_encontrada_template = await compile('templates/simcard', {
            tipo: 'ENCONTRADA',
            simcard: simcard_encontrada
          });

          data.simcards_template = `${simcard_encontrada_template}`;
        }
      }

      // INFORMACIÓN COMUN PARA ESTOS TIPOS DE INCIDENCIAS

      data.foto1 = photos[0];
      data.foto2 = photos[1];
      data.foto3 = photos[2];
      data.foto4 = photos[3];
      data.foto5 = photos[4];
      data.firma = firma[0];
      data.isfirma = true;

      var calificacion = await execute(`select * from incidence_qualification where inqu_id = '${id_incidencia}' allow filtering`, []);

      var smileGood = `<svg class="smile" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1024" height="1024" viewBox="0 0 1024 1024">
      <title></title>
      <g id="icomoon-ignore"></g>
      <path fill="#000" d="M331.219 487.719c56.539 0 102.531-45.992 102.531-102.523 0-56.539-45.992-102.531-102.531-102.531-56.531 0-102.523 45.992-102.523 102.531 0 56.531 45.992 102.523 102.523 102.523zM331.219 342.664c23.453 0 42.531 19.078 42.531 42.531 0 23.445-19.078 42.523-42.531 42.523-23.445 0-42.523-19.078-42.523-42.523 0-23.453 19.078-42.531 42.523-42.531z"></path>
      <path fill="#000" d="M692.781 282.664c-56.539 0-102.531 45.992-102.531 102.531 0 56.531 45.992 102.523 102.531 102.523 56.531 0 102.523-45.992 102.523-102.523 0-56.539-45.992-102.531-102.523-102.531zM692.781 427.719c-23.453 0-42.531-19.078-42.531-42.523 0-23.453 19.078-42.531 42.531-42.531 23.445 0 42.523 19.078 42.523 42.531 0 23.445-19.078 42.523-42.523 42.523z"></path>
      <path fill="#000" d="M257.016 622h509.969v60h-509.969z"></path>
      <path fill="#000" d="M874.039 149.961c-96.703-96.703-225.281-149.961-362.039-149.961s-265.336 53.258-362.039 149.961c-96.703 96.703-149.961 225.281-149.961 362.039s53.258 265.336 149.961 362.039c96.703 96.703 225.281 149.961 362.039 149.961s265.336-53.258 362.039-149.961c96.703-96.703 149.961-225.281 149.961-362.039s-53.258-265.336-149.961-362.039zM512 964c-249.234 0-452-202.766-452-452s202.766-452 452-452 452 202.766 452 452-202.766 452-452 452z"></path>
      </svg>`;

      var smileOk = `<svg class="smile" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1024" height="1024" viewBox="0 0 1024 1024">
      <title></title>
      <g id="icomoon-ignore"></g>
      <path fill="#000" d="M512 769.617c-103.523 0-197.398-65.148-233.594-162.109l-56.211 20.984c21.82 58.453 60.391 108.453 111.539 144.594 52.336 36.984 113.977 56.531 178.266 56.531s125.938-19.547 178.273-56.531c51.148-36.141 89.711-86.141 111.531-144.594l-56.211-20.984c-36.195 96.961-130.070 162.109-233.594 162.109z"></path>
      <path fill="#000" d="M874.039 149.961c-96.703-96.703-225.281-149.961-362.039-149.961s-265.336 53.258-362.039 149.961c-96.703 96.703-149.961 225.281-149.961 362.039s53.258 265.336 149.961 362.039c96.703 96.703 225.281 149.961 362.039 149.961s265.336-53.258 362.039-149.961c96.703-96.703 149.961-225.281 149.961-362.039s-53.258-265.336-149.961-362.039zM512 964c-249.234 0-452-202.766-452-452s202.766-452 452-452 452 202.766 452 452-202.766 452-452 452z"></path>
      <path fill="#000" d="M331.219 503.719c56.539 0 102.531-45.992 102.531-102.523 0-56.539-45.992-102.531-102.531-102.531-56.531 0-102.523 45.992-102.523 102.531 0 56.531 45.992 102.523 102.523 102.523zM331.219 358.664c23.453 0 42.531 19.078 42.531 42.531 0 23.445-19.078 42.523-42.531 42.523-23.445 0-42.523-19.078-42.523-42.523 0-23.453 19.078-42.531 42.523-42.531z"></path>
      <path fill="#000" d="M692.781 503.719c56.531 0 102.523-45.992 102.523-102.523 0-56.539-45.992-102.531-102.523-102.531-56.539 0-102.531 45.992-102.531 102.531 0 56.531 45.992 102.523 102.531 102.523zM692.781 358.664c23.445 0 42.523 19.078 42.523 42.531 0 23.445-19.078 42.523-42.523 42.523-23.453 0-42.531-19.078-42.531-42.523 0-23.453 19.078-42.531 42.531-42.531z"></path>
      </svg>`;

      var smileSad = `<svg class="smile" version="1.1" xmlns="http://www.w3.org/2000/svg" width="1024" height="1024" viewBox="0 0 1024 1024">
      <title></title>
      <g id="icomoon-ignore"></g>
      <path fill="#000" d="M331.219 503.719c56.539 0 102.531-45.992 102.531-102.523 0-56.539-45.992-102.531-102.531-102.531-56.531 0-102.523 45.992-102.523 102.531 0 56.531 45.992 102.523 102.523 102.523zM331.219 358.664c23.453 0 42.531 19.078 42.531 42.531 0 23.445-19.078 42.523-42.531 42.523-23.445 0-42.523-19.078-42.523-42.523 0-23.453 19.078-42.531 42.523-42.531z"></path>
      <path fill="#000" d="M692.781 298.664c-56.539 0-102.531 45.992-102.531 102.531 0 56.531 45.992 102.523 102.531 102.523 56.531 0 102.523-45.992 102.523-102.523 0-56.539-45.992-102.531-102.523-102.531zM692.781 443.719c-23.453 0-42.531-19.078-42.531-42.523 0-23.453 19.078-42.531 42.531-42.531 23.445 0 42.523 19.078 42.523 42.531 0 23.445-19.078 42.523-42.523 42.523z"></path>
      <path fill="#000" d="M682.023 626.469c-49.914-35.273-108.711-53.914-170.023-53.914s-120.109 18.641-170.023 53.914c-48.781 34.469-85.563 82.156-106.367 137.906l56.203 20.984c34.117-91.398 122.602-152.805 220.188-152.805 97.578 0 186.063 61.406 220.18 152.805l56.211-20.984c-20.805-55.75-57.586-103.438-106.367-137.906z"></path>
      <path fill="#000" d="M874.039 149.961c-96.703-96.703-225.281-149.961-362.039-149.961s-265.336 53.258-362.039 149.961c-96.703 96.703-149.961 225.281-149.961 362.039s53.258 265.336 149.961 362.039c96.703 96.703 225.281 149.961 362.039 149.961s265.336-53.258 362.039-149.961c96.703-96.703 149.961-225.281 149.961-362.039s-53.258-265.336-149.961-362.039zM512 964c-249.234 0-452-202.766-452-452s202.766-452 452-452 452 202.766 452 452-202.766 452-452 452z"></path>
      </svg>`;

      const calificacion_tiempo_atencion = `<div class="smile ok ${calificacion[0].inqu_attention_time == 'BUENA' ? 'active' : ''}">${smileOk}</div>` +
        `<div class="smile good ${calificacion[0].inqu_attention_time == 'REGULAR' ? 'active' : ''}">${smileGood}</div>` +
        `<div class="smile sad ${calificacion[0].inqu_attention_time == 'MALA' ? 'active' : ''}">${smileSad}</div>`;

      const calificacion_presentacion = `<div class="smile ok ${calificacion[0].inqu_presentation_technical == 'BUENA' ? 'active' : ''}">${smileOk}</div>` +
        `<div class="smile good ${calificacion[0].inqu_presentation_technical == 'REGULAR' ? 'active' : ''}">${smileGood}</div>` +
        `<div class="smile sad ${calificacion[0].inqu_presentation_technical == 'MALA' ? 'active' : ''}">${smileSad}</div>`;

      const calificacion_atencion = `<div class="smile ok ${calificacion[0].inqu_attention_technical == 'BUENA' ? 'active' : ''}">${smileOk}</div>` +
        `<div class="smile good ${calificacion[0].inqu_attention_technical == 'REGULAR' ? 'active' : ''}">${smileGood}</div>` +
        `<div class="smile sad ${calificacion[0].inqu_attention_technical == 'MALA' ? 'active' : ''}">${smileSad}</div>`;

      var calificacion_template = await compile('templates/calificacion', {
        calificacion_tiempo_atencion: calificacion_tiempo_atencion,
        calificacion_presentacion: calificacion_presentacion,
        calificacion_atencion: calificacion_atencion
      });

      data.calificacion = calificacion_template;
      data.persona_contacto = calificacion[0].inqu_contact_person;
      data.persona_contacto_cargo = calificacion[0].inqu_position;
      data.fallida = false;
      data.cargo = true;
    }

    // RENDERIZADO Y EXPORTACION
    data.url = `http://${process.env.IP_HOST}:${process.env.PORT}`;
    var content = await compile('templates/app', data);

    var fs = require('fs');
    var conversion = require("phantom-html-to-pdf")();

    var html_email = await compile('templates/emails/closeIncidence', {
      nombre_comercio: comercio[0].comm_name,
      incidencia_id: id_incidencia,
      nombre_tecnico: tecnico[0].user_name
    });

    conversion({
      html: content,
      footer: '<div style="width: 100%;height: 50px;background-color: #008994;border-radius: 0px 0px 5px 5px;"></div>'
    }, function (err, pdf) {
      if (err) {
        logger.error(err);
        return 'error';
      } else {
        const filename = `${dir}/${Date.now()}.pdf`;
        var output = fs.createWriteStream(filename);
        pdf.stream.pipe(output);

        var transporter = nodemailer.createTransport({
          service: 'Gmail',
          auth: {
            user: 'soporte.polariscore@gmail.com',
            pass: 'PolarisCore2019'
          }
        });

        var mailOptions = {
          from: 'soporte@polarisCore.com',
          to: comercio[0].comm_email,
          subject: 'Reporte de cierre de incidencia',
          attachments: [
            {
              filename: 'Cierre de incidencia.pdf',
              path: filename
            }
          ],
          html: html_email
        };

        transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
            logger.error(error.stack);
            return 'success';
          } else return 'ocurrió un error enviando el email';
        });
      }
    });

    return 'success';
    // return content;
  } catch (error) {
    logger.error(error.stack);
    return error;
  }

}

reportsPDFController.reports = async (req, res) => {
  var status = await renderPDF(req);
  if (status != 'success') return res.status(401).send({ message: status, status: 'error' });
  else return res.status(200).send({ message: "success", status: 'success' });
  // res.send(status)
}

module.exports = reportsPDFController;