const userController = {};
const jwt = require("../Services/Jwt.js");
const c = require('colors');
//Importamos el modulo paraa encriptar las contraseñas
const bcrypt = require('bcrypt');
const conection = require('../database');
const logger = require('../bin/logger');
const moment = require('moment');
const md5 = require('md5');

//methods

userController.save = (req, res, next) => {
  var request = req.body;
  const query = "insert into user (user_identification,user_name,user_position,user_address,user_id_user,user_state,user_email,user_phone,user_photo,user_location,user_locations,user_immediate_boss, user_role, user_login, user_password, user_session) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) if not exists";
  try {
    var nombre = request.user_name.split(" ");
    var dato = '';
    if (nombre.length > 1) {
      dato += nombre[0].replace(/ /g, "").substr(0, 1);
      dato += nombre[1].replace(/ /g, "");
    }
    var id = dato.toUpperCase() + req.cantidad;
    bcrypt.hash(request.user_password, 10, function (err, hash) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: 'encryption error' });
      } else {
        const parameters = [request.user_identification, request.user_name, request.user_position, request.user_address, id, request.user_state, request.user_email, request.user_phone, request.user_identification + '.jpg', request.user_location, request.user_locations, request.user_immediate_boss, request.user_role, request.user_login, hash, '0'];
        conection.execute(query, parameters, function (err2, result) {
          if (err2) {
            logger.error(err.stack);
            res.status(200).send({ message: err2 });
            return;
          } else {
            if (Object.values(result.rows[0])[0]) {
              req.action = "registrar/usuario";
              req.before = "{}";
              req.after = JSON.stringify(request);
              res.status(200).send({ message: "success", applied: Object.values(result.rows[0])[0] });
              next();
            } else {
              res.status(200).send({ message: "user already exists", applied: Object.values(result.rows[0])[0] });
              return;
            }
          }
        });
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ error: "catch error" });
  }
}

userController.validateEmail = (req, res, next) => {
  var request = req.body;
  const query = "select * from user where user_email = ?";
  try {
    conection.execute(query, [request.user_email], function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: 'query error' });
      } else {
        if (result.rows.length > 0) {
          if (result.rows[0].user_identification == request.user_identification) {
            next();
          } else {
            res.status(200).send({ message: "user already exists", applied: 'email' });
            return;
          }
        } else {
          next();
        }
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ error: "catch error" });
  }
};

userController.logIn = async (req, res) => {
  let company = await getCompany();
  var request = req.body;
  const query = "select * from user where user_email = ?";
  try {
    const parameters = [request.user_email.toLowerCase()];
    conection.execute(query, parameters, function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: 'query error', error: err });
      } else {
        if (result.rowLength > 0) {
          bcrypt.compare(request.user_password, result.rows[0].user_password, (err, data) => {
            if (data) {
              if (result.rows[0].user_session == '0') {
                if (request.gethash) {
                  const query2 = "update user set user_session = ?   where user_identification = ? IF EXISTS;";
                  const parameters2 = ["1", result.rows[0].user_identification];
                  conection.execute(query2, parameters2, function (err2, result2) {
                    if (err2) {
                      logger.error(err2.stack);
                      res.status(404).send({ message: 'query error' });
                    } else {
                      if (Object.values(result2.rows[0])[0]) {

                        let token = jwt.createToken(result.rows[0]);
                        let code = result.rows[0].user_id_user;
                        let email = result.rows[0].user_email;

                        res.status(200).send({
                          token: token,
                          message: "success",
                          roles: result.rows[0].user_role,
                          login: result.rows[0].user_login,
                          id: result.rows[0].user_identification,
                          status: result.rows[0].user_state,
                          position: result.rows[0].user_position,
                          code: code,
                          name: result.rows[0].user_name,
                          email: email,
                          location: result.rows[0].user_location,
                          phone: result.rows[0].user_phone,
                          photo: result.rows[0].user_photo,
                          company: company['cofl_variable'],
                          companyValidate: company['cofl_validate'],
                          identi_commerce: company['cofl_identi_commerce'],
                          cofl_geodist: company['cofl_geodist']
                        });
                      } else {
                        res.status(200).send({ message: "invalid user" });
                      }
                    }
                  });

                } else {
                  res.status(200).send({ message: 'not gethash' });
                }
              } else {
                res.status(200).send({ description: "Usted ya ha iniciado sesión", message: "error", ok: false });
              }
            } else {
              res.status(200).send({ description: "Contraseña inválida", message: "error" });
            }
          });
        } else {
          res.status(200).send({ description: "Usuario inválido", message: "error" });
        }
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

userController.closeSesionsExternal = (req, res, next) => {
  let email = req.body.email;
  let documento = req.body.documento;
  let password = req.body.password;

  getFindAll('select user_identification, user_id_user, user_email, user_password from user where user_identification = ?', [documento]).then(user => {
    let time = moment().unix();
    if (!user) return res.json({ message: 'Usuario no existe', status: 'fail' });

    bcrypt.compare(password, user[0].user_password, (err, data) => {
      if (data) {
        if (user[0].user_email != email) return res.json({ message: 'El correo no coincide con el de acceso a su cuenta', status: 'fail' });
        let token = md5(time);

        conection.execute('update user set user_hashclose = ? where user_identification = ?', [token, documento], function (err, result) {
          if (err) {
            return res.json({ message: 'Ah ocurrido un error', status: 'fail' });
          } else {
            req.body.token = token;
            req.body.code = user[0].user_id_user;
            return next();
          }
        });
      } else return res.json({ message: 'Contraseña Invalida', status: 'fail' });
    });
  })
}

userController.RecuperarToken = async (req, res) => {
  var request = req.body;
  const query = "select * from user where user_email = ?";
  try {
    const parameters = [request.user_email.toLowerCase()];
    conection.execute(query, parameters, function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: 'query error' });
      } else {
        if (result.rowLength > 0) {
          bcrypt.compare(request.user_password, result.rows[0].user_password, (err, data) => {
            if (data) {
              if (request.gethash) {
                res.status(200).send({
                  token: jwt.createToken(result.rows[0]),
                  message: "success",
                  roles: result.rows[0].user_role,
                  login: result.rows[0].user_login,
                  id: result.rows[0].user_identification,
                  status: result.rows[0].user_state,
                  position: result.rows[0].user_position,
                  code: result.rows[0].user_id_user,
                  name: result.rows[0].user_name,
                  email: result.rows[0].user_email,
                  location: result.rows[0].user_location,
                  phone: result.rows[0].user_phone,
                  photo: result.rows[0].user_photo
                })
              } else {
                res.status(200).send({ message: 'not gethash' });
              }
            } else {
              res.status(200).send({ description: "Contraseña inválida", message: "error" });
            }
          });
        } else {
          res.status(200).send({ description: "Usuario inválido", message: "error" });
        }
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};



userController.update = (req, res, next) => {
  var request = req.body;
  req.action = "actualizar/usuario";
  const status = req.estado;
  var auditBefore = [];
  var auditAfter = [];
  try {
    if (status === false) {
      res.status(404).send({ message: "error, user not exits" });
      return;
    };
    const row = JSON.parse(req.before);
    bcrypt.hash(request.user_password, 10, function (err, hash) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: 'encryption error' });
      } else {
        request.user_password = hash;
        try {
          for (let j = 1; j < Object.keys(request).length; j++) {
            if (Object.keys(row[0])[j] === Object.keys(request)[j]) {
              if (Object.values(row[0])[j] !== Object.values(request)[j]) {
                auditBefore.push(JSON.parse('{"' + Object.keys(row[0])[j] + '":"' + Object.values(row[0])[j] + '"}'));
                auditAfter.push(JSON.parse('{"' + Object.keys(request)[j] + '":"' + Object.values(request)[j] + '"}'));
              }
            } else {
              res.status(400).send({ message: "error, wrong json syntax" });
              return;
            }
          }
        } catch (ex) {
          logger.error(ex.stack);
          res.status(400).send({ error: "catch error" });
        }
      }
      req.before = JSON.stringify(auditBefore);
      req.after = JSON.stringify(auditAfter);
    });

    const query = "update user set user_name=?, user_position=? ,user_address=?,user_id_user=?, user_state=?, user_email=?, user_phone=?, user_photo=?, user_location=?, user_locations=?, user_immediate_boss=?, user_role=? where user_identification=?";
    bcrypt.hash(request.user_password, 10, function (err, hash) {
      if (err) {
        logger.error(err.stack);
        res.status(400).send({ message: 'encryption error' });
      } else {
        const parameters = [request.user_name, request.user_position, request.user_address, request.user_id_user,
        request.user_state, request.user_email, request.user_phone, request.user_photo, request.user_location, request.user_locations, request.user_immediate_boss, request.user_role, request.user_identification];
        conection.execute(query, parameters, function (err, result) {
          if (err) {
            logger.error(err.stack);
            res.status(404).send({ message: 'query error' });
            return;
          } else {
            res.status(200).send({ message: "success" });
            next();
          }
        });
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(400).send({ error: "catch error" });
  }
};

userController.findAll = (req, res) => {
  const query = 'select * from user';

  try {
    conection.execute(query, [], function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: 'query error' });
      } else {
        res.json(result.rows);

      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
}

userController.delete = (req, res, next) => {
  const status = req.estado;
  try {
    if (status === false) {
      res.status(404).send({ message: "error, user not exits" });
      return;
    };
    const query = "delete from user where user_identification = ?";
    const parameters = [req.headers.id];
    conection.execute(query, parameters, function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: " query error" });
      } else {
        req.action = "eliminar/usuario";
        req.after = "{}";
        res.status(200).send({ message: "success" });
        next();
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

userController.find = (req, res) => {
  const query = 'select * from user where user_identification = ?';
  try {
    const parameters = [req.headers.id];
    conection.execute(query, parameters, function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ msg: 'query error' });
      } else {
        res.json(result.rows);
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

userController.getUser = async (req, res, next) => {
  const query = "select * from user where user_identification = ?";
  var estado = false;
  try {
    await conection.execute(query, [req.headers.id], function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ msg: 'query error' });
      } else {
        if (result.rows.length > 0) {
          req.before = JSON.stringify(result.rows);
          estado = true;
        }
        req.estado = estado;
        next();
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};


userController.updatePassword = (req, res, next) => {
  var request = req.body;
  const query = "update user set user_password =?, user_login =?  where user_identification =? IF EXISTS";
  try {
    bcrypt.hash(request.user_password, 10, function (err, hash) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: 'encryption error' });
      } else {
        const parameters = [hash, "1", request.user_identification];
        conection.execute(query, parameters, function (err, result) {
          if (err) {
            res.status(404).send({ message: 'query error' });
          } else {
            res.status(200).send({ message: "success" });
            next();
          }
        });
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};


/** Services para app commerce */
userController.updatePasswordAppCommerce = async (req, res, next) => {
  var request = req.body;

  getFindAll('select * from user_commerce where usco_id = ?', [request.usco_id]).then((usuario) => {
    bcrypt.compare(request.usco_passwordOld, usuario[0].usco_password, (err, data) => {
      if (data) {
        const query = "update user_commerce set usco_password =?  where usco_id =? IF EXISTS";

        try {
          bcrypt.hash(request.usco_password, 10, function (err, hash) {
            if (err) {
              logger.error(err.stack);
              res.status(404).send({ message: 'encryption error', status: 'fail' });
            } else {
              const parameters = [hash, request.usco_id];
              conection.execute(query, parameters, function (err, result) {
                if (err) {
                  logger.error(err.stack);
                  res.status(404).send({ message: 'query error', status: 'fail' });
                } else {
                  res.status(200).send({ message: "success", status: 'ok' });
                  next();
                }
              });
            }
          });

        } catch (ex) {
          logger.error(ex.stack);
          res.status(404).send({ error: "catch error", status: 'fail' });
        }

      } else {
        res.status(200).send({ message: 'La contraseña anterior no coincide', status: 'ok' });
      }
    });
  })

};

userController.getMaxUser = async (req, res, next) => {
  const query = "select * from all_exits where alle_id = 0";
  var estado = false;
  var cantidad = 0;
  try {
    await conection.execute(query, { prepare: true }, function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: " query error" });
        return;
      }
      if (result.rows.length > 0) {
        req.cantidad = result.rows[0].alle_users;
        next();
        return;
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
    return;
  }
};

userController.validateCommerce = async (req, res, next) => {
  const codigo = req.body.usco_id;
  const query = "SELECT * FROM commerce WHERE comm_uniqe_code = ? allow filtering;";
  try {
    await conection.execute(query, [codigo], { prepare: true }, function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ message: " query error", status: "fail" });
        return;
      }
      if (result.rows.length > 0) {
        next();
        return;
      } else {
        return res.status(200).send({ message: "El comercio no éxiste", status: "fail" });
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error", status: "fail" });
    return;
  }
};


userController.validarUsuarioComercio = async (req, res, next) => {
  try {
    let resultUser = await userExistsCommerce(req.body.usco_id);

    if (!resultUser) {
      if (req.body.validate) {
        return res.status(200).send({ message: "El usuario no éxiste", status: "true" });
      } else {
        next();
      }
    } else {
      return res.status(200).send({ message: "El usuario ya éxiste", status: "fail" });
    }
  } catch (ex) {
    logger.error(ex.stack);
    return res.status(200).send({ message: "token no valido", status: "fail" });
  }
};

userController.updateMaxUser = async (req, res, next) => {
  const query = "update all_exits  set alle_users = ? where alle_id = 0";
  var estado = false;
  var cantidadN = req.cantidad + 1;
  try {
    await conection.execute(query, [cantidadN], { prepare: true }, function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: " query error" });
        return;
      }
      next();
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
    return;
  }
};

userController.validatePassword = (req, res, next) => {
  var request = req.body;
  if (request.user_passwordOld) {
    const query = "select * from user where user_identification = ?";
    try {
      const parameters = [request.user_identification];
      conection.execute(query, parameters, function (err, result) {
        if (err) {
          logger.error(err.stack);
          res.status(404).send({ message: 'query error' });
        } else {
          if (result.rowLength > 0) {
            bcrypt.compare(request.user_passwordOld, result.rows[0].user_password, (err, data) => {
              if (data) {
                next();
              } else {
                res.status(200).send({ description: "Contraseña Inválida", message: "error" });
              }
            });
          } else {
            res.status(200).send({ description: "Usuario inválido", message: "error" });
          }
        }
      });
    } catch (ex) {
      logger.error(ex.stack);
      res.status(404).send({ error: "catch error" });
    }
  } else {
    next();
  }
};

userController.getUserByCode = async (req, res) => {
  const query = "select * from user where user_id_user = ?";
  var estado = false;
  try {
    await conection.execute(query, [req.headers.id], function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ msg: 'query error' });
      } else {
        res.status(200).send({ msg: 'ssucces', user: result.rows });
        return;
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

userController.getAllTechTechnical = async (req, res) => {
  const query = "SELECT * FROM user WHERE user_position = 'TÉCNICO EN CAMPO' ALLOW FILTERING";
  try {
    conection.execute(query, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ message: " query error" });
        return;
      } else {
        res.json(result.rows);
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ error: "catch error" });
  }
};

userController.getAllTechTechnicalLaboratorio = async (req, res) => {
  const query = "SELECT * FROM user WHERE user_position = 'TÉCNICO' ALLOW FILTERING";
  try {
    conection.execute(query, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(402).send({ message: " query error" });
        return;
      } else {
        res.json(result.rows);
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(401).send({ error: "catch error" });
  }
};


userController.verificationPassword = async (req, res) => {
  var request = req.body;
  const query = "select * from user where user_email = ?";
  try {
    if (!request.user_email || !request.user_password) {
      res.json({ message: "incomplete petition", status: "fail" });
    } else {
      const parameters = [request.user_email.toLowerCase()];
      conection.execute(query, parameters, function (err, result) {
        if (err) {
          logger.error(err.stack);
          res.json({ message: "query error", status: "fail" });
        } else {
          if (result.rowLength > 0) {
            bcrypt.compare(request.user_password, result.rows[0].user_password, (err, data) => {
              if (data) {
                res.json({ message: "success", status: "ok" });
              } else {
                res.json({ message: "invalid  password", status: "fail" });
              }
            });
          } else {
            res.json({ message: "invalid email", status: "fail" });
          }
        }
      });
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};


userController.updateSecurityQuestions = async (req, res) => {
  const query = "update security_questions_user  set sq_questions = ? where sq_user =?";
  var estado = false;
  try {
    const parameters = [req.body.questions, req.body.user];
    await conection.execute(query, parameters, function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ message: " query error" });
        return;
      }
      if (result) {
        res.status(200).send({ message: "success" });
        return;
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
    return;
  }
};

userController.getSecurityQuestions = async (req, res) => {
  const query = "select * from security_questions_user where sq_user =?";
  var estado = false;
  try {
    const parameters = [req.headers.id];
    await conection.execute(query, parameters, function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ message: " query error" });
        return;
      }
      if (result) {
        res.status(200).send({ message: "success", user: result.rows });
        return;
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
    return;
  }
};

userController.updateSession = (req, res, next) => {
  var request = req.body;

  const query = "update user set user_session =?  where user_identification = ? IF EXISTS;";
  try {
    const parameters = [request.user_session, request.user_identification];
    conection.execute(query, parameters, function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ message: 'query error' });
      } else {
        if (Object.values(result.rows[0])[0]) {
          res.status(200).send({ message: "success" });
        } else {
          res.status(200).send({ message: "invalid user" });
        }
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ error: "catch error" });
  }
};

userController.closeSesion = (req, res, next) => {
  var request = req.body;
  const query = "update user set user_session = ? where user_identification =? IF EXISTS";

  try {
    if (!request || !request.user) {
      res.json({ message: "incomplete petition", status: "fail" });
    } else {
      const parameters = ['0', request.user];
      conection.execute(query, parameters, function (err, result) {
        if (err) {
          logger.error(err.stack);
          res.json({ message: "query error", status: "fail" });
        } else {
          if (Object.values(result.rows[0])[0]) {
            res.json({ message: "success", status: "ok" });
          } else {
            res.json({ message: "invalid user", status: "ok" });
          }
        }
      });
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};

userController.verificationToken = (req, res) => {
  res.json({ message: "El token es reciente", status: "ok" });
  return;
};

userController.getUserByCodeToRestartPassword = (req, res) => {
  var request = req.headers.usercode;
  const query = "select user_state, user_id_user, user_email, user_identification, user_position, user_name from user where user_id_user = ? allow filtering";
  try {
    const parameters = [request];
    conection.execute(query, parameters, function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ message: "Query error", status: 'fail' });
      } else {
        res.status(200).send({ message: "success", status: 'ok', result: result.rows });
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ message: "Catch error", status: 'fail' });
  }
};

userController.findByCode = (req, res) => {
  const query = 'select * from user where user_id_user = ? allow filtering';
  try {
    conection.execute(query, [req.headers.code], function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ message: "Query error", status: 'fail' });
      }
      else {
        res.status(200).send({ message: "success", status: 'ok', result: result.rows });
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ message: "Catch error", status: 'fail' });
  }
}

userController.findByCodeUserCommerce = (req, res) => {
  const query = 'select * from user where user_id_user = ? AND user_position = ? allow filtering';
  try {
    conection.execute(query, [req.headers.code, "TÉCNICO EN CAMPO"], function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ message: "Query error", status: 'fail' });
      }
      else {
        res.status(200).send({ message: "success", status: 'ok', result: result.rows });
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ message: "Catch error", status: 'fail' });
  }
}

userController.saveUserCommerce = (req, res, next) => {
  var request = req.body;
  if (!request.usco_id || !request.usco_name || !request.usco_password) {
    res.json({ message: "incomplete petition", status: "fail" });
  }
  const query = "INSERT INTO  user_commerce (usco_id, usco_cellphone, usco_name, usco_password, usco_phone, usco_date_register) values (?, ?, ?, ?, ?, ?) if not exists";
  var fecha = new Date();
  try {
    bcrypt.hash(request.usco_password, 10, function (err, hash) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: 'encryption error' });
      } else {
        const parameters = [request.usco_id, request.usco_cellphone, request.usco_name, hash, request.usco_phone, String(fecha)];
        conection.execute(query, parameters, function (err, result) {
          if (err) {
            res.status(200).send({ message: err });
            return;
          } else {
            if (Object.values(result.rows[0])[0]) {
              res.status(200).send({ message: "success", applied: Object.values(result.rows[0])[0] });
              next();
            } else {
              res.status(200).send({ message: "user already exists", applied: Object.values(result.rows[0])[0] });
              return;
            }
          }
        });
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ error: "catch error" });
  }
}

userController.logInUserCommerce = async (req, res) => {
  var request = req.body;
  const query = "select * from user_commerce where usco_id = ?";

  try {
    const parameters = [request.usco_id];
    conection.execute(query, parameters, function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: 'query error', error: err });
      } else {
        if (result.rowLength > 0) {
          bcrypt.compare(request.usco_password, result.rows[0].usco_password, (err, data) => {
            if (data) {
              res.status(200).send({
                token: jwt.createTokenCommerce(result.rows[0]),
                message: "success",
                id: result.rows[0].usco_id,
                cellphone: result.rows[0].usco_cellphone,
                name: result.rows[0].usco_name,
                status: true,
                phone: result.rows[0].usco_phone
              });
            } else {
              res.status(200).send({ description: "Contraseña inválida", message: "error" });
            }
          });
        } else {
          res.status(200).send({ description: "Usuario inválido", message: "error" });
        }
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

async function getCompany() {
  const query = "select * from company_flag";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, [], (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve("0");
        } else {
          resolve(result.rows[0]);
        }
      });
    });
  } catch (error) {
    logger.error(error.stack);
    resolve("0");
  }
}


async function userExistsCommerce(user_id) {
  try {
    const query = "select * from user_commerce where usco_id = ?";
    return new Promise((resolve, reject) => {
      conection.execute(query, [user_id], (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          if (result.rows.length > 0) {
            resolve(true);
          } else {
            resolve(false);
          }
        }
      });
    });
  } catch (error) {
    logger.error(error.stack);
    return false;
  }
};

function getFindAll(query, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);
  }
}

//exports
module.exports = userController;