const extraController = {};
const conection = require("../database.js");
const logger = require('../bin/logger')

extraController.findAllCountries = async (req, res) => {
    const query = "select * from country";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.findAllCities = async (req, res) => {
    const query = "select * from city";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.findAllTerminalBrands = async (req, res) => {
    const query = "select * from terminals_brand";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.findAllTerminalModels = async (req, res) => {
    const query = "select * from terminal_model";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};


extraController.findAllTypesTypifications = async (req, res) => {
    const query = "select * from type_typification";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.findAllProductivityTechnical = async (req, res) => {
    const query = "select * from technical_productivity";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.saveCountry = async (req, res) => {
    const request = req.body;
    var query = "insert into country (coun_id,coun_name,coun_indicative) values (now(),?,?) if not exists";
    try {
        const parameters = [request.coun_name, request.coun_indicative];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error" });
                return;
            } else {
                if (Object.values(result.rows[0])[0]) {
                    res.status(200).send({ message: "success", applied: Object.values(result.rows[0])[0] });
                } else {
                    res.status(200).send({ message: "country already exists", applied: Object.values(result.rows[0])[0] });
                    return;
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.saveTerminalModel = async (req, res) => {
    const request = req.body;
    const cassandra = require('cassandra-driver');
    const Uuid = cassandra.types.Uuid;
    const id = Uuid.random();
    var query = "insert into terminal_model (temo_id,temo_name,temo_brand) values (?,?,?) if not exists";
    try {
        const parameters = [id, request.temo_name, request.temo_brand];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error" });
                return;
            } else {
                if (Object.values(result.rows[0])[0]) {
                    res.status(200).send({ message: "success", applied: Object.values(result.rows[0])[0], id: id });
                } else {
                    res.status(200).send({ message: "terminal brand already exists", applied: Object.values(result.rows[0])[0], id: false });
                    return;
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.saveTerminalBrand = async (req, res) => {
    const request = req.body;
    var query = "insert into terminals_brand (tebr_id,tebr_name) values (now(),?) if not exists";
    try {
        const parameters = [request.tebr_name];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error" });
                return;
            } else {
                if (Object.values(result.rows[0])[0]) {
                    res.status(200).send({ message: "success", applied: Object.values(result.rows[0])[0] });
                } else {
                    res.status(200).send({ message: "terminal brand already exists", applied: Object.values(result.rows[0])[0] });
                    return;
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.saveCity = async (req, res) => {
    const request = req.body;
    var query = "insert into city (city_id,city_country,city_name) values (now(),?,?) if not exists";
    try {
        const parameters = [request.city_country, request.city_name];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error" });
                return;
            } else {
                if (Object.values(result.rows[0])[0]) {
                    res.status(200).send({ message: "success", applied: Object.values(result.rows[0])[0] });
                } else {
                    res.status(200).send({ message: "country already exists", applied: Object.values(result.rows[0])[0] });
                    return;
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};


extraController.saveTypifications = async (req, res) => {
    const moment = require("moment");
    const date = moment().format("LLL");
    const request = req.body;
    var query = "insert into typification_incidence(tyin_id,tyin_name,tyin_type,tyin_register_by,tyin_date_register)values(now(),?,?,?,?) if not exists";
    try {
        const parameters = [request.tyin_name, request.tyin_type, request.tyin_register_by, date];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error" });
                return;
            } else {
                if (Object.values(result.rows[0])[0]) {
                    res.status(200).send({ message: "success", applied: Object.values(result.rows[0])[0] });
                } else {
                    res.status(200).send({ message: "Typificaction already exists", applied: Object.values(result.rows[0])[0] });
                    return;
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.saveProductivityTechnical = async (req, res) => {
    const moment = require("moment");
    const date = moment().format("LLL");
    const request = req.body;
    var query = "insert into technical_productivity(tepro_id,tepro_city,tepro_country,tepro_quantity_services,tepro_register_by,tepro_date_register)values(now(),?,?,?,?,?) if not exists";
    try {
        const parameters = [request.tepro_city, request.tepro_country, request.tepro_quantity_services, request.tepro_register_by, date];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "error" });
                return;
            } else {
                if (Object.values(result.rows[0])[0]) {
                    res.status(200).send({ message: "success", applied: Object.values(result.rows[0])[0] });
                } else {
                    res.status(200).send({ message: "Productivity already exists", applied: Object.values(result.rows[0])[0] });
                    return;
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.updateCountry = async (req, res) => {
    const request = req.body;
    var query = "update country set coun_name=?,coun_indicative=? where coun_id=?";
    try {
        const parameters = [request.coun_name, request.coun_indicative, request.coun_id];
        conection.execute(query, parameters, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else if (result) {
                res.json({ message: "success" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.updateCity = async (req, res) => {
    const request = req.body;
    var query = "update city set city_name=? where city_id=?";
    try {
        const parameters = [request.city_name, request.city_id];
        conection.execute(query, parameters, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else if (result) {
                res.json({ message: "success" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.updateTerminalBrand = async (req, res) => {
    const request = req.body;
    var query = "update terminals_brand set tebr_name=? where tebr_id=?";
    try {
        const parameters = [request.tebr_name, request.tebr_id];
        conection.execute(query, parameters, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else if (result) {
                res.json({ message: "success" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.updateTerminalModel = async (req, res) => {
    const request = req.body;
    var query = "update terminal_model set temo_name=?, temo_brand=? where temo_id=?";
    try {
        const parameters = [request.temo_name, request.temo, request.temo_id];
        conection.execute(query, parameters, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else if (result) {
                res.json({ message: "success" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.updateTypifications = async (req, res) => {
    const request = req.body;
    var query = "update typification_incidence set tyin_name=?,tyin_type=?  where tyin_id=?";
    try {
        const parameters = [request.tyin_name, request.tyin_type, request.tyin_id];
        conection.execute(query, parameters, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else if (result) {
                res.json({ message: "success" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.updateProductivityTechnical = async (req, res) => {
    const request = req.body;
    var query = "update technical_productivity set tepro_quantity_services=?  where tepro_id=?";
    try {
        const parameters = [request.tepro_quantity_services, request.tepro_id];
        conection.execute(query, parameters, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: " query error" });
                return;
            } else if (result) {
                res.json({ message: "success" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

extraController.company = async (req, res) => {
    const query = "select * from company_flag";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error" });
    }
};

extraController.updateConfigCompany = (req, res) => {
    const request = req.body;
    let name = Object.keys(request);
    let values = Object.values(request);

    if (!request || name.length == 0)
        return res.status(401).send({ message: 'Datos inconpletos', status: 'error' });

    try {
        var query = "update company_flag set ";

        for (let index = 0; index < name.length; index++) {
            const element = name[index];
            query += element + ' = ?,';
        }
        query = query.substring(0, query.length - 1);
        query += ' where cofl_id = ?';
        values.push('1');

        conection.execute(query, values, (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(404).send({ message: " query error", status: 'error' });
            } else if (result) res.json({ message: "success", status: 'ok' });
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ message: "catch error", status: 'error' });
    }

}

//exports
module.exports = extraController;