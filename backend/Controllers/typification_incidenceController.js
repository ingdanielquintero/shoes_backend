const typification_incidence = {};
const conection = require("../database.js");
const logger = require('../bin/logger');

typification_incidence.findAll = async (req, res) => {
    const query = "select * from typification_incidence";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "Query error", status: "fail" });
                return;
            } else {
                res.json({ message: "Success", status: "ok", response: result.rows });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "Catch error", status: "fail" });
    }
};


typification_incidence.findByType = async (req, res) => {
    const query = "select * from typification_incidence WHERE tyin_type= ? ALLOW FILTERING;";
    var parameter = [req.headers.type];
    try {
        conection.execute(query, parameter, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "Query error", status: "fail" });
                return;
            } else {
                res.json({ message: "Success", status: "ok", response: result.rows });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "Catch error", status: "fail" });
    }
};

typification_incidence.obtenerMotivosReporteDeFallas = async (req, res) => {
    const query = "select * from typification_incidence WHERE tyin_type= ? ALLOW FILTERING;";
    var parameter = ['MOTIVO'];
    try {
        conection.execute(query, parameter, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(404).send({ message: "Query error", status: "fail", err: err });
                return;
            } else {
                var resul = result.rows.filter(c => c.tyin_name != 'RETIRO DE POS' && c.tyin_name != 'INSTALACIÓN DE POS ADICIONAL' && c.tyin_name != 'INSTALACIÓN DE POS POR EVENTO');
                res.json({ message: "Success", status: "ok", response: resul });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "Catch error", status: "fail" });
    }
};

//exports
module.exports = typification_incidence;