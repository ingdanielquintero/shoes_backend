const CommerceController = {};
const conection = require("../database.js");
var Request = require("request");
const logger = require('../bin/logger');
const io = require('socket.io-client');   //client.js
var ip = require('ip');
const moment = require("moment");

const config = {
  app_id: 'VV9O1VLSXz8pRNjg602T', // to use Google Maps for Work
  app_code: 'DezhHjCs8WgKEA_oyijc2Q', // to use Google Maps for Work
};

//methods
CommerceController.saveSeveral = async (req, res, next) => {
  const request = req.body;
  const logueado = req.user.id;

  const date = moment().format("LLL");

  var countRound = 0;
  var countInserted = 0;

  var banderaInserted = false;
  var banderaError = false;
  var banderaCiudad = false;
  var banderaPrioridad = false;
  var banderaCodigo = false;

  var totalNoInsert = [];
  var notInserted = [];
  var comerciosRegistrados = [];

  var resultCiudad;
  var resultCodigoUnico;
  let resultPriority = await getPriority();

  var query = "insert into polaris_core.commerce (comm_id, comm_uniqe_code, comm_name, comm_nit, comm_priority, comm_country, comm_city, comm_address, comm_route_code, comm_phone, comm_cellphone, comm_email, comm_schedule_attention, comm_contact_person, comm_date_register, comm_register_by) values (now(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) if not exists";
  let citys = await getFindAll('select * from city', []);

  if (request.length == 0 && resultPriority.rows.length == 0)
    return res.status(400).send({ error: "Ha ocurrido un problema, alguno de los valores en la consulta esta vacio." });

  let geoDist = await getFindAll('select * from company_flag where cofl_id = ?', ['1'])
  let geoDist1 = geoDist[0].cofl_geodist.split('-')[0]
  let geoDist2 = geoDist[0].cofl_geodist.split('-')[1]

  for (i = 0, long = request.length; i < long; i++) {
    let comercio = request[i];
    countRound++;

    banderaCiudad = citys.some(c => c.city_name == comercio.comm_city && c.city_country == comercio.comm_country);
    if (!banderaCiudad) notInserted.push(` ${geoDist1} o ${geoDist2} incorrecto `);

    banderaPrioridad = resultPriority.rows.some(c => c['prco_descrption'] == comercio.comm_priority);
    if (!banderaPrioridad) notInserted.push(" Prioridad inválida");

    resultCodigoUnico = await getCodigoUnico(comercio.comm_uniqe_code);

    if (resultCodigoUnico.length != 0) {
      notInserted.push(" Codigo unico ya existe ");
      banderaCodigo = true;
    }

    if (!banderaCodigo && banderaPrioridad && banderaCiudad) {
      const parameters = [
        `${comercio.comm_uniqe_code}`,
        `${comercio.comm_name}`,
        `${comercio.comm_nit}`,
        `${comercio.comm_priority}`,
        `${comercio.comm_country}`,
        `${comercio.comm_city}`,
        `${comercio.comm_address}`,
        `${comercio.comm_route_code}`,
        `${comercio.comm_phone}`,
        `${comercio.comm_cellphone}`,
        `${comercio.comm_email}`,
        `${comercio.comm_schedule_attention}`,
        `${comercio.comm_contact_person}`,
        date,
        logueado
      ];

      resultadoInsert = await getRelaizarInsert(query, parameters);
      banderaInserted = Object.values(resultadoInsert[0])[0];

      banderaCodigo = false;
      banderaPrioridad = false;
      banderaCiudad = false;
      banderaPais = false;
    } else {
      totalNoInsert.push("Fila ==>  " + (i + 2) + notInserted);
      banderaCodigo = false;
      banderaPrioridad = false;
      banderaCiudad = false;
      banderaPais = false;
      notInserted = [];
    }

    if (banderaError) {
      countError++;
      banderaError = false;
    }

    if (banderaInserted) {
      countInserted++;
      comerciosRegistrados.push(comercio);
      banderaInserted = false;
    }

    if ((i % 100) == 0) {
      console.log("cargando comercios ... ", i);
    }
  }

  if (countRound == request.length) {
    getLocation(comerciosRegistrados, req.user.code);
    return res.status(200).send({ message: "Resultados de el cargue ", response: totalNoInsert, countInserted: countInserted });
  }
};

//servicio que lista las prioridades de un comercio
CommerceController.getPrioritys = async (req, res) => {
  try {
    let prioritys = await getPriority();
    res.json(prioritys.rows);
  }
  catch (ex) {
    logger.error(ex.stack);
    res.status(402).send({ error: "catch error 1" });
  }
}

//modelo de PRIORIDAD
async function getPriority() {
  var resultPriority;
  var queryConsulta = "SELECT prco_descrption FROM PRIORITY_COMMERCE";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          return;
        } else {
          resultPriority = result;
          resolve(resultPriority);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(402).send({ error: "catch error 1" });
  }
}

//modelo de COMERCIO
async function getCommerce() {
  var resultCommerce;
  var queryConsulta = "SELECT comm_uniqe_code,comm_nit FROM commerce";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          return;
        } else {
          resultCommerce = result;
          resolve(resultCommerce);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(402).send({ error: "catch error 1" });
  }
}


// Servicio que crea un comercio
CommerceController.create = async (req, res, next) => {
  try {
    if (!req.body.comm_uniqe_code || !req.body.comm_name || !req.body.comm_nit || !req.body.comm_address || !req.body.comm_city || !req.body.comm_phone || !req.body.comm_email || !req.headers.authenticator) {
      res.status(400).send({ message: "incomplete parameters to make the request" });
      return;
    }
    const logueado = req.user.id;
    const request = req.body;
    comm_uniqe_code = request.comm_uniqe_code;
    comm_name = request.comm_name;
    comm_nit = request.comm_nit;
    comm_priority = request.comm_priority;
    comm_country = request.comm_country;
    comm_city = request.comm_city;
    comm_address = request.comm_address;
    comm_route_code = request.comm_route_code;
    comm_phone = request.comm_phone;
    comm_cellphone = request.comm_cellphone;
    comm_email = request.comm_email;
    comm_schedule_attention = request.comm_schedule_attention;
    comm_contact_person = request.comm_contact_person;
    comm_date_register = moment().format("LLL");
    comm_register_by = req.user.id;
    const parameters = [comm_uniqe_code, comm_name, comm_nit, comm_priority, comm_country, comm_city, comm_address, comm_route_code, comm_phone, comm_cellphone, comm_email, comm_schedule_attention, comm_contact_person, comm_date_register, comm_register_by];
    let validateuniqueCode = await validarUnicidad(comm_uniqe_code);
    if (validateuniqueCode) {
      var query = "insert into polaris_core.commerce (comm_id, comm_uniqe_code, comm_name, comm_nit, comm_priority, comm_country, comm_city, comm_address, comm_route_code, comm_phone, comm_cellphone, comm_email, comm_schedule_attention, comm_contact_person, comm_date_register, comm_register_by) values (now(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) if not exists";

      conection.execute(query, parameters, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          console.log("error: " + err);
          res.status(404).send({ message: "error" });
          return;
        } else {
          if (Object.values(result.rows[0])[0]) {
            res.status(200).send({ message: "success", applied: Object.values(result.rows[0])[0] });
            req.action = "registrar/comercio";
            req.before = "{}";
            req.after = JSON.stringify(request);
            next();

          } else {
            res.status(200).send({ message: "commerce already exists", applied: Object.values(result.rows[0])[0] });
            return;
          }
        }
      });
    }
    else {
      res.status(200).send({ message: "commerce already exists", applied: "false" });
    }
  } catch (ex) {
    console.log(ex);
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

//Método que valida si un código único de comercio ya fue registrado
async function validarUnicidad(comm_uniqe_code) {
  query = "select * from polaris_core.commerce where comm_uniqe_code ='" + comm_uniqe_code + "' ALLOW FILTERING";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, [], (err, result) => {
        if (err) {
          logger.error(err.stack);
          reject(false);
        }
        else {
          if (result.rows.length < 1) {
            resolve(true);
          } else {
            var rta = result.rows[0];
            resolve(false);
          }
        }
      });
    });
  }
  catch (error) {
    logger.error(error.stack);
    res.status(406).send({ message: "Not Acceptable" });
  }
}

//Servicio que devuelve todos los comercios
CommerceController.getAll = async (req, res) => {
  const query = "select * from polaris_core.commerce";
  let commerces = await getFindAllNotLimit(query, []);
  res.json(commerces);
};

//Servicio que devuelve un comercio dado un Id
CommerceController.getById = async (req, res) => {
  try {
    if (!req.headers.id) {
      res.status(400).send({ message: "incomplete parameters to make the request" });
      return;
    }
    const query = "select * from polaris_core.commerce where comm_uniqe_code = ?";

    conection.execute(query, [req.headers.id], (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.json({ "Data": "error in BD" });
      }
      else {
        if (result.rows.length < 1) {
          res.json(result.rows);
        } else {
          var rta = result.rows[0];
          res.json(rta);
        }
      }
    });

  }
  catch (error) {
    logger.error(error.stack);
    res.status(406).send({ message: "Not Acceptable" });
  }
};

//Servicio que devuelve un comercio dado un Id
CommerceController.getCommerce = async (req, res, next) => {
  var estado = false;
  try {
    if (!req.headers.id) {
      res.status(400).send({ message: "incomplete parameters to make the request" });
      return;
    }

    const query = "select * from polaris_core.commerce where comm_uniqe_code = ?";

    conection.execute(query, [req.headers.id], { prepare: true }, function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: "query error" });
        return;
      }
      if (result.rows.length > 0) {
        req.before = JSON.stringify(result.rows);
        estado = true;
      }
      req.estado = estado;
      next();
    });

  }
  catch (error) {
    console.log(error);
    logger.error(error.stack);
    res.status(406).send({ message: "Not Acceptable" });
  }
};



//Servicio que devuelve un comercio dado un codigo
CommerceController.getByCode = async (req, res) => {
  try {
    if (!req.headers.id) {
      res.status(400).send({ message: "incomplete parameters to make the request" });
      return;
    }
    const query = "select * from polaris_core.commerce where comm_uniqe_code = '" + req.headers.id + "'";

    conection.execute(query, [], (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.json({ "Data": "error in BD", err });
      }
      else {
        if (result.rows.length < 1) {
          res.json({ "Data": "No data in BD" });
        } else {
          var rta = result.rows[0];
          res.json(rta);
        }
      }
    });

  }
  catch (error) {
    logger.error(error.stack);
    res.status(406).send({ message: "Not Acceptable" });
  }
};



//Servicio que elimina un comercio
CommerceController.delete = async (req, res, next) => {
  const status = req.estado;
  try {
    const query = "delete from polaris_core.commerce  where comm_id = " + req.headers.id + "IF EXISTS";
    conection.execute(query, [], (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: " error in BD" });
        return;
      } else {
        req.action = "eliminar/comercio ";
        req.after = "{}";
        res.json({ message: "success" });
        next();

      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

//Servicio que elimina un comercio
CommerceController.deleteUserCommerce = async (req, res, next) => {
  const status = req.estado;
  try {
    const query = "delete from polaris_core.user_commerce  where usco_id = ? IF EXISTS";
    conection.execute(query, [req.headers.code], (err, result) => {
      if (err) {
        logger.error(err.stack);
        return;
      } else {
        next();
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

//Servicio que actualiza un comercio
CommerceController.update = async (req, res, next) => {

  try {
    if (!req.headers.id || !req.body) {
      res.status(400).send({ message: "incomplete parameters to make the request" });
      return;
    }
    const comm_id = req.headers.id;
    const request = req.body;
    comm_priority = request.comm_priority;
    comm_country = request.comm_country;
    comm_city = request.comm_city;
    comm_address = request.comm_address;
    comm_route_code = request.comm_route_code;
    comm_phone = request.comm_phone;
    comm_cellphone = request.comm_cellphone;
    comm_email = request.comm_email;
    comm_schedule_attention = request.comm_schedule_attention;
    comm_contact_person = request.comm_contact_person;
    comm_name = request.comm_name;
    const parameters = [comm_name, comm_priority, comm_country, comm_city, comm_address, comm_route_code, comm_phone, comm_cellphone, comm_email, comm_schedule_attention, comm_contact_person, comm_id];

    var query = "update polaris_core.commerce set  comm_name=?, comm_priority=?, comm_country=?, comm_city=?, comm_address=?, comm_route_code=?, comm_phone=?, comm_cellphone=?, comm_email=?, comm_schedule_attention=?, comm_contact_person=? where comm_id=?";

    conection.execute(query, parameters, (err, result) => {

      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: "error in BD" });
        return;
      } else {
        req.action = "actualizar/comercio";
        req.after = JSON.stringify(req.body);
        res.status(200).send({ message: "success" });
        next();
      }
    });

  }
  catch (error) {
    logger.error(error.stack);
    res.status(406).send({ message: "Not Acceptable" });
  }
};

//------> Las sigueintes 2 consultas son para el cargue masivo <------//
async function getRelaizarInsert(query, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}
async function getCodigoUnico(cod) {
  var parameters = [cod];
  var query = "select * from commerce where comm_uniqe_code = ? allow filtering";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}

function normalized(texto) {
  return texto.normalize('NFD').replace(/([^n\u0300-\u036f]|n(?!\u0303(?![\u0300-\u036f])))[\u0300-\u036f]+/gi, "$1").normalize();
}

async function getLocation(comercios, user) {
  const query = "update localication  set loca_latitude = ?, loca_length = ? where loca_id =?";
  var parameters = [];
  var comerciosSinUbicacion = [];
  const validate = [null, '', undefined, ' ']
  for (let index = 0; index < comercios.length; index++) {
    const comercio = comercios[index];
    if (!validate.includes(comercio['comm_latitude']) && !validate.includes(comercio['comm_longitude'])) {
      parameters = [String(comercio['comm_latitude']), String(comercio['comm_longitude']), comercio['comm_uniqe_code']];
      var validation = getRelaizarInsert(query, parameters);
      if (!validation) {
        comerciosSinUbicacion.push({
          comercio: {
            "comm_uniqe_code": comercio['comm_uniqe_code'],
            "comm_name": comercio['comm_name'],
            "comm_country": comercio["comm_country"],
            "comm_city": comercio["comm_city"],
            "comm_address": comercio["comm_address"]
          }, error: 'Error al intentar registrar la ubicación'
        });
      }
    } else {
      var direccion = normalized(comercio['comm_address'] + "," + comercio['comm_city'] + "," + comercio['comm_country']);
      direccion = direccion.replace("#", "").replace(".", "");
      Request.post({
        "headers": { "content-type": "application/json" },
        "url": "https://places.cit.api.here.com/places/v1/discover/search?at=0,0&q=" + direccion + "&app_id=" + config.app_id + "&app_code=" + config.app_code + "&pretty&Accept-Language=es-CO,es;&addressFilter=city=" + normalized(comercio['comm_city']) + ";"
      }, (error, response, body) => {
        if (error) {
          logger.error(error.stack);
          comerciosSinUbicacion.push({
            comercio: {
              "comm_uniqe_code": comercio['comm_uniqe_code'],
              "comm_name": comercio['comm_name'],
              "comm_country": comercio["comm_country"],
              "comm_city": comercio["comm_city"],
              "comm_address": comercio["comm_address"]
            }, error: 'Error al buscar la dirección'
          });
        } else {
          var result = JSON.parse(body)
          var items = [];
          if (result['results']) {
            items = result['results']['items'];
            if (items && items.length > 0) {
              var coords = items[0]['position'];
              if ((items[0]['title']).toUpperCase() != comercio["comm_city"]) {
                parameters = [String(coords[0]), String(coords[1]), comercio['comm_uniqe_code']];
                var validation = getRelaizarInsert(query, parameters);
                if (!validation) {
                  comerciosSinUbicacion.push({
                    comercio: {
                      "comm_uniqe_code": comercio['comm_uniqe_code'],
                      "comm_name": comercio['comm_name'],
                      "comm_country": comercio["comm_country"],
                      "comm_city": comercio["comm_city"],
                      "comm_address": comercio["comm_address"]
                    }, error: 'Error al intentar registrar la ubicación'
                  });
                }
              } else {
                comerciosSinUbicacion.push({
                  comercio: {
                    "comm_uniqe_code": comercio['comm_uniqe_code'],
                    "comm_name": comercio['comm_name'],
                    "comm_country": comercio["comm_country"],
                    "comm_city": comercio["comm_city"],
                    "comm_address": comercio["comm_address"]
                  }, error: 'Error al intentar registrar la ubicación'
                });
              }
            } else {
              comerciosSinUbicacion.push({
                comercio: {
                  "comm_uniqe_code": comercio['comm_uniqe_code'],
                  "comm_name": comercio['comm_name'],
                  "comm_country": comercio["comm_country"],
                  "comm_city": comercio["comm_city"],
                  "comm_address": comercio["comm_address"]
                }, error: 'Error la dirección no fue encontrada'
              });
            }
          } else {
            comerciosSinUbicacion.push({
              comercio: {
                "comm_uniqe_code": comercio['comm_uniqe_code'],
                "comm_name": comercio['comm_name'],
                "comm_country": comercio["comm_country"],
                "comm_city": comercio["comm_city"],
                "comm_address": comercio["comm_address"]
              }, error: 'Error la dirección no fue encontrada'
            });
          }
        }
        if ((index == comercios.length - 1) && comerciosSinUbicacion.length > 0) {
          console.log("comerciosSinUbicacion ", comerciosSinUbicacion);
          crearNotificacionUbicacion(comerciosSinUbicacion, user);
        }
      });
    }
  }
}


async function crearNotificacionUbicacion(comerciosSinUbicacion, user) {
  const queryNotification = "insert into polaris_core.notifications (noti_id , noti_msg , noti_type , noti_dest , noti_origin , noti_date_create, noti_state  ) values (now(),?,?,?,?,?,?)";
  const noti_date_create = moment().format("LLL");
  const noti_state = "No leída";
  const noti_type = "Cargue masivo";
  var noti_msg = "Cargue masivo: No se encontraron la ubicación de algunos comercios " + JSON.stringify(comerciosSinUbicacion);
  const parametersNotification = [noti_msg, noti_type, user, user, noti_date_create, noti_state];
  var validation = getRelaizarInsert(queryNotification, parametersNotification);
  if (validation) {
    sendNotification();
  }
}

function sendNotification() {
  var socket = io.connect('http://' + process.env.IP_HOST + `:${process.env.PORT}`, { reconnect: true });
  socket.emit('getNotificaciones', { id: '', doc: '' });
  socket.disconnect();
}

//Servicio que devuelve todos los comercios de una ciudad
CommerceController.getAllByCity = async (req, res) => {
  const query = "select * from polaris_core.commerce where comm_city =? allow filtering";
  let commerces = await getFindAllNotLimit(query, [req.headers.id]);
  res.json(commerces);
};

function getFindAll(query, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve([]);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve([]);
  }
}

function getFindAllNotLimit(query, params) {
  var data = [];
  return new Promise((resolve, reject) => {
    conection.stream(query, params, { prepare: true, autoPage: true })
      .on('readable', function () {
        var row;
        while (row = this.read()) {
          data.push(row);
        }
      }).on('end', function () {
        resolve(data);
      });
  });
}

//exports
module.exports = CommerceController;