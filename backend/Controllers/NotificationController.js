"use strict"
const NotificationController = {};
const conection = require("../database.js");
const c = require('colors');
const logger = require('../bin/logger');
const funcionesGenerales = require('../bin/funcionesGenerales');

//servicio que crea una notificación
NotificationController.create = async (req, res) => {
    try {
        if (!req.body) {
            res.send({ message: "incomplete parameters to make the request" });
            return;
        }
        const request = req.body
        const noti_msg = request.msg;
        const noti_type = request.type;
        const noti_dest = request.dest;
        const noti_origin = request.origin;
        const moment = require("moment");
        const noti_date_create = moment().format("LLL");
        const noti_state = "No leída";
        const parameters = [noti_msg, noti_type, noti_dest, noti_origin, noti_date_create, noti_state];
        var query = "insert into polaris_core.notifications (noti_id , noti_msg , noti_type , noti_dest , noti_origin , noti_date_create, noti_state  ) values (now(),?,?,?,?,?,?)";
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: "error in BD", status: "fail", applied: "false" });
                return;
            } else {
                res.status(200).send({ message: "success", status: "ok", applied: "true" });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ message: "Catch error", status: "fail", applied: "false" });
    }
};

//servicio que devuelve las notificaciones de un usuario
NotificationController.getByDest = async (req, res) => {

    if (!req.headers.id) {
        res.send({ message: "incomplete parameters to make the request" });
        return;
    }
    const query = "select * from polaris_core.notifications where noti_dest = ?";

    try {
        conection.execute(query, [req.headers.id], { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.send({ message: "query error" });
                return;
            }
            if (result.rows.length > 0) {
                res.json(result.rows);
            }
            else {
                res.json({});
            }

        });
    } catch (ex) {
        logger.error(ex.stack);
        res.send({ error: "catch error" });
    }
};

//servicio que devuelve las notificaciones de un usuario
NotificationController.getByDestUserCommerce = async (req, res) => {

    if (!req.headers.id) {
        res.send({ message: "incomplete parameters to make the request" });
        return;
    }
    const query = "select * from polaris_core.notifications_user_commerce where nouc_dest = ?";

    try {
        conection.execute(query, [req.headers.id], { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.send({ message: "query error", status: "fail" });
                return;
            }
            if (result.rows.length > 0) {
                res.json(result.rows);
            }
            else {
                res.json({
                    "message": "No se encontraron notificaciones",
                    "status": "fail"
                }
                );
            }

        });
    } catch (ex) {
        logger.error(ex.stack);
        res.send({ error: "catch error" });
    }
};

//servicio que devuelve las notificaciones de un usuario
NotificationController.getByDestUserCommerce = async (req, res) => {

    if (!req.headers.id) {
        res.send({ message: "incomplete parameters to make the request" });
        return;
    }
    const query = "select * from polaris_core.notifications_user_commerce where nouc_dest = ?";

    try {
        conection.execute(query, [req.headers.id], { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.send({ message: "query error" });
                return;
            }
            if (result.rows.length > 0) {
                res.json(result.rows);
            }
            else {
                res.json({});
            }

        });
    } catch (ex) {
        logger.error(ex.stack);
        res.send({ error: "catch error" });
    }
};

//Servicio que elimina una Notificación
NotificationController.delete = async (req, res) => {
    if (!req.headers.id) {
        res.status(400).send({ message: "incomplete parameters to make the request" });
        return;
    }
    try {
        const query = "delete from polaris_core.notifications  where noti_id = " + req.headers.id + " IF EXISTS";
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "err", status: 'fail' });
                return;
            } else {
                req.action = "eliminar/notificación ";
                req.after = "{}";
                res.json({ message: "success", status: 'ok' });

            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
    }
};

//Servicio que elimina una Notificación
NotificationController.deleteUserCommerce = async (req, res) => {
    if (!req.headers.id) {
        res.status(400).send({ message: "Parámetros incompletos", status: "fail" });
        return;
    }
    try {
        const query = "delete from polaris_core.notifications_user_commerce  where nouc_id = " + req.headers.id + " IF EXISTS";
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.json({ message: "err", status: 'fail' });
                return;
            } else {
                req.action = "eliminar/notificación ";
                req.after = "{}";
                res.json({ message: "ok", status: 'success' });

            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error" });
    }
};

//Servicio que actualiza una Notificación
NotificationController.update = async (req, res) => {
    try {
        if (!req.headers.id) {
            res.status(400).send({ message: "incomplete parameters to make the request" });
            return;
        }
        const noti_id = req.headers.id;
        const noti_state = "Leída";
        const parameters = [noti_state, noti_id];
        var query = "update polaris_core.notifications set noti_state=? where noti_id=?";
        conection.execute(query, parameters, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.send({ message: "error in BD" });
                return;
            } else {
                res.status(200).send({ message: "success" });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.send({ message: "Not Acceptable" });
    }
};

//Servicio que actualiza una Notificación
NotificationController.updateUserCommerce = async (req, res) => {
    try {
        if (!req.headers.id) {
            res.status(400).send({ message: "incomplete parameters to make the request" });
            return;
        }
        const noti_id = req.headers.id;
        const noti_state = "Leída";
        const parameters = [noti_state, noti_id];
        var query = "update polaris_core.notifications_user_commerce set nouc_state=? where nouc_id=?";
        conection.execute(query, parameters, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.send({ message: "error in BD", status: "fail" });
                return;
            } else {
                res.status(200).send({ message: "Se actualizó con éxito", status: "success" });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.send({ message: "Not Acceptable" });
    }
};

NotificationController.getByDestToMob = async (req, res) => {

    if (!req.headers.id)
        return res.status(200).send({ message: "incomplete parameters to make the request", status: "fail", applied: "false" });

    const query = "select * from polaris_core.notifications where noti_dest = ?";

    try {
        conection.execute(query, [req.headers.id], { prepare: true }, async function (err, result) {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ message: "Query error", status: "fail", applied: "false" });
            }

            if (result.rows.length > 0) {
                let notificaciones = await funcionesGenerales.ordenarFecha(result.rows, 'noti_date_create');
                return res.status(200).send({ message: "success", status: "ok", applied: "true", notificacion: notificaciones });
            } else return res.status(200).send({ message: "success", status: "ok", applied: "true", notificacion: {} });

        });
    } catch (ex) {
        logger.error(ex.stack);
        return res.status(200).send({ message: "Catch error", status: "fail", applied: "false" });
    }
};

NotificationController.allNotifications = async () => {
    var queryConsulta = "SELECT * FROM notifications";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(queryConsulta, { prepare: true }, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve([]);
                } else resolve(result.rows); 
            });
        });
    } catch (ex) {
        logger.error(ex.stack);
        resolve([]);
    }
}

NotificationController.allNotificationsUserCommerce = async (id) => {
    var queryConsulta = "SELECT * FROM notifications_user_commerce where nouc_dest = ? AND nouc_state = 'No leída' ALLOW FILTERING; ";
    try {
        return new Promise((resolve, reject) => {
            conection.execute(queryConsulta, [id + ""], { prepare: true }, (err, result) => {
                if (err) {
                    logger.error(err.stack);
                    resolve([]);
                    return;
                } else {
                    resolve(result.rows);
                }
            });
        });
    } catch (ex) {
        logger.error(ex.stack);
        resolve([]);
    }
}
//exports
module.exports = NotificationController