const fileUploadController = {};
const conection = require("../database.js");
const c = require('colors');
var multer = require('multer');
var path = require('path');
const fs = require('fs');
const logger = require('../bin/logger');
var OS = process.platform;
var pathC = "";
var pathO = "";
var pathI = "";
var pathM = "";
var nombreImagen = "";

var pathOS = function () {
  if (OS == "win32") {
    pathC = 'C:/polaris/imagesProfiles';
    pathO = 'C:/polaris/imagesObservations';
    pathI = 'C:/polaris/imagesIncidences';
    pathM = 'C:/polaris/imagesModelsTerminals';
  } else if (OS == "linux") {
    pathC = '/opt/polaris/imagesProfiles';
    pathO = '/opt/polaris/imagesObservations';
    pathI = '/opt/polaris/imagesIncidences';
    pathM = '/opt/polaris/imagesModelsTerminals';
  }
};

pathOS();

var store = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, pathC);
  },
  filename: function (req, file, cb) {
    cb(null, nombreImagen + ".jpg");
  }
});

var upload = multer({ storage: store }).single('file');


var store2 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, pathO);
  },
  filename: function (req, file, cb) {
    cb(null, nombreImagen + path.extname(file.originalname));
  }
});

var upload2 = multer({ storage: store2 }).single('file');

var store3 = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, pathM);
  },
  filename: function (req, file, cb) {
    cb(null, nombreImagen + ".jpg");
  }
});

var upload3 = multer({ storage: store3 }).single('file');

var storage7 = multer.diskStorage({
  destination: function (req, file, callback) {
    callback(null, pathI);
  },
  filename: function (req, file, callback) {
    callback(null, file.originalname);
  }
});

var upload7 = multer({ storage: storage7 }).array('file');

fileUploadController.save = async (req, res, next) => {
  nombreImagen = req.params.identificacion;
  var urlImage = "";
  await upload(req, res, function (err) {
    if (err) {
      logger.error(err.stack);
      return res.json({ message: "archive error " + err, status: "fail" });
    }
    try {
      var imgenExtension = nombreImagen + ".jpg";
      var query = "update user set user_photo = ? WHERE user_identification = ? IF EXISTS";
      const parameter = [imgenExtension, nombreImagen];
      return new Promise((resolve, reject) => {
        conection.execute(query, parameter, { prepare: true }, (err, result) => {
          if (err) {
            logger.error(err.stack);
            return res.json({ message: "query error", status: "fail" });
          }

          urlImage = pathC + "/" + nombreImagen + ".jpg";
          //do all database record saving activity|
          return res.json({ message: "success", status: "ok", data: { nombreImagen: nombreImagen + ".jpg" } });
          //return res.json({ originalname: nombreImagen + path.extname(req.file.originalname), uploadname: req.file.filename });
        });
      });

    } catch (error) {
      logger.error(error.stack);
      return res.json({ message: "query error", status: "fail" });
    }
  });
}

fileUploadController.saveImgProfiles = async (req, res, next) => {
  datos = req.body;
  nombreImagen = datos.identificacion;
  extension = '.' + datos.photo.substring(datos.photo.indexOf('/') + 1, datos.photo.indexOf(';base64'));
  rutaC = pathC + "/" + nombreImagen + extension;
  const base64Data = datos.photo.replace(/^data:([A-Za-z-+/]+);base64,/, '');
  try {
    fs.writeFile(rutaC, new Buffer(base64Data, "base64"), function (err) {
      if (err != null) {
        logger.error(err.stack);
      }
    });
    var imgenExtension = nombreImagen + extension;
    var query = "update user set user_photo = ? WHERE user_identification = ? IF EXISTS";
    const parameter = [imgenExtension, nombreImagen];
    return new Promise((resolve, reject) => {
      conection.execute(query, parameter, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          return res.json({ message: "query error", status: "fail" });
        }
        //do all database record saving activity
        return res.json({ message: "success", status: "ok", data: { nombreImagen: nombreImagen + extension } });
      });
    });

  } catch (error) {
    logger.error(error.stack);
    return res.json({ message: "query error", status: "fail" });
  }
}

fileUploadController.saveObservations = async (req, res, next) => {
  datos = req.body;
  var nombreImagen = datos.serial + "_" + datos.identificacion + "_";
  var nombreImagenFinal = "";
  var base64Data = "";
  var data = {};
  var longitud = 1;

  if (datos.firstPhoto != "") {
    extensionImage1 = '.jpg';
    longitud++;
  }
  if (datos.secondPhoto != "") {
    extensionImage2 = '.jpg';
    longitud++;
  }

  for (var index = 1; index < longitud; index++) {
    nombreImagenFinal = "";
    nombreImagenFinal = nombreImagen + index + eval("extensionImage" + index);
    rutaC = pathO + "/" + nombreImagenFinal;
    if (index == 1) {
      base64Data = datos.firstPhoto.replace(/^data:([A-Za-z-+/]+);base64,/, '');
    } else {
      base64Data = datos.secondPhoto.replace(/^data:([A-Za-z-+/]+);base64,/, '');
    }

    fs.writeFile(rutaC, new Buffer(base64Data, "base64"), function (err) {
      if (err != null) {
        logger.error(err.stack);
      }
    });
    data['image' + index] = nombreImagenFinal;
  }
  return res.json({ message: "success", status: "ok", data: data });

}

fileUploadController.saveObservationQA = async (req, res, next) => {
  nombreImagen = req.params.serial;
  var data = nombreImagen.split("_");

  upload2(req, res, function (err) {
    if (err) {
      logger.error(err.stack);
      return res.json({ message: "query error", status: "fail", err });
    }
    try {
      const moment = require("moment");
      var date_send = moment().format("LLL");
      var imgenExtension = nombreImagen + path.extname(req.file.originalname);
      var query = "insert into  terminal_observation (teob_id,teob_description,teob_serial_terminal,teob_id_user,teob_photo,teob_fecha, teob_open) values (now(),?,?,?,?,?,?) if not exists";
      const parameter = ['', data[0], data[1], imgenExtension, date_send, '0'];
      return new Promise((resolve, reject) => {
        conection.execute(query, parameter, { prepare: true }, (err, result) => {
          if (err) {
            logger.error(err.stack);
            return res.json({ message: "query error", status: "fail" });
          }
          //do all database record saving activity
          return res.json({ message: "success", status: "ok", data: { nombreImagen: nombreImagen + path.extname(req.file.originalname) } });
          //return res.json({ originalname: nombreImagen + path.extname(req.file.originalname), uploadname: req.file.filename });
        });
      });

    } catch (error) {
      logger.error(error.stack);
      return res.json({ message: "query error", status: "fail" });
    }
  });
}

fileUploadController.saveImgModel = async (req, res, next) => {
  var data = JSON.parse(req.params.data);
  nombreImagen = data['name'];
  var urlImage = "";
  await upload3(req, res, function (err) {
    if (err) {
      logger.error(err.stack);
      return res.json({ message: "query error", status: "fail" });
    }
    try {
      var imgenExtension = nombreImagen + ".jpg";
      var query = "update terminal_model SET temo_photo = ? WHERE temo_id = ? IF EXISTS";
      const parameter = [imgenExtension, data['idmodel']];

      return new Promise((resolve, reject) => {
        conection.execute(query, parameter, { prepare: true }, (err, result) => {
          if (err) {
            logger.error(err.stack);
            return res.json({ message: "query error", status: "fail" });
          }

          urlImage = pathM + "/" + nombreImagen + ".jpg";
          return res.json({ message: "success", status: "ok", data: { nombreImagen: nombreImagen + ".jpg" } });
        });
      });

    } catch (error) {
      logger.error(error.stack);
      return res.json({ message: "query error", status: "fail" });
    }
  });
}

fileUploadController.view = async (req, res, next) => {
  var ruta = pathC + "/" + req.params.name;
  fs.access(ruta, err => {
    if (err) {
      logger.error(err.stack);
      res.sendFile(path.join(pathC + "/default.png"));
    } else {
      res.sendFile(path.join(ruta));
    }
  });
}

fileUploadController.download = async (req, res, next) => {
  var ruta = pathC + "/" + req.body.filename;
  fs.access(ruta, err => {
    if (err) {
      logger.error(err.stack);
      res.sendFile(path.join(pathC + "/default.png"));
    } else {
      res.sendFile(path.join(ruta));
    }
  });
}

fileUploadController.saveObservationsIncidencesImages = async (req, res) => {
  var data = [];
  const multiparty = require('multiparty');
  var form = new multiparty.Form();
  var count = 0;

  form.on('error', function (err) {
    logger.error('Error parsing form: ' + err.stack);
  });

  // Parts are emitted when parsing the form
  form.on('part', function (part) {
    if (!part.filename) {
      // filename is not defined when this is a field and not a file
      // ignore field's content
      part.resume();
    }

    if (part.filename) {
      // filename is defined when this is a file
      // save file
      part.pipe(fs.createWriteStream(`${pathI}/${part.filename}`))
        .on('close', () => {
          console.log(`Upload complete:  ${pathI}/${part.filename}`);
          logger.info(`Upload complete:  ${pathI}/${part.filename}`);
        });

      count++;
      data.push(`${part.filename}`);
      // ignore file's content here
      part.resume();
    }

    part.on('error', function (err) {
      // decide what to do
      logger.error(err.stack);
    });
  });

  // Close emitted after form parsed
  form.on('close', function () {
    logger.info(`Upload complete:  Todos los archivos se han cargado`);
    res.json({ message: 'Received ' + count + ' files', status: "ok", data: data });
    data = [];
  });

  form.parse(req);
  return;
}

fileUploadController.saveObservationsIncidences = async (req, res) => {
  var body = req.body;
  var arrayImagenes = body.imagenes;

  await arrayImagenes.forEach(async (image) => {
    if (image.indexOf('firma') > 0)
      await saveObservationIncidence(body.incidencia, body.identificacion, image, '1');
    else await saveObservationIncidence(body.incidencia, body.identificacion, image, '0');
  });

  return res.json({ message: "success", status: "ok", data: true });
}

async function saveObservationIncidence(incidencia, user, photo, firma) {
  const query = "INSERT INTO observation_incidence (id, obin_date, obin_incidence, obin_observation, obin_user, obin_photo, obin_firm) VALUES (now(),?,?,?,?,?,?)";
  try {
    const moment = require("moment");
    const date = moment().format("LLL");
    var parameters = [date, incidencia, '', user, photo, firma];
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error("error en el query " + err.stack);
          resolve(false);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);
  }
}


//exports
module.exports = fileUploadController;