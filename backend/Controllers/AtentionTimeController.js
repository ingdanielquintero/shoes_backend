"use strict"
const AtentionTimeController = {};
const conection = require("../database.js");
const c = require('colors');
const logger = require('../bin/logger');

AtentionTimeController.updateAtentionTime = async (req, res) => {
    const query = "update atention_time set atti_installation = ?, atti_priority =?, atti_support_and_removal =? where atti_id = ?";
    
    try {
        const parameters = [req.body.atti_installation, req.body.atti_priority, req.body.atti_support_and_removal, req.body.atti_id];
        await conection.execute(query, parameters, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: " query error" });
                return;
            }
            if (result) {
                res.status(200).send({ message: "success" });
                return;
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
        return;
    }
};

AtentionTimeController.getAll = async (req, res) => {
     
    const query = "select * from polaris_core.atention_time";
    try {
        conection.execute(query, [], { prepare: true }, function (err, result) {
            if (err) {
                logger.error(err.stack);
                res.send({ message: "query error" });
                return;
            }
            if (result.rows.length > 0) {                
                res.json(result.rows);                
            }  
              else {
                res.json({});
              }          
            
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.send({ error: "catch error" });
    }
};

//exports
module.exports = AtentionTimeController