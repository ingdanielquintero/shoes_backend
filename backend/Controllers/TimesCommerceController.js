const timesCommerceController = {};
const conection = require("../database.js");
const c = require('colors');
const logger = require('../bin/logger');

timesCommerceController.findAll = async (req, res) => {
    const query = "select * from times_by_commerce";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error" });
    }
};

timesCommerceController.getCommerceByCode = async (req, res) => {
    var cod = req.headers.cod;
    const parameter = [cod];
    const query = "select * from commerce where comm_uniqe_code = ? allow filtering";
    try {
        conection.execute(query, parameter, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error" });
    }
};

timesCommerceController.updateTimesCommerce = async (req, res) => {
    var tiempoDiagnostico = req.body.tiempoDiagnostico;
    var tiempoReparacion = req.body.tiempoReparacion;
    var codigoUnico = req.body.codigoUnico;
    const parameter = [tiempoDiagnostico, tiempoReparacion, codigoUnico];
    const query = "update times_by_commerce set tbco_diagnostic_time = ?, tbco_repair_time = ? where tbco_id = ?";
    try {
        conection.execute(query, parameter, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: " query error" });
                return;
            } else {
                res.json(true);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error" });
    }
};

timesCommerceController.insertTimesCommerce = async (req, res) => {
    var codigoUnico = req.body.codigoUnico;
    var nombre = req.body.nombre;
    var pais = req.body.pais;
    var ciudad = req.body.ciudad;
    var tiempoDiagnostico = req.body.tiempoDiagnostico;
    var tiempoReparacion = req.body.tiempoReparacion;
    const parameter = [codigoUnico, nombre, pais, ciudad, tiempoDiagnostico, tiempoReparacion];
    const query = "insert into times_by_commerce (tbco_id, tbco_name, tbco_country, tbco_city, tbco_diagnostic_time, tbco_repair_time) values(?,?,?,?,?,?)";
    try {
        conection.execute(query, parameter, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: " query error" });
                return;
            } else {
                res.json(true);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error" });
    }
};

timesCommerceController.getAllCommerce = async (req, res) => {
    const query = "select * from times_by_commerce";
    try {
        conection.execute(query, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error" });
    }
};

timesCommerceController.findAllLaboratory = async (req, res) => {
    const query = "select * from times_by_laboratory";
    try {
        conection.execute(query, [], (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: " query error" });
                return;
            } else {
                res.json(result.rows);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error" });
    }
};

timesCommerceController.updateTimesLaboratory = async (req, res) => {
    const parameter = [req.body.nombre, req.body.tiempoDiagnostico, req.body.tiempoReparacion, '1'];
    const query = " update times_by_laboratory set tbla_name = ?, tbla_diagnostic_time = ?, tbla_repair_time = ? where tbla_id = ? ";
    try {
        conection.execute(query, parameter, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ message: " query error" });
                return;
            } else {
                res.json(true);
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error" });
    }
};
//exports
module.exports = timesCommerceController;
