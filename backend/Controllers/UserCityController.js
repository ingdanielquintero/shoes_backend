const userCityController = {};
const logger = require('../bin/logger');

const conection = require('../database');

userCityController.obtenerCitysUser = async (req, res) => {
	const code = req.headers.usercode;
	var data = {};
	const query = "select * from user_city where usercode='" + code + "' ALLOW FILTERING";
	try {
		conection.execute(query, [], (err, result) => {
			if (err) {
				logger.error(err.stack);
				res.status(404).send({ message: "error in BD" });
				return;
			} else {
				data = result.rows;
				res.json(data);
			}
		});
	}
	catch (error) {
		logger.error(error.stack);
		res.status(406).send({ message: "Not Acceptable" });
	}
};

userCityController.saveUserCity = async (req, res) => {
	try {
		if (!req.body) {
			res.status(200).send({ message: "incomplete parameters to make the request", status: "fail" });
			return;
		}

		var usercode = req.headers.usercode;
		var usercity = req.body.usercity;

		usercity = JSON.parse(usercity);
		usercity = JSON.stringify(usercity);

		const parameters = [usercity, usercode];
		var query = "update polaris_core.user_city set citys =? where usercode =?";

		conection.execute(query, parameters, { prepare: true }, (err, result) => {
			if (err) {
				logger.error(err.stack);
				res.status(404).send({ message: "error in BD" });
				return;
			} else {
				res.status(200).send({ message: "success" });
				req.before = "{}";
				req.after = JSON.stringify(usercity);
			}
		});

	} catch (error) {
		logger.error(error.stack);
		res.status(200).send({ message: "catch error", status: "fail" });
	}
};

userCityController.obtenerUsuariosPais = (req, res) => {
	var pais = req.body.pais;

	getFindAll('select * from user_city', []).then(async (usersCitys) => {
		var usuarios = [];
		for (let i = 0; i < usersCitys.length; i++) {

			var usuario = await getFindAll(`select user_identification, user_name, user_position from user where user_id_user = '${usersCitys[i].usercode}'`, []);
			var ciudades = JSON.parse(usersCitys[i].citys);
			
			for (let j = 0; j < ciudades.length; j++) {
				if (ciudades[j].pais == pais) {
					usersCitys[i].user = usuario[0];
					usersCitys[i].citys = ciudades[j];
					usuarios.push(usersCitys[i]);
				}
			}

			if ((i + 1) == usersCitys.length) return res.json(usuarios);
		}
	})

}

async function getFindAll(query, parameters) {
	try {
		return new Promise((resolve, reject) => {
			conection.execute(query, parameters, (err, result) => {
				if (err) {
					logger.error(err.stack);
					resolve(false);
				} else {
					resolve(result.rows);
				}
			});
		});
	} catch (ex) {
		logger.error(ex.stack);
		resolve(false);

	}
}

module.exports = userCityController;