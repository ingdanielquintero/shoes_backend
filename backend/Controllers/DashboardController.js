const DashboardController = {};
const conection = require("../database.js");
const c = require('colors');
const logger = require('../bin/logger');

async function getFindAll(query, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve([]);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve([]);
  }

}

async function getUserCitys(usercode) {
  try {
    return new Promise(async (resolve, reject) => {
      var data = [];

      var query = "select * from user_city where usercode ='" + usercode + "'  allow filtering";
      var results = await getFindAll(query, []);

      if (results[0] != undefined) {
        var ciudades = JSON.parse(results[0].citys);
        for (let j = 0; j < ciudades.length; j++) {
          const citys = ciudades[j].ciudades;
          for (let k = 0; k < citys.length; k++) {
            const city = citys[k];
            data.push(city);
          }
        }
      }
      resolve(data);
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);
  }
}

DashboardController.graficoIncidencias = async (req, res) => {
  console.log(req.body);
  
  try {
    if (req.body.type == undefined) return res.json({ message: "catch error", status: "fail" });
    let usercitys = await getUserCitys(req.body.usercode);
    
    if (usercitys.length < 1) return res.json([]);

    let commerces = await getFindAll("select comm_uniqe_code, comm_city from commerce", []);
    let codesCommerces = [];

    commerces = commerces.filter(c => {
      if (usercitys.includes(c.comm_city)) {
        codesCommerces.push(c.comm_uniqe_code);
        return true;
      } else return false;
    });

    if (codesCommerces.length < 1) return res.json([]);

    let incidencias = await getFindAll("select * from incidence", []);
    incidencias = incidencias.filter(c => {
      return codesCommerces.includes(c.inci_code_commerce) && c.inci_type == req.body.type;
    });
    
    res.json(incidencias);
  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};

DashboardController.example = async (req, res) => {
  try {
    const moment = require("moment");

    query = "select audi_id,audi_action,audi_date from audit";
    let audits = await getFindAll(query, []);

    res.json({
      message: "success",
      fecha: moment().date() + '/' + (moment().month() + 1) + '/' + moment().year(),
      auditoria: audits
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};


//exports
module.exports = DashboardController;