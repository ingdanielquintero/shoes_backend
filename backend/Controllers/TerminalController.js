const terminalsController = {};
const conection = require("../database.js");
const c = require('colors');
const funcionesGenerales = require('../bin/funcionesGenerales.js')
const logger = require('../bin/logger.js');

//methods
terminalsController.save = async (req, res, next) => {

  try {
    const moment = require("moment");
    const date = moment().format("LLL");
    const request = req.body;
    const logueado = req.user.code;

    const parameters = [
      request.term_serial,
      request.term_brand,
      request.term_model,
      request.term_technology,
      request.term_security_seal,
      request.term_mk,
      request.term_buy_date,
      request.term_start_date_warranty,
      request.term_warranty_time,
      request.term_date_finish,
      request.term_status,
      request.term_localication,
      request.term_imei,
      request.term_num_terminal,
      request.term_status_temporal,
      date,
      logueado,
      request.term_warranty_type
    ];

    var query = "insert into terminal (term_serial, term_brand, term_model, term_technology,  term_security_seal, " +
      "term_mk, term_buy_date, term_start_date_warranty, term_warranty_time, term_date_finish, term_status, term_localication, " +
      "term_imei, term_num_terminal,term_status_temporal, term_date_register, term_register_by, term_warranty_type) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) IF NOT EXISTS";

    conection.execute(query, parameters, { prepare: true }, (err, result) => {
      if (err) {
        logger.error(err.stack);
        return res.status(404).send({ message: "error" });
      } else {
        if (result.rows) {
          if (Object.values(result.rows[0])[0]) {
            res.status(200).send({ message: "success", applied: true });
            req.action = "registrar/terminal";
            req.before = "{}";
            req.after = JSON.stringify(request);
            next();
          } else {
            return res.status(200).send({ message: "terminal already exists", applied: Object.values(result.rows[0])[0] });
          }
        } else {
          res.status(404).send({ message: "error" });
        }
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

terminalsController.find = async (req, res) => {
  const query = "select * from terminal where term_serial = ?";
  try {
    const parameters = [req.headers.id];
    conection.execute(query, parameters, async (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: "query error " });
        return;
      } else {
        if (result.rowLength > 0) {
          for (let entry of result.rows) {
            if (entry.term_technology) {
              entry.term_technology = await convertirTecnologiaLetras(entry.term_technology);
            }
          }
        }
        res.json(result.rows);
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

terminalsController.findAllQuotitation = async (req, res) => {
  try {
    const query = "select * from terminal WHERE term_status = 'COTIZACIÓN'";
    let terminals = await getFindAllNotLimit(query, []);

    if (terminals.length > 0) {
      for (let entry of terminals) {
        if (entry.term_technology) {
          if (entry.term_technology)
            entry.term_technology = await convertirTecnologiaLetras(entry.term_technology);
        }
      }
    }

    res.json(terminals);
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

terminalsController.updateGroup = async (req, res, next) => {
  const request = req.body;
  req.action = "actualizar/terminal";
  const status = req.estado;
  var auditBefore = [];
  var auditAfter = [];
  try {
    if (status === false) {
      res.status(404).send({ message: "error, terminal not exits" });
      return;
    };
    for (var j = 0; j < request.length; j++) {
      var id = request[0]['term_id'];
      let update = await updateOne(id);
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

terminalsController.update = async (req, res, next) => {
  const request = req.body;
  req.action = "actualizar/terminal";
  const status = req.estado;
  var auditBefore = [];
  var auditAfter = [];
  try {
    if (status === false) {
      res.status(404).send({ message: "error, terminal not exits" });
      return;
    };
    const row = JSON.parse(req.before);
    for (let j = 0; j < Object.keys(request).length; j++) {
      if (Object.keys(row[0])[j] === Object.keys(request)[j]) {
        if (Object.values(row[0])[j] !== Object.values(request)[j]) {
          auditBefore.push(JSON.parse('{"' + Object.keys(row[0])[j] + '":"' + Object.values(row[0])[j] + '"}'));
          auditAfter.push(JSON.parse('{"' + Object.keys(request)[j] + '":"' + Object.values(request)[j] + '"}'));
        }
      } else {
        res.status(400).send({ message: "error, wrong json syntax" });
        return;
      }
    }

    req.before = JSON.stringify(auditBefore);
    req.after = JSON.stringify(auditAfter);
    var query = "update terminal set term_brand=?, term_model=?, term_technology=?,  term_security_seal=?, term_mk=?, term_buy_date=?, term_start_date_warranty=?, term_warranty_time=?, term_date_finish=?, term_status=?, term_localication=?, term_imei=?, term_num_terminal=?,term_status_temporal=? where term_serial=? IF EXISTS";
    const parameters = [request.term_brand, request.term_model, request.term_technology, request.term_security_seal,
    request.term_mk, request.term_buy_date, request.term_start_date_warranty, request.term_warranty_time, request.term_date_finish, request.term_status, request.term_localication, request.term_imei, request.term_num_terminal, request.term_status_temporal, req.headers.id];

    conection.execute(query, parameters, { prepare: true }, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(402).send({ message: " query error" });
        return;
      } else if (result) {
        res.json({ message: "success" });
        next();
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(401).send({ error: "catch error" });
  }
};

terminalsController.delete = async (req, res, next) => {
  const status = req.estado;
  try {
    if (status === false) {
      res.status(404).send({ message: "error, terminal not exits" });
      return;
    };
    var query = "delete from terminal  where term_serial =?";
    const parameters = [req.headers.id];
    conection.execute(query, parameters, { prepare: true }, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: "query error" });
        return;
      } else {
        req.action = "eliminar/terminal";
        req.after = "{}";
        res.json({ message: "success" });
        next();
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

terminalsController.getTerminal = async (req, res, next) => {
  const query = "select * from terminal where term_serial = ?";
  var estado = false;
  try {
    conection.execute(query, [req.headers.id], { prepare: true }, function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: "query error" });
        return;
      }
      if (result.rows.length > 0) {
        req.before = JSON.stringify(result.rows);
        estado = true;
      }
      req.estado = estado;
      next();
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

terminalsController.saveSeveral = async (req, res, next) => {
  try {
    var notInserted = [];
    var totalNoInsert = [];
    var countInserted = 0;
    const moment = require("moment");
    const date = moment().format("LLL");
    const logueado = req.user.code;
    var request = req.body;
    var opt = req.body[request.length - 1]['opcion'];
    let resultStatus = await getStatus();
    let resultBrand = await getBrand();
    let resultLocation = await getLocation(opt);
    let modelos = await getFindAll('select * from terminal_model', []);

    var booleanStatus = false;
    var booleanBrand = false;
    var booleanLocation = false;
    var banderaError = false;
    var banderaInserted = false;
    var countRound = 0;
    var countError = 0;
    var resultadoInsert;
    var query = "insert into terminal (term_serial, term_brand, term_model, term_technology, term_security_seal, term_mk, " +
      "term_buy_date, term_start_date_warranty, term_warranty_time, term_date_finish, term_status, term_localication, " +
      "term_imei, term_num_terminal, term_status_temporal, term_date_register, term_register_by, term_warranty_type) " +
      "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) if not exists";
    request.pop();

    if (request.length != 0 && resultStatus.rows.length != 0 && resultBrand.rows.length != 0 && resultLocation.rows.length != 0) {
      for (i = 0; i < request.length; i++) {
        countRound++;

        var data = resultStatus.rows.find(c => { return c.get('test_description') === request[i].term_status });
        if (data) {
          booleanStatus = true;
        } else {
          notInserted.push(" estado inválida");
        }

        let booleanTerminal = modelos.find(c => c.temo_brand == request[i].term_brand && c.temo_name == request[i].term_model);
        if (!booleanTerminal) {
          notInserted.push(" modelo inválido ");
        }

        var datav = resultBrand.rows.find(c => { return c.get('tebr_name') === request[i].term_brand });
        if (datav) {
          booleanBrand = true;
        } else {
          notInserted.push(" marca inválida");
        }

        var location = "" + request[i].term_localication;
        switch (opt) {
          case "1":
            var datawarehouse = resultLocation.rows.find(c => { return c.get('ware_id_warehouse') === location && c.get('ware_status') == 'ACTIVO' });
            if (datawarehouse) {
              booleanLocation = true;
            } else {
              notInserted.push(" localizacion inválida");
            }

            break;
          case "2":
            var datacomerce = resultLocation.rows.find(c => { return c.get('comm_uniqe_code') === location });
            if (datacomerce) {
              booleanLocation = true;
            } else {
              notInserted.push(" localizacion inválida");
            }
            if (request[i].term_status != 'OPERATIVO') {
              notInserted.push(" El estado no es operativo");
              booleanLocation = false;
            }
            break;
          case "3":
            var listaTecnicos = resultLocation.rows.find(c => { return (c.get('user_position') === 'TÉCNICO EN CAMPO') && c.get('user_id_user') === location });
            if (listaTecnicos) {
              booleanLocation = true;
            } else {
              notInserted.push(" localizacion inválida");
            }
            break;
        }


        if (request[i].term_serial == null && request[i].term_serial == undefined) {
          booleanSerial = false;
          notInserted.push(" El Serial esta vacío");
        }

        if (booleanLocation && booleanBrand && booleanTerminal && booleanStatus) {
          const parameters = [
            "" + request[i].term_serial, "" + request[i].term_brand,
            "" + request[i].term_model,
            "" + request[i].term_technology,
            "" + request[i].term_security_seal,
            "" + request[i].term_mk,
            "" + request[i].term_buy_date,
            "" + request[i].term_start_date_warranty,
            "" + request[i].term_warranty_time,
            "" + request[i].term_date_finish,
            "" + request[i].term_status,
            "" + request[i].term_localication,
            "" + request[i].term_imei,
            "" + request[i].term_num_terminal,
            "0",
            date,
            logueado,
            request[i].term_warranty_type
          ];

          resultadoInsert = await getRelaizarInsert(query, parameters);

          if (resultadoInsert) {
            banderaInserted = true;
          }

          if (banderaError) {
            countError++;
            banderaError = false;
          }

          if (banderaInserted) {
            countInserted++;
            banderaInserted = false;
          } else {
            notInserted.push(" El Serial ya existe");
            totalNoInsert.push("Fila ==>  " + (i + 2) + notInserted);
          }

          booleanTechnology = false;
          booleanLocation = false;
          booleanBrand = false;
          booleanStatus = false;
          booleanNumber = false;
          booleanSerial = false;
          notInserted = [];

        } else {
          totalNoInsert.push("Fila ==>  " + (i + 2) + notInserted);
          booleanTechnology = false;
          booleanLocation = false;
          booleanBrand = false;
          booleanStatus = false;
          booleanNumber = false;
          booleanSerial = false;
          notInserted = [];
        }

        if ((countRound % 100) == 0) {
          console.log("cargando terminales ... ", countRound);
        }
      }
      if (countRound == request.length) {
        res.status(200).send({ message: "Resultados de el cargue ", response: totalNoInsert, countInserted: countInserted });
        return;
      }
    } else {
      res.status(200).send({ error: "Ha ocurrido un problema, alguno de los valores en la consulta esta vacio." });
    }


  } catch (error) {
    logger.error(error.stack);
  }
};

//ubicacion de bodega
async function getLocation(opt) {
  var resultLocation;
  switch (opt) {
    case "1":
      var queryConsulta = "select ware_id_warehouse,ware_status from warehouse";
      try {
        return new Promise((resolve, reject) => {
          conection.execute(queryConsulta, { prepare: true }, (err, result) => {
            if (err) {
              logger.error(err.stack);
              return;
            } else {
              resultLocation = result;
              resolve(resultLocation);
            }
          });
        });
      } catch (ex) {
        logger.error(ex.stack);
        res.status(402).send({ error: "catch error 2" });
      }
      break;
    case "2":
      var queryConsulta = "select comm_uniqe_code from commerce";
      try {
        return new Promise((resolve, reject) => {
          conection.execute(queryConsulta, { prepare: true }, (err, result) => {
            if (err) {
              logger.error(err.stack);
              return;
            } else {
              resultLocation = result;
              resolve(resultLocation);
            }
          });
        });
      } catch (ex) {
        logger.error(ex.stack);
        res.status(402).send({ error: "catch error 2" });
      }
      break;
    case "3":
      var queryConsulta = "select * from user";
      try {
        return new Promise((resolve, reject) => {
          conection.execute(queryConsulta, { prepare: true }, (err, result) => {
            if (err) {
              logger.error(err.stack);
              return;
            } else {
              resultLocation = result;
              resolve(resultLocation);
            }
          });
        });
      } catch (ex) {
        logger.error(ex.stack);
        res.status(402).send({ error: "catch error 2" });
      }
      break;
  }
}

// marca de marcas
async function getBrand() {
  var resultBrand;
  var queryConsulta = "select tebr_name from terminals_brand";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          return;
        } else {
          resultBrand = result;
          resolve(resultBrand);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(402).send({ error: "catch error 4" });
  }
}



// estado de estados
async function getStatus() {
  var resultStatus;
  var queryConsulta = "select test_description from terminal_state";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          return;
        } else {
          resultStatus = result;
          resolve(resultStatus);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(402).send({ error: "catch error 5" });
  }
}

// estado de serial
async function getModelo(brand, name) {
  var parameters = [brand, name];
  var query = "select * from terminal_model where temo_brand = ? and temo_name = ? allow filtering;";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          if (result.rowLength > 0) {
            resolve(true);
          } else {
            resolve(false);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}


// estado de serial
async function getSerial(serial) {
  var parameters = [serial];
  var query = "select term_serial from terminal where term_serial = ?";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          if (result.rowLength > 0) {
            resolve(true);
          } else {
            resolve(false);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}

// estado de numero
async function getNumber(number) {
  var parameters = [number];
  var query = "select * from terminal where term_num_terminal = ? allow filtering";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          if (result.rowLength > 0) {
            resolve(true);
          } else {
            resolve(false);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}

terminalsController.findTerminalsAssociatedDiagnosis = async (req, res) => {
  try {
    const request = req.body;

    if (!request || !request.code)
      return res.json({ message: "incomplete petition", status: "fail" });

    const query = "select * from terminal where term_status=? AND term_localication=? ALLOW FILTERING;";
    const parameters = ['PREDIAGNÓSTICO', request.code];
    let technologies = await getFindAll("select * from technology", []);
    let terminals = getFindAllNotLimit(query, parameters);

    for (let i = 0; i < terminals.length; i++) {
      const terminal = terminals[i];
      var tecnologias = terminal.term_technology.split(",");
      let dato = '';

      for (let i = 0; i < technologies.length; i++) {
        let element = technologies[i];
        if (tecnologias.includes(element.tech_code)) {
          dato += element.tech_description + ",";
        }
      }
      terminal.term_technology = dato.substr(0, dato.length - 1);
    }

    res.json({ message: "success", status: "ok", data: { terminales: terminals } });
  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};

terminalsController.findAllTipesValidatorTerminal = (req, res) => {
  const query = "select * from terminal_types_validation";
  try {
    const parameters = [];
    conection.execute(query, parameters, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.json({ message: "query error", status: "fail", data: { tipificaciones: [] } });
        return;
      } else {
        res.json({ message: "success", status: "ok", data: { tipificaciones: result.rows } });
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail", data: { tipificaciones: [] } });
  }
};

terminalsController.findAllValidatorTerminal = async (req, res) => {
  const query = "select * from terminal_validation ";
  try {
    const parameters = [];
    conection.execute(query, parameters, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.json({ message: "query error", status: "fail" });
        return;
      } else {
        res.json({ message: "success", status: "ok", data: { validaciones: result.rows } });
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};

terminalsController.findTerminalsObservation = async (req, res) => {
  const query = "select teob_id,teob_description,teob_fecha,teob_id_user,teob_photo,teob_serial_terminal from terminal_observation where teob_serial_terminal=? and teob_open='0' allow filtering";
  const request = req.body;
  try {
    if (!request || !request.serial) {
      res.json({ message: "incomplete petition", status: "fail" });
    } else {
      const parameters = [request.serial];
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          res.json({ message: "query error", status: "fail" });
          return;
        } else {
          res.json({ message: "success", status: "ok", data: { observaciones: result.rows } });
        }
      });
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};

terminalsController.findTerminalsSpares = async (req, res) => {
  try {
    if (!req.body.model) return res.json({ message: "incomplete petition", status: "fail" });

    const modelo = req.body.model;

    getFindAllNotLimit("select * from spare", []).then(async (rows) => {
      let repuestos = [];
      for (let i = 0, long = rows.length; i < long; i++) {
        const repuesto = rows[i];
        if (repuesto.spar_terminal_model.includes(modelo))
          repuestos.push(repuesto);
      }

      repuestos = await funcionesGenerales.eliminarDuplicadosPorAtri(repuestos, 'spar_code');
      repuestos = await funcionesGenerales.ordenarAcendente(repuestos, 'spar_code');
      return res.json({ message: "success", status: "ok", data: { repuestos: repuestos } });
    });

  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};

terminalsController.findTerminalsBySerial = async (req, res) => {
  const query = "SELECT * FROM terminal WHERE term_serial = ? AND term_status ='NUEVO' ALLOW FILTERING";
  const query2 = "SELECT * FROM terminal WHERE term_serial = ? AND term_status ='OPERATIVO' ALLOW FILTERING";
  const request = req.body;
  var respuesta = [];
  try {
    if (!request) {
      res.status(401).send({ message: "incomplete petition" });
    } else {
      const parameters = [request['serial']];
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          res.status(402).send({ message: " query error" });
          return;
        } else {
          respuesta.push(result.rows);
          conection.execute(query2, parameters, (err, result) => {
            if (err) {
              logger.error(err.stack);
              res.status(402).send({ message: " query error" });
              return;
            } else {
              respuesta.push(result.rows);
              res.json(respuesta);
            }
          });
        }
      });
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.status(403).send({ error: "catch error" });
  }
};

terminalsController.changeLocation = async (req, res) => {
  const moment = require("moment");
  const date = moment().format("LLL");
  const logueado = req.user.code;
  const changeTerminal = req.body['data'][0];
  const changeSimCard = req.body['data'][1];
  const changeAccessory = req.body['data'][2];
  const changeTechnical = req.body['data'][3];
  const modoAsignacion = req.body['data'][4];
  var resultado = [];

  if (changeTerminal.length != 0) resultado.push(await getChangeTerminal(changeTerminal, changeTechnical, modoAsignacion));
  if (changeSimCard.length != 0) resultado.push(await getChangeSimCard(changeSimCard, changeTechnical));
  if (changeAccessory.length != 0) resultado.push(await getChangeAccessory(changeAccessory, changeTechnical, date, logueado, modoAsignacion));

  res.status(200).send({ message: "Asignación completa", result: resultado });
};

async function getChangeTerminal(terminal, destino, caso) {
  var resultChange = [];
  if (caso == '1') {
    var query = "update terminal set term_localication = ? WHERE term_serial = ?";
  } else {
    var query = "update terminal set term_localication = ?, term_status = 'PREDIAGNÓSTICO' WHERE term_serial = ?";
  }
  for (i = 0; i < terminal.length; i++) {
    const parameter = [destino, terminal[i].term_serial];
    try {
      conection.execute(query, parameter, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          return;
        } else {
          resultChange.push("Cambio exitoso" + terminal[i].term_serial);
        }
      });
    } catch (ex) {
      res.status(402).send({ error: "catch error 5" });
    }
    if ((i + 1) == terminal.length) {
      return new Promise((resolve, reject) => {
        resolve(resultChange);
      });
    }
  }
}

terminalsController.updateStatus = async (req, res, next) => {
  try {
    request = req.body;

    var query = "update terminal set term_status=? ,term_localication =?, term_num_terminal=? where term_serial=? IF EXISTS";
    parameters = [request.term_status, request.term_localication, '0', request.term_serial];
    conection.execute(query, parameters, function (err, result) {
      if (err) {
        logger.error(err.stack);
        return;
      } else {
        if (Object.values(result.rows[0])[0] === true) {
          if (request.term_status === "DADO DE BAJA") {
            req.action = "dar de baja/terminal";
            req.after = JSON.stringify({ observacion: req.headers.observation, terminal: req.body });
          } else {
            req.action = "actualizar/terminal";
            req.after = JSON.stringify(req.body);
          }

          res.json({ "respuesta": true });
        } else {
          res.json({ "respuesta": false });
        }
        next();
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
}

terminalsController.saveDiagnosis = async (req, res) => {

  try {
    var errores = [null, '', undefined, ' '];
    const request = req.body;
    const moment = require("moment");
    const fecha = (moment().date()) + "/" + (moment().month() + 1) + "/" + moment().year();
    const logueado = req.user.code;

    var queryUser = "select * from user_terminals where uste_user in (?) and uste_date = ?;";
    const parametersUser = [logueado, fecha];
    let terminalesCompletadas = await getFindAll(queryUser, parametersUser);

    if (terminalesCompletadas === false)
      res.json({ message: "invalid user", status: "fail" });

    if (terminalesCompletadas.length > 0)
      terminalesCompletadas = parseInt(terminalesCompletadas[0]['uste_completed_terminals']) + 1;

    var ok = false;

    if (request.validaciones && request.tipificaciones && request.reparable && request.observacion) {
      if (request.reparable == 'SI') {
        if (request.falla) {
          ok = await saveValidation(request, '1', logueado);

          if (!ok) { res.json({ message: "ocurrio un error al guardar las validaciones", status: "fail" }); }
          ok = await saveTipificacion(request, '1');

          if (!ok) { res.json({ message: "ocurrio un error al guardar las tipificaciones", status: "fail" }); }
          if (request.repuestos) {
            request.repuestos.type = 'SOLICITADOS';
            ok = await saveSparesTerminalLog(request, '1');
            if (!ok) { res.json({ message: "ocurrio un error al guardar os repuestos", status: "fail" }); }
          }

          if (request.falla == 'USO') { ok = await updateStatusss(request.observacion.teob_serial_terminal, 'COTIZACIÓN'); }
          if (request.falla == 'FABRICA') {
            let queryTerminal = "SELECT * FROM  terminal where term_serial = ?;";
            let terminalGarantia = await getFindAll(queryTerminal, [request.observacion.teob_serial_terminal]);

            if (terminalGarantia.length > 0) {
              if (!errores.includes(terminalGarantia[0]['term_date_finish'])) {
                var fechaGarantia = terminalGarantia[0]['term_date_finish'];

                if (new Date().getTime() <= new Date(fechaGarantia).getTime()) {
                  ok = await updateStatusss(request.observacion.teob_serial_terminal, 'GARANTÍA');
                } else {
                  ok = await updateStatusss(request.observacion.teob_serial_terminal, 'COTIZACIÓN');
                }
              } else {
                ok = await updateStatusss(request.observacion.teob_serial_terminal, 'COTIZACIÓN');
              }
            } else {
              return res.json({ message: "No se encontro el terminal", status: "fail" });
            }
          }

          if (request.falla !== 'USO' && request.falla !== 'FABRICA') {
            res.json({ message: "dato erroneo en la falla", status: "fail" });
            return;
          }

          ok = await saveObservacion(request, req.user.code);

          if (!ok) { res.json({ message: "ocurrio un error al guardar la observacion", status: "fail" }); }
          res.json({ message: "success", status: "ok" });
        } else {
          res.json({ message: "faltan parametros en la peticion", status: "fail" });
        }

      } else {
        if (request.reparable == 'NO') {
          ok = await saveValidation(request, '1', logueado);
          if (!ok) res.json({ message: "ocurrio un error al guardar las validaciones", status: "fail" });
          ok = await saveTipificacion(request, '1');
          if (!ok) res.json({ message: "ocurrio un error al guardar las tipificaciones", status: "fail" });
          ok = await saveObservacion(request, req.user.code);
          if (!ok) res.json({ message: "ocurrio un error al guardar la observacion", status: "fail" });
          ok = await updateStatusss(request.observacion.teob_serial_terminal, 'DIAGNÓSTICO');


          // Se guarda la información para la historia clinica de la terminal por DIAGNOSTICO
          console.log(ok);

          res.json({ message: "success", status: "ok" });
        } else {
          res.json({ message: "parametros incompletos", status: "fail" });
        }

      }

      request.usuario = logueado;
      request.serial = request.observacion.teob_serial_terminal;
      request.estado = 'DIAGNÓSTICO';
      request.open = true;
      insertClinicHistory(request, 'DIAGNÓSTICO');

    } else {
      res.json({ message: "faltan parametros en la peticion", status: "fail" });
    }
  }
  catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};

async function saveValidation(request, codigo, logueado) {
  try {
    const moment = require("moment");
    const date = moment().format("LLL");
    return new Promise((resolve, reject) => {
      var datos = '';
      var query = "insert into  terminal_validation_status (tevs_terminal_serial,tevs_terminal_validation,tevs_status,tevs_date,tevs_open,tevs_user) values (?,?,?,?,?,?)";
      var info = '';
      var serial = '';
      if (request.validaciones) {
        datos = request.validaciones;
        serial = datos[0].tevs_terminal_serial;
        for (let i = 0; i < datos.length; i++) {
          info += datos[i]['tevs_terminal_validation'] + '-' + datos[i]['tevs_status'] + ',';
        }
        info = info.substr(0, info.length - 1);
      } else {
        resolve(false);
      }
      parameters = [serial, info, codigo, date, '0', logueado];
      conection.execute(query, parameters, function (err, result) {
        if (err) {
          logger.error(err.stack);
          resolve(false);
          return;
        } else {
          resolve(true);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);
  }
}

async function saveTipificacion(request, codigo) {
  try {
    const moment = require("moment");
    const date = moment().format("LLL");
    return new Promise((resolve, reject) => {
      var datos = '';
      var query = "insert into  terminal_types_states (tets_terminal_serial,tets_terminal_type_validation,tets_status,tets_date,tets_open) values (?,?,?,?,?)";
      var info = '';
      var serial = '';
      if (request.tipificaciones) {
        datos = request.tipificaciones;
        serial = datos[0].tets_terminal_serial;
        for (let i = 0; i < datos.length; i++) {
          info += datos[i]['tets_terminal_type_validation'] + ',';
        }
        info = info.substr(0, info.length - 1);
      } else {
        resolve(false);
      }

      parameters = [serial, info, codigo, date, '0'];
      conection.execute(query, parameters, function (err, result) {
        if (err) {
          logger.error(err.stack);
          resolve(false);
          return;
        } else {
          resolve(true);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);
  }
}

async function saveObservacion(request, user) {
  try {
    const moment = require("moment");
    const date = moment().format("LLL");
    const logueado = user;
    return new Promise((resolve, reject) => {
      if ((request.observacion.teob_description === '' || request.observacion.teob_description === ' ') && (request.observacion.teob_photo === '' || request.observacion.teob_photo === ' ')) {
        resolve(false);
      }
      var query = "insert into  terminal_observation (teob_id,teob_description,teob_serial_terminal,teob_id_user,teob_photo,teob_fecha,teob_date,teob_open) values (now(),?,?,?,?,?,?,?) if not exists";
      parameters = [request.observacion.teob_description, request.observacion.teob_serial_terminal, logueado, '', date, date, '0']

      if (request.observacion.teob_description !== '' && request.observacion.teob_description !== ' ') {
        conection.execute(query, parameters, function (err, result) {
          if (err) {
            logger.error(err.stack);
            resolve(false);
          } else {
            if (Object.values(result.rows[0])[0] === true) {
            } else {
              resolve(false);
            }
          }
        });
      }
      if (request.observacion.teob_photo && request.observacion.teob_photo !== '' && request.observacion.teob_photo !== ' ') {
        var fotos = request.observacion.teob_photo.split('/');
        for (let i = 0; i < fotos.length; i++) {
          var queryX = "insert into  terminal_observation (teob_id,teob_description,teob_serial_terminal,teob_id_user,teob_photo,teob_fecha,teob_date,teob_open) values (now(),?,?,?,?,?,?,?) if not exists";
          var parametersX = ['', request.observacion.teob_serial_terminal, logueado, fotos[i], date, date, '0'];
          conection.execute(queryX, parametersX, function (err, result) {
            if (err) {
              resolve(false);
            } else {
              if (Object.values(result.rows[0])[0] === true) {
              } else {
                resolve(false);
              }
            }
          });
        }

      }
      resolve(true);
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);
  }
}

async function updateStatusss(serial, status) {
  try {
    return new Promise((resolve, reject) => {
      var query = "update terminal set term_status=? where term_serial=? IF EXISTS";
      parameters = [status, serial]
      conection.execute(query, parameters, function (err, result) {
        if (err) {
          logger.error(err.stack);
          resolve(false);
          return;
        } else {
          if (Object.values(result.rows[0])[0] === true) {
            resolve(true);
          } else {
            resolve(false);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);
  }
};

async function saveSparesTerminalLog(request, codigo) {
  try {
    const moment = require("moment");
    const date = moment().format("LLL");
    return new Promise((resolve, reject) => {
      var datos = '';
      var query = "insert into terminal_spares_warranty (tesw_id,tesw_serial,tesw_repuestos,tesw_status,tesw_date,tesw_open, tesw_type) values (now(),?,?,?,?,?,?)";
      var info = '';
      if (request.repuestos.tesw_repuestos) {
        datos = request.repuestos.tesw_repuestos;
        for (let i = 0; i < datos.length; i++) {
          info += datos[i]['codigo'] + '-' + datos[i]['nombre'] + '-' + datos[i]['cantidad'] + '-' + datos[i]['bodega'] + ',';
        }
        info = info.substr(0, info.length - 1);
      } else {
        resolve(false);
      }
      parameters = [request.repuestos.tesw_serial, info, codigo + "", date, '0', request.repuestos.type];
      conection.execute(query, parameters, function (err, result) {
        if (err) {
          console.log(err)
          resolve(false);
          return;
        } else {
          resolve(true);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);

    resolve(false);
  }
};

async function getChangeSimCard(simCard, destino) {
  var resultChange = [];
  var query = "update sim_card set sica_location = ?, sica_status = ? WHERE sica_serial = ?";
  for (let i = 0; i < simCard.length; i++) {
    const parameter = [destino, simCard[i].sica_status, simCard[i].sica_serial];
    try {
      conection.execute(query, parameter, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          return;
        } else {
          resultChange.push("Cambio exitoso" + simCard[i].sica_serial);
        }
      });
      if ((i + 1) == simCard.length) {
        return new Promise((resolve, reject) => {
          resolve(resultChange);
        });
      }
    } catch (ex) {
      logger.error(ex.stack);
      resolve([]);
    }
  }
}

async function getRealizarUbicacionAnterior(accesorios) {
  return new Promise(async (resolve, reject) => {
    var queryUpdate = "update accessory set acce_quantity = ? WHERE acce_id = ?";
    var resultadoRealizarUbicacionAnterior = [];

    for (i = 0; i < accesorios.length; i++) {
      var accesorio = accesorios[i];
      accesorio = await getFindAll('select * from accessory where acce_id = ?', [accesorio.accesorio.acce_id]);

      if (accesorio.length > 0) {
        var cantidad = "" + ((parseInt(accesorio[0].acce_quantity)) - (parseInt(accesorios[i].cantidad)));
        const parameterUpdate = [cantidad, accesorio[0].acce_id];
        try {
          await conection.execute(queryUpdate, parameterUpdate, { prepare: true }, (err, result) => {
            if (err) {
              logger.error(err.stack);
              resolve(false);
            } else {
              resultadoRealizarUbicacionAnterior.push("Ubicacion anterior Actualizada");
            }
          });
        } catch (ex) {
          logger.error(ex.stack);
          resolve(false);
        }
      }
    }

    resolve(resultadoRealizarUbicacionAnterior);
  });
}

function getChangeAccessory(accesorios, destino, date, logueado, tipo) {
  return new Promise(async (resolve, reject) => {
    try {
      var realizarUbicacionAnterior = await getRealizarUbicacionAnterior(accesorios);
      var resultadoAsigacionAccesorio = [];

      for (i = 0; i < accesorios.length; i++) {
        var accesorio = accesorios[i];
        var querySelect = "select * from accessory where acce_code = ? and acce_status = ? and acce_warehouse = ? allow filtering";
        var parametersSelect = [];

        parametersSelect = (tipo == '1' ?
          [accesorio.accesorio.acce_code, accesorio.accesorio.acce_status, destino]
          : [accesorio.accesorio.acce_code, accesorio.estadoNuevo, destino]);

        let dataResultados = await getIfExists(querySelect, parametersSelect);

        if (dataResultados.length > 0) {
          var idUpdate = dataResultados[0]['acce_id'];
          var quantity = `${parseInt(accesorio.cantidad) + parseInt(dataResultados[0]['acce_quantity'])}`;
          var parametersUpdate = [quantity, idUpdate];
          var queryUpdate = "update accessory set acce_quantity = ? where acce_id = ? if exists";

          let resultadoUpdate = await getRealizarActualizacion(queryUpdate, parametersUpdate);

          if (resultadoUpdate) resultadoAsigacionAccesorio.push(resultadoUpdate);

        } else {
          var parameterInsert = [];

          if (tipo == '1') {
            parameterInsert = [
              `${accesorios[i].accesorio.acce_code}`,
              `${accesorios[i].accesorio.acce_terminal_model}`,
              `${accesorios[i].accesorio.acce_name}`,
              `${accesorios[i].accesorio.acce_status}`,
              `${accesorios[i].cantidad}`,
              date,
              `${destino}`,
              logueado
            ];
          } else {
            parameterInsert = [
              `${accesorios[i].accesorio.acce_code}`,
              `${accesorios[i].accesorio.acce_terminal_model}`,
              `${accesorios[i].accesorio.acce_name}`,
              `${accesorios[i].estadoNuevo}`,
              `${accesorios[i].cantidad}`,
              date,
              `${destino}`,
              logueado
            ];
          }

          var queryInsert = "insert into accessory (acce_id,acce_code,acce_terminal_model,acce_name,acce_status,acce_quantity, " +
            "acce_date_register,acce_warehouse,acce_register_by) values (now(),?,?,?,?,?,?,?,?) if not exists";

          let resultadoInsert = await getRelaizarInsert(queryInsert, parameterInsert);

          if (resultadoInsert) resultadoAsigacionAccesorio.push(resultadoInsert);
        }

        if ((i + 1) == accesorios.length) resolve(resultadoAsigacionAccesorio, realizarUbicacionAnterior);
      }

    } catch (ex) {
      logger.error(ex.stack);
      resolve([]);

    }
  });
}

terminalsController.updateStatus2 = async (req, res) => {
  try {
    var validate = [undefined, null];
    var request = req.body;
    var query = "update terminal set term_status=?, term_status_temporal=? ,term_localication =? ";
    var parameters = [request.term_status, request.term_status_temporal, request.term_localication];

    if (!validate.includes(request.term_warranty_type)) {
      query += ", term_warranty_type =?";
      parameters.push(request.term_warranty_type);
    }

    if (!validate.includes(request.term_date_finish) && !validate.includes(request.term_start_date_warranty) && !validate.includes(request.term_warranty_time)) {
      query += ", term_date_finish = ?, term_start_date_warranty = ?, term_warranty_time = ?";
      parameters.push(request.term_date_finish);
      parameters.push(request.term_start_date_warranty);
      parameters.push(request.term_warranty_time);
    }

    query += "where term_serial=? IF EXISTS";
    parameters.push(request.term_serial);


    conection.execute(query, parameters, function (err, result) {
      if (err) {
        logger.error(err.stack);
        return;
      } else {
        if (Object.values(result.rows[0])[0] === true) {
          if (request.term_user_reparation != null) {
            var queryUpdate = "update terminal set term_user_reparation ='" + request.term_user_reparation + "' where term_Serial = '" + request.term_serial + "';"
            actualizarTerm_user_reparation(queryUpdate, []);
          }
          res.json({ "respuesta": true });
        } else {
          res.json({ "respuesta": false });
        }
      }
      // res.json({ "respuesta": false });      
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
}

async function getRelaizarInsert(query, parameters) {
  return new Promise((resolve) => {
    try {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          resolve(Object.values(result.rows[0])[0]);
        }
      });
    } catch (ex) {
      logger.error(ex.stack);
      resolve(false);
    }
  });
}

terminalsController.findAll = async (req, res) => {
  getFindAllNotLimit('select * from terminal', []).then(async (terminals) => {
    let technologies = await getFindAll("select * from technology", []);

    for (let i = 0; i < terminals.length; i++) {
      const terminal = terminals[i];
      var tecnologias = terminal.term_technology.split(",");
      let dato = '';

      for (let i = 0; i < technologies.length; i++) {
        let element = technologies[i];
        if (tecnologias.includes(element.tech_code)) {
          dato += element.tech_description + ",";
        }
      }
      terminal.term_technology = dato.substr(0, dato.length - 1);
    }

    res.json(terminals);
  })
};

async function getIfExists(querySelect, parametersSelect) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(querySelect, parametersSelect, (err, result) => {
        if (err) {
          logger.error(err.stack);
          console.log("error al Consultar ", err);
          resolve([]);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}

async function getRealizarActualizacion(query, parameters) {
  return new Promise((resolve) => {
    try {
      conection.execute(query, parameters, (err) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        }
        else resolve(true);
      });
    } catch (ex) {
      logger.error(ex.stack);
      resolve(false);
    }
  });
}

//SERVICIOS PARA EL CONTROL DE TERMINALES

terminalsController.getTerminalObservations = async (req, res) => {
  try {
    if (!req.headers) {
      res.status(400).send({ message: "incomplete parameters to make the request" });
      return;
    }
    const request = req.headers;
    const parameters = [request.id];
    var query = "select * from terminal_observation where teob_serial_terminal = ? and teob_open= '0' allow filtering;";
    conection.execute(query, parameters, { prepare: true }, (err, result) => {

      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: "error in BD", err });
        return;
      } else {
        res.status(200).send(result.rows);

      }
    });

  }
  catch (error) {
    logger.error(error.stack);
    res.status(406).send({ message: "Not Acceptable" });
  }
};


terminalsController.getTerminalSparesWarranty = async (req, res) => {
  try {
    if (!req.headers) {
      res.status(400).send({ message: "incomplete parameters to make the request" });
      return;
    }
    const request = req.headers;
    const parameters = [request.id];
    var query = "select * from terminal_spares_warranty where tesw_serial = ? and tesw_open= '0' allow filtering;";
    conection.execute(query, parameters, { prepare: true }, (err, result) => {

      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: "error in BD", err });
        return;
      } else {
        res.status(200).send(result.rows);
        req.action = "obtener garantia repuesto/terminal";
      }
    });

  }
  catch (error) {
    logger.error(error.stack);
    res.status(406).send({ message: "Not Acceptable" });
  }
};


terminalsController.getTerminalValidationStatus = async (req, res) => {
  try {
    if (!req.headers) {
      res.status(400).send({ message: "incomplete parameters to make the request" });
      return;
    }
    const request = req.headers;
    const parameters = [request.id];
    var query = "select * from terminal_validation_status where tevs_terminal_serial = ? and tevs_open='0' allow filtering;";
    conection.execute(query, parameters, { prepare: true }, (err, result) => {

      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: "error in BD", err });
        return;
      } else {
        res.status(200).send(result.rows);
        req.action = "obtener estado de validación/terminal";
      }
    });

  }
  catch (error) {
    logger.error(error.stack);
    res.status(406).send({ message: "Not Acceptable" });
  }
};

terminalsController.getTerminalTypesStatus = async (req, res) => {
  try {
    if (!req.headers) {
      res.status(400).send({ message: "incomplete parameters to make the request" });
      return;
    }
    const request = req.headers;
    const parameters = [request.id];
    var query = "select * from terminal_types_states where tets_terminal_serial = ? and tets_open = '0' allow filtering;";
    conection.execute(query, parameters, { prepare: true }, (err, result) => {

      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: "error in BD", err });
        return;
      } else {
        res.status(200).send(result.rows);
      }
    });

  }
  catch (error) {
    logger.error(error.stack);
    res.status(406).send({ message: "Not Acceptable" });
  }
};

terminalsController.getAllTerminalTypesStatus = (req, res) => {
  try {
    if (!req.body) {
      return res.status(400).send({ message: "incomplete parameters to make the request" });
    }

    var query = "select * from terminal_types_states";

    conection.execute(query, [], { prepare: true }, (err, result) => {
      if (err) {
        logger.error(err.stack);
        return res.status(404).send([]);
      } else {
        res.status(200).send(result.rows);
      }
    });
  }

  catch (error) {
    logger.error(error.stack);
    res.status(406).send({ message: "Not Acceptable" });
  }
};

terminalsController.getTerminalAndSparesAsigned = async (req, res) => {
  const query = "select * from terminal where term_localication= ? allow filtering;";
  const queryF = "select * from spare where spar_warehouse= ? allow filtering";
  const request = req.body;

  try {
    if (!request || !request.user) {
      res.json({ message: "incomplete petition", status: "fail" });
    } else {
      const parameters = [request.user];
      let terminals = await getFindAllNotLimit(query, parameters);
      let spares = await getFindAllNotLimit(queryF, parameters);
      if (terminals === false || spares === false) {
        res.json({ message: "invalid user", status: "fail" });
        return;
      }

      if (terminals.length > 0) {
        for (let entry of terminals) {
          if (entry.term_technology) {
            entry.term_technology = await convertirTecnologiaLetras(entry.term_technology);
          }
        }
      }
      res.json({ message: "success", status: "ok", data: { terminales: terminals, repuestos: spares } });
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};

function getFindAll(query, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);

  }
}

terminalsController.saveObservations = async (req, res, next) => {
  try {
    request = req.body;
    if (!request.teob_serial_terminal || !request.teob_description || !request.teob_id_user) {
      return res.json({ message: "incomplete petition", status: "fail" });
    }
    json = { observacion: request };
    let register = await saveObservacion(json, request['teob_id_user']);
    if (register) {
      return res.status(200).send({ message: "success", status: "ok" });
    } else {
      return res.status(200).send({ message: "fail", status: "fail" });
    }
  } catch (ex) {
    logger.error(ex.stack);
    return res.status(200).send({ message: "catch error", status: "fail" });
  }
}

terminalsController.findTerminalsAssociateRepairs = async (req, res) => {
  try {

    const request = req.body;
    if (!request || !request.code) {
      res.json({ message: "incomplete petition", status: "fail" });
    } else {

      const parameters = ['REPARACIÓN', request.code];
      const query = "select * from terminal where term_status=? AND term_localication=? ALLOW FILTERING;";

      terminales = await getFindAllNotLimit(query, parameters);

      if (terminales === false)
        res.json({ message: "query error", status: "fail" });

      terminales = terminales.filter(c => { return !c.term_user_reparation || c.term_user_reparation === '' });

      if (terminales.length > 0) {
        for (let entry of terminales) {
          if (entry.term_technology) {

            const queryTipificaciones = "select tets_terminal_type_validation from terminal_types_states where tets_terminal_serial = ? and tets_status=? allow filtering;";
            let tipificaciones = await getFindAll(queryTipificaciones, [entry.term_serial, '1']);

            if (tipificaciones.length > 0)
              tipificaciones = tipificaciones[0]['tets_terminal_type_validation'];

            const queryValidaciones = "select tevs_terminal_validation from terminal_validation_status where tevs_terminal_serial = ? and tevs_status= ? allow filtering;";
            let parametersV = [entry.term_serial, '3'];
            let validaciones = await getFindAll(queryValidaciones, parametersV);

            if (validaciones.length == 0) {
              parametersV = [entry.term_serial, '2'];
              validaciones = await getFindAll(queryValidaciones, parametersV);
            }

            if (validaciones.length == 0) {
              parametersV = [entry.term_serial, '1'];
              validaciones = await getFindAll(queryValidaciones, parametersV);
            }

            if (validaciones.length > 0) validaciones = validaciones[0]['tevs_terminal_validation'];

            const queryRepuestos = "select tesw_repuestos from terminal_spares_warranty where tesw_serial  = ? and tesw_status=? allow filtering;";
            let repuestos = await getFindAll(queryRepuestos, [entry.term_serial, '1']);
            let repuestos2 = await getFindAll(queryRepuestos, [entry.term_serial, '2']);

            if (repuestos.length > 0) repuestos = repuestos[0]['tesw_repuestos'];
            if (repuestos2.length > 0) repuestos2 = repuestos2[0]['tesw_repuestos'];

            entry.tipificaciones = tipificaciones;
            entry.repuestos = repuestos;
            entry.validaciones = validaciones;

            var querySolicitados = `select tesw_repuestos from terminal_spares_warranty where tesw_serial = '${entry.term_serial}' and tesw_type= 'SOLICITADOS' allow filtering;`;
            var solicitados = await getFindAll(querySolicitados, []);

            solicitados = await funcionesGenerales.ordenarFechaAscendente(solicitados, 'tesw_date');

            if (solicitados.length > 0)
              entry.solicitados = solicitados[solicitados.length - 1].tesw_repuestos;
            else entry.solicitados = '';

            var queryAutorizados = `select tesw_repuestos from terminal_spares_warranty where tesw_serial = '${entry.term_serial}' and tesw_type= 'AUTORIZADOS' allow filtering;`;
            let autorizados = await getFindAll(queryAutorizados, []);

            autorizados = await funcionesGenerales.ordenarFechaAscendente(autorizados, 'tesw_date');

            if (autorizados.length > 0)
              entry.autorizados = autorizados[autorizados.length - 1].tesw_repuestos;
            else entry.autorizados = '';

            entry.repuestosDefectuosos = repuestos2;
            entry.term_technology = await convertirTecnologiaLetras(entry.term_technology);
          }
        }
      }
      res.json({ message: "success", status: "ok", data: { terminales: terminales } });
    }
  } catch (ex) {
    logger.error(ex.stack);
    console.log('*************** catch error findTerminalsAssociateRepairs ******************');

    res.json({ message: "catch error", status: "fail" });
  }
};

terminalsController.findAllByEstatus = async (req, res) => {

  if (req.headers.estatus == null) {
    res.status(200).send({ error: "Datos incompletos" });
    return;
  }

  try {
    const query = "select * from terminal WHERE term_status = ?";
    var estado = req.headers.estatus;
    let terminals = await getFindAllNotLimit(query, [estado]);

    if (terminals.length > 0) {
      for (let entry of terminals) {
        if (entry.term_technology) {
          if (entry.term_technology) {
            entry.term_technology = await convertirTecnologiaLetras(entry.term_technology);
          }
        }
      }
      res.json(terminals);
    } else {
      res.json([]);
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

terminalsController.saveValidationWeb = (req, res, next) => {
  try {
    request = req.body;
    json = { validaciones: request };
    const logueado = req.user.code;

    let register = saveValidation(json, '3', logueado);
    if (register) {
      res.status(200).send({ message: "success" });
      return;
    } else {
      res.status(200).send({ message: "fail" });
      return;
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
}

//codigo mauricio
terminalsController.getTerminalByLocation = async (req, res) => {

  try {
    const request = req.body[0];

    if (!request)
      return res.status(401).send({ message: "incomplete petition" });

    const query = "select * from terminal where term_localication = ? ALLOW FILTERING";
    const parameters = [request];

    let terminals = await getFindAllNotLimit(query, parameters);
    res.json(terminals);
  } catch (ex) {
    logger.error(ex.stack);
    res.status(403).send({ error: "catch error" });
  }
};

/**
 * TSR cambio de terminales - simcar y repuestos
 */
terminalsController.changeLocationWhitSpare = async (req, res) => {
  const changeDestination = req.body['data'][3];
  const changeTerminal = req.body['data'][0];

  if (changeTerminal.length > 0) {
    for (i = 0; i < changeTerminal.length; i++) {
      await getChangeTerminalSpare(changeTerminal[i], changeDestination);
    }
  }

  const changeSimCard = req.body['data'][1];

  if (changeSimCard.length > 0) {
    for (i = 0; i < changeSimCard.length; i++) {
      await getChangeSimCardSpare(changeSimCard[i], changeDestination);
    }
  }

  const changeSpare = req.body['data'][2];
  const logueado = req.user.code;
  const moment = require("moment");
  const date = moment().format("LLL");

  if (changeSpare.length > 0) await getChangeSpare(changeSpare, changeDestination, date, logueado);

  res.status(200).send({ message: "Asignación completa", result: true });
};

async function getChangeSpare(repuesto, destino, date, logueado) {
  return new Promise(async (resolve, reject) => {
    try {
      await getRealizarUbicacionAnteriorRepuesto(repuesto);

      for (i = 0; i < repuesto.length; i++) {
        var querySelect = "select * from spare where spar_code = ? and spar_status = ? and spar_warehouse = ? allow filtering";
        const parametersSelect = [repuesto[i].repuesto.spar_code, repuesto[i].repuesto.spar_status, destino];
        var repuestoExiste = await getFindAll(querySelect, parametersSelect);

        if (repuestoExiste.length > 0) {
          var cantidad = `${parseInt(repuesto[i].cantidad) + parseInt(repuestoExiste[0]['spar_quantity'])}`;
          var queryUpdate = "update spare set spar_quantity = ? where spar_id = ? if exists";
          const parametersUpdate = [cantidad, `${repuestoExiste[0]['spar_id']}`];

          await executeQuery(queryUpdate, parametersUpdate);

        } else {
          var queryInsert = "insert into polaris_core.spare (spar_id, spar_code, spar_terminal_model, spar_name, spar_status, spar_status_temporal, spar_quantity, spar_warehouse, spar_date_register, spar_register_by) values (now(),?,?,?,?,?,?,?,?,?) if not exists";
          const parameterInsert = [
            `${repuesto[i].repuesto.spar_code}`,
            `${repuesto[i].repuesto.spar_terminal_model}`,
            `${repuesto[i].repuesto.spar_name}`,
            `${repuesto[i].repuesto.spar_status}`,
            `${repuesto[i].repuesto.spar_status_temporal}`,
            `${repuesto[i].cantidad}`,
            `${destino}`,
            date,
            logueado
          ];

          await executeQuery(queryInsert, parameterInsert);
        }
      }
      resolve(true);
    } catch (ex) {
      logger.error(ex.stack);
      resolve(false);
    }
  });
}

async function getRealizarUbicacionAnteriorRepuesto(repuestosDeCambio) {
  return new Promise(async (resolve, reject) => {
    try {
      for (i = 0; i < repuestosDeCambio.length; i++) {
        var repuestosCambiar = repuestosDeCambio[i];
        var repuesto = await getFindAll("select * from spare where spar_id = ?", [repuestosCambiar.repuesto.spar_id]);

        var queryUpdate = "update spare set spar_quantity = ? WHERE spar_id = ?";
        var cantidad = `${parseInt(repuesto[0].spar_quantity) - parseInt(repuestosCambiar.cantidad)}`;
        const parameterUpdate = [cantidad, repuestosCambiar.repuesto.spar_id];

        conection.execute(queryUpdate, parameterUpdate, { prepare: true }, (err, result) => {
          if (err) {
            logger.error(err.stack);
            resolve(false);
          }
        });
      }
      resolve(true);
    } catch (ex) {
      resolve(false);
    }
  });
}

async function actualizarTerm_user_reparation(query, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          resolve(false);
          console.log("error al Consultar ", err);
        } else {
          resolve(true);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}

function executeQuery(query, parameters) {
  return new Promise((resolve, reject) => {
    try {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          console.log("error al Consultar ", err);
          resolve(false);
        } else {
          resolve(true);
        }
      });
    } catch (ex) {
      logger.error(ex.stack);
      resolve(false);
    }
  });
}

async function getChangeSimCardSpare(simCard, destino) {
  return new Promise((resolve, reject) => {
    try {
      var query = "update sim_card set sica_location = ? WHERE sica_serial = ?";
      const parameter = [destino, simCard.sica_serial];
      conection.execute(query, parameter, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else resolve(true);
      });
    } catch (ex) {
      logger.error(ex.stack);
      resolve(false);
    }
  });
}

async function getChangeTerminalSpare(terminal, destino) {
  return new Promise((resolve, reject) => {
    try {
      var query = "update terminal set term_localication = ? WHERE term_serial = ?";
      const parameter = [destino, terminal.term_serial];
      conection.execute(query, parameter, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          resolve(true);
        }
      });
    } catch (ex) {
      logger.error(ex.stack);
      resolve(true);
    }
  });
}

terminalsController.asociarTerminalUser = async (req, res) => {
  const request = req.body;
  try {
    if (!request.user) {
      res.json({ message: "parametros incompletos", status: "fail" });
      return;
    }
    var queryUser = "select * from user_terminals where uste_user =? and uste_date = ?";
    var updateUser = "update user_terminals set uste_associated_terminals=? where uste_user in (?) and uste_date = ? ";
    const moment = require("moment");
    const fecha = (moment().date()) + "/" + (moment().month() + 1) + "/" + moment().year();
    const logueado = request.user;
    const parametersUser = [logueado, fecha];
    let terminalesCompletadas = await getFindAll(queryUser, parametersUser);
    if (terminalesCompletadas === false) {
      res.json({ message: "invalid user", status: "fail" });
    }

    if (terminalesCompletadas) {

      if (terminalesCompletadas.length > 0) {
        terminalesCompletadas = parseInt(terminalesCompletadas[0]['uste_associated_terminals']) + 1;
      } else {
        terminalesCompletadas = '1';
      }
    }
    const parametersUpdateUser = [terminalesCompletadas + '', logueado, fecha];
    let terminalesCompletadasok = await getFindAll(updateUser, parametersUpdateUser);

    if (terminalesCompletadasok === false) {
      res.json({ message: "error de bd", status: "fail" });
    } else {
      res.json({ message: "success", status: "ok" });
    }

  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};

terminalsController.gestionarTerminalUser = async (req, res) => {
  const request = req.body;
  console.log('************* gestionarTerminalUser *****************');
  console.log(request);

  try {
    if (!request.user) {
      res.json({ message: "parametros incompletos", status: "fail" });
      return;
    }
    var queryUser = "select * from user_terminals where uste_user =? and uste_date = ?";
    var updateUser = "update user_terminals set uste_completed_terminals=?, uste_repaired_terminals =?, uste_diagnosed_terminals =? where uste_user in (?) and uste_date = ? "; const moment = require("moment");
    const fecha = (moment().date()) + "/" + (moment().month() + 1) + "/" + moment().year();
    const logueado = request.user;
    const parametersUser = [logueado, fecha];
    let terminalesCompletadas = await getFindAll(queryUser, parametersUser);
    if (terminalesCompletadas === false) {
      res.json({ message: "invalid user", status: "fail" });
    }

    let terminalesUser = await getFindAll(queryUser, parametersUser);

    if (terminalesUser === false) {
      res.json({ message: "invalid user", status: "fail" });
    }

    var reparadas = 0;
    var diagnosticadas = 0;

    if (terminalesUser) {
      if (terminalesUser.length > 0) {
        terminalesCompletadas = parseInt(terminalesUser[0]['uste_completed_terminals']) + 1;

        if (request.tipo == "REPARACIÓN") {
          if (terminalesUser[0]['uste_repaired_terminals'] != null) {
            reparadas = parseInt(terminalesUser[0]['uste_repaired_terminals']) + 1;
          } else {
            reparadas = 1;
          }

          if (terminalesUser[0]['uste_diagnosed_terminals'] == null) {
            diagnosticadas = 0;
          } else {
            diagnosticadas = parseInt(terminalesUser[0]['uste_diagnosed_terminals'])
          }

        }

        if (request.tipo == "DIAGNÓSTICO") {
          if (terminalesUser[0]['uste_diagnosed_terminals'] != null) {
            diagnosticadas = parseInt(terminalesUser[0]['uste_diagnosed_terminals']) + 1;
          } else {
            diagnosticadas = 1;
          }

          if (terminalesUser[0]['uste_repaired_terminals'] == null) {
            reparadas = 0;
          } else {
            reparadas = parseInt(terminalesUser[0]['uste_repaired_terminals'])
          }
        }



        const parametersUpdateUser = [terminalesCompletadas + '', '' + reparadas, '' + diagnosticadas, logueado, fecha];
        let terminalesCompletadasok = await getFindAll(updateUser, parametersUpdateUser);

        if (terminalesCompletadasok === false) {
          res.json({ message: "error de bd", status: "fail" });
        } else {
          res.json({ message: "success", status: "ok" });
        }

      } else {
        res.json({ message: "error el usuario no tiene terminales", status: "fail" });
      }
    }

  } catch (ex) {
    logger.error(ex.stack);
    console.log('*************** Cathc error gestionarTerminalUser **********');

    res.json({ message: "catch error", status: "fail" });
  }
};

terminalsController.getTerminalesAsignadas = async (req, res) => {
  const request = req.headers;
  try {
    if (!request.id) {
      res.json({ message: "parametros incompletos", status: "fail" });
    }
    const moment = require("moment");
    const fecha = (moment().date()) + "/" + (moment().month() + 1) + "/" + moment().year();
    const users = request.id;

    var query = "select * from user_terminals where uste_user in (" + users + ") and uste_date = '" + fecha + "';";

    conection.execute(query, [], { prepare: true }, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ message: "error", err });
        return;
      } else {
        if (result.rows) {
          res.status(200).send({ message: "success", data: result.rows });
        } else {
          res.status(200).send({ message: "error", data: result });
        }
      }
    });

  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};

terminalsController.updateDateAns = async (req, res, next) => {
  try {
    request = req.body;
    var query = "update terminal set term_date_ans=?  where term_serial=? IF EXISTS";
    parameters = [request.ansNew, request.term_serial];
    conection.execute(query, parameters, function (err, result) {
      if (err) {
        logger.error(err.stack);
        return;
      } else {
        if (Object.values(result.rows[0])[0] === true) {
          req.action = "actualizar fecha ans/terminal";
          req.before = JSON.stringify({ serial: request.term_serial, ans: request.term_date_ans });
          req.after = JSON.stringify({ observation: request.observation, serial: request.term_serial, ans: request.ansNew });
          res.json({ "respuesta": true });
        } else {
          res.json({ "respuesta": false });
        }
        next();
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

terminalsController.terminalProductive = async (req, res) => {
  const query = "select * from user_terminals where  uste_user =? ";
  const request = req.body;
  try {
    if (!request || !request.user || !request.fechaInicial || !request.fechaFinal) {
      res.json({ message: "incomplete petition", status: "fail" });
    } else {
      const parameters = [request.user];
      terminales = await getFindAll(query, parameters);
      if (terminales === false) {
        res.json({ message: "query error", status: "fail" });
      }

      var terminals = [];
      var fechaC1 = new Date(request.fechaInicial);
      var fechaC2 = new Date(request.fechaFinal);

      if (terminales) {
        for (let entry of terminales) {
          if (entry) {
            var auxiliarFI = entry.uste_date.split("/");
            var fechaE1 = auxiliarFI[1] + '/' + auxiliarFI[0] + '/' + auxiliarFI[2];
            fechaE1 = new Date(fechaE1);
            if (fechaE1 <= fechaC2 && fechaE1 >= fechaC1) {
              terminals.push(entry);
            }
          }
        }
      }
      res.json({ message: "success", status: "ok", data: { productividad: terminals } });
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};

terminalsController.updateTerminalHistory = async (req, res, next) => {
  let history = await TerminalHistory(req);
  var his = "";
  const request = req.body;
  if (history == "" || history == null) {
    his = JSON.stringify([request.terminal_history]);
  } else {
    var historial = JSON.parse(history['tehi_historial']);

    var nuevoHistorial = request.terminal_history;
    historial.push(nuevoHistorial);

    if (historial.length > 100) {
      historial = historial.slice(historial.length - 100, historial.length)
    }

    his = JSON.stringify(historial);
  }
  const query = "update terminal_history set tehi_historial =? where tehi_serial = ?";
  const parameters = [his, request.tehi_serial];
  try {
    conection.execute(query, parameters, { prepare: true }, function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ message: "query error", error: err });
        return;
      } else {
        res.status(200).send({ message: "success", status: "ok" });
        return;
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ error: "catch error" });
  }
};

terminalsController.getClinicHistoryTerminal = (req, res, next) => {
  const request = req.body;
  try {
    if (!request.serial) res.json({ message: "parametros incompletos", status: "fail" });

    var query = "select * from clinic_history_terminal where term_serial = ? allow filtering";

    conection.execute(query, [request.serial], { prepare: true }, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ message: "error", err });
        return;
      } else {
        if (result.rows)
          res.status(200).send({ message: "success", data: result.rows });
        else
          res.status(200).send({ message: "error", data: result });
      }
    });

  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
}

terminalsController.saveClinicHistoryTerminal = (req, res) => {
  insertClinicHistory(req.body, req.body.status).then(exito => {
    exito == true ? res.send({ message: 'success' }) : res.send({ message: 'error' });
  });
}

function insertClinicHistory(request, status) {
  return new Promise(async (resolve, reject) => {
    try {
      const moment = require("moment");
      const date = moment().format("LLL");
      var query = '';
      var parameters = [];

      if (status == 'DIAGNÓSTICO') {
        query = 'INSERT INTO clinic_history_terminal (actions, date_register, id, term_serial, open, date_close) VALUES (?,?,now(),?,?,?)';
        parameters = [JSON.stringify({ diagnostico: [request], reparacion: [] }), date, request.serial, request.open, null];
      } else {
        await getFindAll('select * from clinic_history_terminal where term_serial = ? allow filtering', [request.serial]).then(async (historys) => {
          var historys = await funcionesGenerales.ordenarFechaAscendente(historys, 'date_register');
          historys = historys[historys.length - 1];

          var historial = JSON.parse(historys.actions);

          if (status == 'REPARACIÓN') historial.reparacion.push(request);
          else if (status == 'NUEVO DIAGNÓTICO') historial.diagnostico.push(request);

          query = 'update clinic_history_terminal set actions = ?, open = ?, date_close = ? where id = ?';

          let date_close = request.open == true ? date : null;
          parameters = [JSON.stringify(historial), request.open, date_close, historys.id];
        });
      }

      conection.execute(query, parameters, function (err, result) {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else resolve(true);
      });

    } catch (error) {
      logger.error(error.stack);
      resolve(false);
    }
  });

}

async function TerminalHistory(req) {
  var serial = req.headers.id;
  const query = "select * from terminal_history where tehi_serial = ?";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, [serial], (err, result) => {
        if (err) {
          logger.error(err.stack);
          reject({ "Error": "error in BD" });
        }
        else {

          rta = result.rows[0];

          if (result.rows[0] != undefined) {

          } else {
            rta = "";
          }
          resolve(rta);

        }
      });

    });
  }
  catch (error) {
    logger.error(error.stack);
    res.status(406).send({ message: "Not Acceptable" });
  }
}


terminalsController.getTerminalHistory = async (req, res) => {
  let history = await TerminalHistory(req);
  try {
    if (history['Error'] != undefined) {
      res.status(404).send({ message: "query error", error: history, data: [] });
      return;
    } else {
      res.status(200).send({ message: "success", data: history });
      return;
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};


terminalsController.saveDiagnosisQA = async (req, res) => {
  console.log('********************* saveDiagnosisQA *************');
  console.log(req.body);

  try {
    const request = req.body;
    var queryUser = "select * from user_terminals where uste_user in (?) and uste_date = ?;";
    const moment = require("moment");
    const fecha = (moment().date()) + "/" + (moment().month() + 1) + "/" + moment().year();
    const logueado = req.user.code;
    const parametersUser = [logueado, fecha];
    let terminalesCompletadas = await getFindAll(queryUser, parametersUser);
    if (terminalesCompletadas === false) {
      res.json({ message: "invalid user", status: "fail" });
    }
    if (terminalesCompletadas.length > 0) {
      terminalesCompletadas = parseInt(terminalesCompletadas[0]['uste_completed_terminals']) + 1;
    }
    const parametersUpdateUser = [terminalesCompletadas + '', logueado, fecha];
    let terminalesCompletadasok = true;
    if (terminalesCompletadasok === false) {
      res.json({ message: "error de bd", status: "fail" });
    }

    if (request.validaciones) {
      var ok = await saveValidation(request, '2', logueado);
      if (!ok) { res.json({ message: "ocurrio un error al guardar las validaciones", status: "fail" }); }
      var ok = await updateStatusss(request.validaciones[0].tevs_terminal_serial, 'QA');
      if (!ok) { res.json({ message: "ocurrio un error al cambiar el estado a QA", status: "fail" }); }
      res.json({ message: "success", status: "ok" });

      request.usuario = logueado;
      request.serial = request.validaciones[0].tevs_terminal_serial;
      request.estado = 'VALIDACIONES REPARACION';
      request.open = true;
      insertClinicHistory(request, 'REPARACIÓN');
    } else {
      res.json({ message: "faltan parametros en la peticion", status: "fail" });
    }
  }
  catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};

terminalsController.closeDiagnosis = async (req, res, next) => {
  try {
    const request = req.body;
    var finalizarDiagnosticoR = "update terminal_spares_warranty  set tesw_open = '1' where tesw_serial = ? and tesw_status in('1','2','3')";
    var finalizarDiagnosticoV = "update terminal_validation_status  set tevs_open = '1' where tevs_terminal_serial = ? and tevs_status in('1','2','3')";
    var finalizarDiagnosticoO = "select * from terminal_observation where  teob_serial_terminal = ?";
    var finalizarDiagnosticoOF = "update terminal_observation  set teob_open = '1' where teob_id= ?";
    var finalizarDiagnosticoT = "update terminal_types_states  set tets_open = '1' where tets_terminal_serial = ? and tets_status in('1','2','3')";
    var parametrosfin = [request.term_serial];
    let finalizarDiagnosticoRepuestos = await getFindAll(finalizarDiagnosticoR, parametrosfin);
    if (finalizarDiagnosticoRepuestos === false) {
      res.json({ message: "error de bd repuestos ", status: "fail" });
    }
    let finalizarDiagnosticoObservaciones = await getFindAll(finalizarDiagnosticoO, parametrosfin);
    if (finalizarDiagnosticoObservaciones === false) {
      res.json({ message: "error de bd observaciones", status: "fail" });
    }
    if (finalizarDiagnosticoObservaciones) {
      for (let entry of finalizarDiagnosticoObservaciones) {
        if (entry) {
          let ok = await getFindAll(finalizarDiagnosticoOF, [entry.teob_id]);
          if (ok === false) {
            res.json({ message: "error de bd observaciones z", status: "fail" });
          }
        }
      }
    }
    let finalizarDiagnosticoValidaciones = await getFindAll(finalizarDiagnosticoV, parametrosfin);
    if (finalizarDiagnosticoValidaciones === false) {
      res.json({ message: "error de bd validaciones", status: "fail" });
    }
    let finalizarDiagnosticoTípificaciones = await getFindAll(finalizarDiagnosticoT, parametrosfin);
    if (finalizarDiagnosticoTípificaciones === false) {
      res.json({ message: "error de bd tipificaciones ", status: "fail" });
    }
    res.json({ message: "success", status: "ok" });
  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
}

terminalsController.saveNewDiagnosis = async (req, res) => {
  try {
    const request = req.body;
    var queryUser = "select * from user_terminals where uste_user in (?) and uste_date = ?;";
    var queryUpdateTerm = "update terminal set term_user_reparation = ? where term_serial = ?;";
    const moment = require("moment");
    const fecha = (moment().date()) + "/" + (moment().month() + 1) + "/" + moment().year();
    const logueado = req.user.code;
    const parametersUser = [logueado, fecha];
    let terminalesCompletadas = await getFindAll(queryUser, parametersUser);
    if (terminalesCompletadas === false) {
      res.json({ message: "invalid user", status: "fail" });
    }
    if (terminalesCompletadas.length > 0) {
      terminalesCompletadas = parseInt(terminalesCompletadas[0]['uste_completed_terminals']) + 1;
    }
    const parametersUpdateUser = [terminalesCompletadas + '', logueado, fecha];
    let terminalesCompletadasok = true;
    if (terminalesCompletadasok === false) {
      res.json({ message: "error de bd", status: "fail" });
    }

    if (request.validaciones && request.tipificaciones && request.observacion) {
      var parametrosUopdate = ['NUEVO DIAGNÓSTICO', request.observacion.teob_serial_terminal];
      if (request.repuestos) {
        var ok = await getFindAll(queryUpdateTerm, parametrosUopdate);
        if (ok === false) { res.json({ message: "ocurrio un error al actualizar la terminal", status: "fail" }); }
        var ok = await saveValidation(request, '1', logueado);
        if (!ok) { res.json({ message: "ocurrio un error al guardar las validaciones", status: "fail" }); }
        var ok = await saveTipificacion(request, '1');
        if (!ok) { res.json({ message: "ocurrio un error al guardar las tipificaciones", status: "fail" }); }
        request.repuestos.type = 'SOLICITADOS';
        var ok = await saveSparesTerminalLog(request, '1');
        if (!ok) { res.json({ message: "ocurrio un error al guardar los repuestos", status: "fail" }); }
        var ok = await saveObservacion(request, req.user.code);
        res.json({ message: "success", status: "ok" });
      } else {
        var ok = await getFindAll(queryUpdateTerm, parametrosUopdate);
        if (!ok) { res.json({ message: "ocurrio un error al actualizar la terminal", status: "fail" }); }
        var ok = await saveValidation(request, '1', logueado);
        if (!ok) { res.json({ message: "ocurrio un error al guardar las validaciones", status: "fail" }); }
        var ok = await saveTipificacion(request, '1');
        if (!ok) { res.json({ message: "ocurrio un error al guardar las tipificaciones", status: "fail" }); }
        var ok = await saveObservacion(request, req.user.code);
        res.json({ message: "success", status: "ok" });
      }

      request.usuario = logueado;
      request.serial = request.observacion.teob_serial_terminal;
      request.estado = 'NUEVO DIAGNÓSTICO';
      request.open = true;
      insertClinicHistory(request, 'NUEVO DIAGNÓSTICO');
    } else {
      res.json({ message: "faltan parametros en la peticion", status: "fail" });
    }
  }
  catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};

terminalsController.saveDiagnosisSpare = async (req, res) => {
  try {
    const request = req.body;
    var queryUser = "select * from user_terminals where uste_user in (?) and uste_date = ?;";
    var queryUpdateTerm = "update terminal set term_user_reparation = ? where term_serial = ?;";
    const moment = require("moment");
    const fecha = (moment().date()) + "/" + (moment().month() + 1) + "/" + moment().year();
    const logueado = req.user.code;
    const parametersUser = [logueado, fecha];
    let terminalesCompletadas = await getFindAll(queryUser, parametersUser);
    if (terminalesCompletadas === false) {
      res.json({ message: "invalid user", status: "fail" });
    }
    if (terminalesCompletadas.length > 0) {
      terminalesCompletadas = parseInt(terminalesCompletadas[0]['uste_completed_terminals']) + 1;
    }
    const parametersUpdateUser = [terminalesCompletadas + '', logueado, fecha];
    let terminalesCompletadasok = true;
    if (terminalesCompletadasok === false) {
      res.json({ message: "error de bd", status: "fail" });
    }

    if (request.validaciones && request.observacion && request.repuestos) {
      var parametrosUopdate = ['REPUESTO DEFECTUOSO', request.observacion.teob_serial_terminal];
      var ok = await getFindAll(queryUpdateTerm, parametrosUopdate);
      if (ok === false) { res.json({ message: "ocurrio un error al actualizar la terminal", status: "fail" }); }
      var ok = await saveValidation(request, '2', logueado);
      if (!ok) { res.json({ message: "ocurrio un error al guardar las validaciones", status: "fail" }); }
      var ok = await saveSparesTerminalLog(request, '2');
      if (!ok) { res.json({ message: "ocurrio un error al guardar los repuestos", status: "fail" }); }
      var ok = await saveObservacion(request, req.user.code);
      res.json({ message: "success", status: "ok" });
    } else {
      res.json({ message: "faltan parametros en la peticion", status: "fail" });
    }
  }
  catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};

terminalsController.saveSparesWeb = async (req, res, next) => {
  try {
    request = req.body;
    console.log(request);

    json = { repuestos: request };
    let register = await saveSparesTerminalLog(json, request.tipo)
    if (register) {
      res.status(200).send({ message: "success" });
      return;
    } else {
      res.status(200).send({ message: "fail" });
      return;
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
}

terminalsController.getTerminalByOrigin = async (req, res) => {
  const parameter = [req.headers.origen, req.headers.serial];
  const query = "select * from terminal where term_localication = ? and term_serial = ? allow filtering";
  try {
    conection.execute(query, parameter, { prepare: true }, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ message: "query error ", error: err });
        return;
      } else {
        res.json(result.rows);
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ message: "catch error", error: ex });
  }
};

terminalsController.ChangeByCommerce = async (req, res) => {
  const terminales = req.body.terminales;
  const ubicacion = req.body.ubicacion;
  const hoy = req.body.hoy;
  const ans = req.body.ans;
  var bandera = false;
  const query = "update terminal set term_date_ans = ?, term_date_reception = ?, term_localication = ?, term_status = ?, term_warranty_type =? where term_serial = ?";
  for (i = 0; i < terminales.length; i++) {
    const parameter = [ans, hoy, ubicacion, 'PREDIAGNÓSTICO', terminales[i]['term_warranty_type'], terminales[i]['term_serial']];

    try {
      conection.execute(query, parameter, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          bandera = true;
          res.status(200).send({ message: "query error " });
          return;
        }
      });
    } catch (ex) {
      logger.error(ex.stack);
      res.status(200).send({ error: "catch error" });
    }
  }
  if (bandera) {
    res.status(200).send(false);
  } else {
    res.status(200).send(true);
  }
};

terminalsController.ChangeByWareHouses = async (req, res) => {
  const terminales = req.body.terminales;

  for (i = 0; i < terminales.length; i++) {
    await ChangeByWareHouses(req, terminales[i]['term_serial'], terminales[i]['term_warranty_type']).then(res => {
      if (!res) {
        return res.status(200).send(false);
      }
    })
  }

  res.status(200).send(true);

};

function ChangeByWareHouses(req, serial, tipo) {
  return new Promise((resolve, reject) => {
    const ubicacion = req.body.ubicacion;
    const hoy = req.body.hoy;
    const fin = req.body.fin;
    const time = req.body.time || '';
    const query = "update terminal set term_date_finish = ?, term_start_date_warranty = ?, term_localication = ?, term_warranty_time = ?, term_warranty_type = ? where term_serial = ?";

    const parameter = [fin, hoy, ubicacion, time, tipo, serial];

    try {
      conection.execute(query, parameter, { prepare: true }, (err, result) => {
        if (err) {
          console.log(err);
          resolve(false)
        } else {
          resolve(true);
        }
      });
    } catch (ex) {
      resolve(false);
    }
  });
}

terminalsController.asociarTerminalUserTSR = async (req, res) => {
  const request = req.body;
  const cant = req.body.cant;
  try {
    if (!request.user || !request.code) {
      res.json({ message: "parametros incompletos", status: "fail" });
      return;
    }
    var queryUser = "select * from user_terminals where uste_user =? and uste_date = ?";
    const moment = require("moment");
    const fecha = (moment().date()) + "/" + (moment().month() + 1) + "/" + moment().year();
    const logueado = request.user;
    const parametersUser = [logueado, fecha];
    let terminalesCompletadas = await getFindAll(queryUser, parametersUser);
    if (terminalesCompletadas === false) {
      res.json({ message: "invalid user", status: "fail" });
    }
    if (terminalesCompletadas) {
      if (request.code === '1') {
        if (terminalesCompletadas.length > 0) {
          terminalesCompletadas = parseInt(terminalesCompletadas[0]['uste_associated_terminals']) + cant;
          var datosUpdate = [terminalesCompletadas + "", logueado, fecha];
          var updateUser = "update user_terminals set uste_associated_terminals=? where uste_user in (?) and uste_date = ? ";

        } else {
          var datosUpdate = [cant + "", '0', logueado, fecha];
          var updateUser = "update user_terminals set uste_associated_terminals=?,uste_completed_terminals=? where uste_user in (?) and uste_date = ? ";

        }
      }
      if (request.code === '2') {
        if (terminalesCompletadas.length > 0) {
          terminalesCompletadas = parseInt(terminalesCompletadas[0]['uste_associated_terminals']) - cant;
          var datosUpdate = [terminalesCompletadas + "", logueado, fecha];
          var updateUser = "update user_terminals set uste_associated_terminals=? where uste_user in (?) and uste_date = ? ";

        } else {
          var datosUpdate = [cant + "", '0', logueado, fecha];
          var updateUser = "update user_terminals set uste_associated_terminals=?,uste_completed_terminals=? where uste_user in (?) and uste_date = ? ";
        }
      }
    }
    const parametersUpdateUser = datosUpdate;
    let terminalesCompletadasok = await getFindAll(updateUser, parametersUpdateUser);
    if (terminalesCompletadasok === false) {
      res.json({ message: "error de bd", status: "fail" });
    }
    res.json({ message: "success", status: "ok" });
  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail" });
  }
};

terminalsController.terminalsFind = async (req, res, next) => {
  try {
    request = req.body;
    if (!request.serial) {
      res.json({ message: "incomplete petition", status: "fail" });
    }
    const queryValidaciones = "select * from terminal_validation_status where tevs_terminal_serial= ? and tevs_status in ('1','2','3') and tevs_open='0' allow filtering";
    const queryTipificaciones = "select * from terminal_types_states where tets_terminal_serial= ? and tets_status in ('1','2','3') and tets_open='0' allow filtering";
    const queryRepuestos = "select * from terminal_spares_warranty where tesw_serial= ? and tesw_open='0' allow filtering";
    const queryobservaciones = "select * from terminal_observation where teob_serial_terminal= ?  and teob_open='0' allow filtering";
    const querydatosTerminal = "select * from terminal where term_serial=?"
    const parameters = [request.serial];
    let validaciones = await getFindAll(queryValidaciones, parameters);
    let tipificaciones = await getFindAll(queryTipificaciones, parameters);
    let repuestos = await getFindAll(queryRepuestos, parameters);
    let observaciones = await getFindAll(queryobservaciones, parameters);
    let terminal = await getFindAll(querydatosTerminal, parameters);
    if (!validaciones || !tipificaciones || !repuestos || !observaciones || !terminal) {

      res.status(200).send({ message: "fail", status: "fail" });
      // return;
    } else {
      if (terminal[0] != undefined) {
        if (terminal[0].term_technology) {
          terminal[0].term_technology = await convertirTecnologiaLetras(terminal[0].term_technology);
        }
      }

      var fecha = new Date('1', '1', '1999');
      if (validaciones.length > 0) {
        for (let entry of validaciones) {
          if (entry.tevs_date && entry.tevs_status == '1') {
            var aux = new Date(entry.tevs_date);
            if (aux >= fecha) {
              fecha = aux;
            }
          }
        }
      }
      validaciones = validaciones.filter(c => { return new Date(c.tevs_date) >= fecha });
      tipificaciones = tipificaciones.filter(c => { return new Date(c.tets_date) >= fecha });
      repuestos = repuestos.filter(c => { return new Date(c.tesw_date) >= fecha && (c.tesw_status == '1' || c.tesw_status == '2' || c.tesw_status == '3') });
      res.status(200).send({ message: "success", status: "ok", "terminal": terminal, "validaciones": validaciones, "tipificaciones": tipificaciones, "repuestos": repuestos, "observaciones": observaciones });
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ message: "catch error", status: "fail", error: ex });
  }
}

terminalsController.terminalsFindDate = async (req, res, next) => {
  try {
    request = req.body;
    if (!request.user || !request.fechaInicio || !request.fechaFin) {
      res.json({ message: "incomplete petition", status: "fail" });
    }
    const queryValidaciones = "select * from terminal_validation_status where tevs_status in ('1','2') and tevs_user = ? allow filtering";
    const parameters = [request.user];
    let validaciones = await getFindAll(queryValidaciones, parameters);
    if (!validaciones) {
      res.status(200).send({ message: "fail", status: "fail" });
      return;
    } else {
      var datosDiagnosticos = '';
      var datosReparados = '';
      var diagnosticos = validaciones.filter(c => { return c.tevs_status == '1' && new Date(c.tevs_date) >= new Date(request.fechaInicio) && new Date(c.tevs_date) <= new Date(request.fechaFin) });
      var reparaciones = validaciones.filter(c => { return c.tevs_status == '2' && new Date(c.tevs_date) >= new Date(request.fechaInicio) && new Date(c.tevs_date) <= new Date(request.fechaFin) });
      if (diagnosticos.length > 0) {
        for (let entry of diagnosticos) {
          if (entry.tevs_terminal_serial) {
            datosDiagnosticos += "'" + entry.tevs_terminal_serial + "',";
          }
        }
      }
      var terminalesDiagnosticadas = [];
      if (datosDiagnosticos.length > 0) {
        var query = "select * from terminal where term_serial in (" + datosDiagnosticos.substr(0, datosDiagnosticos.length - 1) + ")";
        var parametros = []
        let terminales = await getFindAllNotLimit(query, parametros);
        if (terminales) {
          if (terminales.length > 0) {
            for (let entry of terminales) {
              if (entry.term_technology) {
                entry.term_technology = await convertirTecnologiaLetras(entry.term_technology);
              }
            }
          }
          terminalesDiagnosticadas = terminales;
        }
      }

      if (reparaciones.length > 0) {
        for (let entry of reparaciones) {
          if (entry.tevs_terminal_serial) {
            datosReparados += "'" + entry.tevs_terminal_serial + "',";
          }
        }
      }
      var terminalesReparadas = [];
      if (datosReparados.length > 0) {
        var query = "select * from terminal where term_serial in (" + datosReparados.substr(0, datosReparados.length - 1) + ")";
        var parametros = []
        let terminales = await getFindAllNotLimit(query, parametros);
        if (terminales) {
          if (terminales.length > 0) {
            for (let entry of terminales) {
              if (entry.term_technology) {
                entry.term_technology = await convertirTecnologiaLetras(entry.term_technology);
              }
            }
          }
          terminalesReparadas = terminales;
        }
      }
      res.status(200).send({ message: "success", status: "ok", 'diagnosticos': terminalesDiagnosticadas, "reparaciones": terminalesReparadas });
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ message: "catch error", status: "fail" });
  }
}

terminalsController.getUbicationByCode = async (req, res, next) => {
  var respuesta = [];
  const parameter = [req.headers.code];
  const queryW = "select * from warehouse where ware_id_warehouse = ?";
  const queryC = "select * from commerce where comm_uniqe_code = ?";
  const queryU = "select * from user where user_id_user = ?";
  try {
    conection.execute(queryW, parameter, { prepare: true }, (err, result) => {
      if (err) {
        logger.error(err.stack); return;
      } else {
        respuesta.push(result.rows);
        try {
          conection.execute(queryC, parameter, { prepare: true }, (err, result) => {
            if (err) {
              logger.error(err.stack); return;
            } else {
              respuesta.push(result.rows);
              try {
                conection.execute(queryU, parameter, { prepare: true }, (err, result) => {
                  if (err) {
                    logger.error(err.stack); return;
                  } else {
                    respuesta.push(result.rows);
                    res.status(200).send({ message: "success", status: "ok", data: respuesta });
                  }
                });
              } catch (ex) {
                logger.error(ex.stack);
                res.status(200).send({ message: "catch error", status: "fail" });
              }
            }
          });
        } catch (ex) {
          logger.error(ex.stack);
          res.status(200).send({ message: "catch error", status: "fail" });
        }
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ message: "catch error", status: "fail" });
  }
}

terminalsController.findTerminalWithCommerce = async (req, res, next) => {
  var respuesta = [];
  try {
    var serial = req.body.serial;
    if (!serial) {
      res.json({ message: "incomplete petition", status: "fail" });
    }
    const queryTerminal = "select * from terminal where term_serial = ?";
    const parametersTerminal = [serial];
    let soloTerminal = await obtenerTerminalPorSerial(queryTerminal, parametersTerminal);
    if (soloTerminal.length > 0) {
      if (soloTerminal[0].term_technology) {
        soloTerminal[0].term_technology = await convertirTecnologiaLetras(soloTerminal[0].term_technology);
        respuesta.push(soloTerminal);
      }
      let soloUbicacion = await obtenerInforUbicacion(soloTerminal[0].term_localication);
      if (soloUbicacion[0].length > 0) {
        respuesta.push(soloUbicacion[0]);
        res.status(200).send({ message: "Success", status: "ok", terminal: respuesta[0], nombreUbicacion: respuesta[1][0] });
      } else if (soloUbicacion[1].length > 0) {
        respuesta.push(soloUbicacion[1]);
        res.status(200).send({ message: "Success", status: "ok", terminal: respuesta[0], nombreUbicacion: respuesta[1][0] });
      } else {
        respuesta.push(soloUbicacion[2]);
        res.status(200).send({ message: "Success", status: "ok", terminal: respuesta[0], nombreUbicacion: respuesta[1][0] });
      }
    } else {
      res.status(200).send({ message: "Success", status: "ok", terminal: {}, nombreUbicacion: {} });
    }

  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ message: "catch error", status: "fail", error: ex });
  }
}

async function obtenerTerminalPorSerial(query, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);
  }
}
async function obtenerInforUbicacion(cod) {
  var respuesta = [];
  var parameters = [cod];
  const queryW = "select ware_name from warehouse where ware_id_warehouse = ?";
  const queryC = "select comm_name from commerce where comm_uniqe_code = ?";
  const queryU = "select user_name from user where user_id_user = ?";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryW, parameters, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack); return;
        } else {
          respuesta.push(result.rows);
          try {
            conection.execute(queryC, parameters, { prepare: true }, (err, result) => {
              if (err) {
                logger.error(err.stack); return;
              } else {
                respuesta.push(result.rows);
                try {
                  conection.execute(queryU, parameters, { prepare: true }, (err, result) => {
                    if (err) {
                      logger.error(err.stack); return;
                    } else {
                      respuesta.push(result.rows);
                      resolve(respuesta);
                    }
                  });
                } catch (ex) {
                  logger.error(ex.stack);
                  respuesta.push(result.rows);
                  resolve(respuesta);
                }
              }
            });
          } catch (ex) {
            logger.error(ex.stack);
            respuesta.push(false);
            resolve(respuesta);
          }
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    respuesta.push(false);
    resolve(respuesta);
  }
}

terminalsController.summary = async (req, res, next) => {
  try {
    const query = "select * from terminal where term_localication = ? allow filtering";
    const parameter = [req.body.technical];
    let terminals = await getFindAllNotLimit(query, parameter);

    if (terminals.length > 0) {
      let respuesta = await procesarRespuesta(terminals);
      res.status(200).send({ message: "Success", status: "ok", response: respuesta });
    } else {
      res.status(200).send({ message: "Success", status: "ok", response: terminals });
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ message: "Catch error", status: "fail", error: ex });
  }
}
async function procesarRespuesta(result) {
  var modelos = [];
  var resultados = [];
  var temporal = [];
  try {
    return new Promise((resolve, reject) => {
      result.forEach((element, pos) => {
        modelos.push({ marca: element['term_brand'], modelo: element['term_model'] });
      });
      modelos = modelos.filter((valorActual, indiceActual, arreglo) => {
        return arreglo.findIndex(
          valorDelArreglo => JSON.stringify(valorDelArreglo) === JSON.stringify(valorActual)
        ) === indiceActual
      });
      modelos.forEach((element, pos) => {
        temporal = result.filter(c => { return c.term_model == element['modelo'] });
        resultados.push({ marca: element['marca'], modelo: element['modelo'], cantidad: temporal.length });
      });
      resolve(resultados);
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve([]);
  }
};

terminalsController.getAllTerminalesAsignadas = async (req, res) => {

  try {

    var query = "select * from user_terminals";
    conection.execute(query, [], { prepare: true }, (err, result) => {
      if (err) {
        res.status(200).send({ message: "error", err, data: [] });
        return;
      } else {
        if (result.rows) {
          res.status(200).send({ message: "success", data: result.rows });
        } else {
          res.status(200).send({ message: "error", data: result });
        }
      }
    });

  } catch (ex) {
    logger.error(ex.stack);
    res.json({ message: "catch error", status: "fail", data: [] });
  }
};

function getFindAllNotLimit(query, params) {
  var data = [];
  return new Promise((resolve, reject) => {
    conection.stream(query, params, { prepare: true, autoPage: true })
      .on('readable', function () {
        var row;
        while (row = this.read()) {
          data.push(row);
        }
      }).on('end', function () {
        resolve(data);
      });
  });
}

async function convertirTecnologiaLetras(tecnologia) {
  let technologies = await getFindAll("select * from technology", []);
  var tecnologias = tecnologia.split(",");
  var dato = '';
  try {
    return new Promise((resolve, reject) => {
      for (let i = 0; i < technologies.length; i++) {
        let element = technologies[i];
        if (tecnologias.includes(element.tech_code)) {
          dato += element.tech_description + ",";
        }
      }
      resolve(dato.substr(0, dato.length - 1));
    });
  } catch (error) {
    logger.error(error.stack);
    resolve([]);
  }

}

terminalsController.findAllTecnologies = async (req, res) => {
  const query = "select * from technology";
  try {
    conection.execute(query, [], (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ message: " query error" });
        return;
      } else {
        res.json(result.rows);
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ error: "catch error" });
  }
};

//exports
module.exports = terminalsController;