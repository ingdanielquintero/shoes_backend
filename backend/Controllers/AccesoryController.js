const accesoriesController = {};
const conection = require("../database.js");
const c = require('colors');
const Uuid = require('cassandra-driver').types.Uuid;
const logger = require('../bin/logger');
const moment = require("moment");

//methods
accesoriesController.save = async (req, res, next) => {
  const request = req.body;
  var query = "insert into accessory (acce_id,acce_code,acce_terminal_model,acce_name,acce_status,acce_status_temporal,acce_quantity,acce_warehouse,acce_date_register,acce_register_by) values (now(),?,?,?) if not exists";
  try {
    const parameters = [request.acce_terminal_model, request.acce_nombre, request.acce_quantity];
    conection.execute(query, parameters, { prepare: true }, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: "query error" });
        return;
      } else {
        if (Object.values(result.rows[0])[0]) {
          res.status(200).send({ message: "success", applied: Object.values(result.rows[0])[0] });
          req.action = "registrar/accesorio";
          req.before = "{}";
          req.after = JSON.stringify(request);
          next();
        } else {
          res.status(200).send({ message: "accesory already exists", applied: Object.values(result.rows[0])[0] });
          return;
        }
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

accesoriesController.find = async (req, res) => {
  const query = "select * from accessory where acce_id = ?";
  try {
    const parameters = [req.headers.id];
    conection.execute(query, parameters, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: "query error " });
        return;
      } else {
        res.json(result.rows);
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

accesoriesController.findAll = async (req, res) => {
  try {
    const query = "select * from accessory";
    let accesorys = await getFindAllNotLimit(query, []);
    res.json(accesorys);
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

accesoriesController.findAllAccesoryStatus = async (req, res) => {
  const query = "select * from accessory_status";
  try {
    conection.execute(query, [], (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: " query error" });
        return;
      } else {
        res.json(result.rows);
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

accesoriesController.update = async (req, res, next) => {

  try {
    const status = req.estado;
    if (status === false) {
      const request = req.body;
      var query = "insert into accessory (acce_id, acce_code,acce_terminal_model,acce_name,acce_status,acce_status_temporal,acce_quantity,acce_warehouse,acce_date_register,acce_register_by) values (now(),?,?,?,?,?,?,?,?,?) if not exists";
      try {
        const date = new Date();
        var acce_creation = date.format("%Y-%m-%d %H:%M:%S", false);
        const acce_user = req.user.code;
        const parameters = [request.acce_code, request.acce_terminal_model, request.acce_name, request.acce_status, request.acce_status_temporal, request.acce_quantity, request.acce_warehouse, "" + acce_creation, acce_user];

        conection.execute(query, parameters, { prepare: true }, (err, result) => {
          if (err) {
            logger.error(err.stack);
            res.status(200).send({ message: "query error", err });
            return;
          } else {
            if (Object.values(result.rows[0])[0]) {

              req.action = "registrar/accesorio";
              req.before = "{}";
              req.after = JSON.stringify(request);
              res.status(200).send({ message: "success", applied: Object.values(result.rows[0])[0] });
              next();
              return;
            } else {
              res.status(200).send({ message: "accesory already exists", applied: Object.values(result.rows[0])[0] });
              return;
            }
          }
        });
      } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ error: "catch error" });
      }

    } else {

      const request = req.body;
      req.action = "actualizar/accesorio";
      var auditBefore = [];
      var auditAfter = [];

      const row = JSON.parse(req.before);
      for (let j = 0; j < Object.keys(request).length; j++) {
        if (Object.keys(row[0])[j] === Object.keys(request)[j]) {
          if (Object.values(row[0])[j] !== Object.values(request)[j]) {
            auditBefore.push(JSON.parse('{"' + Object.keys(row[0])[j] + '":"' + Object.values(row[0])[j] + '"}'));
            auditAfter.push(JSON.parse('{"' + Object.keys(request)[j] + '":"' + Object.values(request)[j] + '"}'));
          }
        } else {
          res.status(400).send({ message: "error, wrong json syntax" });
          return;
        }
      }
      req.before = JSON.stringify(auditBefore);
      req.after = JSON.stringify(auditAfter);

      var query = "update accessory set acce_code=?,acce_terminal_model=?,acce_name=?,acce_status=?,acce_status_temporal=?,acce_quantity=?,acce_warehouse=? where acce_id=?";
      var cantidad = request.acce_quantity
      const parameters = [request.acce_code, request.acce_terminal_model, request.acce_name, request.acce_status, request.acce_status_temporal, "" + cantidad, request.acce_warehouse, request.acce_id];
      conection.execute(query, parameters, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          res.status(404).send({ message: " query error", err });
          return;
        } else if (result) {
          res.json({ message: "success" });
          next();
        }
      });
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

accesoriesController.delete = async (req, res, next) => {
  const status = req.estado;
  try {
    if (req.user) {
      if (!req.user.role.includes("1")) {
        res.status(404).send({ message: 'You do not have the necessary permissions to execute this request.' });
        return;
      }
    } else {
      res.status(404).send({ message: 'Invalid Token.' });
    }
    if (status === false) {
      res.status(404).send({ message: "error, accessory not exits" });
      return;
    };
    var query = "delete from accessory  where acce_id = ?";
    const parameters = [req.headers.id];
    conection.execute(query, parameters, { prepare: true }, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: "query error" });
        return;
      } else {
        req.action = "eliminar/accesorio";
        req.after = "{}";
        res.json({ message: "success" });
        next();
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

accesoriesController.getAccessory = async (req, res, next) => {
  const query = "select * from accessory where acce_id = ?";
  var estado = false;

  try {
    if (req.headers.id != "0") {
      await conection.execute(query, [req.headers.id], { prepare: true }, function (err, result) {
        if (err) {
          logger.error(err.stack);
          res.status(404).send({ message: "query error", err });
          return;
        }
        if (result.rows.length > 0) {
          req.before = JSON.stringify(result.rows);
          estado = true;
          var row = req.before;
        }
        req.estado = estado;
        next();
      });
    } else {
      req.before = "";
      estado = false;
      req.estado = estado;
      next();
    }


  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

accesoriesController.saveSeveral = async (req, res, next) => {
  const date = moment().format("LLL");
  const logueado = req.user.code;

  var request = req.body;
  var opt = req.body[request.length - 1]['opcion'];

  var notInserted = [];
  var totalNoInsert = [];
  var dataResultados = [];

  var countInserted = 0;
  var countUpdate = 0;
  let i;

  var banderaTerminal = false;
  var banderaEstado = false;
  var booleanWareHouse = false;
  var banderaUpdate = false;
  var banderaInserted = false;

  var resultadoInsert;
  var resultadoUpdate;

  var queryInsert = "insert into accessory (acce_id,acce_code,acce_terminal_model,acce_name,acce_status,acce_quantity,acce_date_register,acce_warehouse,acce_register_by) values (now(),?,?,?,?,?,?,?,?) if not exists";
  var queryUpdate = "update accessory set acce_quantity = ? where acce_id = ? if exists";
  var querySelect = "select * from accessory where acce_code = ? and acce_status = ? and acce_warehouse = ? allow filtering";

  let terminalsEstados = await getFindAll('select acst_name from accessory_status', []);
  let terminalModels = await getFindAll('select temo_name from terminal_model', []);

  if (terminalsEstados.length == 0)
    return res.status(406).send({ error: "No se encontraron estado asociados." });

  if (terminalModels.length == 0 && terminalsEstados.length == 0)
    return res.status(405).send({ error: "No se encontraron terminales asociados." });

  let warehouses = [];
  opt == '1' ? warehouses = await getFindAll('select ware_id_warehouse, ware_status  from warehouse', [])
    : warehouses = await getFindAll('select comm_uniqe_code from commerce', []);

  console.log(warehouses);


  for (i = 0, longRequest = request.length; i < longRequest; i++) {
    const parameters = ["" + request[i].acce_code, "" + request[i].acce_terminal_model, "" + request[i].acce_name, "" + request[i].acce_status, "" + request[i].acce_quantity, date, "" + request[i].acce_warehouse, logueado];

    banderaEstado = terminalsEstados.some(c => c['acst_name'] == request[i].acce_status);
    if (!banderaEstado) notInserted.push(" Estado inválido");

    if (opt == '1') {
      booleanWareHouse = warehouses.some(c => c['ware_status'] == 'ACTIVO' && c['ware_id_warehouse'] == request[i].acce_warehouse);
      if (!booleanWareHouse) notInserted.push(" Ubicación inválida");

    } else {
      booleanWareHouse = warehouses.some(c => c['comm_uniqe_code'] == request[i].acce_warehouse);
      if (!booleanWareHouse) notInserted.push(" Ubicación inválida");
    }

    banderaTerminal = terminalModels.some(c => c['temo_name'] == request[i].acce_terminal_model);
    if (!banderaTerminal) notInserted.push(" Modelo inválido");

    if (banderaEstado && booleanWareHouse && banderaTerminal) {
      const parametersSelect = [request[i].acce_code, request[i].acce_status, request[i].acce_warehouse];
      dataResultados = await getIfExists(querySelect, parametersSelect);

      banderaEstado = false;
      booleanWareHouse = false;
      banderaTerminal = false;

      if (dataResultados.length != 0) {
        var idUpdate = "" + dataResultados[0]['acce_id'];
        var quantity = "" + ((parseInt(request[i].acce_quantity)) + (parseInt(dataResultados[0]['acce_quantity'])));
        const parametersUpdate = [quantity, idUpdate];

        resultadoUpdate = await getRealizarActualizacion(queryUpdate, parametersUpdate);
        banderaUpdate = Object.values(resultadoUpdate[0])[0];

      } else {
        resultadoInsert = await getRelaizarInsert(queryInsert, parameters);
        banderaInserted = Object.values(resultadoInsert[0])[0];
      }

      if (banderaUpdate) {
        countUpdate++;
        banderaUpdate = false;
      }

      if (banderaInserted) {
        countInserted++;
        banderaInserted = false;
      }

    } else {
      totalNoInsert.push("Fila ==>  " + (i + 2) + notInserted);
      banderaEstado = false;
      booleanWareHouse = false;
      banderaTerminal = false;
      notInserted = [];
    }

    if ((i % 100) == 0) {
      console.log("cargando accesorios ... ", i);
    }

  }

  if (i == request.length) {
    return res.status(200).send({
      message: "Resultados de el cargue ",
      response: totalNoInsert,
      countInserted: countInserted,
      countUpdate: countUpdate
    });
  }
};

async function getIfExists(querySelect, parametersSelect) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(querySelect, parametersSelect, (err, result) => {
        if (err) {
          logger.error(err.stack);
          console.log("error al Consultar ", err);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}
async function getRelaizarInsert(query, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          console.log("error al Consultar ", err);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}
async function getRealizarActualizacion(query, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          console.log("error al Consultar ", err);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}

async function getTerminal() {
  var resultTerminal;
  var queryConsulta = "select temo_name from terminal_model";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          return;
        } else {
          resultTerminal = result;
          resolve(resultTerminal);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(402).send({ error: "catch error 2" });
  }
}

accesoriesController.updateStatus = async (status, status_temporal, id) => {
  try {
    var query = "update accessory set acce_status=?,acce_status_temporal=? where acce_id=?";
    const parameters = [status, status_temporal, id];
    conection.execute(query, parameters, { prepare: true }, (err, result) => {
      if (err) {
        logger.error(err.stack);
        return false;
      } else if (result) {
        return true;
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    return false;
  }
};


accesoriesController.getByCode = async (req, res) => {
  const query = "select * from accessory where acce_code = ?";
  let accessorys = await getFindAllNotLimit(query, [req.headers.id]);
  res.status(200).send({ msg: 'ssucces', user: accessorys });
}

accesoriesController.getById = async (req, res) => {
  const query = "select * from accessory where acce_id = ?";
  try {
    await conection.execute(query, [req.headers.id], function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ msg: 'query error' });
      } else {
        res.status(200).send({ msg: 'success', user: result.rows });
        return;
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ error: "catch error" });
  }
};

accesoriesController.getByCodeLocationStatus = async (req, res) => {
  const query = "SELECT * from accessory WHERE acce_status = ? AND acce_code = ? AND acce_warehouse = ? ALLOW FILTERING;";
  const data = JSON.parse(req.headers.data);
  try {
    await conection.execute(query, [data.estado, data.codigo, data.ubicacion], function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ msg: 'query error' });
      } else {
        res.status(200).send({ msg: 'success', user: result.rows });
        return;
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ error: "catch error" });
  }
};

accesoriesController.getAllAccessoryByStatus = async (req, res) => {
  const query = "SELECT * FROM accessory WHERE  acce_status = 'NUEVO' ALLOW FILTERING";
  const query2 = "SELECT * FROM accessory WHERE  acce_status ='OPERATIVO' ALLOW FILTERING";
  var respuesta = [];
  try {
    conection.execute(query, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(402).send({ message: " query error" });
        console.log(err);
        return;
      } else {
        respuesta.push(result.rows);
        conection.execute(query2, (err, result) => {
          if (err) {
            res.status(402).send({ message: " query error" });
            return;
          } else {
            respuesta.push(result.rows);
            res.json(respuesta);
          }
        });
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(401).send({ error: "catch error" });
  }
};

accesoriesController.getAllAccessoryByTechnical = async (req, res) => {
  const query = "SELECT * FROM accessory WHERE acce_warehouse = ? ALLOW FILTERING";
  const query2 = "SELECT temo_brand FROM terminal_model WHERE temo_name = ? ALLOW FILTERING";
  var respuesta = [];
  try {
    conection.execute(query, [req.body.technical], async (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ message: "Query error", status: "fail" });
        console.log(err);
        return;
      } else {
        var accesorios = result.rows;
        if (accesorios.length > 0) {
          for (let i = 0; i < accesorios.length; i++) {
            if (accesorios[i]['acce_terminal_model'] != "") {
              let prueba = await traerNombreMarca(query2, accesorios[i]['acce_terminal_model']);
              respuesta.push({ accesorio: accesorios[i], marca: prueba });
            } else {
              respuesta.push({ accesorio: accesorios[i], marca: prueba });
            }
            if (i == accesorios.length - 1) {
              res.status(200).send({ message: "Success", status: "ok", response: respuesta });
            }

          }
        } else {
          res.status(200).send({ message: "Success", status: "ok", response: respuesta });
        }
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ error: "Catch error", status: "fail" });
  }
};

accesoriesController.getByUbication = (req, res) => {
  const query = "SELECT * FROM accessory WHERE acce_warehouse = ? ALLOW FILTERING";
  try {
    conection.execute(query, [req.body.ubicacion], (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(200).send({ message: "Query error", status: "fail" });
      } else {
        res.status(200).send(result.rows);
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(200).send({ message: "SucCatch errorcess", status: "fail" });
  }
}

async function traerNombreMarca(query2, modelo) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query2, [modelo], (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve('[]');
          return;
        } else {
          resolve(result.rows[0]['temo_brand']);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(402).send({ error: "catch error 2" });
  }
}

accesoriesController.findAllAndorid = async (req, res) => {
  const query = "select * from accessory";
  let accessorys = await getFindAllNotLimit(query, []);
  res.status(200).send({ message: "Success", status: "ok", response: accessorys });
};

//ubicacion de bodega
async function getWareHouse() {
  var resultWareHouse;
  var queryConsulta = "select ware_id_warehouse, ware_status  from warehouse";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, [], { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          console.log(err);
          return;
        } else {
          resultWareHouse = result;
          resolve(resultWareHouse);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(402).send({ error: "catch error 2" });
  }
}

//ubicacion de bodega
async function getCommerces() {
  var resultWareHouse;
  var queryConsulta = "select comm_uniqe_code from commerce";
  console.log('****** query comercios ********');

  try {
    return new Promise((resolve, reject) => {
      conection.execute(queryConsulta, [], { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          console.log(err);
          return;
        } else {
          resultWareHouse = result;
          console.log(resultWareHouse);

          resolve(resultWareHouse);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(402).send({ error: "catch error 2" });
  }
}

function getFindAll(query, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve(false);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve(false);
  }
}

function getFindAllNotLimit(query, params) {
  var data = [];
  return new Promise((resolve, reject) => {
    conection.stream(query, params, { prepare: true, autoPage: true })
      .on('readable', function () {
        var row;
        while (row = this.read()) {
          data.push(row);
        }
      }).on('end', function () {
        resolve(data);
      });
  });
}

//exports
module.exports = accesoriesController;