const sparesController = {};
const conection = require("../database.js");
const c = require('colors');
const logger = require('../bin/logger');

sparesController.findAll = async (req, res) => {
  const query = "select * from spare";
  let spares = await getFindAllNotLimit(query, []);
  res.json(spares);
};

sparesController.findAllSpareStatus = async (req, res) => {
  const query = "select * from spare_status";
  try {
    conection.execute(query, [], (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: " query error" });
        return;
      } else {
        res.json(result.rows);
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

sparesController.updateStatus = async (status, status_temporal, id) => {
  try {
    var query = "update spare set spar_status=?,spar_status_temporal=? where spar_id=?";
    const parameters = [status, status_temporal, id];
    conection.execute(query, parameters, { prepare: true }, (err, result) => {
      if (err) {
        logger.error(err.stack);
        return false;
      } else if (result) {
        return true;
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    return false;
  }
}



sparesController.updateSpare = async (req, res) => {
  console.log('****************** updateSpare ************');
  console.log(req.body);

  var spare = await validateSpare(req);

  if (spare['spar_id'] != undefined && req.body.spar_warehouse_new == undefined) {
    try {
      var spar_code = req.body.spar_code;
      var spar_status = req.body.spar_status;
      var spar_warehouse = req.body.spar_warehouse;
      var spar_quantity = req.body.spar_quantity;

      var cantidad = parseInt(spare['spar_quantity']) - parseInt(spar_quantity)

      var spar_id = spare['spar_id'];
      var query = "update spare set spar_status=?, spar_quantity=?, spar_warehouse =? where spar_id=?";
      const parameters = [spar_status, '' + cantidad, spar_warehouse, spar_id];
      conection.execute(query, parameters, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          res.status(200).send({ message: "error", error: err });
        } else if (result) {
          res.status(200).send({ message: "success", applied: result.rows });
        }
      });
    } catch (ex) {
      logger.error(ex.stack);
      return false;
    }

  } else {

    spare = await validateSpare3(req);

    if (spare['spar_id'] != undefined) {

      try {
        var spar_code = req.body.spar_code;
        var spar_status = req.body.spar_status;
        var spar_warehouse = req.body.spar_warehouse_new;
        var spar_quantity = req.body.spar_quantity;

        var cantidad = parseInt(spare['spar_quantity']) + parseInt(spar_quantity)

        var spar_id = spare['spar_id'];
        var query = "update spare set spar_status=?, spar_quantity=?, spar_warehouse =? where spar_id=?";
        const parameters = [spar_status, '' + cantidad, spar_warehouse, spar_id];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
          if (err) {
            console.log('***************** error al actualizar repuestos *********');
            console.log(spar_warehouse);

            logger.error(err.stack);
            res.status(200).send({ message: "error", error: err });
          } else if (result) {
            res.status(200).send({ message: "success", applied: result.rows });
          }
        });
      } catch (ex) {
        logger.error(ex.stack);
        return false;
      }

    } else {
      spare = await validateSpare2(req);

      spare['spar_quantity'] = req.body.spar_quantity;
      spare['spar_status'] = req.body.spar_status;
      spare['spar_warehouse'] = req.body.spar_warehouse_new;

      var query = "insert into spare (spar_id,spar_code,spar_terminal_model,spar_name,spar_status,spar_status_temporal,spar_quantity,spar_warehouse,spar_date_register,spar_register_by) values (now(),?,?,?,?,?,?,?,?,?) if not exists";

      const date = new Date();
      var spar_creation = date.format("%Y-%m-%d %H:%M:%S", false);
      const spar_user = req.user.code;
      const parameters = [spare['spar_code'], spare['spar_terminal_model'], spare['spar_name'], spare['spar_status'], spare['spar_status_temporal'], spare['spar_quantity'], spare['spar_warehouse'], spar_creation, spar_user];
      conection.execute(query, parameters, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          res.status(200).send({ message: "query error", err });
          return;
        } else {
          if (Object.values(result.rows[0])[0]) {
            res.status(200).send({ message: "success", applied: Object.values(result.rows[0])[0] });
            return;
          } else {
            res.status(200).send({ message: "spar already exists", applied: Object.values(result.rows[0])[0] });
            return;
          }
        }
      });
    }
  }
}


async function validateSpare(req) {
  var spar_code = req.body.spar_code;
  var spar_status = req.body.spar_status;
  var spar_warehouse = req.body.spar_warehouse;

  const query = "select * from spare where spar_code = ? and spar_status = ? and spar_warehouse = ? ALLOW FILTERING";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, [spar_code, spar_status, spar_warehouse], (err, result) => {
        if (err) {
          logger.error(err.stack);
          reject({ "Error": "error in BD" });
        }
        else {
          rta = result.rows[0];

          if (result.rows[0] == undefined) {
            rta = "";
          }

          resolve(rta);
        }
      });

    });
  }
  catch (error) {
    logger.error(error.stack);
    res.status(406).send({ message: "Not Acceptable" });
  }
}



async function validateSpare3(req) {

  var spar_code = req.body.spar_code;
  var spar_status = req.body.spar_status;
  var spar_warehouse = req.body.spar_warehouse_new;

  const query = "select * from spare where spar_code = ? and spar_status = ? and spar_warehouse = ? ALLOW FILTERING";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, [spar_code, spar_status, spar_warehouse], (err, result) => {
        if (err) {
          logger.error(err.stack);
          reject({ "Error": "error in BD" });
        } else {
          rta = result.rows[0];

          if (result.rows[0] != undefined) {
            rta = "";
          }
          resolve(rta);

        }
      });
    });
  }
  catch (error) {
    logger.error(err.stack);
    res.status(406).send({ message: "Not Acceptable" });
  }
}



async function validateSpare2(req) {

  var spar_code = req.body.spar_code;
  var spar_warehouse = req.body.spar_warehouse;

  const query = "select * from spare where spar_code = ? and spar_warehouse = ? ALLOW FILTERING";
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, [spar_code, spar_warehouse], (err, result) => {
        if (err) {
          logger.error(err.stack);
          reject({ "Error": "error in BD" });
        }
        else {

          rta = result.rows[0];

          if (result.rows[0] != undefined) {
            rta = "";
          }
          resolve(rta);
        }
      });

    });
  }
  catch (error) {
    logger.error(error.stack);
    res.status(406).send({ message: "Not Acceptable" });
  }
}



sparesController.update = async (req, res, next) => {
  try {
    const status = req.estado;
    if (status === false) {

      const request = req.body;
      var query = "insert into spare (spar_id,spar_code,spar_terminal_model,spar_name,spar_status,spar_status_temporal,spar_quantity,spar_warehouse,spar_date_register,spar_register_by) values (now(),?,?,?,?,?,?,?,?,?) if not exists";
      try {
        const date = new Date();
        var spar_creation = date.format("%Y-%m-%d %H:%M:%S", false);
        const spar_user = req.user.code;
        const parameters = [request.spar_code, request.spar_terminal_model, request.spar_name, request.spar_status, request.spar_status_temporal, request.spar_quantity, request.spar_warehouse, spar_creation, spar_user];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
          if (err) {
            logger.error(err.stack);
            res.status(404).send({ message: "query error", err });
            return;
          } else {
            if (Object.values(result.rows[0])[0]) {

              req.action = "registrar/repuesto";
              req.before = "{}";
              req.after = JSON.stringify(request);
              res.status(200).send({ message: "success", applied: Object.values(result.rows[0])[0] });
              next();
              return;
            } else {
              res.status(200).send({ message: "spar already exists", applied: Object.values(result.rows[0])[0] });
              return;
            }
          }
        });
      } catch (ex) {
        logger.error(ex.stack);
        res.status(404).send({ error: "catch error" });
      }

    } else {

      const request = req.body;
      req.action = "actualizar/repuesto";
      var auditBefore = [];
      var auditAfter = [];

      const row = JSON.parse(req.before);
      for (let j = 0; j < Object.keys(request).length; j++) {
        if (Object.keys(row[0])[j] === Object.keys(request)[j]) {
          if (Object.values(row[0])[j] !== Object.values(request)[j]) {
            auditBefore.push(JSON.parse('{"' + Object.keys(row[0])[j] + '":"' + Object.values(row[0])[j] + '"}'));
            auditAfter.push(JSON.parse('{"' + Object.keys(request)[j] + '":"' + Object.values(request)[j] + '"}'));
          }
        } else {
          res.status(400).send({ message: "error, wrong json syntax" });
          return;
        }
      }
      req.before = JSON.stringify(auditBefore);
      req.after = JSON.stringify(auditAfter);
      var query = "update spare set spar_code=?,spar_terminal_model=?,spar_name=?,spar_status=?,spar_status_temporal=?,spar_quantity=?,spar_warehouse=?where spar_id=?";
      var cantidad = parseInt(request.spar_quantity);

      const parameters = [request.spar_code, request.spar_terminal_model, request.spar_name, request.spar_status, request.spar_status_temporal, "" + cantidad, request.spar_warehouse, request.spar_id];
      conection.execute(query, parameters, { prepare: true }, (err, result) => {
        if (err) {
          logger.error(err.stack);
          res.status(404).send({ message: " query error" });
          return;
        } else if (result) {
          res.json({ message: "success" });
          next();
        }
      });
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
}

sparesController.getSpare = async (req, res, next) => {
  const query = "select * from spare where spar_id = ?";
  var estado = false;
  try {
    if (req.headers.id != "0") {
      await conection.execute(query, [req.headers.id], { prepare: true }, function (err, result) {
        if (err) {
          logger.error(err.stack);
          res.status(404).send({ message: "query error", err });
          return;
        }
        if (result.rows.length > 0) {
          req.before = JSON.stringify(result.rows);
          estado = true;
        }
        req.estado = estado;
        next();
      });
    } else {
      req.before = "";
      estado = false;
      req.estado = estado;
      next();
    }


  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
}

// cargue masivo de repuestos
sparesController.saveSeveral = async (req, res, next) => {
  try {

    let terminalStatus = await getFindAll('select spst_name from spare_status', []);
    let terminalModels = await getFindAll('select temo_name from terminal_model', []);
    let warehouses = await getFindAll('select ware_id_warehouse, ware_status from warehouse', []);

    if (terminalStatus.length == 0 || terminalModels.length == 0 || warehouses.length == 0)
      return res.status(405).send({ error: "No se encontraron terminales o estados asociados." });

    const moment = require("moment");
    const date = moment().format("LLL");
    const logueado = req.user.code;

    var notInserted = [];
    var totalNoInsert = [];

    var countInserted = 0;
    var countUpdate = 0;
    let i;

    var request = req.body;

    var banderaTerminal = false;
    var banderaEstado = false;
    var booleanWareHouse = false;
    var banderaUpdate = false;
    var banderaInserted = false;

    var queryInsert = "insert into polaris_core.spare (spar_id, spar_code, spar_terminal_model, spar_name, spar_status, spar_status_temporal, spar_quantity, spar_warehouse, spar_date_register, spar_register_by) values (now(),?,?,?,?,?,?,?,?,?) if not exists";
    var queryUpdate = "update polaris_core.spare set spar_quantity = ? where spar_id = ? if exists";
    var querySelect = "select * from spare where spar_code = ? and spar_status = ? and spar_warehouse = ? allow filtering";

    for (i = 0, long = request.length; i < long; i++) {
      let repuesto = request[i];

      const parameters = [
        `${repuesto.spar_code}`,
        `${repuesto.spar_terminal_model}`,
        `${repuesto.spar_name}`,
        `${repuesto.spar_status}`,
        `${repuesto.spar_status_temporal}`,
        `${repuesto.spar_quantity}`,
        `${repuesto.spar_location}`,
        date,
        logueado
      ];

      banderaEstado = terminalStatus.some(c => c['spst_name'] == repuesto.spar_status);
      if (!banderaEstado) notInserted.push(" Estado inválido");

      booleanWareHouse = warehouses.some(c => c['ware_status'] == 'ACTIVO' && c['ware_id_warehouse'] == repuesto.spar_location);
      if (!booleanWareHouse) notInserted.push(" Ubicación inválida");

      banderaTerminal = terminalModels.some(c => repuesto.spar_terminal_model.includes(c['temo_name']));
      if (!banderaTerminal) notInserted.push(" Modelo inválido");

      if (banderaEstado && booleanWareHouse && banderaTerminal) {
        const parametersSelect = [repuesto.spar_code, repuesto.spar_status, repuesto.spar_location];
        let repuestosExists = await getFindAll(querySelect, parametersSelect);

        banderaEstado = false;
        booleanWareHouse = false;
        banderaTerminal = false;

        if (repuestosExists.length != 0) {
          var idUpdate = "" + repuestosExists[0]['spar_id'];
          var quantity = "" + ((parseInt(repuesto.spar_quantity)) + (parseInt(repuestosExists[0]['spar_quantity'])));
          const parametersUpdate = [quantity, idUpdate];
          resultadoUpdate = await getRealizarActualizacion(queryUpdate, parametersUpdate);
          banderaUpdate = Object.values(resultadoUpdate[0])[0];
        } else {
          resultadoInsert = await getRelaizarInsert(queryInsert, parameters);
          banderaInserted = Object.values(resultadoInsert[0])[0];
        }

        if (banderaUpdate) {
          countUpdate++;
          banderaUpdate = false;
        }

        if (banderaInserted) {
          countInserted++;
          banderaInserted = false;
        }
      } else {
        totalNoInsert.push("Fila ==>  " + (i + 2) + notInserted);
        banderaEstado = false;
        booleanWareHouse = false;
        banderaTerminal = false;
        notInserted = [];
      }

      if ((i % 100) == 0) {
        console.log("cargando repuestos ... ", i);
      }
    }

    if (i == request.length)
      return res.status(200).send({ message: "Resultados de el cargue ", response: totalNoInsert, countInserted: countInserted, countUpdate: countUpdate });

  } catch (error) {
    logger.error(error.stack);
  }
};

async function getIfExists(querySelect, parametersSelect) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(querySelect, parametersSelect, (err, result) => {
        if (err) {
          logger.error(err.stack);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}
async function getRelaizarInsert(query, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}
async function getRealizarActualizacion(query, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    return;
  }
}

function getFindAll(query, parameters) {
  try {
    return new Promise((resolve, reject) => {
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          resolve([]);
        } else {
          resolve(result.rows);
        }
      });
    });
  } catch (ex) {
    logger.error(ex.stack);
    resolve([]);
  }
}

sparesController.getByCode = async (req, res) => {
  const query = "select * from spare where spar_code = ?";
  let spares = await getFindAllNotLimit(query, [req.headers.id]);
  res.status(200).send({ msg: 'ssucces', user: spares });
};

sparesController.getById = async (req, res) => {
  const query = "select * from spare where spar_id = ?";
  var estado = false;
  try {
    await conection.execute(query, [req.headers.id], function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ msg: 'query error' });
      } else {
        res.status(200).send({ msg: 'ssucces', user: result.rows });
        return;
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};


sparesController.getByCodeLocationStatus = async (req, res) => {
  const query = "SELECT * from spare WHERE spar_status = ? AND spar_code = ? AND spar_warehouse = ? ALLOW FILTERING;";
  const data = JSON.parse(req.headers.data);
  try {
    await conection.execute(query, [data.estado, data.codigo, data.ubicacion], function (err, result) {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ msg: 'query error' });
      } else {
        res.status(200).send({ msg: 'ssucces', user: result.rows });
        return;
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

sparesController.delete = async (req, res, next) => {
  const status = req.estado;
  try {
    if (req.user) {
      if (!req.user.role.includes("1")) {
        res.status(404).send({ message: 'You do not have the necessary permissions to execute this request.' });
        return;
      }
    } else {
      res.status(404).send({ message: 'Invalid Token.' });
    }
    if (status === false) {
      res.status(404).send({ message: "error, accessory not exits" });
      return;
    };
    var query = "delete from spare where spar_id = ?";
    const parameters = [req.headers.id];
    conection.execute(query, parameters, { prepare: true }, (err, result) => {
      if (err) {
        logger.error(err.stack);
        res.status(404).send({ message: "query error" });
        return;
      } else {
        req.action = "eliminar/repuesto";
        req.after = "{}";
        res.json({ message: "success" });
        next();
      }
    });
  } catch (ex) {
    logger.error(ex.stack);
    res.status(404).send({ error: "catch error" });
  }
};

sparesController.getSpareByLocation = async (req, res) => {
  const query = "SELECT * FROM spare WHERE spar_warehouse = ? ALLOW FILTERING;";
  const request = req.body[0];
  try {
    if (!request) {
      res.status(402).send({ message: "incomplete petition" });
    } else {
      const parameters = [request];
      conection.execute(query, parameters, (err, result) => {
        if (err) {
          logger.error(err.stack);
          res.status(401).send({ message: " query error" });
          return;
        } else {
          res.json(result.rows);
        }
      });
    }
  } catch (ex) {
    logger.error(ex.stack);
    res.status(400).send({ error: "catch error" });
  }
};

function getFindAllNotLimit(query, params) {
  var data = [];
  return new Promise((resolve, reject) => {
    conection.stream(query, params, { prepare: true, autoPage: true })
      .on('readable', function () {
        var row;
        while (row = this.read()) {
          data.push(row);
        }
      }).on('end', function () {
        resolve(data);
      });
  });
}

//exports
module.exports = sparesController;