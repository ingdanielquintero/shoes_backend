const empleadoShoe = {};
const jwt = require("../Services/Jwt.js");
//Importamos el modulo paraa encriptar las contraseñas
const conection = require('../database');
const logger = require('../bin/logger');


//methods

empleadoShoe.save = async (req, res, next) => {
    const request = req.body;
    const date = new Date();

    const query = "insert into empleado (emp_id, emp_area, emp_celular, emp_direccion, emp_fecha_creacion, emp_nombre, emp_estado, emp_usuario) values (now(),?,?,?,?,?,?,?) if not exists";
    const parameters = [request.emp_area, request.emp_celular, request.emp_direccion, date.format("%Y-%m-%d %H:%M:%S", false), request.emp_nombre, "ACTIVO", req.user.id];
    try {
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: err });
            } else if (result) {
                return res.status(200).send({ status: true, message: "Empleado creado" });
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ status: false, error: "catch error" });
    }
}


empleadoShoe.update = async (req, res, next) => {
    const request = req.body;
    var query = "update empleado set emp_area =?, emp_celular =?, emp_direccion =?, emp_nombre =? where emp_id =? if exists";
    try {
        const parameters = [request.emp_area, request.emp_celular, request.emp_direccion, request.emp_nombre, request.emp_id];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ status: false, message: "query error", err });
                return;
            } else {
                if (Object.values(result.rows[0])[0]) {
                    return res.status(200).send({ status: true, message: "Empleado actualizado" });
                } else {
                    return res.status(200).send({ status: false, message: Object.values(result.rows[0])[0] });
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ status: false, error: "catch error" });
    }
}



empleadoShoe.updateEstado = async (req, res, next) => {
    const request = req.body;
    var query = "update empleado set emp_estado =? where emp_id =? if exists";
    try {
        const parameters = [request.emp_estado, request.emp_id];
        conection.execute(query, parameters, { prepare: true }, (err, result) => {
            if (err) {
                logger.error(err.stack);
                res.status(200).send({ status: false, message: "query error", err });
                return;
            } else {
                if (Object.values(result.rows[0])[0]) {
                    return res.status(200).send({ status: true, message: "Empleado actualizado" });
                } else {
                    return res.status(200).send({ status: false, message: Object.values(result.rows[0])[0] });
                }
            }
        });
    } catch (ex) {
        logger.error(ex.stack);
        res.status(200).send({ status: false, error: "catch error" });
    }
}

empleadoShoe.getAll = async (req, res) => {
    const query = "select * from empleado where emp_usuario =? allow filtering";
    try {
        conection.execute(query, [req.user.id], (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: "error in BD" });
            } else {
                return res.status(200).send({ status: true, data: result.rows });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ status: false, message: "Not Acceptable" });
    }
};

empleadoShoe.getById = async (req, res) => {
    const request = req.headers;
    const query = "select * from empleado where emp_id =?";
    try {
        conection.execute(query, [request.id], (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: "error in BD" });
            } else {
                return res.status(200).send({ status: true, data: result.rows });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ status: false, message: "Not Acceptable" });
    }
};

empleadoShoe.getByArea = async (req, res) => {
    const request = req.headers;
    const query = "select * from empleado where emp_usuario =? and emp_area =? allow filtering";
    try {
        conection.execute(query, [req.user.id, request.emp_area ], (err, result) => {
            if (err) {
                logger.error(err.stack);
                return res.status(200).send({ status: false, message: "error in BD" });
            } else {
                return res.status(200).send({ status: true, data: result.rows });
            }
        });
    }
    catch (error) {
        logger.error(error.stack);
        res.status(200).send({ status: false, message: "Not Acceptable" });
    }
};


//exports
module.exports = empleadoShoe;


